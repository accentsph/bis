﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
   public partial class Form_ConvertHeightWeight : Form
   {
      EntryForm_Client frmClient;
      public Form_ConvertHeightWeight(EntryForm_Client c)
      {
         InitializeComponent();
         frmClient = c;
      }
      private void ComputeHeight(object sender, EventArgs e)
      {
         //e.Equals//e.GetType.ToString
         
         decimal totinches = 0, totft=0, totin=0 ;
         decimal inchtocm = (decimal)2.54;
         NumericUpDown numUD =(NumericUpDown)sender;//as NumericUpDown;
         if (numUD !=null)
         {
            if (numUD.Name=="NumCM")
            {
               totinches = NumCM.Value / inchtocm;
               totin = (totinches % 12);
               totft = ((totinches - totin) / 12);
               NumFeet.Value = totft;
               NumInch.Value = totin;
            }
            else
            {
               totft = NumFeet.Value * 12;
               totin = NumInch.Value;
               totinches = totft + totin;
               NumCM.Value = totinches * inchtocm;
            }
            button1.Enabled = NumCM.Value > 0;
         }
      }
      private void ComputeWeight(object sender, EventArgs e)
      {
         decimal lb2kg = (decimal)2.2046;
         NumericUpDown numUD = (NumericUpDown)sender;//as NumericUpDown;
         if (numUD != null)
         {
            if (numUD.Name=="NumLb")
            {
               NumKg.Value = NumLb.Value / lb2kg;
            }
            else
            {
               NumLb.Value = NumKg.Value * lb2kg;
            }
            button2.Enabled = NumKg.Value > 0;
         }
      }

      private void button1_Click(object sender, EventArgs e)
      {
         frmClient.SetHeight(NumCM.Value);
         NumLb.Select();
         button1.Enabled = false;
      }

      private void label12_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void button2_Click(object sender, EventArgs e)
      {
         frmClient.SetWeight(NumKg.Value);
         NumKg.Select();
         button2.Enabled = false;
      }
   }
}
