﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace BIS
{
    class MySQLDatabase : Database
    {
        private MySqlConnection connection;
        //private string connectionString;

        public static bool TestConnection(string server, string database, string uid, string password, BISSSLCertificate certificate = null)
        {
            MySqlConnection conn = null;
            try
            {
               conn = CreateConnection(server, database, uid, password, certificate);
                conn.Open();
                if(conn.State == ConnectionState.Open)
                {
                    conn.Close();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error Message: " + e.Message);
                Console.WriteLine(e.StackTrace);
            }
            return false;
        }

        public MySQLDatabase(string server, string database, string uid, string password, BISSSLCertificate certificate=null)
        {
            connection = CreateConnection(server, database, uid, password, certificate);
        }

        private static MySqlConnection CreateConnection(string server, string database, string uid, string password, BISSSLCertificate certificate = null)
        {
            string connectionString = null;
            var csb = new MySqlConnectionStringBuilder
            {
                Server = server,
                UserID = uid,
                Password = password,
                Database = database,
                CertificateFile = certificate != null ? certificate.CertificateFilePath : null,
                CertificatePassword = certificate != null ? certificate.CertificatePassword : null,
                SslMode = certificate != null ? MySqlSslMode.Required : MySqlSslMode.None
            };
            connectionString = csb.ConnectionString;
            return (new MySqlConnection(connectionString));
        }

        public bool OpenConnection()
        {
            try
            {
                if (connection.State != System.Data.ConnectionState.Open)
                {
                    connection.Open();
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool CloseConnection()
        {
            try
            {
                if (connection.State != System.Data.ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public bool IsOpen()
        {
            if (connection.State == System.Data.ConnectionState.Open)
            {
                return (true);
            }
            return (false);
        }

        public bool CreateDatabase(string dbname)
        {
            return true;
        }

        public bool Update(string tableName, List<SQLParameter> parameters, SQLParameter[] parameters2)
        {
            MySqlCommand command = null;
            try
            {
                OpenConnection();
                StringBuilder sb = new StringBuilder("UPDATE " + tableName + " set ");
                command = connection.CreateCommand();
                for (int i = 0; i < parameters.Count; i++)
                {
                    String paramId = "@" + parameters[i].ID;
                    sb.Append(((parameters[i].ColumnName == null) ? parameters[i].ID : parameters[i].ColumnName) + parameters[i].BooleanOperator + paramId);
                    if (i < parameters.Count - 1)
                    {
                        sb.Append(", ");
                    }
                    command.Parameters.AddWithValue(paramId, parameters[i].Value);
                }
                sb.Append(" WHERE ");
                for(int i = 0; i < parameters2.Length; i++)
                {
                    String paramId = "@" + parameters2[i].ID;
                    sb.Append(((parameters2[i].ColumnName == null) ? parameters2[i].ID : parameters2[i].ColumnName) + parameters2[i].BooleanOperator + paramId);
                    if (i < parameters2.Length - 1)
                    {
                        sb.Append(" " + parameters2[i+1].AndOr + " ");
                    }
                    command.Parameters.AddWithValue(paramId, parameters2[i].Value);
                }
                Console.WriteLine(sb.ToString());
                command.CommandText = sb.ToString();
                command.ExecuteNonQuery();
                CloseConnection();
            }
            catch (Exception e)
            {
                connection.Close();
                Console.WriteLine(e.StackTrace);
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (false);
            }
            return (true);
        }

        /*public bool InsertMany(string tableName, List<List<SQLParameter>> columns)
        {
            MySqlCommand command = null;
            foreach(List<SQLParameter> parameters in columns)
            {

            }
        }

        public string GenerateInsertCommandText(List<SQLParameter> parameters)
        {

        }*/

        public bool Insert(string tableName, List<SQLParameter> parameters)
        {
            MySqlCommand command = null;
            try
            {
                OpenConnection();
                StringBuilder sb = new StringBuilder("INSERT INTO " + tableName + "(");
                StringBuilder sb1 = new StringBuilder(" VALUES(");
                command = connection.CreateCommand();
                for (int i = 0; i < parameters.Count; i++)
                {
                    sb.Append(parameters[i].ColumnName == null ? parameters[i].ID : parameters[i].ColumnName);
                    String paramId = "@" + parameters[i].ID;
                    sb1.Append(paramId);
                    if (i < parameters.Count - 1)
                    {
                        sb.Append(", ");
                        sb1.Append(", ");
                    }
                    object obj = parameters[i].Value;
                    Console.WriteLine(paramId + " - " + (obj == null ? "null" : obj.ToString()));
                    //command.Parameters.AddWithValue(paramId, parameters[i].Value);
                    command.Parameters.AddWithValue(paramId, (obj == null) ? null : obj);
                    //command.Parameters.AddWithValue(paramId, (obj == null) ? (object)DBNull.Value : obj.ToString());
                }
                sb.Append(")");
                sb1.Append(")");
                sb.Append(sb1);
                Console.WriteLine(sb.ToString());
                command.CommandText = sb.ToString();
                command.ExecuteNonQuery();
                CloseConnection();
            }
            catch (Exception e)
            {
                connection.Close();
                Console.WriteLine(e.StackTrace);
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (false);
            }
            return (true);
        }

        public int InsertScalar(string tableName, List<SQLParameter> parameters)
        {
            MySqlCommand command = null;
            try
            {
                OpenConnection();
                StringBuilder sb = new StringBuilder("INSERT INTO " + tableName + "(");
                StringBuilder sb1 = new StringBuilder(" VALUES(");
                command = connection.CreateCommand();
                for (int i = 0; i < parameters.Count; i++)
                {
                     sb.Append(parameters[i].ColumnName == null ? parameters[i].ID : parameters[i].ColumnName);
                     String paramId = "@" + parameters[i].ID;
                    sb1.Append(paramId);
                    if (i < parameters.Count - 1)
                    {
                        sb.Append(", ");
                        sb1.Append(", ");
                    }
                    object obj = parameters[i].Value;
                    Console.WriteLine(paramId + " - " + (obj == null ? "null" : obj.ToString()));
                    command.Parameters.AddWithValue(paramId, (obj == null) ? null : obj);
                }
                sb.Append(")");
                sb1.Append(")");
                sb.Append(sb1);
                Console.WriteLine(sb.ToString());
                command.CommandText = sb.ToString() + ";SELECT last_insert_id();";
                object ob = command.ExecuteScalar();
                Console.WriteLine("returned: " + ob.ToString());
                int id = Convert.ToInt32(ob);
                CloseConnection();
                return (id);
            }
            catch (Exception e)
            {
                connection.Close();
                Console.WriteLine(e.StackTrace);
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (-1);
            }
        }

        public IDataReader Select(string query, List<SQLParameter> parameters = null, string option = null)
        {
            IDataReader reader = null;
            MySqlCommand command = null;
            try
            {
                OpenConnection();
                command = connection.CreateCommand();
                if(parameters != null)
                {
                    command.CommandText = addParams(query, parameters, ref command);
                }
                else
                {
                    command.CommandText = query;
                }
                command.CommandText += (option == null) ? "" : " " + option;
                Console.WriteLine(command.CommandText);
                reader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                connection.Close();
                reader = null;
                command = null;
                Console.WriteLine(e.StackTrace);
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            return reader;
        }

        public object ScalarSelect(string query, List<SQLParameter> parameters = null, string option = null)
        {
            MySqlCommand command = null;
            try
            {
                OpenConnection();
                command = connection.CreateCommand();
                if(parameters != null)
                {
                    command.CommandText = addParams(query, parameters, ref command);
                }
                else
                {
                    command.CommandText = query;
                }
                command.CommandText += (option != null) ? (" " + option) : "";
                return command.ExecuteScalar();
            }
            catch (Exception e)
            {
                command = null;
                Console.WriteLine(e.StackTrace);
                connection.Close();
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public bool NonQuery(string comm)
        {
            MySqlCommand command = null;
            try
            {
                OpenConnection();
                command = connection.CreateCommand();
                command.CommandText = comm;
                command.ExecuteNonQuery();
                CloseConnection();
                Console.WriteLine(comm);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                CloseConnection();
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (false);
            }
            return (true);
        }

        public bool NonQuery(string query, List<SQLParameter> parameters)
        {
            MySqlCommand command = null;
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    OpenConnection();
                }
                command = connection.CreateCommand();
                command.CommandText = addParams(query, parameters, ref command);
                command.ExecuteNonQuery();
                CloseConnection();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                CloseConnection();
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (false);
            }
            return (true);
        }

        private string addParams(string query, List<SQLParameter> parameters, ref MySqlCommand command)
        {
            StringBuilder sb = new StringBuilder(query + " WHERE ");
            for (int i = 0; i < parameters.Count; i++)
            {
                String paramId = "@" + parameters[i].ID;

                sb.Append(((parameters[i].ColumnName == null) ? parameters[i].ID : parameters[i].ColumnName) + " " + parameters[i].BooleanOperator + " " + paramId);
                if (i < parameters.Count - 1)
                {
                    sb.Append(" " + parameters[i+1].AndOr + " ");
                }
                object obj = parameters[i].Value;
                command.Parameters.AddWithValue(paramId, (obj == null) ? (object)DBNull.Value : ((parameters[i].BooleanOperator == "LIKE") ? "%" + obj.ToString() +"%" : obj.ToString()));
            }
            string tmp = sb.ToString();
            Console.WriteLine(tmp);
            return tmp;
        }

        public IDbConnection Connection
        {
            get
            {
                return connection;
            }
        }

        public bool Delete(string tableName, List<SQLParameter> parameters)
        {
            MySqlCommand command = null;
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    OpenConnection();
                }
                command = connection.CreateCommand();
                command.CommandText = addParams("DELETE FROM " + tableName, parameters, ref command);
                command.ExecuteNonQuery();
                CloseConnection();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                CloseConnection();
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (false);
            }
            return (true);
        }

        public bool Backup(string file)
        {
            OpenConnection();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    using (MySqlBackup mb = new MySqlBackup(cmd))
                    {
                        cmd.Connection = connection;
                        mb.ExportToFile(file);
                        CloseConnection();
                    }
                }
            }
            catch(Exception e)
            {

                Console.WriteLine(e.StackTrace);
                CloseConnection();
                MessageBox.Show(e.Message, "MySQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public void Restore()
        {
        }

        public void AddParams(MySqlCommand command, MySqlParameter parameters)
        {

        }
    }
}
