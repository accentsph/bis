﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIS
{
    public class DatabaseTableView
    {
        private string name;
        private string searchColumn;
        
        public string Name
        {
            get { return name; }
        }

        public string SearchColumn
        {
            get { return searchColumn; }
        }

        public DatabaseTableView(string name, string searchColumn)
        {
            this.name = name;
            this.searchColumn = searchColumn;
        }
    }
}
