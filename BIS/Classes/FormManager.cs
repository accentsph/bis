﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
   class FormManager
   {
      public static bool isFieldsFilled(Control ctr, List<string> exemptions)
      {
         Console.WriteLine("Checking fields..");
         foreach (Control c in ctr.Controls)
         {
            bool exempted = false;
            foreach (string s in exemptions)
            {
               if (c.Name == s)
               {
                  exempted = true;
                  exemptions.Remove(s);
                  break;
               }
            }
            if (!exempted)
            {
               if (c is TextBox)
               {
                  if (c.Text.Length <= 0)
                  {
                     MessageBox.Show("Please enter " + c.Name + ".");
                     c.Focus();
                     return false;
                  }
                    Console.Write(c.Name + " is filled, ");
               }
               else if(c is ComboBox)
               {
                    try
                    {
                        ComboBox cb = (ComboBox)c;
                        if (cb.SelectedValue != null)
                        {
                            if (Convert.ToInt32(cb.SelectedValue) <= 1)
                            {
                                MessageBox.Show("Please enter " + c.Name + ".");
                                c.Focus();
                                return false;
                            }
                        }
                        else
                        {
                            if(cb.Text.Length < 1)
                            {
                                MessageBox.Show("Please enter " + c.Name + ".");
                                c.Focus();
                                return false;
                            }
                        }
                            
                        Console.Write(c.Name + " is filled, ");
                    }
                    catch(Exception err)
                    {
                        Console.WriteLine(err.StackTrace);
                        MessageBox.Show("Please enter " + c.Name + ".");
                        c.Focus();
                        Console.WriteLine("Checking complete");
                        return false;
                    }
               }
                else
                {
                    if(c.Controls != null)
                    {
                        if(!isFieldsFilled(c, exemptions))
                        {
                            Console.WriteLine("Checking complete");
                            return false;
                        }
                    }
                }
            }
         }
         Console.WriteLine("Checking complete");
         return true;
      }
        
        public static void populateFields(Control control, DataTable table)
        {
            //DataTable dt = new DataTable();
            if(table.Rows.Count > 0)
            {
                for(int i = 0; i < table.Columns.Count; i++)
                {
                    Control c = control.Controls[table.Columns[i].ColumnName];
                    object ob = table.Rows[0][i];
                    if (c != null)
                    {
                        if (c is TextBox)
                        {
                            c.Text = DataTypeManager.ParseDataToString(ob);
                        }
                        else if (c is ComboBox)
                        {
                            int ind = DataTypeManager.ParseDataToInt(ob);
                            if (ind > -1)
                            {
                                try
                                {
                                    ((ComboBox)c).SelectedValue = ind;
                                }
                                catch (Exception er)
                                {
                                    Console.WriteLine(er.StackTrace);
                                    try
                                    {
                                        ((ComboBox)c).SelectedValue = ind;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.StackTrace);
                                        c.Text = DataTypeManager.ParseDataToString(ob);
                                    }
                                }
                            }
                        }
                        else if (c is DateTimePicker)
                        {
                            DateTime dt = DataTypeManager.ParseDataToDateTime(ob);
                            if (dt >= new DateTime(1, 1, 1))
                            {
                                ((DateTimePicker)c).Value = dt;
                            }
                        }
                        else if (c is CheckBox)
                        {
                            ((CheckBox)c).Checked = DataTypeManager.ParseDataToBool(ob);
                        }
                        else if (c is NumericUpDown)
                        {
                            ((NumericUpDown)c).Value = DataTypeManager.ParseDataToDecimal(ob);
                        }
                    }
                }
            
            }
        }
        public static void populateFields(Control control, IDataReader reader)
        {
            if (reader != null)
            {
                if (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Control c = control.Controls[reader.GetName(i)];
                        if (c != null)
                        {
                            if (c is TextBox)
                            {
                                c.Text = DataTypeManager.ParseDataToString(reader.GetValue(i));
                            }
                            else if (c is ComboBox)
                            {
                                int ind = DataTypeManager.ParseDataToInt(reader.GetValue(i));
                                if (ind > -1)
                                {
                                    try
                                    {
                                        ((ComboBox)c).SelectedValue = ind;
                                    }
                                    catch (Exception er)
                                    {
                                        Console.WriteLine(er.StackTrace);
                                        try
                                        {
                                            ((ComboBox)c).SelectedValue = ind;
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.StackTrace);
                                            c.Text = DataTypeManager.ParseDataToString(reader.GetValue(i));
                                        }
                                    }
                                }
                            }
                            else if (c is DateTimePicker)
                            {
                                DateTime dt = DataTypeManager.ParseDataToDateTime(reader.GetValue(i));
                                if (dt >= new DateTime(1, 1, 1))
                                {
                                    ((DateTimePicker)c).Value = dt;
                                }
                            }
                            else if (c is CheckBox)
                            {
                                ((CheckBox)c).Checked = DataTypeManager.ParseDataToBool(reader.GetValue(i));
                            }
                            else if (c is NumericUpDown)
                            {
                                ((NumericUpDown)c).Value = DataTypeManager.ParseDataToDecimal(reader.GetValue(i));
                            }
                        }
                    }
                }
                reader.Close();
                reader.Dispose();
            }
        }

        public static List<SQLParameter> getParameters(Control control, List<string> exemptions = null)
      {
         List<SQLParameter> prms = new List<SQLParameter>();
         foreach (Control c in control.Controls)
         {
            bool exempted = false;
            if(exemptions != null)
            {
                foreach (string s in exemptions)
                {
                    if (c.Name == s)
                    {
                        exempted = true;
                        exemptions.Remove(s);
                        break;
                    }
                }
            }
            if (!exempted)
            {
                if (c is TextBox && ((TextBox)c).ReadOnly == false)
                {
                    prms.Add(new SQLParameter(c.Name, c.Text));
                }
                else if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    if (cb.SelectedValue == null)
                    {
                        if (cb.Text.Length < 1)
                        {
                            prms.Add(new SQLParameter(c.Name, null));
                        }
                        else
                        {
                            prms.Add(new SQLParameter(c.Name, c.Text));
                        }
                    }
                    else if (cb.Text.Length < 1)
                    {
                        prms.Add(new SQLParameter(c.Name, null));
                    }
                    else
                    {
                        prms.Add(new SQLParameter(c.Name, ((ComboBox)c).SelectedValue.ToString()));
                    }
                }
                else if (c is DateTimePicker)
                {
                    prms.Add(new SQLParameter(c.Name, ((DateTimePicker)c).Value.ToString("yyyy-MM-dd")));
                }
                else if (c is NumericUpDown)
                {
                    prms.Add(new SQLParameter(c.Name, ((NumericUpDown)c).Value));
                }
                else
                {
                    prms.AddRange(getParameters(c, exemptions));
                }
            }
         }
         return (prms);
      }

        public static void SetComboBoxDataSource(ComboBox cb, IDataReader reader)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Load(reader);
                reader.Close();
                cb.DataSource = dt;
                cb.DisplayMember = "Description";
                cb.ValueMember = "sysID";
            }
            catch(Exception err)
            {
                reader.Close();
                Console.WriteLine(err.Message);
                Console.WriteLine(err.StackTrace);
                MessageBox.Show(err.Message);
            }
        }

      public static void SetComboBoxDataSource(ComboBox cb, string strQry, string vm, string dm, bool limitedToList = false, List<SQLParameter> sqllist = null, string dbname = "dbstandard")
      {
            MySQLDatabase db = null;
            try
            {
                DataTable dt = new DataTable();
                IDataReader drs;
                if(dbname == "dbbarangay")
                {
                    db = DatabaseManager.GetInstance();
                }
                else
                {
                    db = DatabaseManager.GetInstance(dbname);
                }
                db.CloseConnection();
                drs = db.Select(strQry, sqllist);
                dt.Load(drs);
                drs.Close();
                cb.IntegralHeight = false;
                cb.MaxDropDownItems = 15;
                cb.DropDownWidth = 283;

                cb.DataSource = dt;
                cb.DisplayMember = dm;
                cb.ValueMember = vm;
                if (limitedToList)
                {
                    cb.DropDownStyle = ComboBoxStyle.DropDownList;
                }
                else
                {
                    cb.DropDownStyle = ComboBoxStyle.DropDown;
                    cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                }
            cb.SelectedIndex = -1;
            }
            catch(Exception err)
            {
                Console.WriteLine(err);
            }
            finally
            {
                if (db != null && db.IsOpen())
                {
                    db.CloseConnection();
                }
            }


            //cv.SelectedIndex = -1;
            //cv.SelectedValue = -1;
        }

      public static void ProcessComboBoxes(Control ctrl, string action, bool limittolist = false)
      {
         foreach (Control crl in ctrl.Controls)
         {

            if (crl is ComboBox)
            {
               switch (action)
               {
                  case "Businesses":
                     switch (crl.Name)
                     {
                        case "ClientID":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT clientID as sysId, `Name of Resident` as Description FROM `vwbusinesses` ORDER BY `Name of Resident`", "sysID", "description", limittolist,null,"dbbarangay");
                           break;
                        default:
                           break;
                     }
                     break;
                  case "Populate":
                     switch (crl.Name)
                     {
                        case "POD":
                            SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwcity order by description", "sysID", "description", limittolist);
                           break;
                        case "POB":
                            SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwcity order by description", "sysID", "description", limittolist);
                           break;
                        case "purokID":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT PurokID as sysID, purok as description FROM vwpuroks order by description", "sysID", "description", limittolist, null, "dbbarangay" );
                           break;
                        case "Disability":
                            SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwdisabilities order by description", "sysID", "description", limittolist);
                            break;
                        case "Gender":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwgender order by description", "sysID", "description", limittolist);
                           break;
                        case "Citizenship":
                            SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwnationalities order by description", "sysID", "description", limittolist );
                            break;
                        case "Status":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwcivilstatus order by description", "sysID", "description", limittolist);
                           break;
                        case "Attainment":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vweducationalattainment order by description", "sysID", "description", limittolist);
                           break;
                        case "Occupation":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwoccupations order by description", "sysID", "description", limittolist);
                           break;
                        case "Hair":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwhaircolors order by description", "sysID", "description", limittolist);
                           break;
                        case "Complexion":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwcomplexion order by description", "sysID", "description", limittolist);
                           break;
                        case "Religion":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwreligions order by description", "sysID", "description", limittolist);
                           break;
                        case "Ethnicity":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwethnicities order by description", "sysID", "description", limittolist);
                           break;
                        case "Build":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwphysicalappearance order by description", "sysID", "description", limittolist);
                           break;
                        case "Employment":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwemploymentstatus order by description", "sysID", "description", limittolist);
                           break;
                        case "WorkerType":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwclassofworkers order by description", "sysID", "description", limittolist);
                           break;
                        case "Industry":
                                    //break;
                        case "Category":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwindustries order by description", "sysID", "description", limittolist);
                           break;
                        case "Designation":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwbarangaystaffs order by description", "sysID", "description", limittolist);
                           break;
                        case "Position":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwbarangayofficials order by description", "sysID", "description", limittolist);
                           break;
                        //case "DocID":
                        //   SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwdocuments order by description", "sysID", "description", limittolist);
                        //   break;
                        case "Ownership":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vwlegalorganization order by description", "sysID", "description", limittolist);
                           break;
                        case "BranchType":
                           SetComboBoxDataSource((ComboBox)crl, "SELECT * FROM vweconomicorganization order by description", "sysID", "description", limittolist);
                           break;


                        default:
                           break;
                     }
                     break;
                  
                  default:
                     break;
               }
            }
            else if (crl.Controls != null && crl.Controls.Count > 0)
            {
               ProcessComboBoxes(crl, action);
            }
         }
      }

      public static AutoCompleteStringCollection TextBoxAutoCompSource(string sqlstr, List<SQLParameter> sqllist = null)
      {
         AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
         IDataReader dr;
         MySQLDatabase db = DatabaseManager.GetInstance("dbstandard");
         db.CloseConnection();
         dr = db.Select(sqlstr);
         if (dr != null)
         {
            while (dr.Read())
            {
               acsc.Add(dr.GetString(0));
            }
            dr.Close();
            return acsc;
         }
         return null;
      }
      public static void ProcessAddressComboData(string brgyID, ComboBox Brgy, ComboBox City, ComboBox Prov=null,ComboBox Region =null)
      {
         MySQLDatabase db;
         List<SQLParameter> paramss = new List<SQLParameter>();
         DataTable dt = new DataTable();
         db = DatabaseManager.GetInstance();
         db.CloseConnection();
        try
        {
            SetComboBoxDataSource(Brgy, "select b.id AS id,b.brgyDesc AS Barangay from dbstandarddata.refbrgy b where (b.id = " + brgyID + ")", "id", "Barangay");
            SetComboBoxDataSource(City, "select c.id AS id,c.citymunDesc AS City from dbstandarddata.refcitymun c join dbstandarddata.refbrgy b on c.id = b.cityID where b.id = " + brgyID, "id", "City");
            if (Prov!=null)
            {
                SetComboBoxDataSource(Prov, "select p.id AS id,p.provDesc AS Province from dbstandarddata.refprovince p join dbstandarddata.refcitymun c on p.id = c.provid join dbstandarddata.refbrgy b on c.id = b.cityID where b.id = " + brgyID, "id", "Province");
            }
        }
        catch (Exception e)
        {
            db.CloseConnection();
            MessageBox.Show(e.Message);
        }
      }
   }
}
