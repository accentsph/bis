﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIS
{
    class DatabaseManager
    {
        private static MySQLDatabase db;
        private static Dictionary<string, MySQLDatabase> otherDatabases;
        public static MySQLDatabase GetInstance(bool reinstantiate = false)
        {
            if(db == null || reinstantiate)
            {
                string encryptionKey = AppConfiguration.GetConfig("encryptionkey");
                string dbname = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("dbmain"), encryptionKey);
                string pw = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("db_password"), encryptionKey);
                string uid = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("db_uid"), encryptionKey);
                string server = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("dbserver"), encryptionKey);
                db = new MySQLDatabase(server, dbname, uid, pw);
            }
            return db;
        }
        
        public static MySQLDatabase GetInstance(string key, bool reinstantiate = false)
        {
            if(otherDatabases == null)
            {
                otherDatabases = new Dictionary<string, MySQLDatabase>();
            }
            string encryptionKey = AppConfiguration.GetConfig("encryptionkey");
            string dbname = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig(key), encryptionKey);
            string pw = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("db_password"), encryptionKey);
            string uid = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("db_uid"), encryptionKey);
            string server = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("dbserver"), encryptionKey);
            if(!otherDatabases.ContainsKey(key))
            {
                otherDatabases.Add(key, null);
            }
            if(otherDatabases[key] == null || reinstantiate)
            {
                otherDatabases[key] = new MySQLDatabase(server, dbname, uid, pw);
            }
            return (otherDatabases[key]);
        }
    }
}
