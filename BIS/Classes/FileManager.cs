﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace BIS
{
    class FileManager
    {
        /*public static async void uploadImageAsync(string imagePath, string imageName)
        {
            FileStream fileStream = null;
            try
            {
                string projectName = "afccor";
                var clientSecrets = new ClientSecrets();
                clientSecrets.ClientId = "968167537497-2foju9o90852mk3c2gbrpnmdep67l71r.apps.googleusercontent.com";
                clientSecrets.ClientSecret = "nWf1mpkq4FDOQor5Dv10Ffvf";
                var scopes = new[] { @"https://www.googleapis.com/auth/devstorage.full_control" };
                var cts = new CancellationTokenSource();
                var userCredential = await GoogleWebAuthorizationBroker.AuthorizeAsync(clientSecrets, scopes, "thakiraitsolutions@gmail.com", cts.Token);
                await userCredential.RefreshTokenAsync(cts.Token);
                var service = new Google.Apis.Storage.v1.StorageService();
                var bucketsQuery = service.Buckets.List(projectName);
                bucketsQuery.OauthToken = userCredential.Token.AccessToken;
                var buckets = bucketsQuery.Execute();
                var bucketToUpload = buckets.Items.FirstOrDefault().Name;
                var newObject = new Google.Apis.Storage.v1.Data.Object()
                {
                    Bucket = bucketToUpload,
                    Name = "Photo/" + imageName
                };
            
                fileStream = new FileStream(imagePath, FileMode.Open);
                var uploadRequest = new Google.Apis.Storage.v1.ObjectsResource.InsertMediaUpload(service, newObject,
                bucketToUpload, fileStream, "image/jpg");
                uploadRequest.OauthToken = userCredential.Token.AccessToken;
                await uploadRequest.UploadAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Something went wrong while uploading photo. A default photo will be set as this customer's photo. To upload a new photo, edit this customer.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Dispose();
                }
            }
        }*/

        public static string SelectFile(string title, string filter)
        {
         try
         {

            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = title;
            theDialog.Filter = filter;
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
               return (theDialog.FileName.ToString());
            }
         }
         catch(Exception err)
         {
            Console.Write("Error message: " + err.Message);
            Console.Write(err.StackTrace);
         }
            
         return null;
        }

      public static string SelectedFolder(string title)
      {
         try
         {
            FolderBrowserDialog theDialog = new FolderBrowserDialog();
            theDialog.Description = title;
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
               return theDialog.SelectedPath;
            }
         }
         catch (Exception err)
         {
            Console.Write("Error message: " + err.Message);
            Console.Write(err.StackTrace);
         }

         return null;
      }

        public static string ReadFromFile(string filePath)
        {
            try
            {
                return File.ReadAllText(filePath);
            }
            catch(Exception err)
            {
                Console.WriteLine(err.StackTrace);
                return null;
            }
        }

        public static bool DeleteFile(string filePath)
        {
            try
            {
                File.Delete(filePath);
                return true;
            }
            catch(Exception err)
            {
                Console.WriteLine(err.StackTrace);
                return false;
            }
        }

        public static bool WriteToFile(string folderPath, string fileName, string text = null)
        {
            try
            {
                if (folderPath == null)
                {
                    folderPath = "D:\\";
                }
                string filePath = folderPath + (folderPath.ElementAt(folderPath.Length - 1) == '\\' ? "" : "\\") + fileName;
                System.IO.Directory.CreateDirectory(folderPath);
                if(text != null)
                {
                    File.WriteAllText(filePath, text);
                }
                return true;
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                MessageBox.Show("Something went wrong while creating the file.\r\n\r\nError message:\t" + err.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool SaveImageToLocalFolder(string folderPath, string fileName, Image image)
        {
            string filePath = null;
            try
            {
                if (folderPath == null)
                {
                    folderPath = "D:\\";
                }
                filePath = folderPath + (folderPath.ElementAt(folderPath.Length - 1) == '\\' ? "" : "\\") + fileName;
                System.IO.Directory.CreateDirectory(folderPath);
                if (image != null)
                {
                    image.Save(filePath);
                }
                return true;
            }
            catch (Exception err)
            {
                Console.WriteLine("File Path: " + filePath);
                Console.WriteLine("Error Message: " + err.Message);
                Console.WriteLine(err.StackTrace);
                MessageBox.Show("Something went wrong while saving the image.\r\n\r\nError message:\t" + err.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public static bool EmptyDirectory(string directoryPath)
        {
            DirectoryInfo directory;
            try
            {
                directory = new DirectoryInfo(directoryPath);
                foreach (System.IO.FileInfo file in directory.GetFiles()) file.Delete();
                foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
            }
            catch (Exception err)
            {
                Console.WriteLine("Error message: " + err.Message);
                Console.WriteLine(err.StackTrace);
                return false;
            }
            return true;
        }

        public static bool CopyFile(string filePath, string destinationFilePath)
        {
            try
            {
                File.Copy(filePath, destinationFilePath);
            }
            catch(Exception err)
            {
                Console.WriteLine("Error message: " + err.Message);
                Console.WriteLine(err.StackTrace);
                return false;
            }
            return true;
        }

        public static void LoadImageToPictureBox(string photoPath, PictureBox pb)
        {
            try
            {
                pb.Load(photoPath);
                pb.Image.Tag = null;
                pb.Tag = photoPath;
            }
            catch (Exception err)
            {
                Console.WriteLine("Error message: " + err.Message);
                Console.WriteLine(err.StackTrace);
                pb.Tag = null;
            }
        }
    }
}
