﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIS
{
    class BISSSLCertificate
    {
        private string certificateFilePath, certificatePassword;

        public BISSSLCertificate(string certficateFilePath, string certificatePassword=null)
        {
            certficateFilePath = this.certificateFilePath;
            certificatePassword = this.certificatePassword;
        }

        public string CertificateFilePath
        {
            get { return certificateFilePath; }
        }

        public string CertificatePassword
        {
            get { return certificatePassword; }
        }
    }
}
