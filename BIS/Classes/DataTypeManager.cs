﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIS
{
    class DataTypeManager
    {
        public static string ParseDataToString(object data, string header = null)
        {
            if (data == null)
            {
                return ("");
            }
            if (data.ToString() == "")
            {
                return null;
            }
            if (data is DateTime)
            {
                return (Convert.ToDateTime(data).ToString("MMM. dd, yyyy"));
            }
            if (data is Decimal)
            {
                return (Convert.ToDecimal(data).ToString("#.00"));
            }
            if (data is Boolean)
            {
                return (((Boolean)data).ToString());
            }
            return (data.ToString());
        }

        public static int ParseDataToInt(object data)
        {
            try
            {
                return(Convert.ToInt32(data));
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return -1;
            }
        }

        public static DateTime ParseDataToDateTime(object data)
        {
            try
            {
                return Convert.ToDateTime(data);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return new DateTime(1, 1, 1);
            }
        }

        public static bool ParseDataToBool(object data)
        {
            try
            {
                return Convert.ToBoolean(data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        public static Decimal ParseDataToDecimal(object data)
        {
            try
            {
                return Convert.ToDecimal(data);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return -1;
            }
        }
    }
}
