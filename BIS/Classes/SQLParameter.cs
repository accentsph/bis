﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BIS
{
    public class SQLParameter
    {
        private string id;
        private object value;
        private string booleanOperator;
        private string columnName;
        private string andorr;

        public SQLParameter(String id, object value, string booleanOperator = "=", string columnName = null, string andorr = "AND")
        {
            this.id = id.Replace(" ", "_");
            this.value = value;
            this.booleanOperator = booleanOperator;
            this.columnName = columnName;
            this.andorr = andorr;
            if(this.columnName != null && columnName.Contains(" "))
            {
                this.columnName = "`" + this.columnName + "`";
            }
        }

        public string ID
        {
            get
            {
                return this.id;
            }
        }

        public object Value
        {
            get
            {
                return this.value;
            }
        }

        public string BooleanOperator
        {
            get
            {
                return this.booleanOperator;
            }
        }

        public string ColumnName
        {
            get
            {
                return this.columnName;
            }
            set
            {
                columnName = value;
            }
        }

        public string AndOr
        {
            get
            {
                return this.andorr;
            }
        }
    }
}
