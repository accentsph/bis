﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
//using System.IO;

namespace BIS
{
    class HashComputer
    {
        public static string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public class Encryptor
        {
            public static string EncryptData(string textData, string Encryptionkey)
            {
                RijndaelManaged objrij = new RijndaelManaged();
                //set the mode for operation of the algorithm
                objrij.Mode = CipherMode.CBC;
                //set the padding mode used in the algorithm.
                objrij.Padding = PaddingMode.PKCS7;
                //set the size, in bits, for the secret key.
                objrij.KeySize = 0x80;
                //set the block size in bits for the cryptographic operation.
                objrij.BlockSize = 0x80;
                //set the symmetric key that is used for encryption & decryption.
                byte[] passBytes = Encoding.UTF8.GetBytes(Encryptionkey);
                //set the initialization vector (IV) for the symmetric algorithm
                byte[] EncryptionkeyBytes = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
                int len = passBytes.Length;
                if (len > EncryptionkeyBytes.Length)
                {
                    len = EncryptionkeyBytes.Length;
                }
                Array.Copy(passBytes, EncryptionkeyBytes, len);
                objrij.Key = EncryptionkeyBytes;
                objrij.IV = EncryptionkeyBytes;
                //Creates symmetric AES object with the current key and initialization vector IV.
                ICryptoTransform objtransform = objrij.CreateEncryptor();
                byte[] textDataByte = Encoding.UTF8.GetBytes(textData);
                //Final transform the test string.
                return Convert.ToBase64String(objtransform.TransformFinalBlock(textDataByte, 0, textDataByte.Length));
            }

            public static string DecryptData(string EncryptedText, string Encryptionkey)
            {
                RijndaelManaged objrij = new RijndaelManaged();
                try
                {
                    objrij.Mode = CipherMode.CBC;
                    objrij.Padding = PaddingMode.PKCS7;
                    objrij.KeySize = 0x80;
                    objrij.BlockSize = 0x80;
                    byte[] encryptedTextByte = Convert.FromBase64String(EncryptedText);
                    byte[] passBytes = Encoding.UTF8.GetBytes(Encryptionkey);
                    byte[] EncryptionkeyBytes = new byte[0x10];
                    int len = passBytes.Length;
                    if (len > EncryptionkeyBytes.Length)
                    {
                        len = EncryptionkeyBytes.Length;
                    }
                    Array.Copy(passBytes, EncryptionkeyBytes, len);
                    objrij.Key = EncryptionkeyBytes;
                    objrij.IV = EncryptionkeyBytes;
                    byte[] TextByte = objrij.CreateDecryptor().TransformFinalBlock(encryptedTextByte, 0, encryptedTextByte.Length);
                    return Encoding.UTF8.GetString(TextByte);  //it will return readable string
                }
                catch(Exception err)
                {
                    Console.WriteLine(err.StackTrace);
                    Console.WriteLine(err.Message);
                    return null;
                }
            }
        }
    }
}
