﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIS
{
    class ActiveUser
    {
        //change
        public static int clientid = -1;
        public static string username;
        public static string photo;
        public static string clientname;
        
        public static void Logout()
        {
            clientid = -1;
            username = null;
            photo = null;
            clientname = null;
        }

        public static bool IsLogged()
        {
            if(clientid > -1)
            {
                return true;
            }
            return false;
        }
    }
}
