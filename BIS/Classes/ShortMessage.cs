using System;
using System.Collections.Generic;
using System.Text;

namespace BIS
{
	public class ShortMessage
	{
		private string index;
		private string status;
		private string sender;
		private string alphabet;
		private string sent;
		private string message;

		public string Index
		{
			get { return index;}
			set { index = value;}
		}

		public string Status
		{
			get { return status;}
			set { status = value;}
		}

		public string Sender
		{
			get { return sender;}
			set { sender = value;}
		}

		public string Alphabet
		{
			get { return alphabet;}
			set { alphabet = value;}
		}

		public string Sent
		{
			get { return sent;}
			set { sent = value;}
		}

		public string Message
		{
			get { return message;}
			set { message = value;}
		}

        public bool isValidPersonalNumberPH()
        {
            if (sender.Length == 13)
            {
                if (sender.StartsWith("+639"))
                {
                    return true;
                }
            }
            else if (sender.Length == 11)
            {
                if (sender.StartsWith("09"))
                {
                    return true;
                }
            }
            return false;
        }
    }

    public class ShortMessageCollection : List<ShortMessage>
    {
    }
}
