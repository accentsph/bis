﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace BIS
{
    interface Database
    {
        IDataReader Select(string query, List<SQLParameter> parameters = null, string options = null);
        object ScalarSelect(string query, List<SQLParameter> parameters = null, string options = null);
        bool CreateDatabase(string dbname);
        bool CloseConnection();
        bool OpenConnection();
        bool NonQuery(string comm);
        IDbConnection Connection
        {
            get;
        }
    }
}
