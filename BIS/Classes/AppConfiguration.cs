﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace BIS
{
    public class AppConfiguration
    {
        public enum Settings
        {
            AppSettings = 0,
            ConnectionStrings = 1
        }

        private static Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        public static string GetConfig(string key)
        {
            if(config.AppSettings.Settings[key] == null)
            {
                return null;
            }
            return config.AppSettings.Settings[key].Value;
        }

        public static bool SetConfig(string key, string value)
        {
            try
            {
                if (config.AppSettings.Settings[key] == null)
                {
                    config.AppSettings.Settings.Add(key, value);
                }
                else
                {
                    config.AppSettings.Settings[key].Value = value;
                }
                return SaveConfig(Settings.AppSettings);
            }
            catch(Exception err)
            {
                Console.WriteLine(err.StackTrace);
                return false;
            }
        }

        public static bool SaveConfig(Settings settings)
        {
            try
            {
                config.Save(ConfigurationSaveMode.Modified);
                switch (settings)
                {
                    case Settings.AppSettings:
                        ConfigurationManager.RefreshSection("appSettings");
                        break;
                    case Settings.ConnectionStrings:
                        ConfigurationManager.RefreshSection("connectionStrings");
                        break;
                }
                return true;
            }
            catch(Exception err)
            {
                Console.WriteLine(err.StackTrace);
                return false;
            }
        }
    }
}
