﻿namespace BIS
{
   partial class Form_Settings
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Settings));
         this.splitContainer1 = new System.Windows.Forms.SplitContainer();
         this.label14 = new System.Windows.Forms.Label();
         this.PreviousTab = new System.Windows.Forms.Label();
         this.NextTab = new System.Windows.Forms.Label();
         this.tabControl1 = new System.Windows.Forms.TabControl();
         this.tabPage1 = new System.Windows.Forms.TabPage();
         this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
         this.label4 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.label7 = new System.Windows.Forms.Label();
         this.label5 = new System.Windows.Forms.Label();
         this.Region = new System.Windows.Forms.ComboBox();
         this.Province = new System.Windows.Forms.ComboBox();
         this.City = new System.Windows.Forms.ComboBox();
         this.Barangay = new System.Windows.Forms.ComboBox();
         this.label6 = new System.Windows.Forms.Label();
         this.brgyname = new System.Windows.Forms.TextBox();
         this.brgyaddress1 = new System.Windows.Forms.TextBox();
         this.label10 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.brgyaddress2 = new System.Windows.Forms.TextBox();
         this.brgyaddress3 = new System.Windows.Forms.TextBox();
         this.brgycontactno = new System.Windows.Forms.TextBox();
         this.brgyemailad = new System.Windows.Forms.TextBox();
         this.label8 = new System.Windows.Forms.Label();
         this.tabPage2 = new System.Windows.Forms.TabPage();
         this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
         this.label32 = new System.Windows.Forms.Label();
         this.label29 = new System.Windows.Forms.Label();
         this.label26 = new System.Windows.Forms.Label();
         this.label25 = new System.Windows.Forms.Label();
         this.label13 = new System.Windows.Forms.Label();
         this.CityLogo = new System.Windows.Forms.TextBox();
         this.label11 = new System.Windows.Forms.Label();
         this.BrgyLogo = new System.Windows.Forms.TextBox();
         this.label12 = new System.Windows.Forms.Label();
         this.AppIcon = new System.Windows.Forms.TextBox();
         this.label24 = new System.Windows.Forms.Label();
         this.label23 = new System.Windows.Forms.Label();
         this.AppCaption = new System.Windows.Forms.TextBox();
         this.label15 = new System.Windows.Forms.Label();
         this.label17 = new System.Windows.Forms.Label();
         this.label52 = new System.Windows.Forms.Label();
         this.label21 = new System.Windows.Forms.Label();
         this.label16 = new System.Windows.Forms.Label();
         this.label22 = new System.Windows.Forms.Label();
         this.label31 = new System.Windows.Forms.Label();
         this.label19 = new System.Windows.Forms.Label();
         this.label20 = new System.Windows.Forms.Label();
         this.SystemPath = new System.Windows.Forms.TextBox();
         this.label28 = new System.Windows.Forms.Label();
         this.PhotoPath = new System.Windows.Forms.TextBox();
         this.ReportsPath = new System.Windows.Forms.TextBox();
         this.BrgyFilesPath = new System.Windows.Forms.TextBox();
         this.SignaturePath = new System.Windows.Forms.TextBox();
         this.label30 = new System.Windows.Forms.Label();
         this.FIDPath = new System.Windows.Forms.TextBox();
         this.label34 = new System.Windows.Forms.Label();
         this.tabPage3 = new System.Windows.Forms.TabPage();
         this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
         this.label41 = new System.Windows.Forms.Label();
         this.label42 = new System.Windows.Forms.Label();
         this.dbmain = new System.Windows.Forms.TextBox();
         this.dbstandard = new System.Windows.Forms.TextBox();
         this.dbbiometrics = new System.Windows.Forms.TextBox();
         this.dbsyslogs = new System.Windows.Forms.TextBox();
         this.label43 = new System.Windows.Forms.Label();
         this.label44 = new System.Windows.Forms.Label();
         this.db_password = new System.Windows.Forms.TextBox();
         this.label45 = new System.Windows.Forms.Label();
         this.db_uid = new System.Windows.Forms.TextBox();
         this.label47 = new System.Windows.Forms.Label();
         this.label48 = new System.Windows.Forms.Label();
         this.label50 = new System.Windows.Forms.Label();
         this.dbserver = new System.Windows.Forms.TextBox();
         this.label51 = new System.Windows.Forms.Label();
         this.label27 = new System.Windows.Forms.Label();
         this.textBox13 = new System.Windows.Forms.TextBox();
         this.panel1 = new System.Windows.Forms.Panel();
         this.button1 = new System.Windows.Forms.Button();
         this.button2 = new System.Windows.Forms.Button();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
         this.splitContainer1.Panel1.SuspendLayout();
         this.splitContainer1.Panel2.SuspendLayout();
         this.splitContainer1.SuspendLayout();
         this.tabControl1.SuspendLayout();
         this.tabPage1.SuspendLayout();
         this.tableLayoutPanel1.SuspendLayout();
         this.tabPage2.SuspendLayout();
         this.tableLayoutPanel2.SuspendLayout();
         this.tabPage3.SuspendLayout();
         this.tableLayoutPanel3.SuspendLayout();
         this.panel1.SuspendLayout();
         this.SuspendLayout();
         // 
         // splitContainer1
         // 
         this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
         this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitContainer1.Location = new System.Drawing.Point(11, 10);
         this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.splitContainer1.Name = "splitContainer1";
         this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer1.Panel1
         // 
         this.splitContainer1.Panel1.Controls.Add(this.label14);
         this.splitContainer1.Panel1.Controls.Add(this.PreviousTab);
         this.splitContainer1.Panel1.Controls.Add(this.NextTab);
         // 
         // splitContainer1.Panel2
         // 
         this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
         this.splitContainer1.Size = new System.Drawing.Size(730, 650);
         this.splitContainer1.SplitterDistance = 65;
         this.splitContainer1.TabIndex = 0;
         // 
         // label14
         // 
         this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label14.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label14.ForeColor = System.Drawing.Color.White;
         this.label14.Location = new System.Drawing.Point(0, 0);
         this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size(660, 65);
         this.label14.TabIndex = 108;
         this.label14.Text = "Settings";
         this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // PreviousTab
         // 
         this.PreviousTab.Dock = System.Windows.Forms.DockStyle.Right;
         this.PreviousTab.Enabled = false;
         this.PreviousTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.PreviousTab.Location = new System.Drawing.Point(660, 0);
         this.PreviousTab.Name = "PreviousTab";
         this.PreviousTab.Size = new System.Drawing.Size(35, 65);
         this.PreviousTab.TabIndex = 107;
         this.PreviousTab.Text = "<";
         this.PreviousTab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.PreviousTab.Click += new System.EventHandler(this.PreviousTab_Click);
         // 
         // NextTab
         // 
         this.NextTab.Dock = System.Windows.Forms.DockStyle.Right;
         this.NextTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.NextTab.Location = new System.Drawing.Point(695, 0);
         this.NextTab.Name = "NextTab";
         this.NextTab.Size = new System.Drawing.Size(35, 65);
         this.NextTab.TabIndex = 106;
         this.NextTab.Text = ">";
         this.NextTab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.NextTab.Click += new System.EventHandler(this.NextTab_Click);
         // 
         // tabControl1
         // 
         this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
         this.tabControl1.Controls.Add(this.tabPage1);
         this.tabControl1.Controls.Add(this.tabPage2);
         this.tabControl1.Controls.Add(this.tabPage3);
         this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
         this.tabControl1.Location = new System.Drawing.Point(0, 0);
         this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tabControl1.Name = "tabControl1";
         this.tabControl1.SelectedIndex = 0;
         this.tabControl1.Size = new System.Drawing.Size(730, 581);
         this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
         this.tabControl1.TabIndex = 0;
         // 
         // tabPage1
         // 
         this.tabPage1.BackColor = System.Drawing.Color.Transparent;
         this.tabPage1.Controls.Add(this.tableLayoutPanel1);
         this.tabPage1.Location = new System.Drawing.Point(4, 5);
         this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tabPage1.Name = "tabPage1";
         this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tabPage1.Size = new System.Drawing.Size(722, 572);
         this.tabPage1.TabIndex = 0;
         this.tabPage1.Text = "tabPage1";
         this.tabPage1.ToolTipText = "Page 1";
         // 
         // tableLayoutPanel1
         // 
         this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
         this.tableLayoutPanel1.ColumnCount = 2;
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
         this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
         this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
         this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
         this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
         this.tableLayoutPanel1.Controls.Add(this.label7, 0, 0);
         this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
         this.tableLayoutPanel1.Controls.Add(this.Region, 1, 1);
         this.tableLayoutPanel1.Controls.Add(this.Province, 1, 2);
         this.tableLayoutPanel1.Controls.Add(this.City, 1, 3);
         this.tableLayoutPanel1.Controls.Add(this.Barangay, 1, 4);
         this.tableLayoutPanel1.Controls.Add(this.label6, 0, 6);
         this.tableLayoutPanel1.Controls.Add(this.brgyname, 1, 6);
         this.tableLayoutPanel1.Controls.Add(this.brgyaddress1, 1, 7);
         this.tableLayoutPanel1.Controls.Add(this.label10, 0, 10);
         this.tableLayoutPanel1.Controls.Add(this.label9, 0, 11);
         this.tableLayoutPanel1.Controls.Add(this.brgyaddress2, 1, 8);
         this.tableLayoutPanel1.Controls.Add(this.brgyaddress3, 1, 9);
         this.tableLayoutPanel1.Controls.Add(this.brgycontactno, 1, 10);
         this.tableLayoutPanel1.Controls.Add(this.brgyemailad, 1, 11);
         this.tableLayoutPanel1.Controls.Add(this.label8, 0, 7);
         this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tableLayoutPanel1.ImeMode = System.Windows.Forms.ImeMode.Disable;
         this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 2);
         this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tableLayoutPanel1.Name = "tableLayoutPanel1";
         this.tableLayoutPanel1.RowCount = 12;
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel1.Size = new System.Drawing.Size(716, 568);
         this.tableLayoutPanel1.TabIndex = 1;
         this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
         // 
         // label4
         // 
         this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label4.ForeColor = System.Drawing.Color.Navy;
         this.label4.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label4.Location = new System.Drawing.Point(5, 146);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(136, 34);
         this.label4.TabIndex = 3;
         this.label4.Text = "Barangay";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label3
         // 
         this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.ForeColor = System.Drawing.Color.Navy;
         this.label3.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label3.Location = new System.Drawing.Point(5, 110);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(136, 34);
         this.label3.TabIndex = 2;
         this.label3.Text = "City";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label2
         // 
         this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.ForeColor = System.Drawing.Color.Navy;
         this.label2.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label2.Location = new System.Drawing.Point(5, 74);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(136, 34);
         this.label2.TabIndex = 1;
         this.label2.Text = "Province";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label1
         // 
         this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label1.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.ForeColor = System.Drawing.Color.Navy;
         this.label1.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label1.Location = new System.Drawing.Point(5, 38);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(136, 34);
         this.label1.TabIndex = 0;
         this.label1.Text = "Region";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label7
         // 
         this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label7.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel1.SetColumnSpan(this.label7, 2);
         this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label7.ForeColor = System.Drawing.Color.MediumBlue;
         this.label7.Image = ((System.Drawing.Image)(resources.GetObject("label7.Image")));
         this.label7.Location = new System.Drawing.Point(5, 2);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(706, 34);
         this.label7.TabIndex = 6;
         this.label7.Text = "GEOGRAPHICAL COMPONENT";
         this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label5
         // 
         this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label5.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel1.SetColumnSpan(this.label5, 2);
         this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.ForeColor = System.Drawing.Color.MediumBlue;
         this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
         this.label5.Location = new System.Drawing.Point(5, 182);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(706, 34);
         this.label5.TabIndex = 7;
         this.label5.Text = "BARANGAY PROFILE";
         this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // Region
         // 
         this.Region.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Region.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.Region.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Region.FormattingEnabled = true;
         this.Region.Location = new System.Drawing.Point(149, 40);
         this.Region.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.Region.Name = "Region";
         this.Region.Size = new System.Drawing.Size(562, 30);
         this.Region.TabIndex = 9;
         this.Region.SelectedValueChanged += new System.EventHandler(this.Region_SelectedValueChanged);
         // 
         // Province
         // 
         this.Province.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Province.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.Province.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Province.FormattingEnabled = true;
         this.Province.Location = new System.Drawing.Point(149, 76);
         this.Province.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.Province.Name = "Province";
         this.Province.Size = new System.Drawing.Size(562, 30);
         this.Province.TabIndex = 8;
         this.Province.SelectedValueChanged += new System.EventHandler(this.Province_SelectedValueChanged);
         // 
         // City
         // 
         this.City.Dock = System.Windows.Forms.DockStyle.Fill;
         this.City.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.City.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.City.FormattingEnabled = true;
         this.City.Location = new System.Drawing.Point(149, 112);
         this.City.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.City.Name = "City";
         this.City.Size = new System.Drawing.Size(562, 30);
         this.City.TabIndex = 10;
         this.City.SelectedValueChanged += new System.EventHandler(this.City_SelectedValueChanged);
         // 
         // Barangay
         // 
         this.Barangay.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Barangay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.Barangay.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Barangay.FormattingEnabled = true;
         this.Barangay.Location = new System.Drawing.Point(149, 148);
         this.Barangay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.Barangay.Name = "Barangay";
         this.Barangay.Size = new System.Drawing.Size(562, 30);
         this.Barangay.TabIndex = 11;
         // 
         // label6
         // 
         this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label6.ForeColor = System.Drawing.Color.Navy;
         this.label6.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label6.Location = new System.Drawing.Point(5, 218);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(136, 34);
         this.label6.TabIndex = 12;
         this.label6.Text = "Name";
         this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // brgyname
         // 
         this.brgyname.Dock = System.Windows.Forms.DockStyle.Fill;
         this.brgyname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.brgyname.Location = new System.Drawing.Point(149, 220);
         this.brgyname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.brgyname.Name = "brgyname";
         this.brgyname.Size = new System.Drawing.Size(562, 28);
         this.brgyname.TabIndex = 13;
         // 
         // brgyaddress1
         // 
         this.brgyaddress1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.brgyaddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.brgyaddress1.Location = new System.Drawing.Point(149, 256);
         this.brgyaddress1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.brgyaddress1.Name = "brgyaddress1";
         this.brgyaddress1.Size = new System.Drawing.Size(562, 28);
         this.brgyaddress1.TabIndex = 15;
         // 
         // label10
         // 
         this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label10.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label10.ForeColor = System.Drawing.Color.Navy;
         this.label10.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label10.Location = new System.Drawing.Point(5, 364);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(136, 34);
         this.label10.TabIndex = 17;
         this.label10.Text = "Contact No.";
         this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label9
         // 
         this.label9.AutoEllipsis = true;
         this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label9.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label9.ForeColor = System.Drawing.Color.Navy;
         this.label9.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label9.Location = new System.Drawing.Point(5, 400);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(136, 166);
         this.label9.TabIndex = 16;
         this.label9.Text = "eMail Address";
         this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // brgyaddress2
         // 
         this.brgyaddress2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.brgyaddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.brgyaddress2.Location = new System.Drawing.Point(149, 294);
         this.brgyaddress2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.brgyaddress2.Name = "brgyaddress2";
         this.brgyaddress2.Size = new System.Drawing.Size(562, 28);
         this.brgyaddress2.TabIndex = 18;
         // 
         // brgyaddress3
         // 
         this.brgyaddress3.Dock = System.Windows.Forms.DockStyle.Fill;
         this.brgyaddress3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.brgyaddress3.Location = new System.Drawing.Point(149, 330);
         this.brgyaddress3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.brgyaddress3.Name = "brgyaddress3";
         this.brgyaddress3.Size = new System.Drawing.Size(562, 28);
         this.brgyaddress3.TabIndex = 19;
         // 
         // brgycontactno
         // 
         this.brgycontactno.Dock = System.Windows.Forms.DockStyle.Fill;
         this.brgycontactno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.brgycontactno.Location = new System.Drawing.Point(149, 366);
         this.brgycontactno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.brgycontactno.Name = "brgycontactno";
         this.brgycontactno.Size = new System.Drawing.Size(562, 28);
         this.brgycontactno.TabIndex = 20;
         // 
         // brgyemailad
         // 
         this.brgyemailad.Dock = System.Windows.Forms.DockStyle.Fill;
         this.brgyemailad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.brgyemailad.Location = new System.Drawing.Point(149, 402);
         this.brgyemailad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.brgyemailad.Multiline = true;
         this.brgyemailad.Name = "brgyemailad";
         this.brgyemailad.Size = new System.Drawing.Size(562, 162);
         this.brgyemailad.TabIndex = 21;
         // 
         // label8
         // 
         this.label8.BackColor = System.Drawing.SystemColors.ButtonFace;
         this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label8.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label8.ForeColor = System.Drawing.Color.Navy;
         this.label8.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label8.Location = new System.Drawing.Point(5, 254);
         this.label8.Name = "label8";
         this.tableLayoutPanel1.SetRowSpan(this.label8, 3);
         this.label8.Size = new System.Drawing.Size(136, 108);
         this.label8.TabIndex = 14;
         this.label8.Text = "Address";
         this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // tabPage2
         // 
         this.tabPage2.BackColor = System.Drawing.Color.Transparent;
         this.tabPage2.Controls.Add(this.tableLayoutPanel2);
         this.tabPage2.Location = new System.Drawing.Point(4, 5);
         this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tabPage2.Name = "tabPage2";
         this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tabPage2.Size = new System.Drawing.Size(722, 572);
         this.tabPage2.TabIndex = 1;
         this.tabPage2.Text = "tabPage2";
         this.tabPage2.ToolTipText = "Page 2";
         // 
         // tableLayoutPanel2
         // 
         this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
         this.tableLayoutPanel2.ColumnCount = 3;
         this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
         this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
         this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
         this.tableLayoutPanel2.Controls.Add(this.label32, 2, 10);
         this.tableLayoutPanel2.Controls.Add(this.label29, 2, 7);
         this.tableLayoutPanel2.Controls.Add(this.label26, 2, 4);
         this.tableLayoutPanel2.Controls.Add(this.label25, 2, 2);
         this.tableLayoutPanel2.Controls.Add(this.label13, 0, 2);
         this.tableLayoutPanel2.Controls.Add(this.CityLogo, 1, 4);
         this.tableLayoutPanel2.Controls.Add(this.label11, 0, 4);
         this.tableLayoutPanel2.Controls.Add(this.BrgyLogo, 1, 3);
         this.tableLayoutPanel2.Controls.Add(this.label12, 0, 3);
         this.tableLayoutPanel2.Controls.Add(this.AppIcon, 1, 2);
         this.tableLayoutPanel2.Controls.Add(this.label24, 2, 3);
         this.tableLayoutPanel2.Controls.Add(this.label23, 0, 1);
         this.tableLayoutPanel2.Controls.Add(this.AppCaption, 1, 1);
         this.tableLayoutPanel2.Controls.Add(this.label15, 0, 0);
         this.tableLayoutPanel2.Controls.Add(this.label17, 0, 6);
         this.tableLayoutPanel2.Controls.Add(this.label52, 0, 5);
         this.tableLayoutPanel2.Controls.Add(this.label21, 0, 8);
         this.tableLayoutPanel2.Controls.Add(this.label16, 0, 7);
         this.tableLayoutPanel2.Controls.Add(this.label22, 0, 9);
         this.tableLayoutPanel2.Controls.Add(this.label31, 2, 8);
         this.tableLayoutPanel2.Controls.Add(this.label19, 0, 10);
         this.tableLayoutPanel2.Controls.Add(this.label20, 0, 11);
         this.tableLayoutPanel2.Controls.Add(this.SystemPath, 1, 6);
         this.tableLayoutPanel2.Controls.Add(this.label28, 2, 6);
         this.tableLayoutPanel2.Controls.Add(this.PhotoPath, 1, 7);
         this.tableLayoutPanel2.Controls.Add(this.ReportsPath, 1, 10);
         this.tableLayoutPanel2.Controls.Add(this.BrgyFilesPath, 1, 11);
         this.tableLayoutPanel2.Controls.Add(this.SignaturePath, 1, 8);
         this.tableLayoutPanel2.Controls.Add(this.label30, 2, 9);
         this.tableLayoutPanel2.Controls.Add(this.FIDPath, 1, 9);
         this.tableLayoutPanel2.Controls.Add(this.label34, 2, 11);
         this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tableLayoutPanel2.ImeMode = System.Windows.Forms.ImeMode.Disable;
         this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 2);
         this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tableLayoutPanel2.Name = "tableLayoutPanel2";
         this.tableLayoutPanel2.RowCount = 16;
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel2.Size = new System.Drawing.Size(716, 568);
         this.tableLayoutPanel2.TabIndex = 2;
         this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
         // 
         // label32
         // 
         this.label32.AutoSize = true;
         this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label32.Location = new System.Drawing.Point(678, 362);
         this.label32.Margin = new System.Windows.Forms.Padding(0);
         this.label32.Name = "label32";
         this.label32.Size = new System.Drawing.Size(36, 34);
         this.label32.TabIndex = 44;
         this.label32.Text = "...";
         this.label32.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label32.Click += new System.EventHandler(this.label32_Click);
         // 
         // label29
         // 
         this.label29.AutoSize = true;
         this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label29.Location = new System.Drawing.Point(678, 254);
         this.label29.Margin = new System.Windows.Forms.Padding(0);
         this.label29.Name = "label29";
         this.label29.Size = new System.Drawing.Size(36, 34);
         this.label29.TabIndex = 41;
         this.label29.Text = "...";
         this.label29.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label29.Click += new System.EventHandler(this.label29_Click);
         // 
         // label26
         // 
         this.label26.AutoSize = true;
         this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label26.Location = new System.Drawing.Point(678, 146);
         this.label26.Margin = new System.Windows.Forms.Padding(0);
         this.label26.Name = "label26";
         this.label26.Size = new System.Drawing.Size(36, 34);
         this.label26.TabIndex = 39;
         this.label26.Text = "...";
         this.label26.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label26.Click += new System.EventHandler(this.label26_Click);
         // 
         // label25
         // 
         this.label25.AutoSize = true;
         this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label25.Location = new System.Drawing.Point(678, 74);
         this.label25.Margin = new System.Windows.Forms.Padding(0);
         this.label25.Name = "label25";
         this.label25.Size = new System.Drawing.Size(36, 34);
         this.label25.TabIndex = 38;
         this.label25.Text = "...";
         this.label25.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label25.Click += new System.EventHandler(this.label25_Click);
         // 
         // label13
         // 
         this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label13.ForeColor = System.Drawing.Color.Navy;
         this.label13.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label13.Location = new System.Drawing.Point(5, 74);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(135, 34);
         this.label13.TabIndex = 1;
         this.label13.Text = "Icon";
         this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // CityLogo
         // 
         this.CityLogo.Dock = System.Windows.Forms.DockStyle.Fill;
         this.CityLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.CityLogo.Location = new System.Drawing.Point(148, 148);
         this.CityLogo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.CityLogo.Name = "CityLogo";
         this.CityLogo.Size = new System.Drawing.Size(525, 28);
         this.CityLogo.TabIndex = 30;
         this.CityLogo.Tag = "";
         // 
         // label11
         // 
         this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label11.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label11.ForeColor = System.Drawing.Color.Navy;
         this.label11.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label11.Location = new System.Drawing.Point(5, 146);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(135, 34);
         this.label11.TabIndex = 3;
         this.label11.Text = "City Logo";
         this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // BrgyLogo
         // 
         this.BrgyLogo.Dock = System.Windows.Forms.DockStyle.Fill;
         this.BrgyLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.BrgyLogo.Location = new System.Drawing.Point(148, 112);
         this.BrgyLogo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.BrgyLogo.Name = "BrgyLogo";
         this.BrgyLogo.Size = new System.Drawing.Size(525, 28);
         this.BrgyLogo.TabIndex = 29;
         this.BrgyLogo.Tag = "";
         // 
         // label12
         // 
         this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label12.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label12.ForeColor = System.Drawing.Color.Navy;
         this.label12.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label12.Location = new System.Drawing.Point(5, 110);
         this.label12.Name = "label12";
         this.label12.Size = new System.Drawing.Size(135, 34);
         this.label12.TabIndex = 2;
         this.label12.Text = "Barangay Logo";
         this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // AppIcon
         // 
         this.AppIcon.Dock = System.Windows.Forms.DockStyle.Fill;
         this.AppIcon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.AppIcon.Location = new System.Drawing.Point(148, 76);
         this.AppIcon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.AppIcon.Name = "AppIcon";
         this.AppIcon.Size = new System.Drawing.Size(525, 28);
         this.AppIcon.TabIndex = 28;
         this.AppIcon.Tag = "";
         // 
         // label24
         // 
         this.label24.AutoSize = true;
         this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label24.Location = new System.Drawing.Point(678, 110);
         this.label24.Margin = new System.Windows.Forms.Padding(0);
         this.label24.Name = "label24";
         this.label24.Size = new System.Drawing.Size(36, 34);
         this.label24.TabIndex = 37;
         this.label24.Text = "...";
         this.label24.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label24.Click += new System.EventHandler(this.label24_Click);
         // 
         // label23
         // 
         this.label23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label23.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label23.ForeColor = System.Drawing.Color.Navy;
         this.label23.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label23.Location = new System.Drawing.Point(5, 38);
         this.label23.Name = "label23";
         this.label23.Size = new System.Drawing.Size(132, 34);
         this.label23.TabIndex = 35;
         this.label23.Tag = "c";
         this.label23.Text = "Caption";
         this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // AppCaption
         // 
         this.AppCaption.Dock = System.Windows.Forms.DockStyle.Fill;
         this.AppCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.AppCaption.Location = new System.Drawing.Point(148, 40);
         this.AppCaption.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.AppCaption.Name = "AppCaption";
         this.AppCaption.Size = new System.Drawing.Size(525, 28);
         this.AppCaption.TabIndex = 36;
         // 
         // label15
         // 
         this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label15.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel2.SetColumnSpan(this.label15, 3);
         this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label15.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label15.ForeColor = System.Drawing.Color.MediumBlue;
         this.label15.Image = ((System.Drawing.Image)(resources.GetObject("label15.Image")));
         this.label15.Location = new System.Drawing.Point(5, 2);
         this.label15.Name = "label15";
         this.label15.Size = new System.Drawing.Size(706, 34);
         this.label15.TabIndex = 22;
         this.label15.Text = "SYSTEM VARIABLES";
         this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label17
         // 
         this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label17.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label17.ForeColor = System.Drawing.Color.Navy;
         this.label17.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label17.Location = new System.Drawing.Point(5, 218);
         this.label17.Name = "label17";
         this.label17.Size = new System.Drawing.Size(132, 34);
         this.label17.TabIndex = 12;
         this.label17.Text = "System Path";
         this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label52
         // 
         this.label52.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label52.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel2.SetColumnSpan(this.label52, 3);
         this.label52.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label52.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label52.ForeColor = System.Drawing.Color.MediumBlue;
         this.label52.Image = ((System.Drawing.Image)(resources.GetObject("label52.Image")));
         this.label52.Location = new System.Drawing.Point(5, 182);
         this.label52.Name = "label52";
         this.label52.Size = new System.Drawing.Size(706, 34);
         this.label52.TabIndex = 45;
         this.label52.Text = "SYSTEM PATH AND LOCATIONS";
         this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label21
         // 
         this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label21.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label21.ForeColor = System.Drawing.Color.Navy;
         this.label21.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label21.Location = new System.Drawing.Point(5, 290);
         this.label21.Name = "label21";
         this.label21.Size = new System.Drawing.Size(132, 34);
         this.label21.TabIndex = 33;
         this.label21.Text = "Signature Path";
         this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label16
         // 
         this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label16.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label16.ForeColor = System.Drawing.Color.Navy;
         this.label16.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label16.Location = new System.Drawing.Point(5, 254);
         this.label16.Name = "label16";
         this.label16.Size = new System.Drawing.Size(132, 34);
         this.label16.TabIndex = 32;
         this.label16.Text = "Photo Path";
         this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label22
         // 
         this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label22.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label22.ForeColor = System.Drawing.Color.Navy;
         this.label22.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label22.Location = new System.Drawing.Point(5, 326);
         this.label22.Name = "label22";
         this.label22.Size = new System.Drawing.Size(132, 34);
         this.label22.TabIndex = 34;
         this.label22.Text = "FID Path";
         this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label31
         // 
         this.label31.AutoSize = true;
         this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label31.Location = new System.Drawing.Point(678, 290);
         this.label31.Margin = new System.Windows.Forms.Padding(0);
         this.label31.Name = "label31";
         this.label31.Size = new System.Drawing.Size(36, 34);
         this.label31.TabIndex = 43;
         this.label31.Text = "...";
         this.label31.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label31.Click += new System.EventHandler(this.label31_Click);
         // 
         // label19
         // 
         this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label19.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label19.ForeColor = System.Drawing.Color.Navy;
         this.label19.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label19.Location = new System.Drawing.Point(5, 362);
         this.label19.Name = "label19";
         this.label19.Size = new System.Drawing.Size(132, 34);
         this.label19.TabIndex = 17;
         this.label19.Text = "Reports";
         this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label20
         // 
         this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label20.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label20.ForeColor = System.Drawing.Color.Navy;
         this.label20.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label20.Location = new System.Drawing.Point(5, 398);
         this.label20.Name = "label20";
         this.label20.Size = new System.Drawing.Size(132, 34);
         this.label20.TabIndex = 16;
         this.label20.Tag = "c";
         this.label20.Text = "Barangay Files";
         this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // SystemPath
         // 
         this.SystemPath.Dock = System.Windows.Forms.DockStyle.Fill;
         this.SystemPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.SystemPath.Location = new System.Drawing.Point(148, 220);
         this.SystemPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.SystemPath.Name = "SystemPath";
         this.SystemPath.Size = new System.Drawing.Size(525, 28);
         this.SystemPath.TabIndex = 13;
         this.SystemPath.Tag = "";
         // 
         // label28
         // 
         this.label28.AutoSize = true;
         this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label28.Location = new System.Drawing.Point(678, 218);
         this.label28.Margin = new System.Windows.Forms.Padding(0);
         this.label28.Name = "label28";
         this.label28.Size = new System.Drawing.Size(36, 34);
         this.label28.TabIndex = 40;
         this.label28.Text = "...";
         this.label28.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label28.Click += new System.EventHandler(this.label28_Click);
         // 
         // PhotoPath
         // 
         this.PhotoPath.Dock = System.Windows.Forms.DockStyle.Fill;
         this.PhotoPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.PhotoPath.Location = new System.Drawing.Point(148, 256);
         this.PhotoPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.PhotoPath.Name = "PhotoPath";
         this.PhotoPath.Size = new System.Drawing.Size(525, 28);
         this.PhotoPath.TabIndex = 15;
         this.PhotoPath.Tag = "";
         // 
         // ReportsPath
         // 
         this.ReportsPath.Dock = System.Windows.Forms.DockStyle.Fill;
         this.ReportsPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.ReportsPath.Location = new System.Drawing.Point(148, 364);
         this.ReportsPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.ReportsPath.Name = "ReportsPath";
         this.ReportsPath.Size = new System.Drawing.Size(525, 28);
         this.ReportsPath.TabIndex = 20;
         this.ReportsPath.Tag = "";
         // 
         // BrgyFilesPath
         // 
         this.BrgyFilesPath.Dock = System.Windows.Forms.DockStyle.Fill;
         this.BrgyFilesPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.BrgyFilesPath.Location = new System.Drawing.Point(148, 400);
         this.BrgyFilesPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.BrgyFilesPath.Name = "BrgyFilesPath";
         this.BrgyFilesPath.Size = new System.Drawing.Size(525, 28);
         this.BrgyFilesPath.TabIndex = 21;
         this.BrgyFilesPath.Tag = "";
         // 
         // SignaturePath
         // 
         this.SignaturePath.Dock = System.Windows.Forms.DockStyle.Fill;
         this.SignaturePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.SignaturePath.Location = new System.Drawing.Point(148, 292);
         this.SignaturePath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.SignaturePath.Name = "SignaturePath";
         this.SignaturePath.Size = new System.Drawing.Size(525, 28);
         this.SignaturePath.TabIndex = 18;
         this.SignaturePath.Tag = "";
         // 
         // label30
         // 
         this.label30.AutoSize = true;
         this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label30.Location = new System.Drawing.Point(678, 326);
         this.label30.Margin = new System.Windows.Forms.Padding(0);
         this.label30.Name = "label30";
         this.label30.Size = new System.Drawing.Size(36, 34);
         this.label30.TabIndex = 42;
         this.label30.Text = "...";
         this.label30.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label30.Click += new System.EventHandler(this.label30_Click);
         // 
         // FIDPath
         // 
         this.FIDPath.Dock = System.Windows.Forms.DockStyle.Fill;
         this.FIDPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.FIDPath.Location = new System.Drawing.Point(148, 328);
         this.FIDPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.FIDPath.Name = "FIDPath";
         this.FIDPath.Size = new System.Drawing.Size(525, 28);
         this.FIDPath.TabIndex = 19;
         this.FIDPath.Tag = "";
         // 
         // label34
         // 
         this.label34.AutoSize = true;
         this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label34.Location = new System.Drawing.Point(678, 398);
         this.label34.Margin = new System.Windows.Forms.Padding(0);
         this.label34.Name = "label34";
         this.label34.Size = new System.Drawing.Size(36, 34);
         this.label34.TabIndex = 48;
         this.label34.Text = "...";
         this.label34.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.label34.Click += new System.EventHandler(this.label34_Click);
         // 
         // tabPage3
         // 
         this.tabPage3.BackColor = System.Drawing.Color.Transparent;
         this.tabPage3.Controls.Add(this.tableLayoutPanel3);
         this.tabPage3.Location = new System.Drawing.Point(4, 5);
         this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tabPage3.Name = "tabPage3";
         this.tabPage3.Size = new System.Drawing.Size(722, 572);
         this.tabPage3.TabIndex = 2;
         this.tabPage3.Text = "tabPage3";
         this.tabPage3.ToolTipText = "Page 3";
         // 
         // tableLayoutPanel3
         // 
         this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
         this.tableLayoutPanel3.ColumnCount = 2;
         this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
         this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
         this.tableLayoutPanel3.Controls.Add(this.label41, 0, 9);
         this.tableLayoutPanel3.Controls.Add(this.label42, 0, 10);
         this.tableLayoutPanel3.Controls.Add(this.dbmain, 1, 7);
         this.tableLayoutPanel3.Controls.Add(this.dbstandard, 1, 8);
         this.tableLayoutPanel3.Controls.Add(this.dbbiometrics, 1, 9);
         this.tableLayoutPanel3.Controls.Add(this.dbsyslogs, 1, 10);
         this.tableLayoutPanel3.Controls.Add(this.label43, 0, 2);
         this.tableLayoutPanel3.Controls.Add(this.label44, 0, 4);
         this.tableLayoutPanel3.Controls.Add(this.db_password, 1, 3);
         this.tableLayoutPanel3.Controls.Add(this.label45, 0, 3);
         this.tableLayoutPanel3.Controls.Add(this.db_uid, 1, 2);
         this.tableLayoutPanel3.Controls.Add(this.label47, 0, 7);
         this.tableLayoutPanel3.Controls.Add(this.label48, 0, 8);
         this.tableLayoutPanel3.Controls.Add(this.label50, 0, 1);
         this.tableLayoutPanel3.Controls.Add(this.dbserver, 1, 1);
         this.tableLayoutPanel3.Controls.Add(this.label51, 0, 0);
         this.tableLayoutPanel3.Controls.Add(this.label27, 1, 6);
         this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tableLayoutPanel3.ImeMode = System.Windows.Forms.ImeMode.Disable;
         this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
         this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.tableLayoutPanel3.Name = "tableLayoutPanel3";
         this.tableLayoutPanel3.RowCount = 16;
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
         this.tableLayoutPanel3.Size = new System.Drawing.Size(722, 572);
         this.tableLayoutPanel3.TabIndex = 3;
         // 
         // label41
         // 
         this.label41.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label41.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label41.ForeColor = System.Drawing.Color.Navy;
         this.label41.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label41.Location = new System.Drawing.Point(5, 362);
         this.label41.Name = "label41";
         this.label41.Size = new System.Drawing.Size(137, 34);
         this.label41.TabIndex = 17;
         this.label41.Text = "Biometrics";
         this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label42
         // 
         this.label42.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label42.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label42.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label42.ForeColor = System.Drawing.Color.Navy;
         this.label42.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label42.Location = new System.Drawing.Point(5, 398);
         this.label42.Name = "label42";
         this.label42.Size = new System.Drawing.Size(137, 34);
         this.label42.TabIndex = 16;
         this.label42.Tag = "";
         this.label42.Text = "System Logs";
         this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // dbmain
         // 
         this.dbmain.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dbmain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.dbmain.Location = new System.Drawing.Point(150, 292);
         this.dbmain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.dbmain.Name = "dbmain";
         this.dbmain.Size = new System.Drawing.Size(567, 28);
         this.dbmain.TabIndex = 18;
         this.dbmain.Tag = "databases";
         // 
         // dbstandard
         // 
         this.dbstandard.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dbstandard.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.dbstandard.Location = new System.Drawing.Point(150, 328);
         this.dbstandard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.dbstandard.Name = "dbstandard";
         this.dbstandard.Size = new System.Drawing.Size(567, 28);
         this.dbstandard.TabIndex = 19;
         this.dbstandard.Tag = "databases";
         // 
         // dbbiometrics
         // 
         this.dbbiometrics.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dbbiometrics.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.dbbiometrics.Location = new System.Drawing.Point(150, 364);
         this.dbbiometrics.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.dbbiometrics.Name = "dbbiometrics";
         this.dbbiometrics.Size = new System.Drawing.Size(567, 28);
         this.dbbiometrics.TabIndex = 20;
         this.dbbiometrics.Tag = "databases";
         // 
         // dbsyslogs
         // 
         this.dbsyslogs.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dbsyslogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.dbsyslogs.Location = new System.Drawing.Point(150, 400);
         this.dbsyslogs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.dbsyslogs.Name = "dbsyslogs";
         this.dbsyslogs.Size = new System.Drawing.Size(567, 28);
         this.dbsyslogs.TabIndex = 21;
         this.dbsyslogs.Tag = "databases";
         // 
         // label43
         // 
         this.label43.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label43.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label43.ForeColor = System.Drawing.Color.Navy;
         this.label43.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label43.Location = new System.Drawing.Point(5, 74);
         this.label43.Name = "label43";
         this.label43.Size = new System.Drawing.Size(137, 34);
         this.label43.TabIndex = 1;
         this.label43.Text = "System User";
         this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label44
         // 
         this.tableLayoutPanel3.SetColumnSpan(this.label44, 2);
         this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label44.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label44.ForeColor = System.Drawing.Color.Navy;
         this.label44.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label44.Location = new System.Drawing.Point(5, 146);
         this.label44.Name = "label44";
         this.label44.Size = new System.Drawing.Size(712, 34);
         this.label44.TabIndex = 3;
         this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // db_password
         // 
         this.db_password.Dock = System.Windows.Forms.DockStyle.Fill;
         this.db_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.db_password.Location = new System.Drawing.Point(150, 112);
         this.db_password.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.db_password.Name = "db_password";
         this.db_password.Size = new System.Drawing.Size(567, 28);
         this.db_password.TabIndex = 29;
         this.db_password.Tag = "encrypted";
         this.db_password.UseSystemPasswordChar = true;
         // 
         // label45
         // 
         this.label45.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label45.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label45.ForeColor = System.Drawing.Color.Navy;
         this.label45.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label45.Location = new System.Drawing.Point(5, 110);
         this.label45.Name = "label45";
         this.label45.Size = new System.Drawing.Size(137, 34);
         this.label45.TabIndex = 2;
         this.label45.Text = "Password";
         this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // db_uid
         // 
         this.db_uid.Dock = System.Windows.Forms.DockStyle.Fill;
         this.db_uid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.db_uid.Location = new System.Drawing.Point(150, 76);
         this.db_uid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.db_uid.Name = "db_uid";
         this.db_uid.Size = new System.Drawing.Size(567, 28);
         this.db_uid.TabIndex = 28;
         this.db_uid.Tag = "encrypted";
         // 
         // label47
         // 
         this.label47.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label47.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label47.ForeColor = System.Drawing.Color.Navy;
         this.label47.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label47.Location = new System.Drawing.Point(5, 290);
         this.label47.Name = "label47";
         this.label47.Size = new System.Drawing.Size(137, 34);
         this.label47.TabIndex = 33;
         this.label47.Text = "Main Database";
         this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label48
         // 
         this.label48.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label48.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label48.ForeColor = System.Drawing.Color.Navy;
         this.label48.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label48.Location = new System.Drawing.Point(5, 326);
         this.label48.Name = "label48";
         this.label48.Size = new System.Drawing.Size(137, 34);
         this.label48.TabIndex = 34;
         this.label48.Text = "Standard Data";
         this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label50
         // 
         this.label50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label50.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label50.ForeColor = System.Drawing.Color.Navy;
         this.label50.ImageAlign = System.Drawing.ContentAlignment.TopRight;
         this.label50.Location = new System.Drawing.Point(5, 38);
         this.label50.Name = "label50";
         this.label50.Size = new System.Drawing.Size(135, 34);
         this.label50.TabIndex = 35;
         this.label50.Tag = "c";
         this.label50.Text = "Server Address";
         this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // dbserver
         // 
         this.dbserver.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dbserver.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.dbserver.Location = new System.Drawing.Point(150, 40);
         this.dbserver.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.dbserver.Name = "dbserver";
         this.dbserver.Size = new System.Drawing.Size(567, 28);
         this.dbserver.TabIndex = 36;
         this.dbserver.Tag = "encrypted";
         // 
         // label51
         // 
         this.label51.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label51.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel3.SetColumnSpan(this.label51, 2);
         this.label51.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label51.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label51.ForeColor = System.Drawing.Color.MediumBlue;
         this.label51.Image = ((System.Drawing.Image)(resources.GetObject("label51.Image")));
         this.label51.Location = new System.Drawing.Point(5, 2);
         this.label51.Name = "label51";
         this.label51.Size = new System.Drawing.Size(712, 34);
         this.label51.TabIndex = 22;
         this.label51.Text = "MySQL CONFIGURATION";
         this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label27
         // 
         this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label27.BackColor = System.Drawing.Color.Transparent;
         this.tableLayoutPanel3.SetColumnSpan(this.label27, 2);
         this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label27.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label27.ForeColor = System.Drawing.Color.MediumBlue;
         this.label27.Image = ((System.Drawing.Image)(resources.GetObject("label27.Image")));
         this.label27.Location = new System.Drawing.Point(5, 254);
         this.label27.Name = "label27";
         this.label27.Size = new System.Drawing.Size(712, 34);
         this.label27.TabIndex = 45;
         this.label27.Text = "DATABASES";
         this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // textBox13
         // 
         this.textBox13.Dock = System.Windows.Forms.DockStyle.Fill;
         this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.textBox13.Location = new System.Drawing.Point(5, 79);
         this.textBox13.Name = "textBox13";
         this.textBox13.Size = new System.Drawing.Size(455, 28);
         this.textBox13.TabIndex = 27;
         // 
         // panel1
         // 
         this.panel1.BackColor = System.Drawing.Color.Transparent;
         this.panel1.Controls.Add(this.button1);
         this.panel1.Controls.Add(this.button2);
         this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.panel1.Location = new System.Drawing.Point(11, 660);
         this.panel1.Margin = new System.Windows.Forms.Padding(4);
         this.panel1.Name = "panel1";
         this.panel1.Padding = new System.Windows.Forms.Padding(10);
         this.panel1.Size = new System.Drawing.Size(730, 62);
         this.panel1.TabIndex = 1;
         // 
         // button1
         // 
         this.button1.Dock = System.Windows.Forms.DockStyle.Right;
         this.button1.Location = new System.Drawing.Point(474, 10);
         this.button1.Margin = new System.Windows.Forms.Padding(4);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(123, 42);
         this.button1.TabIndex = 2;
         this.button1.Text = "Save";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.Button1_Click);
         // 
         // button2
         // 
         this.button2.Dock = System.Windows.Forms.DockStyle.Right;
         this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
         this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
         this.button2.Location = new System.Drawing.Point(597, 10);
         this.button2.Margin = new System.Windows.Forms.Padding(10);
         this.button2.Name = "button2";
         this.button2.Size = new System.Drawing.Size(123, 42);
         this.button2.TabIndex = 1;
         this.button2.Text = "Cancel";
         this.button2.UseVisualStyleBackColor = true;
         this.button2.Click += new System.EventHandler(this.Button2_Click);
         // 
         // Form_Settings
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
         this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
         this.ClientSize = new System.Drawing.Size(752, 732);
         this.Controls.Add(this.panel1);
         this.Controls.Add(this.splitContainer1);
         this.DoubleBuffered = true;
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.Name = "Form_Settings";
         this.Padding = new System.Windows.Forms.Padding(11, 10, 11, 10);
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Configuration";
         this.splitContainer1.Panel1.ResumeLayout(false);
         this.splitContainer1.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
         this.splitContainer1.ResumeLayout(false);
         this.tabControl1.ResumeLayout(false);
         this.tabPage1.ResumeLayout(false);
         this.tableLayoutPanel1.ResumeLayout(false);
         this.tableLayoutPanel1.PerformLayout();
         this.tabPage2.ResumeLayout(false);
         this.tableLayoutPanel2.ResumeLayout(false);
         this.tableLayoutPanel2.PerformLayout();
         this.tabPage3.ResumeLayout(false);
         this.tableLayoutPanel3.ResumeLayout(false);
         this.tableLayoutPanel3.PerformLayout();
         this.panel1.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.TabPage tabPage3;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.ComboBox Region;
      private System.Windows.Forms.ComboBox Province;
      private System.Windows.Forms.ComboBox City;
      private System.Windows.Forms.ComboBox Barangay;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox brgyname;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.TextBox brgyaddress1;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.TextBox brgyaddress2;
      private System.Windows.Forms.TextBox brgyaddress3;
      private System.Windows.Forms.TextBox brgycontactno;
      private System.Windows.Forms.TextBox brgyemailad;
      private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
      private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
      private System.Windows.Forms.Label label41;
      private System.Windows.Forms.Label label42;
      private System.Windows.Forms.TextBox dbmain;
      private System.Windows.Forms.TextBox dbstandard;
      private System.Windows.Forms.TextBox dbbiometrics;
      private System.Windows.Forms.TextBox dbsyslogs;
      private System.Windows.Forms.Label label43;
      private System.Windows.Forms.Label label44;
      private System.Windows.Forms.TextBox db_password;
      private System.Windows.Forms.Label label45;
      private System.Windows.Forms.TextBox db_uid;
      private System.Windows.Forms.Label label47;
      private System.Windows.Forms.Label label48;
      private System.Windows.Forms.Label label50;
      private System.Windows.Forms.TextBox dbserver;
      private System.Windows.Forms.Label label51;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label PreviousTab;
      private System.Windows.Forms.Label NextTab;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.TabPage tabPage2;
      private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
      private System.Windows.Forms.Label label32;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.TextBox CityLogo;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.TextBox BrgyLogo;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.TextBox AppIcon;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.TextBox AppCaption;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.Label label52;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.TextBox SystemPath;
      private System.Windows.Forms.Label label28;
      private System.Windows.Forms.TextBox PhotoPath;
      private System.Windows.Forms.TextBox ReportsPath;
      private System.Windows.Forms.TextBox BrgyFilesPath;
      private System.Windows.Forms.TextBox SignaturePath;
      private System.Windows.Forms.Label label30;
      private System.Windows.Forms.TextBox FIDPath;
      private System.Windows.Forms.Label label34;
   }
}