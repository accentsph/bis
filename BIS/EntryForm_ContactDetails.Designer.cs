﻿namespace BIS
{
    partial class EntryForm_ContactDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         this.panel1 = new System.Windows.Forms.Panel();
         this.panel4 = new System.Windows.Forms.Panel();
         this.okLabel = new System.Windows.Forms.Label();
         this.contact = new System.Windows.Forms.TextBox();
         this.countryCode = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.panelSMSGroup = new System.Windows.Forms.Panel();
         this.smsGroup = new System.Windows.Forms.ComboBox();
         this.label2 = new System.Windows.Forms.Label();
         this.panel2 = new System.Windows.Forms.Panel();
         this.contactType = new System.Windows.Forms.ComboBox();
         this.label1 = new System.Windows.Forms.Label();
         this.bNext = new System.Windows.Forms.Button();
         this.button1 = new System.Windows.Forms.Button();
         this.panel1.SuspendLayout();
         this.panel4.SuspendLayout();
         this.panelSMSGroup.SuspendLayout();
         this.panel2.SuspendLayout();
         this.SuspendLayout();
         // 
         // panel1
         // 
         this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel1.Controls.Add(this.panel4);
         this.panel1.Controls.Add(this.panelSMSGroup);
         this.panel1.Controls.Add(this.panel2);
         this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel1.Location = new System.Drawing.Point(0, 0);
         this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.panel1.Name = "panel1";
         this.panel1.Padding = new System.Windows.Forms.Padding(20);
         this.panel1.Size = new System.Drawing.Size(451, 121);
         this.panel1.TabIndex = 0;
         // 
         // panel4
         // 
         this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel4.Controls.Add(this.okLabel);
         this.panel4.Controls.Add(this.contact);
         this.panel4.Controls.Add(this.countryCode);
         this.panel4.Controls.Add(this.label3);
         this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel4.Location = new System.Drawing.Point(20, 78);
         this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.panel4.Name = "panel4";
         this.panel4.Size = new System.Drawing.Size(407, 29);
         this.panel4.TabIndex = 2;
         // 
         // okLabel
         // 
         this.okLabel.Dock = System.Windows.Forms.DockStyle.Fill;
         this.okLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.okLabel.ForeColor = System.Drawing.Color.Red;
         this.okLabel.Location = new System.Drawing.Point(363, 0);
         this.okLabel.Name = "okLabel";
         this.okLabel.Size = new System.Drawing.Size(40, 25);
         this.okLabel.TabIndex = 4;
         this.okLabel.Text = "!";
         this.okLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // contact
         // 
         this.contact.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.contact.Dock = System.Windows.Forms.DockStyle.Left;
         this.contact.Location = new System.Drawing.Point(174, 0);
         this.contact.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.contact.MinimumSize = new System.Drawing.Size(0, 24);
         this.contact.Name = "contact";
         this.contact.Size = new System.Drawing.Size(189, 15);
         this.contact.TabIndex = 3;
         this.contact.TextChanged += new System.EventHandler(this.contact_TextChanged);
         // 
         // countryCode
         // 
         this.countryCode.Dock = System.Windows.Forms.DockStyle.Left;
         this.countryCode.Location = new System.Drawing.Point(123, 0);
         this.countryCode.Name = "countryCode";
         this.countryCode.Size = new System.Drawing.Size(51, 25);
         this.countryCode.TabIndex = 1;
         this.countryCode.Text = "+63";
         this.countryCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.countryCode.Visible = false;
         // 
         // label3
         // 
         this.label3.BackColor = System.Drawing.Color.RoyalBlue;
         this.label3.Dock = System.Windows.Forms.DockStyle.Left;
         this.label3.ForeColor = System.Drawing.Color.White;
         this.label3.Location = new System.Drawing.Point(0, 0);
         this.label3.Name = "label3";
         this.label3.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
         this.label3.Size = new System.Drawing.Size(123, 25);
         this.label3.TabIndex = 0;
         this.label3.Text = "Contact";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelSMSGroup
         // 
         this.panelSMSGroup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelSMSGroup.Controls.Add(this.smsGroup);
         this.panelSMSGroup.Controls.Add(this.label2);
         this.panelSMSGroup.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelSMSGroup.Location = new System.Drawing.Point(20, 49);
         this.panelSMSGroup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.panelSMSGroup.Name = "panelSMSGroup";
         this.panelSMSGroup.Size = new System.Drawing.Size(407, 29);
         this.panelSMSGroup.TabIndex = 1;
         this.panelSMSGroup.Visible = false;
         // 
         // smsGroup
         // 
         this.smsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
         this.smsGroup.FormattingEnabled = true;
         this.smsGroup.Location = new System.Drawing.Point(123, 0);
         this.smsGroup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.smsGroup.Name = "smsGroup";
         this.smsGroup.Size = new System.Drawing.Size(280, 24);
         this.smsGroup.TabIndex = 2;
         this.smsGroup.Enter += new System.EventHandler(this.ContactType_Enter);
         this.smsGroup.Leave += new System.EventHandler(this.SmsGroup_Leave);
         // 
         // label2
         // 
         this.label2.BackColor = System.Drawing.Color.RoyalBlue;
         this.label2.Dock = System.Windows.Forms.DockStyle.Left;
         this.label2.ForeColor = System.Drawing.Color.White;
         this.label2.Location = new System.Drawing.Point(0, 0);
         this.label2.Name = "label2";
         this.label2.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
         this.label2.Size = new System.Drawing.Size(123, 25);
         this.label2.TabIndex = 0;
         this.label2.Text = "SMS Group";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel2
         // 
         this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel2.Controls.Add(this.contactType);
         this.panel2.Controls.Add(this.label1);
         this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel2.Location = new System.Drawing.Point(20, 20);
         this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.panel2.Name = "panel2";
         this.panel2.Size = new System.Drawing.Size(407, 29);
         this.panel2.TabIndex = 0;
         // 
         // contactType
         // 
         this.contactType.Dock = System.Windows.Forms.DockStyle.Fill;
         this.contactType.FormattingEnabled = true;
         this.contactType.Location = new System.Drawing.Point(123, 0);
         this.contactType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.contactType.Name = "contactType";
         this.contactType.Size = new System.Drawing.Size(280, 24);
         this.contactType.TabIndex = 1;
         this.contactType.SelectedIndexChanged += new System.EventHandler(this.contactType_SelectedValueChanged);
         this.contactType.SelectedValueChanged += new System.EventHandler(this.contactType_SelectedValueChanged);
         this.contactType.Enter += new System.EventHandler(this.ContactType_Enter);
         this.contactType.Leave += new System.EventHandler(this.SmsGroup_Leave);
         // 
         // label1
         // 
         this.label1.BackColor = System.Drawing.Color.RoyalBlue;
         this.label1.Dock = System.Windows.Forms.DockStyle.Left;
         this.label1.ForeColor = System.Drawing.Color.White;
         this.label1.Location = new System.Drawing.Point(0, 0);
         this.label1.Name = "label1";
         this.label1.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
         this.label1.Size = new System.Drawing.Size(123, 25);
         this.label1.TabIndex = 0;
         this.label1.Text = "Contact Type";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // bNext
         // 
         this.bNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bNext.FlatAppearance.BorderSize = 0;
         this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bNext.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bNext.ForeColor = System.Drawing.Color.White;
         this.bNext.Location = new System.Drawing.Point(212, 135);
         this.bNext.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bNext.Name = "bNext";
         this.bNext.Size = new System.Drawing.Size(103, 33);
         this.bNext.TabIndex = 41;
         this.bNext.Text = "Add";
         this.bNext.UseVisualStyleBackColor = false;
         this.bNext.Click += new System.EventHandler(this.bNext_Click);
         // 
         // button1
         // 
         this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.button1.FlatAppearance.BorderSize = 0;
         this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.button1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.button1.ForeColor = System.Drawing.Color.White;
         this.button1.Location = new System.Drawing.Point(323, 135);
         this.button1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(103, 33);
         this.button1.TabIndex = 42;
         this.button1.Text = "Cancel";
         this.button1.UseVisualStyleBackColor = false;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // EntryForm_ContactDetails
         // 
         this.AcceptButton = this.bNext;
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.White;
         this.CancelButton = this.button1;
         this.ClientSize = new System.Drawing.Size(451, 183);
         this.ControlBox = false;
         this.Controls.Add(this.button1);
         this.Controls.Add(this.bNext);
         this.Controls.Add(this.panel1);
         this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.Name = "EntryForm_ContactDetails";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
         this.Text = "New Contact";
         this.panel1.ResumeLayout(false);
         this.panel4.ResumeLayout(false);
         this.panel4.PerformLayout();
         this.panelSMSGroup.ResumeLayout(false);
         this.panel2.ResumeLayout(false);
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox contact;
        private System.Windows.Forms.Label countryCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelSMSGroup;
        private System.Windows.Forms.ComboBox smsGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox contactType;
        private System.Windows.Forms.Button bNext;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label okLabel;
    }
}