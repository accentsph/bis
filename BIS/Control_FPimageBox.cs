﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Control_FPimageBox : UserControl
    {
        private Fingerprint fingerprint;
        public Fingerprint Fingerprint
        {
            set
            {
                fingerprint = value;
                fingerName.Text = value.FingerName;
                fpImageBox.Image = value.Image;
            }
        }
        public Control_FPimageBox()
        {
            InitializeComponent();
        }

        private void Control_FPimageBox_Resize(object sender, EventArgs e)
        {
            this.Width = Convert.ToInt32(this.Height * 0.8);
            fpImageBox.Size = new Size(this.Width, this.Width);
            fingerName.Font = new Font(FontFamily.GenericSansSerif, Convert.ToInt32(this.Width * 0.05));
        }
    }
}
