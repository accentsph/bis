﻿using DPUruNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Test : Form
    {
        public Test()
        {
            InitializeComponent();
        }

        private void Test_Load(object sender, EventArgs e)
        {
            ProcessAddressComboData();
        }

        private void ProcessAddressComboData()
        {
            MySQLDatabase db;
            List<SQLParameter> paramss = new List<SQLParameter>();
            DataTable dt = new DataTable();
            db = DatabaseManager.GetInstance();
            db.CloseConnection();
            object brgy = db.ScalarSelect("SELECT defaultbrgyid()");
            db.CloseConnection();
            if (brgy != null)
            {
                try
                {
                    int defaultid = Convert.ToInt32(brgy.ToString());
                    FormManager.SetComboBoxDataSource(BrgyID, "select b.id AS id,b.brgyDesc AS Barangay from dbstandarddata.refbrgy b where (b.id = " + defaultid.ToString() + ")", "id", "Barangay");
                    FormManager.SetComboBoxDataSource(CityID, "select c.id AS id,c.citymunDesc AS City from dbstandarddata.refcitymun c join dbstandarddata.refbrgy b on c.id = b.cityID where b.id = " + defaultid.ToString(), "id", "City");
                    FormManager.SetComboBoxDataSource(ProvID, "select p.id AS id,p.provDesc AS Province from dbstandarddata.refprovince p join dbstandarddata.refcitymun c on p.id = c.provid join dbstandarddata.refbrgy b on c.id = b.cityID where b.id = " + defaultid.ToString(), "id", "Province");

                }
                catch (Exception e)
                {
                    db.CloseConnection();
                    MessageBox.Show(e.Message);

                    throw;
                }
            }
        }
    }
}
