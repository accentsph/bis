﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIS
{
    class Barangay
    {
        private int barangayID;
        private int cityID;
        private int provinceID;
        private int regionID;

        public Barangay(int barangayID, int cityID, int provinceID, int regionID)
        {
            this.barangayID = barangayID;
            this.cityID = cityID;
            this.provinceID = provinceID;
            this.regionID = regionID;
        }

        public int BarangayID
        {
            get { return barangayID; }
            set { barangayID = value; }
        }

        public int CityID
        {
            get { return cityID; }
            set { cityID = value; }
        }

        public int ProvinceID
        {
            get { return provinceID; }
            set { provinceID = value; }
        }

        public int RegionID
        {
            get { return regionID; }
            set { regionID = value; }
        }
    }
}
