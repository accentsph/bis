﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Form_SMS : Form
    {
        public Form_SMS()
        {
            InitializeComponent();
        }

        private void HighlightMenu(ToolStripItem menuitem)
        {
            menuitem.BackColor = Color.FromArgb(231, 233, 240);
            foreach (ToolStripMenuItem c in menuStrip1.Items)
            {
                if (c != menuitem)
                {
                    c.BackColor = Color.White;
                }
            }
        }

        private void MenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            HighlightMenu(e.ClickedItem);
        }

        private void GroupSMSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Control_GroupSMS cgsms = new Control_GroupSMS();
            cgsms.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(cgsms);
        }
    }
}
