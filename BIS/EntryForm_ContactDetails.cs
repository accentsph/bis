﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class EntryForm_ContactDetails : Form
    {
        MySQLDatabase db;
        private ListViewItem item;
        private ListView lv;
        public EntryForm_ContactDetails(ListView lv, ListViewItem item = null)
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
            this.lv = lv;
            this.item = item;
            IDataReader reader = db.Select("SELECT * FROM vwcontacttypes");
            FormManager.SetComboBoxDataSource(contactType, reader);
            reader = db.Select("SELECT * FROM vwsmsgroups");
            FormManager.SetComboBoxDataSource(smsGroup, reader);
            contactType.SelectedIndex = -1;
            if(item != null)
            {
                bNext.Text = "Update";
                contactType.SelectedValue = item.SubItems[0].Tag;
                contact.Text = contactType.Text == "Mobile" ? item.SubItems[1].Text.Remove(0, 3) : item.SubItems[1].Text;
                 if(item.SubItems[2].Tag != null)
                  {
                     smsGroup.SelectedValue = item.SubItems[2].Tag;
                  }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void bNext_Click(object sender, EventArgs e)
        {
            if (!isValidFormat())
            {
                MessageBox.Show("Invalid contact format.");
                return;
            }
            string ctype = contactType.Text == "Mobile" ? countryCode.Text + contact.Text : contact.Text;
            string[] lvitems = { contactType.Text, ctype, ((panelSMSGroup.Visible) ? smsGroup.Text : null) };
            if (item == null)
            {
                ListViewItem lvi = new ListViewItem(lvitems);
                lvi.SubItems[0].Tag = contactType.SelectedValue;
                lvi.SubItems[1].Tag = null;
                if(panelSMSGroup.Visible)
                {
                    lvi.SubItems[2].Tag = smsGroup.SelectedValue;
                }
                lvi.Tag = "new";
                lv.Items.Add(lvi);
            }
            else
            {
                item.SubItems[0].Text = contactType.Text;
                item.SubItems[1].Text = ctype;
                item.SubItems[2].Text = panelSMSGroup.Visible ? contactType.Text : null;
                item.SubItems[0].Tag = contactType.SelectedValue;
                item.SubItems[1].Tag = null;
                if (panelSMSGroup.Visible)
                {
                    item.SubItems[2].Tag = smsGroup.SelectedValue;
                }
            }
            this.DialogResult = DialogResult.OK;
            Close();
        }

        public bool isValidFormat()
        {
            switch (contactType.Text)
            {
                case "Mobile":
                    return FormatManager.isValidMobileNumber(countryCode.Text + contact.Text);
                case "eMail":
                    return FormatManager.isValidEmailAddress(contact.Text);
            }
            return true;
        }

        private void contactType_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            switch(cb.Text)
            {
                case "Mobile":
                    panelSMSGroup.Visible = true;
                    countryCode.Visible = true;
                    countryCode.Text = "+63";
                    break;
                case "Telephone":
                    panelSMSGroup.Visible = false;
                    countryCode.Visible = false;
                    countryCode.Text = "802";
                    break;
                default:
                    panelSMSGroup.Visible = false;
                    countryCode.Visible = false;
                    break;
            }
        }

        private void contact_TextChanged(object sender, EventArgs e)
        {
            if(isValidFormat())
            {
                okLabel.Text = "ok";
                okLabel.ForeColor = Color.Green;
            }
            else
            {
                okLabel.Text = "!";
                okLabel.ForeColor = Color.Red;
            }
        }

        private void ContactType_Enter(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            if (cb != null)
            {
                cb.DropDownStyle = ComboBoxStyle.DropDownList;
            }
        }

        private void SmsGroup_Leave(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            if (cb != null)
            {
                cb.DropDownStyle = ComboBoxStyle.DropDown;
            }
        }
    }
}
