﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Control_StandardControls : UserControl
    {
        private MySQLDatabase db;
        private DatabaseTableView view;
        private ListView listView;
        public DatabaseTableView View
        {
            set { view = value; }
        }
        public ListView ListView
        {
            set { listView = value; }
        }
        public ToolStripMenuItem ButtonNew
        {
            get { return bNew; }
        }
        public ToolStripMenuItem ButtonEdit
        {
            get { return bEdit; }
        }
        public ToolStripMenuItem ButtonPrint
        {
            get { return ButtonPrint; }
        }
        public ToolStripMenuItem ButtonDelete
        {
            get { return bDelete; }
        }
        public ToolStripMenuItem ButtonPrint_Printer
        {
            get { return bPrint_printer; }
        }
        public ToolStripMenuItem ButtonPrint_Screen
        {
            get { return bPrint_screen; }
        }
        public ToolStripMenuItem ButtonPrint_PDF
        {
            get { return bPrint_PDF; }
        }
        public TextBox SearchBox
        {
            get { return searchBox; }
        }
        public DateTimePicker DateStart
        {
            get { return dateBeg; }
        }
        public DateTimePicker DateEnd
        {
            get { return dateEnd; }
        }
        public Control_StandardControls()
        {
            InitializeComponent();
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                db = DatabaseManager.GetInstance();
                List<SQLParameter> prms = new List<SQLParameter>();
                prms.Add(new SQLParameter("search", searchBox.Text, "LIKE", view.SearchColumn));
                IDataReader reader = db.Select("SELECT * FROM " + view.Name, prms);
                ListViewManager.populateListView(listView, reader, false);
            }
        }
    }
}
