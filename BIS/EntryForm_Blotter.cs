﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
   public partial class EntryForm_Blotter : Form
   {
        string id;
      public EntryForm_Blotter(string id)
      {
         InitializeComponent();
            this.id = id;
            if (id != null)
            {
                MessageBox.Show("FOR EDIT!");
            }
      }

      public EntryForm_Blotter()
      {
            InitializeComponent();
      }

      private void panel2_Paint(object sender, PaintEventArgs e)
      {

      }

      private void richTextBox1_TextChanged(object sender, EventArgs e)
      {

      }

      private void CbSearchClients_SelectedIndexChanged(object sender, EventArgs e)
      {

      }

      private void DgvComplainant_Enter(object sender, EventArgs e)
      {
         DataGridView dgv = sender as DataGridView;
         Panel mpanel = dgv.Parent as Panel;
         mpanel.Controls.Add(CbSearchClients);
         CbSearchClients.Visible = true;

      }

      private void bClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void label1_Click(object sender, EventArgs e)
      {
         EntryForm_Client frm = new EntryForm_Client();
         frm.ShowDialog();
         frm.Dispose();
      }
   }
}
