﻿namespace BIS
{
    partial class Control_ResidentsSubForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientPhoto = new System.Windows.Forms.PictureBox();
            this.clientname = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.clientPhoto)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // clientPhoto
            // 
            this.clientPhoto.BackColor = System.Drawing.Color.White;
            this.clientPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.clientPhoto.Dock = System.Windows.Forms.DockStyle.Top;
            this.clientPhoto.Location = new System.Drawing.Point(0, 0);
            this.clientPhoto.Name = "clientPhoto";
            this.clientPhoto.Size = new System.Drawing.Size(200, 200);
            this.clientPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.clientPhoto.TabIndex = 0;
            this.clientPhoto.TabStop = false;
            // 
            // clientname
            // 
            this.clientname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientname.ForeColor = System.Drawing.Color.White;
            this.clientname.Location = new System.Drawing.Point(0, 200);
            this.clientname.Name = "clientname";
            this.clientname.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.clientname.Size = new System.Drawing.Size(200, 80);
            this.clientname.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.clientname);
            this.panel1.Controls.Add(this.clientPhoto);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 280);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(210, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(480, 280);
            this.panel2.TabIndex = 3;
            // 
            // Control_ResidentsSubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Control_ResidentsSubForm";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(700, 300);
            ((System.ComponentModel.ISupportInitialize)(this.clientPhoto)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox clientPhoto;
        private System.Windows.Forms.Label clientname;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}
