﻿namespace BIS
{
    partial class Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrgyID = new System.Windows.Forms.ComboBox();
            this.CityID = new System.Windows.Forms.ComboBox();
            this.ProvID = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // BrgyID
            // 
            this.BrgyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BrgyID.Enabled = false;
            this.BrgyID.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BrgyID.FormattingEnabled = true;
            this.BrgyID.Location = new System.Drawing.Point(85, 38);
            this.BrgyID.Name = "BrgyID";
            this.BrgyID.Size = new System.Drawing.Size(121, 21);
            this.BrgyID.TabIndex = 0;
            // 
            // CityID
            // 
            this.CityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CityID.Enabled = false;
            this.CityID.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CityID.FormattingEnabled = true;
            this.CityID.Location = new System.Drawing.Point(85, 65);
            this.CityID.Name = "CityID";
            this.CityID.Size = new System.Drawing.Size(121, 21);
            this.CityID.TabIndex = 1;
            // 
            // ProvID
            // 
            this.ProvID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProvID.Enabled = false;
            this.ProvID.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ProvID.FormattingEnabled = true;
            this.ProvID.Location = new System.Drawing.Point(85, 92);
            this.ProvID.Name = "ProvID";
            this.ProvID.Size = new System.Drawing.Size(121, 21);
            this.ProvID.TabIndex = 2;
            // 
            // Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ProvID);
            this.Controls.Add(this.CityID);
            this.Controls.Add(this.BrgyID);
            this.Name = "Test";
            this.Text = "Test";
            this.Load += new System.EventHandler(this.Test_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox BrgyID;
        private System.Windows.Forms.ComboBox CityID;
        private System.Windows.Forms.ComboBox ProvID;
    }
}