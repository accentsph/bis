﻿namespace BIS
{
    partial class Control_StandardControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Control_StandardControls));
         this.buttonsMenu = new System.Windows.Forms.MenuStrip();
         this.bNew = new System.Windows.Forms.ToolStripMenuItem();
         this.bEdit = new System.Windows.Forms.ToolStripMenuItem();
         this.bDelete = new System.Windows.Forms.ToolStripMenuItem();
         this.bPrint = new System.Windows.Forms.ToolStripMenuItem();
         this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
         this.bPrint_printer = new System.Windows.Forms.ToolStripMenuItem();
         this.bPrint_screen = new System.Windows.Forms.ToolStripMenuItem();
         this.bPrint_PDF = new System.Windows.Forms.ToolStripMenuItem();
         this.panelSES = new System.Windows.Forms.Panel();
         this.panelEndDate = new System.Windows.Forms.Panel();
         this.dateEnd = new System.Windows.Forms.DateTimePicker();
         this.label5 = new System.Windows.Forms.Label();
         this.panelStartDate = new System.Windows.Forms.Panel();
         this.dateBeg = new System.Windows.Forms.DateTimePicker();
         this.label3 = new System.Windows.Forms.Label();
         this.panelSearch = new System.Windows.Forms.Panel();
         this.searchBox = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.customInstaller1 = new MySql.Data.MySqlClient.CustomInstaller();
         this.buttonsMenu.SuspendLayout();
         this.panelSES.SuspendLayout();
         this.panelEndDate.SuspendLayout();
         this.panelStartDate.SuspendLayout();
         this.panelSearch.SuspendLayout();
         this.SuspendLayout();
         // 
         // buttonsMenu
         // 
         this.buttonsMenu.AutoSize = false;
         this.buttonsMenu.BackColor = System.Drawing.Color.Transparent;
         this.buttonsMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
         this.buttonsMenu.ImageScalingSize = new System.Drawing.Size(25, 25);
         this.buttonsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bNew,
            this.bEdit,
            this.bDelete,
            this.bPrint});
         this.buttonsMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
         this.buttonsMenu.Location = new System.Drawing.Point(0, 218);
         this.buttonsMenu.Name = "buttonsMenu";
         this.buttonsMenu.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
         this.buttonsMenu.ShowItemToolTips = true;
         this.buttonsMenu.Size = new System.Drawing.Size(227, 254);
         this.buttonsMenu.TabIndex = 4;
         this.buttonsMenu.Text = "menuStrip3";
         // 
         // bNew
         // 
         this.bNew.AutoSize = false;
         this.bNew.ForeColor = System.Drawing.Color.SteelBlue;
         this.bNew.Image = ((System.Drawing.Image)(resources.GetObject("bNew.Image")));
         this.bNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bNew.Name = "bNew";
         this.bNew.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.bNew.Size = new System.Drawing.Size(170, 45);
         this.bNew.Text = "New";
         this.bNew.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // bEdit
         // 
         this.bEdit.AutoSize = false;
         this.bEdit.ForeColor = System.Drawing.Color.SteelBlue;
         this.bEdit.Image = ((System.Drawing.Image)(resources.GetObject("bEdit.Image")));
         this.bEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bEdit.Name = "bEdit";
         this.bEdit.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.bEdit.Size = new System.Drawing.Size(170, 45);
         this.bEdit.Text = "Edit";
         this.bEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // bDelete
         // 
         this.bDelete.AutoSize = false;
         this.bDelete.ForeColor = System.Drawing.Color.SteelBlue;
         this.bDelete.Image = ((System.Drawing.Image)(resources.GetObject("bDelete.Image")));
         this.bDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bDelete.Name = "bDelete";
         this.bDelete.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.bDelete.Size = new System.Drawing.Size(170, 45);
         this.bDelete.Text = "Delete";
         this.bDelete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // bPrint
         // 
         this.bPrint.AutoSize = false;
         this.bPrint.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.bPrint_printer,
            this.bPrint_screen,
            this.bPrint_PDF});
         this.bPrint.ForeColor = System.Drawing.Color.SteelBlue;
         this.bPrint.Image = ((System.Drawing.Image)(resources.GetObject("bPrint.Image")));
         this.bPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bPrint.Name = "bPrint";
         this.bPrint.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.bPrint.Size = new System.Drawing.Size(170, 45);
         this.bPrint.Text = "Print";
         this.bPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // toolStripSeparator2
         // 
         this.toolStripSeparator2.Name = "toolStripSeparator2";
         this.toolStripSeparator2.Size = new System.Drawing.Size(125, 6);
         // 
         // bPrint_printer
         // 
         this.bPrint_printer.Name = "bPrint_printer";
         this.bPrint_printer.Size = new System.Drawing.Size(128, 26);
         this.bPrint_printer.Text = "Printer";
         // 
         // bPrint_screen
         // 
         this.bPrint_screen.Name = "bPrint_screen";
         this.bPrint_screen.Size = new System.Drawing.Size(128, 26);
         this.bPrint_screen.Text = "Screen";
         // 
         // bPrint_PDF
         // 
         this.bPrint_PDF.Name = "bPrint_PDF";
         this.bPrint_PDF.Size = new System.Drawing.Size(128, 26);
         this.bPrint_PDF.Text = "PDF";
         // 
         // panelSES
         // 
         this.panelSES.BackColor = System.Drawing.Color.White;
         this.panelSES.Controls.Add(this.panelEndDate);
         this.panelSES.Controls.Add(this.panelStartDate);
         this.panelSES.Controls.Add(this.panelSearch);
         this.panelSES.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelSES.Location = new System.Drawing.Point(0, 0);
         this.panelSES.Margin = new System.Windows.Forms.Padding(4);
         this.panelSES.Name = "panelSES";
         this.panelSES.Size = new System.Drawing.Size(227, 218);
         this.panelSES.TabIndex = 5;
         // 
         // panelEndDate
         // 
         this.panelEndDate.Controls.Add(this.dateEnd);
         this.panelEndDate.Controls.Add(this.label5);
         this.panelEndDate.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelEndDate.Location = new System.Drawing.Point(0, 144);
         this.panelEndDate.Margin = new System.Windows.Forms.Padding(4);
         this.panelEndDate.MaximumSize = new System.Drawing.Size(133, 69);
         this.panelEndDate.MinimumSize = new System.Drawing.Size(133, 69);
         this.panelEndDate.Name = "panelEndDate";
         this.panelEndDate.Size = new System.Drawing.Size(133, 69);
         this.panelEndDate.TabIndex = 5;
         // 
         // dateEnd
         // 
         this.dateEnd.Dock = System.Windows.Forms.DockStyle.Left;
         this.dateEnd.Font = new System.Drawing.Font("Segoe UI", 9F);
         this.dateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.dateEnd.Location = new System.Drawing.Point(0, 32);
         this.dateEnd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.dateEnd.MinimumSize = new System.Drawing.Size(132, 26);
         this.dateEnd.Name = "dateEnd";
         this.dateEnd.ShowUpDown = true;
         this.dateEnd.Size = new System.Drawing.Size(132, 27);
         this.dateEnd.TabIndex = 6;
         // 
         // label5
         // 
         this.label5.Dock = System.Windows.Forms.DockStyle.Top;
         this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
         this.label5.ForeColor = System.Drawing.Color.SteelBlue;
         this.label5.Location = new System.Drawing.Point(0, 0);
         this.label5.MinimumSize = new System.Drawing.Size(112, 32);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(133, 32);
         this.label5.TabIndex = 5;
         this.label5.Text = "End Date";
         this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelStartDate
         // 
         this.panelStartDate.Controls.Add(this.dateBeg);
         this.panelStartDate.Controls.Add(this.label3);
         this.panelStartDate.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelStartDate.Location = new System.Drawing.Point(0, 75);
         this.panelStartDate.Margin = new System.Windows.Forms.Padding(4);
         this.panelStartDate.MaximumSize = new System.Drawing.Size(133, 69);
         this.panelStartDate.MinimumSize = new System.Drawing.Size(133, 69);
         this.panelStartDate.Name = "panelStartDate";
         this.panelStartDate.Size = new System.Drawing.Size(133, 69);
         this.panelStartDate.TabIndex = 4;
         // 
         // dateBeg
         // 
         this.dateBeg.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dateBeg.Font = new System.Drawing.Font("Segoe UI", 9F);
         this.dateBeg.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.dateBeg.Location = new System.Drawing.Point(0, 32);
         this.dateBeg.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.dateBeg.MinimumSize = new System.Drawing.Size(4, 26);
         this.dateBeg.Name = "dateBeg";
         this.dateBeg.ShowUpDown = true;
         this.dateBeg.Size = new System.Drawing.Size(133, 27);
         this.dateBeg.TabIndex = 4;
         // 
         // label3
         // 
         this.label3.Dock = System.Windows.Forms.DockStyle.Top;
         this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
         this.label3.ForeColor = System.Drawing.Color.SteelBlue;
         this.label3.Location = new System.Drawing.Point(0, 0);
         this.label3.MinimumSize = new System.Drawing.Size(112, 32);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(133, 32);
         this.label3.TabIndex = 3;
         this.label3.Text = "Start Date";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelSearch
         // 
         this.panelSearch.Controls.Add(this.searchBox);
         this.panelSearch.Controls.Add(this.label4);
         this.panelSearch.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelSearch.Location = new System.Drawing.Point(0, 0);
         this.panelSearch.Margin = new System.Windows.Forms.Padding(4);
         this.panelSearch.Name = "panelSearch";
         this.panelSearch.Size = new System.Drawing.Size(227, 75);
         this.panelSearch.TabIndex = 2;
         // 
         // searchBox
         // 
         this.searchBox.Dock = System.Windows.Forms.DockStyle.Top;
         this.searchBox.Font = new System.Drawing.Font("Segoe UI", 9F);
         this.searchBox.Location = new System.Drawing.Point(0, 32);
         this.searchBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.searchBox.MaximumSize = new System.Drawing.Size(332, 26);
         this.searchBox.MinimumSize = new System.Drawing.Size(129, 26);
         this.searchBox.Name = "searchBox";
         this.searchBox.Size = new System.Drawing.Size(227, 27);
         this.searchBox.TabIndex = 7;
         this.searchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyDown);
         // 
         // label4
         // 
         this.label4.Dock = System.Windows.Forms.DockStyle.Top;
         this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
         this.label4.ForeColor = System.Drawing.Color.SteelBlue;
         this.label4.Location = new System.Drawing.Point(0, 0);
         this.label4.MinimumSize = new System.Drawing.Size(112, 32);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(227, 32);
         this.label4.TabIndex = 6;
         this.label4.Text = "Search";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // Control_StandardControls
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.White;
         this.Controls.Add(this.buttonsMenu);
         this.Controls.Add(this.panelSES);
         this.Margin = new System.Windows.Forms.Padding(4);
         this.Name = "Control_StandardControls";
         this.Size = new System.Drawing.Size(227, 481);
         this.buttonsMenu.ResumeLayout(false);
         this.buttonsMenu.PerformLayout();
         this.panelSES.ResumeLayout(false);
         this.panelEndDate.ResumeLayout(false);
         this.panelStartDate.ResumeLayout(false);
         this.panelSearch.ResumeLayout(false);
         this.panelSearch.PerformLayout();
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip buttonsMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem bPrint_printer;
        private System.Windows.Forms.ToolStripMenuItem bPrint_screen;
        private System.Windows.Forms.ToolStripMenuItem bPrint_PDF;
        private System.Windows.Forms.Panel panelSES;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelEndDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelStartDate;
        private System.Windows.Forms.Label label3;
        private MySql.Data.MySqlClient.CustomInstaller customInstaller1;
        private System.Windows.Forms.ToolStripMenuItem bNew;
        private System.Windows.Forms.ToolStripMenuItem bEdit;
        private System.Windows.Forms.ToolStripMenuItem bDelete;
        private System.Windows.Forms.ToolStripMenuItem bPrint;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.DateTimePicker dateEnd;
        private System.Windows.Forms.DateTimePicker dateBeg;
    }
}
