﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Form_Login : Form
    {
        MySQLDatabase db;
        public Form_Login()
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
        }

        private void BClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BLogin_Click(object sender, EventArgs e)
        {
            List<string> a = new List<string>();
            a.Add(password.Name);
            if (!FormManager.isFieldsFilled(this, a))
            {
                return;
            }
            List<SQLParameter> paramss = new List<SQLParameter>();
            paramss.Add(new SQLParameter("username", username.Text));
            paramss.Add(new SQLParameter("password", HashComputer.ComputeSha256Hash(password.Text)));
            IDataReader reader = db.Select("SELECT clientid, clientname, username FROM vwusers", paramss);
            if(reader != null)
            {
                if(reader.Read())
                {
                    ActiveUser.clientid = reader.GetInt32(0);
                    ActiveUser.clientname = DataTypeManager.ParseDataToString(reader.GetValue(1));
                    ActiveUser.username = DataTypeManager.ParseDataToString(reader.GetValue(2));
                    reader.Close();
                    reader.Dispose();
                    Form_Main main = new Form_Main();
                    Hide();
                    main.ShowDialog();
                    if (main.isExit)
                    {
                        Close();
                    }
                    else
                    {
                        main.Dispose();
                        main = null;
                        password.Text = "";
                        password.Focus();
                        Show();
                    }
                }
                else
                {
                    MessageBox.Show("Incorrect username and/or password.", "Login Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    password.Text = "";
                    username.Focus();
                }
                db.CloseConnection();
            }
        }

        private void Form_Login_Load(object sender, EventArgs e)
        {
            //DatabaseManager.AddToOtherDatabases("dbstandarddata", new MySQLDatabase("localhost", "dbstandarddata", "thakira", "abc123"));
        }
    }
}
