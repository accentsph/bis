﻿using DPUruNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Form_FPReaderSelector : Form
    {
        FPReader reader;
        public Form_FPReaderSelector()
        {
            InitializeComponent();
        }

        private void Form_FPReaderSelector_Load(object sender, EventArgs e)
        {
            reader = FPReaderManager.GetInstance();
            tReaders.DisplayMember = "Reader";
            tReaders.ValueMember = "SerialNumber";
            try
            {
                foreach (Reader reader in ReaderCollection.GetReaders())
                {
                    tReaders.Items.Add(reader.Description.SerialNumber);
                }
                if (tReaders.Items.Count > 0)
                {
                    tReaders.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show(ex.Message + "\r\n\r\nPlease check if DigitalPersona service has been started", "Cannot access readers");
                Close();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            reader.CurrentReader = ReaderCollection.GetReaders()[tReaders.SelectedIndex];
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
