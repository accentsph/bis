﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Control_GroupSMS : UserControl
    {
        private SMSModem modem = null;

        public Control_GroupSMS()
        {
            InitializeComponent();
            KeyValuePair<string, string> contacts = new KeyValuePair<string, string>();
            tNum.DataSource = new BindingSource(contacts, null);
            tNum.ValueMember = "Key";
            tNum.DisplayMember = "Value";
        }

        private void Control_GroupSMS_Resize(object sender, EventArgs e)
        {
            if(wrapPanelMessage.MinimumSize.Width + wrapPanelRecepient.MinimumSize.Width < Width)
            {
                wrapPanelRecepient.Dock = DockStyle.Left;
                wrapPanelRecepient.Padding = new Padding(0, 0, 20, 20);
                wrapPanelMessage.Dock = DockStyle.Left;
            }
            else
            {
                wrapPanelRecepient.Dock = DockStyle.Top;
                wrapPanelRecepient.Padding = new Padding(0, 0, 0, 20);
                wrapPanelMessage.Dock = DockStyle.Top;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if(tNum.SelectedValue == null)
            {
                string[] row = { "", tNum.Text, "", "Pending" };
                ListViewItem lv = new ListViewItem(row);
                listView1.Items.Add(lv);
            }
            else
            {
                MessageBox.Show(tNum.SelectedValue.ToString());
            }
            tNum.Text = "";
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.SelectedItems)
            {
                listView1.Items.Remove(lvi);
            }
        }

        private void ListView1_SelectedIndexChanged_2(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                bRemove.Enabled = false;
                bClear.Enabled = false;
            }
            else
            {
                bRemove.Enabled = true;
                bClear.Enabled = true;
            }
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lv in listView1.Items)
            {
                try
                {
                    if(modem.sendMsg(lv.SubItems[1].Text, tMessage.Text))
                    {
                        //MessageBox.Show("Message has sent successfully");
                        lv.SubItems[2].Text = "Sent";
                        lv.ForeColor = Color.Green;
                    }
                    else
                    {
                        //MessageBox.Show("Failed to send message");
                        lv.SubItems[2].Text = "Failed";
                        lv.ForeColor = Color.Red;
                    }

                }
                catch (Exception ex)
                {
                    lv.SubItems[2].Text = "Failed";
                    lv.ForeColor = Color.Red;
                }
            }
        }

        private void Control_GroupSMS_Load(object sender, EventArgs e)
        {
            modem = SMSModem.GetInstance();
        }
    }
}
