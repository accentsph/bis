﻿namespace BIS
{
   partial class Form_ConvertHeightWeight
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_ConvertHeightWeight));
         this.splitContainer1 = new System.Windows.Forms.SplitContainer();
         this.label12 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.splitContainer2 = new System.Windows.Forms.SplitContainer();
         this.button1 = new System.Windows.Forms.Button();
         this.panel3 = new System.Windows.Forms.Panel();
         this.label7 = new System.Windows.Forms.Label();
         this.NumCM = new System.Windows.Forms.NumericUpDown();
         this.NumInch = new System.Windows.Forms.NumericUpDown();
         this.NumFeet = new System.Windows.Forms.NumericUpDown();
         this.panel2 = new System.Windows.Forms.Panel();
         this.label5 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.panel1 = new System.Windows.Forms.Panel();
         this.label2 = new System.Windows.Forms.Label();
         this.button2 = new System.Windows.Forms.Button();
         this.panel6 = new System.Windows.Forms.Panel();
         this.label13 = new System.Windows.Forms.Label();
         this.NumKg = new System.Windows.Forms.NumericUpDown();
         this.NumLb = new System.Windows.Forms.NumericUpDown();
         this.panel5 = new System.Windows.Forms.Panel();
         this.label9 = new System.Windows.Forms.Label();
         this.label10 = new System.Windows.Forms.Label();
         this.label11 = new System.Windows.Forms.Label();
         this.panel4 = new System.Windows.Forms.Panel();
         this.label8 = new System.Windows.Forms.Label();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
         this.splitContainer1.Panel1.SuspendLayout();
         this.splitContainer1.Panel2.SuspendLayout();
         this.splitContainer1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
         this.splitContainer2.Panel1.SuspendLayout();
         this.splitContainer2.Panel2.SuspendLayout();
         this.splitContainer2.SuspendLayout();
         this.panel3.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.NumCM)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.NumInch)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.NumFeet)).BeginInit();
         this.panel2.SuspendLayout();
         this.panel1.SuspendLayout();
         this.panel6.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.NumKg)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.NumLb)).BeginInit();
         this.panel5.SuspendLayout();
         this.panel4.SuspendLayout();
         this.SuspendLayout();
         // 
         // splitContainer1
         // 
         this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer1.Location = new System.Drawing.Point(5, 5);
         this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.splitContainer1.Name = "splitContainer1";
         this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer1.Panel1
         // 
         this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer1.Panel1.Controls.Add(this.label12);
         this.splitContainer1.Panel1.Controls.Add(this.label1);
         // 
         // splitContainer1.Panel2
         // 
         this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
         this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
         this.splitContainer1.Size = new System.Drawing.Size(444, 490);
         this.splitContainer1.SplitterDistance = 67;
         this.splitContainer1.SplitterWidth = 6;
         this.splitContainer1.TabIndex = 0;
         // 
         // label12
         // 
         this.label12.BackColor = System.Drawing.Color.Silver;
         this.label12.Dock = System.Windows.Forms.DockStyle.Right;
         this.label12.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
         this.label12.Location = new System.Drawing.Point(379, 0);
         this.label12.Name = "label12";
         this.label12.Padding = new System.Windows.Forms.Padding(1);
         this.label12.Size = new System.Drawing.Size(61, 63);
         this.label12.TabIndex = 1;
         this.label12.Text = "X";
         this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.label12.Click += new System.EventHandler(this.label12_Click);
         // 
         // label1
         // 
         this.label1.BackColor = System.Drawing.Color.Transparent;
         this.label1.Dock = System.Windows.Forms.DockStyle.Left;
         this.label1.Font = new System.Drawing.Font("Britannic Bold", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.ForeColor = System.Drawing.Color.White;
         this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
         this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label1.Location = new System.Drawing.Point(0, 0);
         this.label1.Name = "label1";
         this.label1.Padding = new System.Windows.Forms.Padding(6, 7, 6, 7);
         this.label1.Size = new System.Drawing.Size(404, 63);
         this.label1.TabIndex = 0;
         this.label1.Text = "MEASUREMENT CONVERTER";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // splitContainer2
         // 
         this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer2.IsSplitterFixed = true;
         this.splitContainer2.Location = new System.Drawing.Point(7, 8);
         this.splitContainer2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.splitContainer2.MaximumSize = new System.Drawing.Size(562, 398);
         this.splitContainer2.Name = "splitContainer2";
         this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer2.Panel1
         // 
         this.splitContainer2.Panel1.Controls.Add(this.button1);
         this.splitContainer2.Panel1.Controls.Add(this.panel3);
         this.splitContainer2.Panel1.Controls.Add(this.panel2);
         this.splitContainer2.Panel1.Controls.Add(this.panel1);
         this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(6, 7, 6, 7);
         // 
         // splitContainer2.Panel2
         // 
         this.splitContainer2.Panel2.Controls.Add(this.button2);
         this.splitContainer2.Panel2.Controls.Add(this.panel6);
         this.splitContainer2.Panel2.Controls.Add(this.panel5);
         this.splitContainer2.Panel2.Controls.Add(this.panel4);
         this.splitContainer2.Panel2.Padding = new System.Windows.Forms.Padding(6, 7, 6, 7);
         this.splitContainer2.Size = new System.Drawing.Size(430, 398);
         this.splitContainer2.SplitterDistance = 198;
         this.splitContainer2.SplitterWidth = 3;
         this.splitContainer2.TabIndex = 0;
         // 
         // button1
         // 
         this.button1.AutoSize = true;
         this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.button1.Enabled = false;
         this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
         this.button1.Location = new System.Drawing.Point(6, 144);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(414, 43);
         this.button1.TabIndex = 3;
         this.button1.Text = "Update Height";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // panel3
         // 
         this.panel3.BackColor = System.Drawing.Color.White;
         this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel3.Controls.Add(this.label7);
         this.panel3.Controls.Add(this.NumCM);
         this.panel3.Controls.Add(this.NumInch);
         this.panel3.Controls.Add(this.NumFeet);
         this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel3.Location = new System.Drawing.Point(6, 102);
         this.panel3.Name = "panel3";
         this.panel3.Size = new System.Drawing.Size(414, 42);
         this.panel3.TabIndex = 2;
         // 
         // label7
         // 
         this.label7.BackColor = System.Drawing.Color.LightSkyBlue;
         this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label7.Dock = System.Windows.Forms.DockStyle.Left;
         this.label7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label7.Location = new System.Drawing.Point(248, 0);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(38, 38);
         this.label7.TabIndex = 5;
         this.label7.Text = "=";
         this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // NumCM
         // 
         this.NumCM.BackColor = System.Drawing.Color.LightSkyBlue;
         this.NumCM.DecimalPlaces = 2;
         this.NumCM.Dock = System.Windows.Forms.DockStyle.Right;
         this.NumCM.Font = new System.Drawing.Font("Calibri", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.NumCM.Increment = new decimal(new int[] {
            50,
            0,
            0,
            131072});
         this.NumCM.Location = new System.Drawing.Point(286, 0);
         this.NumCM.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
         this.NumCM.Name = "NumCM";
         this.NumCM.Size = new System.Drawing.Size(124, 48);
         this.NumCM.TabIndex = 2;
         this.NumCM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.NumCM.ValueChanged += new System.EventHandler(this.ComputeHeight);
         // 
         // NumInch
         // 
         this.NumInch.BackColor = System.Drawing.Color.LightSkyBlue;
         this.NumInch.Dock = System.Windows.Forms.DockStyle.Left;
         this.NumInch.Font = new System.Drawing.Font("Calibri", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.NumInch.Location = new System.Drawing.Point(124, 0);
         this.NumInch.Maximum = new decimal(new int[] {
            11,
            0,
            0,
            0});
         this.NumInch.Name = "NumInch";
         this.NumInch.Size = new System.Drawing.Size(124, 48);
         this.NumInch.TabIndex = 1;
         this.NumInch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.NumInch.ValueChanged += new System.EventHandler(this.ComputeHeight);
         // 
         // NumFeet
         // 
         this.NumFeet.BackColor = System.Drawing.Color.LightSkyBlue;
         this.NumFeet.Dock = System.Windows.Forms.DockStyle.Left;
         this.NumFeet.Font = new System.Drawing.Font("Calibri", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.NumFeet.Location = new System.Drawing.Point(0, 0);
         this.NumFeet.Name = "NumFeet";
         this.NumFeet.Size = new System.Drawing.Size(124, 48);
         this.NumFeet.TabIndex = 0;
         this.NumFeet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.NumFeet.ValueChanged += new System.EventHandler(this.ComputeHeight);
         // 
         // panel2
         // 
         this.panel2.BackColor = System.Drawing.Color.White;
         this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel2.Controls.Add(this.label5);
         this.panel2.Controls.Add(this.label6);
         this.panel2.Controls.Add(this.label4);
         this.panel2.Controls.Add(this.label3);
         this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.panel2.Location = new System.Drawing.Point(6, 53);
         this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.panel2.Name = "panel2";
         this.panel2.Size = new System.Drawing.Size(414, 49);
         this.panel2.TabIndex = 1;
         // 
         // label5
         // 
         this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.Location = new System.Drawing.Point(248, 0);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(38, 45);
         this.label5.TabIndex = 4;
         this.label5.Text = "To";
         this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label6
         // 
         this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label6.Dock = System.Windows.Forms.DockStyle.Right;
         this.label6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label6.Location = new System.Drawing.Point(286, 0);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(124, 45);
         this.label6.TabIndex = 3;
         this.label6.Text = "Centimeter";
         this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label4
         // 
         this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label4.Dock = System.Windows.Forms.DockStyle.Left;
         this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label4.Location = new System.Drawing.Point(124, 0);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(124, 45);
         this.label4.TabIndex = 1;
         this.label4.Text = "Inches";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label3
         // 
         this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label3.Dock = System.Windows.Forms.DockStyle.Left;
         this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.Location = new System.Drawing.Point(0, 0);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(124, 45);
         this.label3.TabIndex = 0;
         this.label3.Text = "Feet";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel1
         // 
         this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel1.Controls.Add(this.label2);
         this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel1.Location = new System.Drawing.Point(6, 7);
         this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.panel1.Name = "panel1";
         this.panel1.Size = new System.Drawing.Size(414, 46);
         this.panel1.TabIndex = 0;
         // 
         // label2
         // 
         this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.ForeColor = System.Drawing.Color.White;
         this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
         this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label2.Location = new System.Drawing.Point(0, 0);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(410, 42);
         this.label2.TabIndex = 0;
         this.label2.Text = "CONVERT FEET/INCHES TO CENTIMETER";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // button2
         // 
         this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.button2.Enabled = false;
         this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
         this.button2.Location = new System.Drawing.Point(6, 142);
         this.button2.Name = "button2";
         this.button2.Size = new System.Drawing.Size(414, 44);
         this.button2.TabIndex = 10;
         this.button2.Text = "Update Weight";
         this.button2.UseVisualStyleBackColor = true;
         this.button2.Click += new System.EventHandler(this.button2_Click);
         // 
         // panel6
         // 
         this.panel6.BackColor = System.Drawing.Color.White;
         this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel6.Controls.Add(this.label13);
         this.panel6.Controls.Add(this.NumKg);
         this.panel6.Controls.Add(this.NumLb);
         this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel6.Location = new System.Drawing.Point(6, 101);
         this.panel6.Name = "panel6";
         this.panel6.Size = new System.Drawing.Size(414, 41);
         this.panel6.TabIndex = 9;
         // 
         // label13
         // 
         this.label13.BackColor = System.Drawing.Color.LightSkyBlue;
         this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label13.Dock = System.Windows.Forms.DockStyle.Left;
         this.label13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label13.Location = new System.Drawing.Point(175, 0);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(60, 37);
         this.label13.TabIndex = 9;
         this.label13.Text = "=";
         this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // NumKg
         // 
         this.NumKg.BackColor = System.Drawing.Color.LightSkyBlue;
         this.NumKg.DecimalPlaces = 2;
         this.NumKg.Dock = System.Windows.Forms.DockStyle.Right;
         this.NumKg.Font = new System.Drawing.Font("Calibri", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.NumKg.Increment = new decimal(new int[] {
            50,
            0,
            0,
            131072});
         this.NumKg.Location = new System.Drawing.Point(235, 0);
         this.NumKg.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
         this.NumKg.Name = "NumKg";
         this.NumKg.Size = new System.Drawing.Size(175, 48);
         this.NumKg.TabIndex = 8;
         this.NumKg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.NumKg.ValueChanged += new System.EventHandler(this.ComputeWeight);
         // 
         // NumLb
         // 
         this.NumLb.BackColor = System.Drawing.Color.LightSkyBlue;
         this.NumLb.DecimalPlaces = 2;
         this.NumLb.Dock = System.Windows.Forms.DockStyle.Left;
         this.NumLb.Font = new System.Drawing.Font("Calibri", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.NumLb.Increment = new decimal(new int[] {
            50,
            0,
            0,
            131072});
         this.NumLb.Location = new System.Drawing.Point(0, 0);
         this.NumLb.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
         this.NumLb.Name = "NumLb";
         this.NumLb.Size = new System.Drawing.Size(175, 48);
         this.NumLb.TabIndex = 7;
         this.NumLb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.NumLb.ValueChanged += new System.EventHandler(this.ComputeWeight);
         // 
         // panel5
         // 
         this.panel5.BackColor = System.Drawing.Color.White;
         this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel5.Controls.Add(this.label9);
         this.panel5.Controls.Add(this.label10);
         this.panel5.Controls.Add(this.label11);
         this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.panel5.Location = new System.Drawing.Point(6, 53);
         this.panel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.panel5.Name = "panel5";
         this.panel5.Size = new System.Drawing.Size(414, 48);
         this.panel5.TabIndex = 8;
         // 
         // label9
         // 
         this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label9.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label9.Location = new System.Drawing.Point(175, 0);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(60, 44);
         this.label9.TabIndex = 4;
         this.label9.Text = "To";
         this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label10
         // 
         this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label10.Dock = System.Windows.Forms.DockStyle.Right;
         this.label10.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label10.Location = new System.Drawing.Point(235, 0);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(175, 44);
         this.label10.TabIndex = 3;
         this.label10.Text = "Kilograms";
         this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label11
         // 
         this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.label11.Dock = System.Windows.Forms.DockStyle.Left;
         this.label11.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label11.Location = new System.Drawing.Point(0, 0);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(175, 44);
         this.label11.TabIndex = 1;
         this.label11.Text = "Pounds";
         this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel4
         // 
         this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel4.Controls.Add(this.label8);
         this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel4.Location = new System.Drawing.Point(6, 7);
         this.panel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.panel4.Name = "panel4";
         this.panel4.Size = new System.Drawing.Size(414, 46);
         this.panel4.TabIndex = 4;
         // 
         // label8
         // 
         this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label8.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label8.ForeColor = System.Drawing.Color.White;
         this.label8.Image = ((System.Drawing.Image)(resources.GetObject("label8.Image")));
         this.label8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label8.Location = new System.Drawing.Point(0, 0);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(410, 42);
         this.label8.TabIndex = 0;
         this.label8.Text = "CONVERT POUND TO KILOGRAM";
         this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // Form_ConvertHeightWeight
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(454, 500);
         this.Controls.Add(this.splitContainer1);
         this.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.Name = "Form_ConvertHeightWeight";
         this.Padding = new System.Windows.Forms.Padding(5);
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
         this.splitContainer1.Panel1.ResumeLayout(false);
         this.splitContainer1.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
         this.splitContainer1.ResumeLayout(false);
         this.splitContainer2.Panel1.ResumeLayout(false);
         this.splitContainer2.Panel1.PerformLayout();
         this.splitContainer2.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
         this.splitContainer2.ResumeLayout(false);
         this.panel3.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.NumCM)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.NumInch)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.NumFeet)).EndInit();
         this.panel2.ResumeLayout(false);
         this.panel1.ResumeLayout(false);
         this.panel6.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.NumKg)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.NumLb)).EndInit();
         this.panel5.ResumeLayout(false);
         this.panel4.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.SplitContainer splitContainer2;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Panel panel2;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Panel panel3;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.NumericUpDown NumCM;
      private System.Windows.Forms.NumericUpDown NumInch;
      private System.Windows.Forms.NumericUpDown NumFeet;
      private System.Windows.Forms.Button button2;
      private System.Windows.Forms.Panel panel6;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.NumericUpDown NumKg;
      private System.Windows.Forms.NumericUpDown NumLb;
      private System.Windows.Forms.Panel panel5;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Panel panel4;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label12;
   }
}