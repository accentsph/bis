﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DPCtlUruNet;
using DPUruNet;
using System.IO;

namespace BIS
{
    public partial class EntryForm_Residents : Form
    {
        private FPReader reader = null;
        private EnrollmentControl enrollmentControl;
        private MySQLDatabase db;
        private List<Fingerprint> fingerprints;
        private string clientID;
        private Fid fpImageView;
        private List<string> deletedContacts;
        private bool hasRegisteredFp = false;
        private bool hasRegisteredFp1 = false;
        public EntryForm_Residents()
        {
            InitializeComponent();
            init();
            residentPhoto.Tag = null;
        }

        public EntryForm_Residents(string clientID)
        {
            InitializeComponent();
            this.clientID = clientID;
            init();
            List<SQLParameter> prmsss = new List<SQLParameter>();
            prmsss.Add(new SQLParameter("clientid", clientID));
            string photoPath = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT picpath FROM tbclients", prmsss));
            if(photoPath != null && photoPath.Length > 0)
            {
                try
                {
                    residentPhoto.Load(photoPath);
                    residentPhoto.Image.Tag = null;
                    residentPhoto.Tag = photoPath;
                }
                catch (Exception err)
                {
                    Console.WriteLine("Error message: " + err.Message);
                    Console.WriteLine(err.StackTrace);
                    residentPhoto.Tag = null;
                }
            }
            IDataReader readerr = db.Select("SELECT * FROM tbclients", prmsss);
            DataTable dt = new DataTable();
            dt.Load(readerr);
            FormManager.populateFields(panelClientName, dt);
            FormManager.populateFields(panelAddress, dt);
            readerr = db.Select("SELECT * FROM tbclientpersonaldetails", prmsss);
            dt = new DataTable();
            dt.Load(readerr);
            readerr.Close();
            FormManager.populateFields(panelPersonalDetails, dt);
            FormManager.populateFields(panelEmploymentDetails, dt);
            FormManager.populateFields(panelPhysicalDetails, dt);
            FormManager.populateFields(panelMiscellaneousDetails, dt);
            FormManager.populateFields(panelImmediateFamily, dt);
            readerr = db.Select("SELECT DigitNum, DigitSerial, FPPath FROM tbclientfingerprints", prmsss);
            if(readerr != null)
            {
                while(readerr.Read())
                {
                    hasRegisteredFp = true;
                    try
                    {
                        Fmd fpFmd = Fmd.DeserializeXml(DataTypeManager.ParseDataToString(readerr.GetValue(1)));
                        Fid fpFid = Fid.DeserializeXml(FileManager.ReadFromFile(readerr.GetString(2) + ".xml"));
                        Fingerprint fprint = new Fingerprint(readerr.GetInt32(0), fpFmd, fpFid);
                        fingerprints.Add(fprint);
                        Control_FPimageBox fpib = new Control_FPimageBox();
                        panelFPs.Controls.Add(fpib);
                        //fpib.BringToFront();
                        fpib.Dock = DockStyle.Left;
                        fpib.Fingerprint = fprint;
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine(err.StackTrace);
                        MessageBox.Show(err.Message);

                    }
                    
                }
                readerr.Close();
            }
            readerr = db.Select("SELECT contactid, `contact type`, `contact detail`, `sms group`, contacttypeid, smsgroupid FROM vwclientcontactdetails", prmsss);
            if(reader != null)
            {
               while(readerr.Read())
               {
                  string contactid = DataTypeManager.ParseDataToString(readerr.GetValue(0));
                  string[] lvitems = {
                     DataTypeManager.ParseDataToString(readerr.GetValue(1)),
                     DataTypeManager.ParseDataToString(readerr.GetValue(2)),
                     DataTypeManager.ParseDataToString(readerr.GetValue(3))
                  };
                  ListViewItem lv = new ListViewItem(lvitems);
                  lv.Name = contactid;
                  lv.SubItems[0].Tag = readerr.GetValue(4);
                  if(lv.SubItems[0].Text == "Mobile")
                  {
                     lv.SubItems[2].Tag = readerr.GetValue(5);
                  }
                  listView1.Items.Add(lv);
               }
               readerr.Close();
            }
          //  readerr = db.Select("SELECT")
        }
        private void init()
        {
            deletedContacts = new List<string>();
            fingerprints = new List<Fingerprint>();
            reader = FPReaderManager.GetInstance();
            db = DatabaseManager.GetInstance();
            if (enrollmentControl != null)
            {
                enrollmentControl.Reader = reader.CurrentReader;
            }
            else
            {
                enrollmentControl = new EnrollmentControl(reader.CurrentReader, Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
                enrollmentControl.BackColor = System.Drawing.SystemColors.Window;
                enrollmentControl.Location = new System.Drawing.Point(2, 10);
                enrollmentControl.Name = "ctlEnrollmentControl";
                enrollmentControl.Size = new System.Drawing.Size(482, 346);
                enrollmentControl.TabIndex = 0;
                enrollmentControl.OnCancel += new EnrollmentControl.CancelEnrollment(this.enrollment_OnCancel);
                enrollmentControl.OnCaptured += new EnrollmentControl.FingerprintCaptured(this.enrollment_OnCaptured);
                enrollmentControl.OnDelete += new EnrollmentControl.DeleteEnrollment(this.enrollment_OnDelete);
                enrollmentControl.OnEnroll += new EnrollmentControl.FinishEnrollment(this.enrollment_OnEnroll);
                enrollmentControl.OnStartEnroll += new EnrollmentControl.StartEnrollment(this.enrollment_OnStartEnroll);
            }
            tabPage3.Controls.Add(enrollmentControl);
            FormManager.ProcessAddressComboData(clientID == null? AppConfiguration.GetConfig("barangayid") : Convert.ToString(db.ScalarSelect("SELECT brgyid FROM tbclients WHERE clientid='" + clientID + "'")), BrgyID, CityID, ProvID);
            FormManager.ProcessComboBoxes(this, "Populate", true);
            SetTextBoxAutoComplete();
            RegisterMyEventHandlers(this);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage1;
        }

        private void TabPage1_Enter(object sender, EventArgs e)
        {
            HighlightButton(bPDetails);
        }

        private void HighlightButton(Control control)
        {
            control.BackColor = Color.White;
            control.ForeColor = Color.FromArgb(64, 64, 64);
            foreach(Control c in menuPanel.Controls)
            {
                if(c is Button)
                {
                    if(c != control)
                    {
                        c.BackColor = Color.FromArgb(19, 128, 201);
                        c.ForeColor = Color.FromArgb(123, 200, 252);
                    }
                }
            }
        }

        private void Label5_Click(object sender, EventArgs e)
        {
            this.Close();
            enrollmentControl.Cancel();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage3;
        }

        private void TabPage2_Enter(object sender, EventArgs e)
        {
            HighlightButton(bConInf);
        }

        private void TabPage3_Enter(object sender, EventArgs e)
        {
            HighlightButton(bFP);
            
        }

        private void Button3_Click_1(object sender, EventArgs e)
        {
            if(tabControl1.SelectedIndex < tabControl1.TabCount - 1)
            {
                tabControl1.SelectedIndex++;
            }
            else
            {
                List<string> exmptions = new List<string>();
                exmptions.Add("POD");
                exmptions.Add("DOD");
                exmptions.Add("panelMiscellaneousDetails");
                exmptions.Add("panelMiscellaneousDetails");
                exmptions.Add("sAddress");
                exmptions.Add("Spouse");
                exmptions.Add("txtMessage");
                if (!FormManager.isFieldsFilled(this, exmptions))
                {
                    return;
                }
                
                List<SQLParameter> prms = FormManager.getParameters(panelClientName);
                exmptions = new List<string>();
                exmptions.Add("ProvID");
                exmptions.Add("CityID");
                prms.AddRange(FormManager.getParameters(panelAddress, exmptions));
                int cid = 0;
                SQLParameter clientidParams = null;
                bool flagg = false;
                SQLParameter[] prmss = { null };
                if(clientID == null)
                {
                    cid = db.InsertScalar("tbclients", prms);
                    clientidParams = new SQLParameter("clientidd", cid, "=", "clientid");
                }
                else
                {
                    cid = Convert.ToInt32(clientID);
                    clientidParams = new SQLParameter("clientiddd", cid, "=", "clientid");
                    prmss[0] = clientidParams;
                    flagg = db.Update("tbclients", prms, prmss);
                }
                if(cid > 0)
                {
                     foreach(ListViewItem lvi in listView1.Items)
                     {
                        bool forInsert = true;
                        List<SQLParameter> sprms = new List<SQLParameter>();
                        sprms.Add(clientidParams);
                        sprms.Add(new SQLParameter("contacttype", lvi.SubItems[0].Tag));
                        sprms.Add(new SQLParameter("contact", lvi.SubItems[1].Text));
                        string sg = Convert.ToString(lvi.SubItems[2].Tag);
                        if(lvi.SubItems[0].Text == "Mobile")
                        {
                           sprms.Add(new SQLParameter("smsgroup", sg));
                        }
                        if(lvi.Name != null)
                        {
                           if (Convert.ToInt32(db.ScalarSelect("SELECT COUNT(*) FROM tbclientscontactdetails WHERE contactid='" + lvi.Name + "'")) > 0)
                           {
                              forInsert = false;
                           }
                           if(forInsert)
                           {
                              db.Insert("tbclientscontactdetails", sprms);
                           }
                           else
                           {
                              sprms.Remove(clientidParams);
                              SQLParameter[] sprms1 = { new SQLParameter("contactid", lvi.Name) };
                              db.Update("tbclientscontactdetails", sprms, sprms1);
                           }
                        }
                     }
                    exmptions = new List<string>();
                    exmptions.Add("panelDeceasedDetails");
                    prms = FormManager.getParameters(tabPage2, exmptions);
                    if(clientID != null || flagg)
                    {
                        flagg = db.Update("tbclientpersonaldetails", prms, prmss);
                    }
                    else
                    {
                        prms.Add(clientidParams);
                        flagg = db.Insert("tbclientpersonaldetails", prms);
                        
                    }
                    if(flagg)
                    {
                        //Code to sa pagcreate ng xml file at image file ng fingerprint
                        bool flag = true;
                        string folderName = AppConfiguration.GetConfig("syslocation") + "Clients\\" + cid;
                        string fpFolderName = folderName + "\\Fingerprints\\";
                        bool insertFp = true;
                        if (residentPhoto.Tag != null)
                        {
                            if(Convert.ToString(residentPhoto.Image.Tag) == "new")
                            {
                                SQLParameter[] photoPrms = { clientidParams };
                                string fileXtnsion = ".png";
                                try
                                {
                                    fileXtnsion = Path.GetExtension(Convert.ToString(residentPhoto.Tag));
                                }
                                catch (Exception err)
                                {
                                    Console.WriteLine("Error Message: " + err.Message);
                                    Console.WriteLine(err.StackTrace);
                                }
                                string photoName = "Photo-" + cid + fileXtnsion;
                                string newPhotoPath = folderName + "\\Photo\\" + photoName;
                                List<SQLParameter> photoPrms1 = new List<SQLParameter>();
                                photoPrms1.Add(new SQLParameter("picPath", newPhotoPath));
                                if (db.Update("tbclients", photoPrms1, photoPrms))
                                {
                                    //Image img = new Bitmap(residentPhoto.Image);
                                    //FileManager.SaveImageToLocalFolder(folderName + "\\Photo\\", photoName, img);
                                    FileManager.SaveImageToLocalFolder(folderName + "\\Photo\\", photoName, residentPhoto.Image);
                                }
                            }
                        }
                        if (hasRegisteredFp)
                        {
                            if(hasRegisteredFp1)
                            {
                                List<SQLParameter> deletefpprms = new List<SQLParameter>();
                                deletefpprms.Add(clientidParams);
                                db.Delete("tbclientfingerprints", deletefpprms);
                                FileManager.EmptyDirectory(fpFolderName);
                            }
                            insertFp = hasRegisteredFp1;
                        }
                        if(insertFp)
                        {
                            foreach (Fingerprint fp in fingerprints)
                            {
                                string fileLoc = fpFolderName + fp.FingerName;
                                if (!FileManager.WriteToFile(fpFolderName, fp.FingerName + ".xml", fp.FidToXML()) || !FileManager.SaveImageToLocalFolder(fpFolderName, fp.FingerName + "-" + cid + ".png", fp.Image))
                                {
                                    flag = false;
                                    break;
                                }
                                List<SQLParameter> fpparams = new List<SQLParameter>();
                                fpparams.Add(clientidParams);
                                fpparams.Add(new SQLParameter("digitnum", fp.Position));
                                fpparams.Add(new SQLParameter("digitserial", fp.ToXML()));
                                fpparams.Add(new SQLParameter("FPPath", fileLoc));
                                if (!db.Insert("tbclientfingerprints", fpparams))
                                {
                                    flag = false;
                                    break;
                                }
                            }
                        }
                        if (flag)
                        {
                            Close();
                        }
                        else
                        {
                            //rollback code in case magka error sa pag create ng xml at image file ng fingerprint
                        }
                    }
                    else
                    {
                        if(clientID == null)
                        {
                            prms = new List<SQLParameter>();
                            prms.Add(new SQLParameter("clientid", cid));
                            db.Delete("tbclients", prms);
                        }
                    }
                }
                db.CloseConnection();
            }
        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(tabControl1.SelectedIndex == tabControl1.TabCount - 1)
            {
                bNext.Text = "Finish";
            }
            else
            {
                bNext.Text = "Next";
            }
        }

        private void Label5_MouseHover(object sender, EventArgs e)
        {
            bClose.BackColor = Color.Firebrick;
        }

        private void BClose_MouseLeave(object sender, EventArgs e)
        {
            bClose.BackColor = Color.FromArgb(64, 64, 64);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private void EntryForm_Residents_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        #region Enrollment Control Events
        private void enrollment_OnCancel(EnrollmentControl enrollmentControl, Constants.ResultCode result, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                SendMessage("OnCancel:  " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition);
            }
            else
            {
                SendMessage("OnCancel:  No Reader Connected, finger " + fingerPosition);
            }

            btnCancel.Enabled = false;
        }
        private void enrollment_OnCaptured(EnrollmentControl enrollmentControl, CaptureResult captureResult, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                SendMessage("OnCaptured:  " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition + ", quality " + captureResult.Quality.ToString());
            }
            else
            {
                SendMessage("OnCaptured:  No Reader Connected, finger " + fingerPosition);
            }

            if (captureResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
            {
                if (reader.CurrentReader != null)
                {
                    reader.CurrentReader.Dispose();
                    reader.CurrentReader = null;
                }

                enrollmentControl.Reader = null;

                MessageBox.Show("Error:  " + captureResult.ResultCode);
                btnCancel.Enabled = false;
            }
            else
            {
                if (captureResult.Data != null)
                {
                    foreach (Fid.Fiv fiv in captureResult.Data.Views)
                    {
                        pbFingerprint.Image = reader.CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                    }
                    fpImageView = captureResult.Data;
                }
            }
        }
        private void enrollment_OnDelete(EnrollmentControl enrollmentControl, Constants.ResultCode result, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                SendMessage("OnDelete:  " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition);
                SendMessage("Enrolled Finger Mask: " + enrollmentControl.EnrolledFingerMask);
                SendMessage("Disabled Finger Mask: " + enrollmentControl.DisabledFingerMask);
            }
            else
            {
                SendMessage("OnDelete:  No Reader Connected, finger " + fingerPosition);
            }

            reader.Fmds.Remove(fingerPosition);
            for(int i = 0; i < fingerprints.Count; i++)
            {
                if(fingerPosition == fingerprints[i].Position)
                {
                    fingerprints.RemoveAt(i);
                    panelFPs.Controls.RemoveAt(i);
                }
            }
            if (reader.Fmds.Count == 0)
            {
            }
        }
        private void enrollment_OnEnroll(EnrollmentControl enrollmentControl, DataResult<Fmd> result, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                SendMessage("OnEnroll:  " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition);
                SendMessage("Enrolled Finger Mask: " + enrollmentControl.EnrolledFingerMask);
                SendMessage("Disabled Finger Mask: " + enrollmentControl.DisabledFingerMask);
            }
            else
            {
                SendMessage("OnEnroll:  No Reader Connected, finger " + fingerPosition);
            }
            if (result != null && result.Data != null)
            {
                //reader.Fmds.Add(fingerPosition, result.Data);
                Fingerprint fp = new Fingerprint(fingerPosition, result.Data, fpImageView);
                fingerprints.Add(fp);
                Control_FPimageBox ib = new Control_FPimageBox();
                ib.Dock = DockStyle.Left;
                panelFPs.Controls.Add(ib);
                ib.BringToFront();
                ib.Fingerprint = fp;
            }
            btnCancel.Enabled = false;
        }

        private void enrollment_OnStartEnroll(EnrollmentControl enrollmentControl, Constants.ResultCode result, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                if (hasRegisteredFp && !hasRegisteredFp1)
                {
                    DialogResult rs = MessageBox.Show(
                        "Resident has already registered his/her fingerprints. Continuing this will reset his fingerprint registration.\r\n\r\nAre you sure you want to proceed?",
                        "Attention",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Warning);
                    if (rs == DialogResult.Yes)
                    {
                        fingerprints.Clear();
                        panelFPs.Controls.Clear();
                        hasRegisteredFp1 = true;
                        SendMessage("OnStartEnroll: " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition);
                    }
                    else
                    {
                        enrollmentControl.Cancel();
                    }
                }
            }
            else
            {
                SendMessage("OnStartEnroll:  No Reader Connected, finger " + fingerPosition);
            }
            btnCancel.Enabled = true;
        }
        #endregion

        private void SendMessage(string message)
        {
            txtMessage.Text += message + "\r\n\r\n";
            txtMessage.SelectionStart = txtMessage.TextLength;
            txtMessage.ScrollToCaret();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show("Are you sure you want to cancel this enrollment?", "Are You Sure?", buttons, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                enrollmentControl.Cancel();
            }
        }

        private void EntryForm_Residents_Load(object sender, EventArgs e)
        {
            if (enrollmentControl != null)
            {
                enrollmentControl.Reader = reader.CurrentReader;
            }
            //FormManager.ProcessComboBoxes(this,"SetDataSource");
        
        }
      private void RegisterMyEventHandlers(Control control)
      {
         foreach (Control ctl in control.Controls)
         {
            if (ctl is TextBox || ctl is ComboBox || ctl is DateTimePicker || ctl is RichTextBox || ctl is NumericUpDown)
            {
               ctl.KeyDown += Enter_KeyDown;
               if (ctl is ComboBox)
               {
                  ctl.Enter += ComboBox_Enter;
                  ctl.Leave += ComboBox_Leave;
               }
               if (ctl is TextBox)
               {
                  TextBox tb = ctl as TextBox;
                  if (tb != null)
                  {
                     tb.CharacterCasing = CharacterCasing.Upper;
                  }
               }
            }
            RegisterMyEventHandlers(ctl);
         }
      }

      private void ComboBox_Enter(object sender, EventArgs e)
      {
         ComboBox cb = sender as ComboBox;
         if (cb != null)
         {
            cb.DropDownStyle = ComboBoxStyle.DropDownList;
            if (!cb.DroppedDown)
            {
               cb.DroppedDown = true;
            }
         }

      }
      private void Enter_KeyDown(object sender, KeyEventArgs e)
      {
         Keys key = e.KeyData;
         if (key == Keys.Enter)
         {
            SendKeys.Send("{TAB}");
            e.Handled = true;
            e.SuppressKeyPress = true;
            /////
            // Put some validation codes here
            ////


            //if (FormControlsManager.IsSenderForEnter(sender))
            //{
            //   DataControlValidation(sender);
            //}
            //else
            //{
            //   MessageBox.Show("Gikan sa " + FormControlsManager.SenderName(sender));
            //}
         }
      }

        private void SetTextBoxAutoComplete()
        {            
            string sqlstr = "SELECT Particulars FROM dbstandarddata.vwcityprov ORDER BY Particulars;";
            AutoCompleteStringCollection acsc = FormManager.TextBoxAutoCompSource(sqlstr);
            provAddress.AutoCompleteSource = AutoCompleteSource.CustomSource;
            provAddress.AutoCompleteCustomSource = acsc;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count < 1)
            {
                bEditContact.Enabled = false;
                bRemoveContact.Enabled = false;
            }
            else
            {
                bEditContact.Enabled = true;
                bRemoveContact.Enabled = true;
            }
        }

        private void bAddContact_Click(object sender, EventArgs e)
        {
            EntryForm_ContactDetails ecd = new EntryForm_ContactDetails(listView1);
            ecd.ShowDialog();
            ecd.Dispose();
        }

      private void Combobox_Enter(object sender, EventArgs e)
      {
         
      }
      private void ComboBox_Leave(object sender, EventArgs e)
      {
         ComboBox c = (ComboBox)sender;
         c.DropDownStyle = ComboBoxStyle.DropDown;
      }

        private void BEditContact_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count > 0)
            {
                EntryForm_ContactDetails ecd = new EntryForm_ContactDetails(listView1, listView1.SelectedItems[0] );
                if(ecd.ShowDialog() == DialogResult.OK)
                {

                }
                ecd.Dispose();
            }
        }

        private void BRemoveContact_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count > 0)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete this contact?", "Delete Contact", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if(result == DialogResult.Yes)
                {
                    if(Convert.ToString(listView1.SelectedItems[0].Tag) == "new")
                    {
                        listView1.Items.Remove(listView1.SelectedItems[0]);
                    }
                    else
                    {
                        MessageBox.Show("Can't remove registered numbers.");
                    }
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string photoDirectory = FileManager.SelectFile("Select a photo", "Image files|*.png;*.jpg;*.jpeg");
            if(photoDirectory != null)
            {
                residentPhoto.Load(photoDirectory);
                residentPhoto.Tag = photoDirectory;
                residentPhoto.Image.Tag = "new";
            }
        }

      private void button2_Click_1(object sender, EventArgs e)
      {
         Form_CameraCapture fcc = new Form_CameraCapture(residentPhoto);
         fcc.ShowDialog();
         fcc.Dispose();
      }
   }
}
