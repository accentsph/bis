﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class FormSelectModem : Form
    {
        public FormSelectModem()
        {
            InitializeComponent();
        }

        private void FormSelectModem_Load(object sender, EventArgs e)
        {
            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'"))
            {
                var portnames = SerialPort.GetPortNames();
                var ports = searcher.Get().Cast<ManagementBaseObject>().ToList().Select(p => p["Caption"].ToString());
                cboPortName.DataSource = portnames.Select(n => new KeyValuePair<string, string>(n, ports.FirstOrDefault(s => s.Contains(n)))).ToList();
                cboPortName.ValueMember = "Key";
                cboPortName.DisplayMember = "Value";
            }
            if (cboPortName.Items.Count > 0)
            {
                cboPortName.SelectedIndex = 0;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SMSModem modem = SMSModem.GetInstance();
            if(modem.OpenPort(this.cboPortName.SelectedValue.ToString(), Convert.ToInt32(cboBaudRate.Text)))
            {
                Close();
            }
            else
            {
                MessageBox.Show("Connection Error.");
            }
        }
    }
}
