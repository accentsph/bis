﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
   public partial class EntryForm_Client : Form
   {
      public EntryForm_Client()
      {
         InitializeComponent();
      }



      private void bClose_Click(object sender, EventArgs e)
      {
         Close();
      }
      public void SetHeight(decimal mhght)
      {
         Height.Value = mhght;
      }
      public void SetWeight(decimal mwght)
      {
         Weight.Value = mwght;
      }

      private void EntryForm_Client_Resize(object sender, EventArgs e)
      {
         int wid = splitContainer13.Panel1.Width/3;
         bPDetails.Width = wid;
         bFP.Width = wid;
         bConInf.Width = wid;
      }
      private void OpenConverter(object sender, EventArgs e)
      {
         Form_ConvertHeightWeight convert = new Form_ConvertHeightWeight(this);
         convert.ShowDialog();
         convert.Dispose();
      }
      private void EntryForm_Client_Load(object sender, EventArgs e)
      {
         FormManager.ProcessAddressComboData(AppConfiguration.GetConfig("barangayid"), BrgyID,CityID,ProvID);
         FormManager.ProcessComboBoxes(this, "Populate",  true);
         SetTextBoxAutoComplete();
         //SetTextBoxToUpperCase();
         RegisterMyEventHandlers(this);
      }
      private void RegisterMyEventHandlers(Control control)
      {
         foreach (Control ctl in control.Controls)
         {
            if (ctl is TextBox || ctl is ComboBox || ctl is DateTimePicker || ctl is RichTextBox || ctl is NumericUpDown)
            {
               ctl.KeyDown += Enter_KeyDown;
               if (ctl is ComboBox)
               {
                  ctl.Enter += ComboBox_Enter;
               }
               if (ctl is TextBox)
               {
                  TextBox tb = ctl as TextBox;
                  if (tb != null)
                  {
                     tb.CharacterCasing = CharacterCasing.Upper;
                  }
               }
            }
            RegisterMyEventHandlers(ctl);
         }
      }

      private void ComboBox_Enter(object sender, EventArgs e)
      {
         ComboBox cb = sender as ComboBox;
         if (cb != null)
         {
            if (!cb.DroppedDown)
            {
               cb.DroppedDown = true;
            }
         }

      }
      private void Enter_KeyDown(object sender, KeyEventArgs e)
      {
         Keys key = e.KeyData;
         if (key == Keys.Enter)
         {
            SendKeys.Send("{TAB}");
            e.Handled = true;
            e.SuppressKeyPress = true;
            //if (FormControlsManager.IsSenderForEnter(sender))
            //{
            //   DataControlValidation(sender);
            //}
            //else
            //{
            //   MessageBox.Show("Gikan sa " + FormControlsManager.SenderName(sender));
            //}
         }
      }
      
      private void SetTextBoxAutoComplete()
      {
         string sqlstr = "SELECT Particulars FROM dbstandarddata.vwcityprov ORDER BY Particulars;";
         AutoCompleteStringCollection acsc = FormManager.TextBoxAutoCompSource(sqlstr);
         ProvAddress.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
         POB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
         POD.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

         ProvAddress.AutoCompleteSource = AutoCompleteSource.CustomSource;
         POB.AutoCompleteSource = AutoCompleteSource.CustomSource;
         POD.AutoCompleteSource = AutoCompleteSource.CustomSource;

         ProvAddress.AutoCompleteCustomSource = acsc;
         POB.AutoCompleteCustomSource = acsc;
         POD.AutoCompleteCustomSource = acsc;
      }


      private void BSetReader_Click(object sender, EventArgs e)
      {

      }

      private void BtnCancel_Click(object sender, EventArgs e)
      {

      }

      private void panel8_Paint(object sender, PaintEventArgs e)
      {

      }

      private void panel19_Paint_1(object sender, PaintEventArgs e)
      {

      }

      private void label18_Click(object sender, EventArgs e)
      {

      }

      private void splitContainer7_Panel2_Paint(object sender, PaintEventArgs e)
      {

      }

      private void checkBox1_CheckedChanged(object sender, EventArgs e)
      {

      }

      private void comboBox15_SelectedIndexChanged(object sender, EventArgs e)
      {
      }

      private void purokID_SelectedIndexChanged(object sender, EventArgs e)
      {

      }
   }
}
