﻿namespace BIS
{
    partial class Form_FPReading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tReaders = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pbFP = new System.Windows.Forms.PictureBox();
            this.tResults = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbFP)).BeginInit();
            this.SuspendLayout();
            // 
            // tReaders
            // 
            this.tReaders.BackColor = System.Drawing.Color.White;
            this.tReaders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tReaders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tReaders.FormattingEnabled = true;
            this.tReaders.Location = new System.Drawing.Point(63, 12);
            this.tReaders.Name = "tReaders";
            this.tReaders.Size = new System.Drawing.Size(166, 21);
            this.tReaders.TabIndex = 0;
            this.tReaders.SelectedIndexChanged += new System.EventHandler(this.TReaders_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Reader:";
            // 
            // pbFP
            // 
            this.pbFP.BackColor = System.Drawing.Color.White;
            this.pbFP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbFP.Location = new System.Drawing.Point(15, 50);
            this.pbFP.Name = "pbFP";
            this.pbFP.Size = new System.Drawing.Size(220, 220);
            this.pbFP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFP.TabIndex = 2;
            this.pbFP.TabStop = false;
            // 
            // tResults
            // 
            this.tResults.BackColor = System.Drawing.Color.White;
            this.tResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tResults.Location = new System.Drawing.Point(261, 50);
            this.tResults.Multiline = true;
            this.tResults.Name = "tResults";
            this.tResults.ReadOnly = true;
            this.tResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tResults.Size = new System.Drawing.Size(218, 158);
            this.tResults.TabIndex = 3;
            this.tResults.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(308, 233);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 37);
            this.button2.TabIndex = 5;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Form_FPReading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(507, 291);
            this.ControlBox = false;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tResults);
            this.Controls.Add(this.pbFP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tReaders);
            this.Name = "Form_FPReading";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Fingerprint Reading";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_FPReading_FormClosed);
            this.Load += new System.EventHandler(this.Form_FPReading_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbFP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox tReaders;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbFP;
        private System.Windows.Forms.TextBox tResults;
        private System.Windows.Forms.Button button2;
    }
}