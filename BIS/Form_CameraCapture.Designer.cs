﻿namespace BIS
{
   partial class Form_CameraCapture
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.menuPanel = new System.Windows.Forms.Panel();
         this.bClose = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.panel1 = new System.Windows.Forms.Panel();
         this.bConfirm = new System.Windows.Forms.Button();
         this.bNext = new System.Windows.Forms.Button();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.comboBox2 = new System.Windows.Forms.ComboBox();
         this.label3 = new System.Windows.Forms.Label();
         this.comboBox1 = new System.Windows.Forms.ComboBox();
         this.label2 = new System.Windows.Forms.Label();
         this.pictureBox1 = new System.Windows.Forms.PictureBox();
         this.menuPanel.SuspendLayout();
         this.panel1.SuspendLayout();
         this.groupBox1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
         this.SuspendLayout();
         // 
         // menuPanel
         // 
         this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.menuPanel.Controls.Add(this.bClose);
         this.menuPanel.Controls.Add(this.label1);
         this.menuPanel.Cursor = System.Windows.Forms.Cursors.Default;
         this.menuPanel.Dock = System.Windows.Forms.DockStyle.Top;
         this.menuPanel.Location = new System.Drawing.Point(0, 0);
         this.menuPanel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.menuPanel.Name = "menuPanel";
         this.menuPanel.Size = new System.Drawing.Size(912, 60);
         this.menuPanel.TabIndex = 101;
         // 
         // bClose
         // 
         this.bClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
         this.bClose.Cursor = System.Windows.Forms.Cursors.Hand;
         this.bClose.Dock = System.Windows.Forms.DockStyle.Right;
         this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bClose.ForeColor = System.Drawing.Color.Silver;
         this.bClose.Location = new System.Drawing.Point(852, 0);
         this.bClose.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.bClose.Name = "bClose";
         this.bClose.Size = new System.Drawing.Size(60, 60);
         this.bClose.TabIndex = 5;
         this.bClose.Text = "X";
         this.bClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.bClose.Click += new System.EventHandler(this.bClose_Click);
         // 
         // label1
         // 
         this.label1.Dock = System.Windows.Forms.DockStyle.Left;
         this.label1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.ForeColor = System.Drawing.Color.White;
         this.label1.Location = new System.Drawing.Point(0, 0);
         this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(170, 60);
         this.label1.TabIndex = 0;
         this.label1.Text = "Camera";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel1
         // 
         this.panel1.Controls.Add(this.bConfirm);
         this.panel1.Controls.Add(this.bNext);
         this.panel1.Controls.Add(this.groupBox1);
         this.panel1.Controls.Add(this.pictureBox1);
         this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.panel1.Location = new System.Drawing.Point(0, 60);
         this.panel1.Name = "panel1";
         this.panel1.Padding = new System.Windows.Forms.Padding(10, 10, 10, 50);
         this.panel1.Size = new System.Drawing.Size(912, 463);
         this.panel1.TabIndex = 102;
         // 
         // bConfirm
         // 
         this.bConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bConfirm.Enabled = false;
         this.bConfirm.FlatAppearance.BorderSize = 0;
         this.bConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bConfirm.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bConfirm.ForeColor = System.Drawing.Color.White;
         this.bConfirm.Location = new System.Drawing.Point(671, 347);
         this.bConfirm.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bConfirm.Name = "bConfirm";
         this.bConfirm.Size = new System.Drawing.Size(155, 56);
         this.bConfirm.TabIndex = 42;
         this.bConfirm.Text = "Confirm";
         this.bConfirm.UseVisualStyleBackColor = false;
         this.bConfirm.Click += new System.EventHandler(this.bConfirm_Click);
         // 
         // bNext
         // 
         this.bNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bNext.FlatAppearance.BorderSize = 0;
         this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bNext.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bNext.ForeColor = System.Drawing.Color.White;
         this.bNext.Location = new System.Drawing.Point(671, 263);
         this.bNext.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bNext.Name = "bNext";
         this.bNext.Size = new System.Drawing.Size(155, 56);
         this.bNext.TabIndex = 41;
         this.bNext.Text = "Capture";
         this.bNext.UseVisualStyleBackColor = false;
         this.bNext.Click += new System.EventHandler(this.bNext_Click);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.comboBox2);
         this.groupBox1.Controls.Add(this.label3);
         this.groupBox1.Controls.Add(this.comboBox1);
         this.groupBox1.Controls.Add(this.label2);
         this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.groupBox1.ForeColor = System.Drawing.Color.RoyalBlue;
         this.groupBox1.Location = new System.Drawing.Point(617, 9);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(263, 192);
         this.groupBox1.TabIndex = 3;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Camera Settings";
         // 
         // comboBox2
         // 
         this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.comboBox2.FormattingEnabled = true;
         this.comboBox2.Location = new System.Drawing.Point(19, 131);
         this.comboBox2.Name = "comboBox2";
         this.comboBox2.Size = new System.Drawing.Size(227, 28);
         this.comboBox2.TabIndex = 4;
         this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
         this.label3.Location = new System.Drawing.Point(16, 110);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(79, 18);
         this.label3.TabIndex = 3;
         this.label3.Text = "Resolution";
         // 
         // comboBox1
         // 
         this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.comboBox1.FormattingEnabled = true;
         this.comboBox1.Location = new System.Drawing.Point(19, 66);
         this.comboBox1.Name = "comboBox1";
         this.comboBox1.Size = new System.Drawing.Size(227, 28);
         this.comboBox1.TabIndex = 2;
         this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
         this.label2.Location = new System.Drawing.Point(16, 45);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(61, 18);
         this.label2.TabIndex = 1;
         this.label2.Text = "Camera";
         // 
         // pictureBox1
         // 
         this.pictureBox1.BackColor = System.Drawing.Color.Black;
         this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
         this.pictureBox1.Location = new System.Drawing.Point(10, 10);
         this.pictureBox1.Name = "pictureBox1";
         this.pictureBox1.Size = new System.Drawing.Size(600, 403);
         this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
         this.pictureBox1.TabIndex = 0;
         this.pictureBox1.TabStop = false;
         // 
         // Form_CameraCapture
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.White;
         this.ClientSize = new System.Drawing.Size(912, 523);
         this.ControlBox = false;
         this.Controls.Add(this.panel1);
         this.Controls.Add(this.menuPanel);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Name = "Form_CameraCapture";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Form_CameraCapture";
         this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_CameraCapture_FormClosing);
         this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_CameraCapture_MouseDown);
         this.menuPanel.ResumeLayout(false);
         this.panel1.ResumeLayout(false);
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Panel menuPanel;
      private System.Windows.Forms.Label bClose;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.PictureBox pictureBox1;
      private System.Windows.Forms.ComboBox comboBox1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.ComboBox comboBox2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Button bNext;
      private System.Windows.Forms.Button bConfirm;
   }
}