﻿namespace BIS
{
   partial class MyTableLayoutPanel
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyTableLayoutPanel));
         this.MenuPanel = new System.Windows.Forms.Panel();
         this.isPerson = new System.Windows.Forms.CheckBox();
         this.ClientID = new System.Windows.Forms.TextBox();
         this.BtnSave = new System.Windows.Forms.Button();
         this.BtnUndo = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.bClose = new System.Windows.Forms.Label();
         this.tabControl1 = new System.Windows.Forms.TabControl();
         this.tabPage1 = new System.Windows.Forms.TabPage();
         this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
         this.eMailAddress = new System.Windows.Forms.TextBox();
         this.Telephone = new System.Windows.Forms.TextBox();
         this.Mobile = new System.Windows.Forms.TextBox();
         this.label16 = new System.Windows.Forms.Label();
         this.imageList2 = new System.Windows.Forms.ImageList(this.components);
         this.label15 = new System.Windows.Forms.Label();
         this.label14 = new System.Windows.Forms.Label();
         this.ProvAddress = new System.Windows.Forms.TextBox();
         this.label12 = new System.Windows.Forms.Label();
         this.label13 = new System.Windows.Forms.Label();
         this.CityID = new System.Windows.Forms.ComboBox();
         this.BrgyID = new System.Windows.Forms.ComboBox();
         this.label11 = new System.Windows.Forms.Label();
         this.label10 = new System.Windows.Forms.Label();
         this.label8 = new System.Windows.Forms.Label();
         this.label7 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.labelClientName = new System.Windows.Forms.Label();
         this.Alias = new System.Windows.Forms.TextBox();
         this.LastName = new System.Windows.Forms.TextBox();
         this.MiddleName = new System.Windows.Forms.TextBox();
         this.FirstName = new System.Windows.Forms.TextBox();
         this.label5 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.houseNo = new System.Windows.Forms.TextBox();
         this.purokID = new System.Windows.Forms.ComboBox();
         this.tabPage2 = new System.Windows.Forms.TabPage();
         this.MenuPanel.SuspendLayout();
         this.tabControl1.SuspendLayout();
         this.tabPage1.SuspendLayout();
         this.tableLayoutPanel1.SuspendLayout();
         this.SuspendLayout();
         // 
         // MenuPanel
         // 
         this.MenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.MenuPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.MenuPanel.Controls.Add(this.isPerson);
         this.MenuPanel.Controls.Add(this.ClientID);
         this.MenuPanel.Controls.Add(this.BtnSave);
         this.MenuPanel.Controls.Add(this.BtnUndo);
         this.MenuPanel.Controls.Add(this.label1);
         this.MenuPanel.Controls.Add(this.bClose);
         this.MenuPanel.Dock = System.Windows.Forms.DockStyle.Top;
         this.MenuPanel.Location = new System.Drawing.Point(0, 0);
         this.MenuPanel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.MenuPanel.Name = "MenuPanel";
         this.MenuPanel.Padding = new System.Windows.Forms.Padding(8);
         this.MenuPanel.Size = new System.Drawing.Size(1377, 70);
         this.MenuPanel.TabIndex = 2;
         // 
         // isPerson
         // 
         this.isPerson.AutoSize = true;
         this.isPerson.Checked = true;
         this.isPerson.CheckState = System.Windows.Forms.CheckState.Checked;
         this.isPerson.Location = new System.Drawing.Point(837, 24);
         this.isPerson.Name = "isPerson";
         this.isPerson.Size = new System.Drawing.Size(98, 27);
         this.isPerson.TabIndex = 11;
         this.isPerson.Text = "isPerson";
         this.isPerson.UseVisualStyleBackColor = true;
         this.isPerson.Visible = false;
         // 
         // ClientID
         // 
         this.ClientID.Location = new System.Drawing.Point(638, 30);
         this.ClientID.Name = "ClientID";
         this.ClientID.Size = new System.Drawing.Size(100, 29);
         this.ClientID.TabIndex = 10;
         this.ClientID.Text = "0";
         this.ClientID.Visible = false;
         // 
         // BtnSave
         // 
         this.BtnSave.Dock = System.Windows.Forms.DockStyle.Right;
         this.BtnSave.Enabled = false;
         this.BtnSave.Location = new System.Drawing.Point(1062, 8);
         this.BtnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.BtnSave.Name = "BtnSave";
         this.BtnSave.Size = new System.Drawing.Size(101, 50);
         this.BtnSave.TabIndex = 9;
         this.BtnSave.Text = "&Save";
         this.BtnSave.UseVisualStyleBackColor = true;
         // 
         // BtnUndo
         // 
         this.BtnUndo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.BtnUndo.Dock = System.Windows.Forms.DockStyle.Right;
         this.BtnUndo.Location = new System.Drawing.Point(1163, 8);
         this.BtnUndo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.BtnUndo.Name = "BtnUndo";
         this.BtnUndo.Padding = new System.Windows.Forms.Padding(6, 7, 6, 7);
         this.BtnUndo.Size = new System.Drawing.Size(101, 50);
         this.BtnUndo.TabIndex = 8;
         this.BtnUndo.Text = "Undo";
         this.BtnUndo.UseVisualStyleBackColor = true;
         // 
         // label1
         // 
         this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.ForeColor = System.Drawing.Color.White;
         this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
         this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label1.Location = new System.Drawing.Point(8, 8);
         this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label1.Name = "label1";
         this.label1.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
         this.label1.Size = new System.Drawing.Size(1256, 50);
         this.label1.TabIndex = 7;
         this.label1.Text = "     Client Entry";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // bClose
         // 
         this.bClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
         this.bClose.Dock = System.Windows.Forms.DockStyle.Right;
         this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bClose.ForeColor = System.Drawing.Color.Silver;
         this.bClose.Location = new System.Drawing.Point(1264, 8);
         this.bClose.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.bClose.Name = "bClose";
         this.bClose.Size = new System.Drawing.Size(101, 50);
         this.bClose.TabIndex = 6;
         this.bClose.Text = "X";
         this.bClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.bClose.Visible = false;
         // 
         // tabControl1
         // 
         this.tabControl1.Controls.Add(this.tabPage1);
         this.tabControl1.Controls.Add(this.tabPage2);
         this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
         this.tabControl1.ImageList = this.imageList2;
         this.tabControl1.Location = new System.Drawing.Point(0, 70);
         this.tabControl1.Multiline = true;
         this.tabControl1.Name = "tabControl1";
         this.tabControl1.Padding = new System.Drawing.Point(12, 6);
         this.tabControl1.SelectedIndex = 0;
         this.tabControl1.Size = new System.Drawing.Size(1377, 555);
         this.tabControl1.TabIndex = 3;
         // 
         // tabPage1
         // 
         this.tabPage1.Controls.Add(this.tableLayoutPanel1);
         this.tabPage1.Location = new System.Drawing.Point(4, 37);
         this.tabPage1.Name = "tabPage1";
         this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage1.Size = new System.Drawing.Size(1369, 514);
         this.tabPage1.TabIndex = 0;
         this.tabPage1.Text = "tabPage1";
         this.tabPage1.UseVisualStyleBackColor = true;
         // 
         // tableLayoutPanel1
         // 
         this.tableLayoutPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
         this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
         this.tableLayoutPanel1.ColumnCount = 4;
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
         this.tableLayoutPanel1.Controls.Add(this.eMailAddress, 2, 9);
         this.tableLayoutPanel1.Controls.Add(this.Telephone, 1, 9);
         this.tableLayoutPanel1.Controls.Add(this.Mobile, 0, 9);
         this.tableLayoutPanel1.Controls.Add(this.label16, 2, 8);
         this.tableLayoutPanel1.Controls.Add(this.label15, 1, 8);
         this.tableLayoutPanel1.Controls.Add(this.label14, 0, 8);
         this.tableLayoutPanel1.Controls.Add(this.ProvAddress, 1, 6);
         this.tableLayoutPanel1.Controls.Add(this.label12, 0, 6);
         this.tableLayoutPanel1.Controls.Add(this.label13, 0, 7);
         this.tableLayoutPanel1.Controls.Add(this.CityID, 3, 5);
         this.tableLayoutPanel1.Controls.Add(this.BrgyID, 2, 5);
         this.tableLayoutPanel1.Controls.Add(this.label11, 3, 4);
         this.tableLayoutPanel1.Controls.Add(this.label10, 2, 4);
         this.tableLayoutPanel1.Controls.Add(this.label8, 1, 4);
         this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
         this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
         this.tableLayoutPanel1.Controls.Add(this.labelClientName, 0, 0);
         this.tableLayoutPanel1.Controls.Add(this.Alias, 3, 2);
         this.tableLayoutPanel1.Controls.Add(this.LastName, 2, 2);
         this.tableLayoutPanel1.Controls.Add(this.MiddleName, 1, 2);
         this.tableLayoutPanel1.Controls.Add(this.FirstName, 0, 2);
         this.tableLayoutPanel1.Controls.Add(this.label5, 3, 1);
         this.tableLayoutPanel1.Controls.Add(this.label4, 2, 1);
         this.tableLayoutPanel1.Controls.Add(this.label3, 1, 1);
         this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
         this.tableLayoutPanel1.Controls.Add(this.houseNo, 0, 5);
         this.tableLayoutPanel1.Controls.Add(this.purokID, 1, 5);
         this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
         this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
         this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
         this.tableLayoutPanel1.Name = "tableLayoutPanel1";
         this.tableLayoutPanel1.RowCount = 10;
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
         this.tableLayoutPanel1.Size = new System.Drawing.Size(1363, 427);
         this.tableLayoutPanel1.TabIndex = 0;
         this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
         // 
         // eMailAddress
         // 
         this.tableLayoutPanel1.SetColumnSpan(this.eMailAddress, 2);
         this.eMailAddress.Dock = System.Windows.Forms.DockStyle.Fill;
         this.eMailAddress.Location = new System.Drawing.Point(683, 385);
         this.eMailAddress.Margin = new System.Windows.Forms.Padding(0, 10, 0, 5);
         this.eMailAddress.MaximumSize = new System.Drawing.Size(4, 29);
         this.eMailAddress.Multiline = true;
         this.eMailAddress.Name = "eMailAddress";
         this.eMailAddress.Size = new System.Drawing.Size(4, 29);
         this.eMailAddress.TabIndex = 32;
         // 
         // Telephone
         // 
         this.Telephone.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Telephone.Location = new System.Drawing.Point(343, 385);
         this.Telephone.Margin = new System.Windows.Forms.Padding(0, 10, 0, 5);
         this.Telephone.Name = "Telephone";
         this.Telephone.Size = new System.Drawing.Size(337, 29);
         this.Telephone.TabIndex = 31;
         // 
         // Mobile
         // 
         this.Mobile.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Mobile.Location = new System.Drawing.Point(3, 385);
         this.Mobile.Margin = new System.Windows.Forms.Padding(0, 10, 0, 5);
         this.Mobile.Name = "Mobile";
         this.Mobile.Size = new System.Drawing.Size(337, 29);
         this.Mobile.TabIndex = 30;
         // 
         // label16
         // 
         this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.tableLayoutPanel1.SetColumnSpan(this.label16, 2);
         this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label16.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label16.ForeColor = System.Drawing.Color.White;
         this.label16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label16.ImageKey = "icons8-group-message-40.png";
         this.label16.ImageList = this.imageList2;
         this.label16.Location = new System.Drawing.Point(687, 345);
         this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label16.Name = "label16";
         this.label16.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label16.Size = new System.Drawing.Size(669, 27);
         this.label16.TabIndex = 29;
         this.label16.Text = "          e-Mail Address";
         this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // imageList2
         // 
         this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
         this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
         this.imageList2.Images.SetKeyName(0, "icons8-home-page-40.png");
         this.imageList2.Images.SetKeyName(1, "icons8-building-40.png");
         this.imageList2.Images.SetKeyName(2, "icons8-contact-details-40.png");
         this.imageList2.Images.SetKeyName(3, "icons8-address-book-40.png");
         this.imageList2.Images.SetKeyName(4, "icons8-road-40.png");
         this.imageList2.Images.SetKeyName(5, "icons8-email-40.png");
         this.imageList2.Images.SetKeyName(6, "icons8-home-page-40.png");
         this.imageList2.Images.SetKeyName(7, "icons8-iphone-40.png");
         this.imageList2.Images.SetKeyName(8, "icons8-phone-40.png");
         this.imageList2.Images.SetKeyName(9, "icons8-group-message-40.png");
         this.imageList2.Images.SetKeyName(10, "icons8-automatic-40.png");
         this.imageList2.Images.SetKeyName(11, "icons8-user-40.png");
         this.imageList2.Images.SetKeyName(12, "icons8-overview-40.png");
         this.imageList2.Images.SetKeyName(13, "icons8-general-ledger-40.png");
         this.imageList2.Images.SetKeyName(14, "icons8-bill-40.png");
         this.imageList2.Images.SetKeyName(15, "icons8-people-40.png");
         this.imageList2.Images.SetKeyName(16, "icons8-user-groups-40.png");
         this.imageList2.Images.SetKeyName(17, "house_48.png");
         this.imageList2.Images.SetKeyName(18, "calendar_48.png");
         this.imageList2.Images.SetKeyName(19, "People.png");
         this.imageList2.Images.SetKeyName(20, "calendar_add.png");
         this.imageList2.Images.SetKeyName(21, "calendar_remove.png");
         this.imageList2.Images.SetKeyName(22, "digital_camera.png");
         this.imageList2.Images.SetKeyName(23, "cd.png");
         this.imageList2.Images.SetKeyName(24, "calendar_add.png");
         this.imageList2.Images.SetKeyName(25, "calendar_remove.png");
         this.imageList2.Images.SetKeyName(26, "Accessibility.png");
         this.imageList2.Images.SetKeyName(27, "MyPhone_Menu.png");
         this.imageList2.Images.SetKeyName(28, "Phone number.png");
         this.imageList2.Images.SetKeyName(29, "Mobile-phone.png");
         this.imageList2.Images.SetKeyName(30, "profile_edit.png");
         this.imageList2.Images.SetKeyName(31, "users_32.png");
         this.imageList2.Images.SetKeyName(32, "User group.png");
         this.imageList2.Images.SetKeyName(33, "user.png");
         // 
         // label15
         // 
         this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label15.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label15.ForeColor = System.Drawing.Color.White;
         this.label15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label15.ImageKey = "Phone number.png";
         this.label15.ImageList = this.imageList2;
         this.label15.Location = new System.Drawing.Point(347, 345);
         this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label15.Name = "label15";
         this.label15.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label15.Size = new System.Drawing.Size(329, 27);
         this.label15.TabIndex = 28;
         this.label15.Text = "        Tel. No.";
         this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label14
         // 
         this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label14.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label14.ForeColor = System.Drawing.Color.White;
         this.label14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label14.ImageKey = "Mobile-phone.png";
         this.label14.ImageList = this.imageList2;
         this.label14.Location = new System.Drawing.Point(7, 345);
         this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label14.Name = "label14";
         this.label14.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label14.Size = new System.Drawing.Size(329, 27);
         this.label14.TabIndex = 27;
         this.label14.Text = "       Mobile No.";
         this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // ProvAddress
         // 
         this.tableLayoutPanel1.SetColumnSpan(this.ProvAddress, 3);
         this.ProvAddress.Dock = System.Windows.Forms.DockStyle.Fill;
         this.ProvAddress.Location = new System.Drawing.Point(343, 261);
         this.ProvAddress.Margin = new System.Windows.Forms.Padding(0, 10, 0, 5);
         this.ProvAddress.Multiline = true;
         this.ProvAddress.Name = "ProvAddress";
         this.ProvAddress.Size = new System.Drawing.Size(1017, 33);
         this.ProvAddress.TabIndex = 26;
         // 
         // label12
         // 
         this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label12.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label12.ForeColor = System.Drawing.Color.White;
         this.label12.Location = new System.Drawing.Point(3, 261);
         this.label12.Margin = new System.Windows.Forms.Padding(0, 10, 0, 5);
         this.label12.Name = "label12";
         this.label12.Size = new System.Drawing.Size(337, 33);
         this.label12.TabIndex = 25;
         this.label12.Tag = "c";
         this.label12.Text = "Provincial Address";
         this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label13
         // 
         this.label13.BackColor = System.Drawing.Color.Navy;
         this.tableLayoutPanel1.SetColumnSpan(this.label13, 4);
         this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label13.ForeColor = System.Drawing.Color.White;
         this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label13.ImageKey = "icons8-user-40.png";
         this.label13.Location = new System.Drawing.Point(3, 302);
         this.label13.Margin = new System.Windows.Forms.Padding(0);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(1357, 40);
         this.label13.TabIndex = 24;
         this.label13.Text = "            Contact Details";
         this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // CityID
         // 
         this.CityID.Dock = System.Windows.Forms.DockStyle.Top;
         this.CityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.CityID.FormattingEnabled = true;
         this.CityID.IntegralHeight = false;
         this.CityID.ItemHeight = 22;
         this.CityID.Location = new System.Drawing.Point(1023, 210);
         this.CityID.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
         this.CityID.Name = "CityID";
         this.CityID.Size = new System.Drawing.Size(337, 30);
         this.CityID.TabIndex = 21;
         // 
         // BrgyID
         // 
         this.BrgyID.Dock = System.Windows.Forms.DockStyle.Top;
         this.BrgyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.BrgyID.FormattingEnabled = true;
         this.BrgyID.IntegralHeight = false;
         this.BrgyID.ItemHeight = 22;
         this.BrgyID.Location = new System.Drawing.Point(683, 210);
         this.BrgyID.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
         this.BrgyID.Name = "BrgyID";
         this.BrgyID.Size = new System.Drawing.Size(337, 30);
         this.BrgyID.TabIndex = 20;
         // 
         // label11
         // 
         this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label11.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label11.ForeColor = System.Drawing.Color.White;
         this.label11.Location = new System.Drawing.Point(1023, 170);
         this.label11.Margin = new System.Windows.Forms.Padding(0);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(337, 27);
         this.label11.TabIndex = 19;
         this.label11.Tag = "c";
         this.label11.Text = "Municipality / City";
         this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label10
         // 
         this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label10.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label10.ForeColor = System.Drawing.Color.White;
         this.label10.Location = new System.Drawing.Point(683, 170);
         this.label10.Margin = new System.Windows.Forms.Padding(0);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(337, 27);
         this.label10.TabIndex = 18;
         this.label10.Tag = "c";
         this.label10.Text = "Barangay";
         this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label8
         // 
         this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label8.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label8.ForeColor = System.Drawing.Color.White;
         this.label8.Location = new System.Drawing.Point(343, 170);
         this.label8.Margin = new System.Windows.Forms.Padding(0);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(337, 27);
         this.label8.TabIndex = 17;
         this.label8.Tag = "c";
         this.label8.Text = "Purok";
         this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label7
         // 
         this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label7.ForeColor = System.Drawing.Color.White;
         this.label7.Location = new System.Drawing.Point(3, 170);
         this.label7.Margin = new System.Windows.Forms.Padding(0);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(337, 27);
         this.label7.TabIndex = 16;
         this.label7.Tag = "c";
         this.label7.Text = "House No. / Street / Subdivision";
         this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label6
         // 
         this.label6.BackColor = System.Drawing.Color.Navy;
         this.tableLayoutPanel1.SetColumnSpan(this.label6, 4);
         this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label6.ForeColor = System.Drawing.Color.White;
         this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label6.ImageKey = "icons8-user-40.png";
         this.label6.Location = new System.Drawing.Point(3, 127);
         this.label6.Margin = new System.Windows.Forms.Padding(0);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(1357, 40);
         this.label6.TabIndex = 14;
         this.label6.Text = "            Address";
         this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // labelClientName
         // 
         this.labelClientName.BackColor = System.Drawing.Color.Navy;
         this.tableLayoutPanel1.SetColumnSpan(this.labelClientName, 4);
         this.labelClientName.Dock = System.Windows.Forms.DockStyle.Fill;
         this.labelClientName.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.labelClientName.ForeColor = System.Drawing.Color.White;
         this.labelClientName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.labelClientName.ImageKey = "icons8-user-40.png";
         this.labelClientName.Location = new System.Drawing.Point(3, 3);
         this.labelClientName.Margin = new System.Windows.Forms.Padding(0);
         this.labelClientName.Name = "labelClientName";
         this.labelClientName.Size = new System.Drawing.Size(1357, 40);
         this.labelClientName.TabIndex = 1;
         this.labelClientName.Text = "            Client Name";
         this.labelClientName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // Alias
         // 
         this.Alias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.Alias.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.Alias.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Alias.Location = new System.Drawing.Point(1023, 86);
         this.Alias.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
         this.Alias.Multiline = true;
         this.Alias.Name = "Alias";
         this.Alias.Size = new System.Drawing.Size(337, 28);
         this.Alias.TabIndex = 11;
         // 
         // LastName
         // 
         this.LastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.LastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.LastName.Dock = System.Windows.Forms.DockStyle.Fill;
         this.LastName.Location = new System.Drawing.Point(683, 86);
         this.LastName.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
         this.LastName.Multiline = true;
         this.LastName.Name = "LastName";
         this.LastName.Size = new System.Drawing.Size(337, 28);
         this.LastName.TabIndex = 10;
         // 
         // MiddleName
         // 
         this.MiddleName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.MiddleName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.MiddleName.Dock = System.Windows.Forms.DockStyle.Fill;
         this.MiddleName.Location = new System.Drawing.Point(343, 86);
         this.MiddleName.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
         this.MiddleName.Multiline = true;
         this.MiddleName.Name = "MiddleName";
         this.MiddleName.Size = new System.Drawing.Size(337, 28);
         this.MiddleName.TabIndex = 9;
         // 
         // FirstName
         // 
         this.FirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.FirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.FirstName.Dock = System.Windows.Forms.DockStyle.Fill;
         this.FirstName.Location = new System.Drawing.Point(3, 86);
         this.FirstName.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
         this.FirstName.Multiline = true;
         this.FirstName.Name = "FirstName";
         this.FirstName.Size = new System.Drawing.Size(337, 28);
         this.FirstName.TabIndex = 8;
         // 
         // label5
         // 
         this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.ForeColor = System.Drawing.Color.White;
         this.label5.Location = new System.Drawing.Point(1023, 46);
         this.label5.Margin = new System.Windows.Forms.Padding(0);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(337, 27);
         this.label5.TabIndex = 7;
         this.label5.Tag = "c";
         this.label5.Text = "Alias";
         this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label4
         // 
         this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label4.ForeColor = System.Drawing.Color.White;
         this.label4.Location = new System.Drawing.Point(683, 46);
         this.label4.Margin = new System.Windows.Forms.Padding(0);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(337, 27);
         this.label4.TabIndex = 6;
         this.label4.Tag = "c";
         this.label4.Text = "Last Name";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label3
         // 
         this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.ForeColor = System.Drawing.Color.White;
         this.label3.Location = new System.Drawing.Point(343, 46);
         this.label3.Margin = new System.Windows.Forms.Padding(0);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(337, 27);
         this.label3.TabIndex = 5;
         this.label3.Tag = "c";
         this.label3.Text = "Middle Name";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label2
         // 
         this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.ForeColor = System.Drawing.Color.White;
         this.label2.Location = new System.Drawing.Point(3, 46);
         this.label2.Margin = new System.Windows.Forms.Padding(0);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(337, 27);
         this.label2.TabIndex = 2;
         this.label2.Tag = "c";
         this.label2.Text = "First Name";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // houseNo
         // 
         this.houseNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.houseNo.Dock = System.Windows.Forms.DockStyle.Top;
         this.houseNo.Location = new System.Drawing.Point(3, 210);
         this.houseNo.Margin = new System.Windows.Forms.Padding(0, 10, 0, 8);
         this.houseNo.Multiline = true;
         this.houseNo.Name = "houseNo";
         this.houseNo.Size = new System.Drawing.Size(337, 30);
         this.houseNo.TabIndex = 13;
         // 
         // purokID
         // 
         this.purokID.Dock = System.Windows.Forms.DockStyle.Top;
         this.purokID.FormattingEnabled = true;
         this.purokID.IntegralHeight = false;
         this.purokID.ItemHeight = 22;
         this.purokID.Location = new System.Drawing.Point(343, 210);
         this.purokID.Margin = new System.Windows.Forms.Padding(0, 10, 0, 10);
         this.purokID.Name = "purokID";
         this.purokID.Size = new System.Drawing.Size(337, 30);
         this.purokID.TabIndex = 15;
         // 
         // tabPage2
         // 
         this.tabPage2.Location = new System.Drawing.Point(4, 37);
         this.tabPage2.Name = "tabPage2";
         this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage2.Size = new System.Drawing.Size(1369, 514);
         this.tabPage2.TabIndex = 1;
         this.tabPage2.Text = "tabPage2";
         this.tabPage2.UseVisualStyleBackColor = true;
         // 
         // MyTableLayoutPanel
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(1377, 692);
         this.Controls.Add(this.tabControl1);
         this.Controls.Add(this.MenuPanel);
         this.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.Name = "MyTableLayoutPanel";
         this.Text = "MyTableLayoutPanel";
         this.Load += new System.EventHandler(this.MyTableLayoutPanel_Load);
         this.MenuPanel.ResumeLayout(false);
         this.MenuPanel.PerformLayout();
         this.tabControl1.ResumeLayout(false);
         this.tabPage1.ResumeLayout(false);
         this.tableLayoutPanel1.ResumeLayout(false);
         this.tableLayoutPanel1.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Panel MenuPanel;
      private System.Windows.Forms.CheckBox isPerson;
      private System.Windows.Forms.TextBox ClientID;
      private System.Windows.Forms.Button BtnSave;
      private System.Windows.Forms.Button BtnUndo;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label bClose;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
      private System.Windows.Forms.TabPage tabPage2;
      private System.Windows.Forms.Label labelClientName;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox Alias;
      private System.Windows.Forms.TextBox LastName;
      private System.Windows.Forms.TextBox MiddleName;
      private System.Windows.Forms.TextBox FirstName;
      private System.Windows.Forms.ImageList imageList2;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox houseNo;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.ComboBox purokID;
      private System.Windows.Forms.ComboBox CityID;
      private System.Windows.Forms.ComboBox BrgyID;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.TextBox ProvAddress;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.TextBox Mobile;
      private System.Windows.Forms.TextBox Telephone;
      private System.Windows.Forms.TextBox eMailAddress;
   }
}