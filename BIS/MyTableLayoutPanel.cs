﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
   public partial class MyTableLayoutPanel : Form
   {
      public MyTableLayoutPanel()
      {
         InitializeComponent();
      }

      private void MyTableLayoutPanel_Load(object sender, EventArgs e)
      {
         ProcessAddressComboData();
         FormManager.ProcessComboBoxes(this, "Populate", true);
         RegisterMyEventHandlers(this);
      }
      private void RegisterMyEventHandlers(Control control)
      {
         foreach (Control ctl in control.Controls)
         {
            if (ctl is TextBox || ctl is ComboBox || ctl is DateTimePicker || ctl is RichTextBox || ctl is NumericUpDown)
            {
               ctl.KeyDown += Enter_KeyDown;
               if (ctl is ComboBox)
               {
                  ctl.Enter += ComboBox_Enter;
               }
               if (ctl is TextBox)
               {
                  TextBox tb = ctl as TextBox;
                  if (tb != null)
                  {
                     tb.CharacterCasing = CharacterCasing.Upper;
                  }
               }
            }
            RegisterMyEventHandlers(ctl);
         }
      }

      private void ComboBox_Enter(object sender, EventArgs e)
      {
         ComboBox cb = sender as ComboBox;
         if (cb != null)
         {
            if (!cb.DroppedDown)
            {
               cb.DroppedDown = true;
            }
         }

      }
      private void Enter_KeyDown(object sender, KeyEventArgs e)
      {
         Keys key = e.KeyData;
         if (key == Keys.Enter)
         {
            SendKeys.Send("{TAB}");
            e.Handled = true;
            e.SuppressKeyPress = true;
            SetGeneratedData(sender,e);
         }
      }
      private void SetGeneratedData(object sender, KeyEventArgs e)
      {
         string str1  =null , str2=null, str3=null, str4=null;
         //MessageBox.Show(sender.GetType().ToString());
         switch (sender.GetType().ToString())
         {
            case "System.Windows.Forms.TextBox":
               TextBox tb = (TextBox)sender;
               switch (tb.Name)
               {
                  case "FirstName":
                  case "MiddleName":
                  case "LastName":
                     str1 = "            Client Name ";
                     if (FirstName.Text.Length>0)
                     {
                        str2 = "  :      " + FirstName.Text.ToUpper() + " ";
                     }
                     if (MiddleName.Text.Length > 0)
                     {
                        str3 = MiddleName.Text.ToUpper() + " ";
                     }
                     if (LastName.Text.Length > 0)
                     {
                        str4 = LastName.Text.ToUpper();
                     }
                     labelClientName.Text = str1 + str2 + str3 + str4;
                     break;
                  default:
                     break;
               }
               break;
            default:
               break;
         }

      }
      private void ProcessAddressComboData()
      {
         MySQLDatabase db;
         List<SQLParameter> paramss = new List<SQLParameter>();
         DataTable dt = new DataTable();
         db = DatabaseManager.GetInstance();
         db.CloseConnection();
         object brgy = db.ScalarSelect("SELECT defaultbrgyid()");
         db.CloseConnection();
         if (brgy != null)
         {
            try
            {
               int defaultid = Convert.ToInt32(brgy.ToString());
               FormManager.SetComboBoxDataSource(BrgyID, "select b.id AS id,b.brgyDesc AS Barangay from dbstandarddata.refbrgy b where (b.id = " + defaultid.ToString() + ")", "id", "Barangay");
               FormManager.SetComboBoxDataSource(CityID, "select c.id AS id,c.citymunDesc AS City from dbstandarddata.refcitymun c join dbstandarddata.refbrgy b on c.id = b.cityID where b.id = " + defaultid.ToString(), "id", "City");
               //FormManager.SetComboBoxDataSource(ProvID, "select p.id AS id,p.provDesc AS Province from dbstandarddata.refprovince p join dbstandarddata.refcitymun c on p.id = c.provid join dbstandarddata.refbrgy b on c.id = b.cityID where b.id = " + defaultid.ToString(), "id", "Province");
            }
            catch (Exception e)
            {
               db.CloseConnection();
               MessageBox.Show(e.Message);

               throw;
            }

         }
      }

      private void label12_Click(object sender, EventArgs e)
      {

      }

      private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
      {

      }
   }
}
