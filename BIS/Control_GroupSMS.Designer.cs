﻿namespace BIS
{
    partial class Control_GroupSMS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wrapPanelRecepient = new System.Windows.Forms.Panel();
            this.panelll = new System.Windows.Forms.Panel();
            this.bClear = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tNum = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.wrapPanelMessage = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tMessage = new System.Windows.Forms.TextBox();
            this.bRemove = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.cname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cnumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cgroup = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cstatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.wrapPanelRecepient.SuspendLayout();
            this.panelll.SuspendLayout();
            this.wrapPanelMessage.SuspendLayout();
            this.SuspendLayout();
            // 
            // wrapPanelRecepient
            // 
            this.wrapPanelRecepient.Controls.Add(this.panelll);
            this.wrapPanelRecepient.Location = new System.Drawing.Point(10, 10);
            this.wrapPanelRecepient.MaximumSize = new System.Drawing.Size(541, 307);
            this.wrapPanelRecepient.MinimumSize = new System.Drawing.Size(541, 307);
            this.wrapPanelRecepient.Name = "wrapPanelRecepient";
            this.wrapPanelRecepient.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.wrapPanelRecepient.Size = new System.Drawing.Size(541, 307);
            this.wrapPanelRecepient.TabIndex = 0;
            // 
            // panelll
            // 
            this.panelll.BackColor = System.Drawing.Color.White;
            this.panelll.Controls.Add(this.listView1);
            this.panelll.Controls.Add(this.bRemove);
            this.panelll.Controls.Add(this.bClear);
            this.panelll.Controls.Add(this.button3);
            this.panelll.Controls.Add(this.button2);
            this.panelll.Controls.Add(this.tNum);
            this.panelll.Controls.Add(this.button1);
            this.panelll.Controls.Add(this.label1);
            this.panelll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelll.Location = new System.Drawing.Point(0, 0);
            this.panelll.Name = "panelll";
            this.panelll.Size = new System.Drawing.Size(541, 287);
            this.panelll.TabIndex = 0;
            // 
            // bClear
            // 
            this.bClear.BackColor = System.Drawing.Color.DarkRed;
            this.bClear.Enabled = false;
            this.bClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bClear.ForeColor = System.Drawing.Color.White;
            this.bClear.Location = new System.Drawing.Point(419, 247);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(96, 26);
            this.bClear.TabIndex = 9;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = false;
            this.bClear.Click += new System.EventHandler(this.Button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Navy;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(129, 247);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 26);
            this.button3.TabIndex = 6;
            this.button3.Text = "Add from groups";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Navy;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(27, 247);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 26);
            this.button2.TabIndex = 5;
            this.button2.Text = "Add all contacts";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // tNum
            // 
            this.tNum.FormattingEnabled = true;
            this.tNum.Location = new System.Drawing.Point(24, 50);
            this.tNum.Name = "tNum";
            this.tNum.Size = new System.Drawing.Size(190, 21);
            this.tNum.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Navy;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(220, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 26);
            this.button1.TabIndex = 3;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Recepient";
            // 
            // wrapPanelMessage
            // 
            this.wrapPanelMessage.BackColor = System.Drawing.Color.White;
            this.wrapPanelMessage.Controls.Add(this.button7);
            this.wrapPanelMessage.Controls.Add(this.button6);
            this.wrapPanelMessage.Controls.Add(this.label2);
            this.wrapPanelMessage.Controls.Add(this.tMessage);
            this.wrapPanelMessage.Location = new System.Drawing.Point(13, 323);
            this.wrapPanelMessage.MaximumSize = new System.Drawing.Size(377, 296);
            this.wrapPanelMessage.MinimumSize = new System.Drawing.Size(377, 296);
            this.wrapPanelMessage.Name = "wrapPanelMessage";
            this.wrapPanelMessage.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.wrapPanelMessage.Size = new System.Drawing.Size(377, 296);
            this.wrapPanelMessage.TabIndex = 1;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DarkRed;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(224, 229);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(119, 46);
            this.button7.TabIndex = 11;
            this.button7.Text = "Cancel Send";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Navy;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(33, 229);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(119, 46);
            this.button6.TabIndex = 9;
            this.button6.Text = "Send";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(8, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Message";
            // 
            // tMessage
            // 
            this.tMessage.Location = new System.Drawing.Point(33, 49);
            this.tMessage.MaxLength = 160;
            this.tMessage.Multiline = true;
            this.tMessage.Name = "tMessage";
            this.tMessage.Size = new System.Drawing.Size(310, 174);
            this.tMessage.TabIndex = 0;
            // 
            // bRemove
            // 
            this.bRemove.BackColor = System.Drawing.Color.DarkRed;
            this.bRemove.Enabled = false;
            this.bRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bRemove.ForeColor = System.Drawing.Color.White;
            this.bRemove.Location = new System.Drawing.Point(231, 247);
            this.bRemove.Name = "bRemove";
            this.bRemove.Size = new System.Drawing.Size(96, 26);
            this.bRemove.TabIndex = 11;
            this.bRemove.Text = "Remove";
            this.bRemove.UseVisualStyleBackColor = false;
            this.bRemove.Click += new System.EventHandler(this.Button5_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cname,
            this.cnumber,
            this.cgroup,
            this.cstatus});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(24, 80);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(491, 153);
            this.listView1.TabIndex = 12;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.ListView1_SelectedIndexChanged_2);
            // 
            // cname
            // 
            this.cname.Text = "Name";
            this.cname.Width = 180;
            // 
            // cnumber
            // 
            this.cnumber.Text = "Number";
            this.cnumber.Width = 100;
            // 
            // cgroup
            // 
            this.cgroup.Text = "Group";
            this.cgroup.Width = 100;
            // 
            // cstatus
            // 
            this.cstatus.Text = "Status";
            this.cstatus.Width = 90;
            // 
            // Control_GroupSMS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(239)))), ((int)(((byte)(243)))));
            this.Controls.Add(this.wrapPanelMessage);
            this.Controls.Add(this.wrapPanelRecepient);
            this.Name = "Control_GroupSMS";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(656, 663);
            this.Load += new System.EventHandler(this.Control_GroupSMS_Load);
            this.Resize += new System.EventHandler(this.Control_GroupSMS_Resize);
            this.wrapPanelRecepient.ResumeLayout(false);
            this.panelll.ResumeLayout(false);
            this.panelll.PerformLayout();
            this.wrapPanelMessage.ResumeLayout(false);
            this.wrapPanelMessage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel wrapPanelRecepient;
        private System.Windows.Forms.Panel panelll;
        private System.Windows.Forms.Panel wrapPanelMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox tNum;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tMessage;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bRemove;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader cname;
        private System.Windows.Forms.ColumnHeader cnumber;
        private System.Windows.Forms.ColumnHeader cgroup;
        private System.Windows.Forms.ColumnHeader cstatus;
    }
}
