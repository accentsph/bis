﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Threading;

namespace BIS
{
   public partial class Form_Settings : Form
   {

      private MySQLDatabase db;
      private MySQLDatabase otherdb;
      private Barangay brgy;
      private bool firstLoad = true;

      public Form_Settings()
      {
         InitializeComponent();
         try
         {
            db = DatabaseManager.GetInstance();
            otherdb = DatabaseManager.GetInstance("dbstandard");
            IDataReader reader = otherdb.Select("SELECT * FROM vwregions");
            FormManager.SetComboBoxDataSource(Region, reader);
            int bid = Convert.ToInt32(AppConfiguration.GetConfig("barangayid"));
            int cid = Convert.ToInt32(otherdb.ScalarSelect("SELECT cityid FROM refbrgy WHERE id='" + bid + "'"));
            int pid = Convert.ToInt32(otherdb.ScalarSelect("SELECT provid FROM refcitymun WHERE id='" + cid + "'"));
            int rid = Convert.ToInt32(otherdb.ScalarSelect("SELECT regid FROM refprovince WHERE id='" + pid + "'"));
            brgy = new Barangay(bid, cid, pid, rid);
            Region.SelectedValue = Convert.ToString(otherdb.ScalarSelect("SELECT regid FROM refprovince WHERE id=(SELECT provid FROM refcitymun WHERE id=(SELECT cityid from refbrgy WHERE id='" + AppConfiguration.GetConfig("barangayid") + "'))"));

         }
         catch (Exception err)
         {
            Console.WriteLine(err.StackTrace);
            Console.WriteLine(err.Message);
            tabControl1.SelectedTab = tabPage3;
         }
         FillTextBoxes(tableLayoutPanel1);
         FillTextBoxes(tableLayoutPanel2);
         FillTextBoxes(tableLayoutPanel3);
      }

      private void FillTextBoxes(Control parent)
      {
         string ekey = AppConfiguration.GetConfig("encryptionkey");
         foreach (Control c in parent.Controls)
         {
            if (c is TextBox)
            {
               string txt = AppConfiguration.GetConfig(c.Name);
               string tag = Convert.ToString(c.Tag);
               if (txt != null && c.Tag != null)
               {
                  //MessageBox.Show(txt);
                  if (tag == "encrypted" || tag == "databases")
                  {
                     txt = HashComputer.Encryptor.DecryptData(txt, ekey);
                  }
               }
               c.Text = txt;
            }
         }
      }

      private bool SaveSettings(Control parent)
      {
         string ekey = AppConfiguration.GetConfig("encryptionkey");
         bool flag = true;
         foreach (Control c in parent.Controls)
         {
            if (c is TextBox)
            {
               string txt = c.Text;
               string tag = Convert.ToString(c.Tag);
               if (txt != null && c.Tag != null)
               {
                  if (tag == "encrypted" || tag == "databases")
                  {
                     txt = HashComputer.Encryptor.EncryptData(txt, ekey);
                  }
               }
               if (!(flag = AppConfiguration.SetConfig(c.Name, txt)))
               {
                  break;
               }
            }
         }
         return flag;
      }

      private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
      {

      }

      private void Button2_Click(object sender, EventArgs e)
      {
         this.Close();
         DialogResult = DialogResult.Cancel;
      }

      private void Region_SelectedValueChanged(object sender, EventArgs e)
      {
         IDataReader reader = otherdb.Select("SELECT sysId, Description FROM vwprovinces WHERE regID='" + Convert.ToString(Region.SelectedValue) + "'");
         FormManager.SetComboBoxDataSource(Province, reader);
         if (firstLoad && brgy != null)
         {
            Province.SelectedValue = brgy.ProvinceID;
         }
         else
         {
            if (Province.Items.Count > 0)
            {
               Province.SelectedIndex = 0;
            }
         }
      }

      private void Province_SelectedValueChanged(object sender, EventArgs e)
      {
         IDataReader reader = otherdb.Select("SELECT sysId, Description FROM vwcities WHERE provID='" + Convert.ToString(Province.SelectedValue) + "'");
         FormManager.SetComboBoxDataSource(City, reader);
         if (firstLoad && brgy != null)
         {
            City.SelectedValue = brgy.CityID;
         }
      }

      private void City_SelectedValueChanged(object sender, EventArgs e)
      {
         IDataReader reader = otherdb.Select("SELECT sysId, Description FROM vwbarangays WHERE cityID='" + Convert.ToString(City.SelectedValue) + "'");
         FormManager.SetComboBoxDataSource(Barangay, reader);
         if (firstLoad && brgy != null)
         {
            Barangay.SelectedValue = brgy.BarangayID;
            firstLoad = false;
         }
      }

      private void Button1_Click(object sender, EventArgs e)
      {
         /*if (Barangay.SelectedValue == null)
         {
            MessageBox.Show("Please select a valid barangay");
            return;
         }*/
         if (Barangay.SelectedValue != null)
         {
            AppConfiguration.SetConfig("barangayid", Convert.ToString(Barangay.SelectedValue));
         }
         if (!TestConnection(dbmain))
         {
            return;
         }
         /*MySQLDatabase db = new MySQLDatabase(dbserver.Text, null, db_uid.Text, db_password.Text);
         if(!db.OpenConnection())
         {
             MessageBox.Show("Incorrect server/username/password");
             dbserver.Clear();
             db_uid.Clear();
             db_password.Clear();
             dbserver.Focus();
             return;
         }

         if(!TestConnection(dbstandard))
         {
             return;
         }
         if (!TestConnection(dbbiometrics))
         {
             return;
         }
         if (!TestConnection(dbsyslogs))
         {
             return;
         }*/
         if (SaveSettings(tableLayoutPanel1) && SaveSettings(tableLayoutPanel2) && SaveSettings(tableLayoutPanel3))
         {
            //DatabaseManager.AddToOtherDatabases("dbstandarddata", new MySQLDatabase(dbserver.Text, dbstandard.Text, ))
            foreach (Control c in tableLayoutPanel3.Controls)
            {
               if (c.Tag != null)
               {
                  string tag = Convert.ToString(c.Tag);
                  if (tag == "databases")
                  {
                     if (c.Name == "dbmain")
                     {
                        DatabaseManager.GetInstance(true);
                     }
                     else
                     {
                        DatabaseManager.GetInstance(c.Name, true);
                     }
                  }
               }
            }
            DialogResult = DialogResult.OK;
            this.Close();
         }
      }

      private bool TestConnection(TextBox tb)
      {
         bool flag = false;
         MySQLDatabase db = new MySQLDatabase(dbserver.Text, tb.Text, db_uid.Text, db_password.Text);
         flag = db.OpenConnection();
         if (!flag)
         {
            tb.Clear();
            tb.Focus();
         }
         return flag;
      }

      private void PreviousTab_Click(object sender, EventArgs e)
      {
         switch (tabControl1.SelectedTab.Name)
         {
            case "tabPage3":
               tabControl1.SelectedTab = tabPage2;
               tabPage2.Select();
               break;
            case "tabPage2":
               tabControl1.SelectedTab = tabPage1;
               tabPage1.Select();
               break;
            default:
               break;
         }


         NextTab.Enabled = tabControl1.SelectedTab != tabPage3;
         PreviousTab.Enabled = tabControl1.SelectedTab != tabPage1;
      }

      private void NextTab_Click(object sender, EventArgs e)
      {
         switch (tabControl1.SelectedTab.Name)
         {
            case "tabPage1":
               tabControl1.SelectedTab = tabPage2;
               tabPage2.Select();
               break;
            case "tabPage2":
               tabControl1.SelectedTab = tabPage3;
               tabPage3.Select();
               break;
            default:
               break;
         }
         NextTab.Enabled = tabControl1.SelectedTab != tabPage3;
         PreviousTab.Enabled = tabControl1.SelectedTab != tabPage1;
      }

      private void label25_Click(object sender, EventArgs e)
      {
         AppIcon.Text = FileManager.SelectFile("Select Icon", "Icons|*.ico");
      }

      private void label24_Click(object sender, EventArgs e)
      {
         BrgyLogo.Text = FileManager.SelectFile("Select Image", "Image files|*.ico;*.jpeg;*.jpg;*.png");
      }

      private void label26_Click(object sender, EventArgs e)
      {
         CityLogo.Text = FileManager.SelectFile("Select Image", "Image files|*.ico;*.jpeg;*.jpg;*.png");
      }

      private void label28_Click(object sender, EventArgs e)
      {
         SystemPath.Text = FileManager.SelectedFolder("Select Default Application Folder");
      }

      private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
      {

      }

      private void label29_Click(object sender, EventArgs e)
      {
         PhotoPath.Text = FileManager.SelectedFolder("Select Photo/Images Files Folder");
      }

      private void label31_Click(object sender, EventArgs e)
      {
         SignaturePath.Text = FileManager.SelectedFolder("Select Signature Specimen Files Folder");
      }

      private void label30_Click(object sender, EventArgs e)
      {
         FIDPath.Text = FileManager.SelectedFolder("Select FID Files Folder");
      }

      private void label32_Click(object sender, EventArgs e)
      {
         ReportsPath.Text = FileManager.SelectedFolder("Select Photo/Images Files Folder");
      }

      private void label34_Click(object sender, EventArgs e)
      {
         BrgyFilesPath.Text = FileManager.SelectedFolder("Select Barangay Files Folder");
      }
   }
}
