﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
   public partial class EntryForm_Business : Form
   {
      //Int32 Wide = 1250;
      //Int32 Narrow = 750;
      public EntryForm_Business()
      {
         InitializeComponent();
      }

      private void EntryForm_Business_Load(object sender, EventArgs e)
      {
         FormManager.ProcessAddressComboData(AppConfiguration.GetConfig("baranggayid"), BrgyID,CityID);
         FormManager.ProcessComboBoxes(this, "Businesses", false);
         FormManager.ProcessComboBoxes(this, "Populate", true);
         RegisterMyEventHandlers(this);
      }
      private void RegisterMyEventHandlers(Control control)
      {
         foreach (Control ctl in control.Controls)
         {
            if (ctl is TextBox || ctl is ComboBox || ctl is DateTimePicker || ctl is RichTextBox || ctl is NumericUpDown)
            {
               ctl.KeyDown += Enter_KeyDown;
               if (ctl is ComboBox)
               {
                  ctl.Enter += ComboBox_Enter;
               }
               if (ctl is TextBox)
               {
                  TextBox tb = ctl as TextBox;
                  if (tb != null)
                  {
                     tb.CharacterCasing = CharacterCasing.Upper;
                  }
               }
            }
            RegisterMyEventHandlers(ctl);
         }
      }

      private void ComboBox_Enter(object sender, EventArgs e)
      {
         ComboBox cb = sender as ComboBox;
         if (cb != null)
         {
            if (!cb.DroppedDown)
            {
               cb.DroppedDown = true;
            }
         }

      }
      private void Enter_KeyDown(object sender, KeyEventArgs e)
      {
         Keys key = e.KeyData;
         if (key == Keys.Enter)
         {
            SendKeys.Send("{TAB}");
            e.Handled = true;
            e.SuppressKeyPress = true;
         }
      }
   }
}
