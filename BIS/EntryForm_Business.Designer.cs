﻿namespace BIS
{
   partial class EntryForm_Business
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntryForm_Business));
         this.menuPanel = new System.Windows.Forms.Panel();
         this.TransID = new System.Windows.Forms.ComboBox();
         this.TransHeaderID = new System.Windows.Forms.TextBox();
         this.bClose = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.panel1 = new System.Windows.Forms.Panel();
         this.BtnSave = new System.Windows.Forms.Button();
         this.BtnUndo = new System.Windows.Forms.Button();
         this.splitMain = new System.Windows.Forms.SplitContainer();
         this.splitHeaderNonClient = new System.Windows.Forms.SplitContainer();
         this.label9 = new System.Windows.Forms.Label();
         this.splitContainer1 = new System.Windows.Forms.SplitContainer();
         this.label23 = new System.Windows.Forms.Label();
         this.listView1 = new System.Windows.Forms.ListView();
         this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
         this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
         this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
         this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
         this.imageList1 = new System.Windows.Forms.ImageList(this.components);
         this.panelRemarks = new System.Windows.Forms.Panel();
         this.label5 = new System.Windows.Forms.Label();
         this.panelAmount = new System.Windows.Forms.Panel();
         this.Amount = new System.Windows.Forms.TextBox();
         this.label8 = new System.Windows.Forms.Label();
         this.panelDateTo = new System.Windows.Forms.Panel();
         this.DateTo = new System.Windows.Forms.DateTimePicker();
         this.label7 = new System.Windows.Forms.Label();
         this.panelDateFrom = new System.Windows.Forms.Panel();
         this.DateFrom = new System.Windows.Forms.DateTimePicker();
         this.label6 = new System.Windows.Forms.Label();
         this.panelTransNo = new System.Windows.Forms.Panel();
         this.TransNo = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.panelDocID = new System.Windows.Forms.Panel();
         this.DocID = new System.Windows.Forms.ComboBox();
         this.label3 = new System.Windows.Forms.Label();
         this.panelTransDate = new System.Windows.Forms.Panel();
         this.TransDate = new System.Windows.Forms.DateTimePicker();
         this.label2 = new System.Windows.Forms.Label();
         this.panelClientID = new System.Windows.Forms.Panel();
         this.ClientID = new System.Windows.Forms.ComboBox();
         this.label11 = new System.Windows.Forms.Label();
         this.panelTransTypeID = new System.Windows.Forms.Panel();
         this.TransTypeID = new System.Windows.Forms.ComboBox();
         this.label19 = new System.Windows.Forms.Label();
         this.splitClients = new System.Windows.Forms.SplitContainer();
         this.labelIssuedTo = new System.Windows.Forms.Label();
         this.splitBusinessInfo = new System.Windows.Forms.SplitContainer();
         this.label13 = new System.Windows.Forms.Label();
         this.splitContainer2 = new System.Windows.Forms.SplitContainer();
         this.panel3 = new System.Windows.Forms.Panel();
         this.CapitalPaidUp = new System.Windows.Forms.TextBox();
         this.label28 = new System.Windows.Forms.Label();
         this.panelAuthorizedCapital = new System.Windows.Forms.Panel();
         this.CapitalAuthorized = new System.Windows.Forms.TextBox();
         this.label29 = new System.Windows.Forms.Label();
         this.panel4 = new System.Windows.Forms.Panel();
         this.textBox2 = new System.Windows.Forms.TextBox();
         this.panel2 = new System.Windows.Forms.Panel();
         this.label31 = new System.Windows.Forms.Label();
         this.textBox1 = new System.Windows.Forms.TextBox();
         this.panelBranchType = new System.Windows.Forms.Panel();
         this.BranchType = new System.Windows.Forms.ComboBox();
         this.label27 = new System.Windows.Forms.Label();
         this.panelOwnership = new System.Windows.Forms.Panel();
         this.Ownership = new System.Windows.Forms.ComboBox();
         this.label24 = new System.Windows.Forms.Label();
         this.panelCategory = new System.Windows.Forms.Panel();
         this.Category = new System.Windows.Forms.ComboBox();
         this.label26 = new System.Windows.Forms.Label();
         this.splitClientContacts = new System.Windows.Forms.SplitContainer();
         this.label15 = new System.Windows.Forms.Label();
         this.panelEMailAddress = new System.Windows.Forms.Panel();
         this.eMailAddress = new System.Windows.Forms.TextBox();
         this.label16 = new System.Windows.Forms.Label();
         this.panelTelephone = new System.Windows.Forms.Panel();
         this.Telephone = new System.Windows.Forms.TextBox();
         this.label17 = new System.Windows.Forms.Label();
         this.panelMobile = new System.Windows.Forms.Panel();
         this.Mobile = new System.Windows.Forms.TextBox();
         this.label18 = new System.Windows.Forms.Label();
         this.panelContactSalutation = new System.Windows.Forms.Panel();
         this.ContactSalutation = new System.Windows.Forms.TextBox();
         this.Salutation = new System.Windows.Forms.Label();
         this.panelContactDesignation = new System.Windows.Forms.Panel();
         this.ContactDesignation = new System.Windows.Forms.TextBox();
         this.label21 = new System.Windows.Forms.Label();
         this.panelContactPerson = new System.Windows.Forms.Panel();
         this.ContactPerson = new System.Windows.Forms.TextBox();
         this.label20 = new System.Windows.Forms.Label();
         this.panelAddress = new System.Windows.Forms.Panel();
         this.panelCityID = new System.Windows.Forms.Panel();
         this.CityID = new System.Windows.Forms.ComboBox();
         this.label25 = new System.Windows.Forms.Label();
         this.panelBrgyID = new System.Windows.Forms.Panel();
         this.BrgyID = new System.Windows.Forms.ComboBox();
         this.label14 = new System.Windows.Forms.Label();
         this.panelPurok = new System.Windows.Forms.Panel();
         this.purokID = new System.Windows.Forms.ComboBox();
         this.label22 = new System.Windows.Forms.Label();
         this.panelHouseNo = new System.Windows.Forms.Panel();
         this.houseNo = new System.Windows.Forms.TextBox();
         this.label12 = new System.Windows.Forms.Label();
         this.listBox1 = new System.Windows.Forms.ListBox();
         this.panelLastName = new System.Windows.Forms.Panel();
         this.LastName = new System.Windows.Forms.TextBox();
         this.label10 = new System.Windows.Forms.Label();
         this.menuPanel.SuspendLayout();
         this.panel1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
         this.splitMain.Panel1.SuspendLayout();
         this.splitMain.Panel2.SuspendLayout();
         this.splitMain.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitHeaderNonClient)).BeginInit();
         this.splitHeaderNonClient.Panel1.SuspendLayout();
         this.splitHeaderNonClient.Panel2.SuspendLayout();
         this.splitHeaderNonClient.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
         this.splitContainer1.Panel1.SuspendLayout();
         this.splitContainer1.Panel2.SuspendLayout();
         this.splitContainer1.SuspendLayout();
         this.panelRemarks.SuspendLayout();
         this.panelAmount.SuspendLayout();
         this.panelDateTo.SuspendLayout();
         this.panelDateFrom.SuspendLayout();
         this.panelTransNo.SuspendLayout();
         this.panelDocID.SuspendLayout();
         this.panelTransDate.SuspendLayout();
         this.panelClientID.SuspendLayout();
         this.panelTransTypeID.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitClients)).BeginInit();
         this.splitClients.Panel1.SuspendLayout();
         this.splitClients.Panel2.SuspendLayout();
         this.splitClients.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitBusinessInfo)).BeginInit();
         this.splitBusinessInfo.Panel1.SuspendLayout();
         this.splitBusinessInfo.Panel2.SuspendLayout();
         this.splitBusinessInfo.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
         this.splitContainer2.Panel1.SuspendLayout();
         this.splitContainer2.Panel2.SuspendLayout();
         this.splitContainer2.SuspendLayout();
         this.panel3.SuspendLayout();
         this.panelAuthorizedCapital.SuspendLayout();
         this.panel4.SuspendLayout();
         this.panel2.SuspendLayout();
         this.panelBranchType.SuspendLayout();
         this.panelOwnership.SuspendLayout();
         this.panelCategory.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitClientContacts)).BeginInit();
         this.splitClientContacts.Panel1.SuspendLayout();
         this.splitClientContacts.Panel2.SuspendLayout();
         this.splitClientContacts.SuspendLayout();
         this.panelEMailAddress.SuspendLayout();
         this.panelTelephone.SuspendLayout();
         this.panelMobile.SuspendLayout();
         this.panelContactSalutation.SuspendLayout();
         this.panelContactDesignation.SuspendLayout();
         this.panelContactPerson.SuspendLayout();
         this.panelAddress.SuspendLayout();
         this.panelCityID.SuspendLayout();
         this.panelBrgyID.SuspendLayout();
         this.panelPurok.SuspendLayout();
         this.panelHouseNo.SuspendLayout();
         this.panelLastName.SuspendLayout();
         this.SuspendLayout();
         // 
         // menuPanel
         // 
         this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.menuPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.menuPanel.Controls.Add(this.TransID);
         this.menuPanel.Controls.Add(this.TransHeaderID);
         this.menuPanel.Controls.Add(this.bClose);
         this.menuPanel.Controls.Add(this.label1);
         this.menuPanel.Dock = System.Windows.Forms.DockStyle.Top;
         this.menuPanel.Location = new System.Drawing.Point(0, 0);
         this.menuPanel.Margin = new System.Windows.Forms.Padding(5, 9, 5, 9);
         this.menuPanel.Name = "menuPanel";
         this.menuPanel.Size = new System.Drawing.Size(1250, 50);
         this.menuPanel.TabIndex = 1;
         // 
         // TransID
         // 
         this.TransID.FormattingEnabled = true;
         this.TransID.Location = new System.Drawing.Point(741, 3);
         this.TransID.Name = "TransID";
         this.TransID.Size = new System.Drawing.Size(124, 32);
         this.TransID.TabIndex = 7;
         this.TransID.Visible = false;
         // 
         // TransHeaderID
         // 
         this.TransHeaderID.Location = new System.Drawing.Point(606, 9);
         this.TransHeaderID.Name = "TransHeaderID";
         this.TransHeaderID.Size = new System.Drawing.Size(100, 32);
         this.TransHeaderID.TabIndex = 6;
         this.TransHeaderID.Visible = false;
         // 
         // bClose
         // 
         this.bClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
         this.bClose.Dock = System.Windows.Forms.DockStyle.Right;
         this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bClose.ForeColor = System.Drawing.Color.Silver;
         this.bClose.Location = new System.Drawing.Point(1134, 0);
         this.bClose.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
         this.bClose.Name = "bClose";
         this.bClose.Size = new System.Drawing.Size(112, 46);
         this.bClose.TabIndex = 5;
         this.bClose.Text = "X";
         this.bClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label1
         // 
         this.label1.Dock = System.Windows.Forms.DockStyle.Left;
         this.label1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.ForeColor = System.Drawing.Color.White;
         this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
         this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label1.Location = new System.Drawing.Point(0, 0);
         this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(455, 46);
         this.label1.TabIndex = 0;
         this.label1.Text = "     Business Permit";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel1
         // 
         this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel1.Controls.Add(this.BtnSave);
         this.panel1.Controls.Add(this.BtnUndo);
         this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panel1.Location = new System.Drawing.Point(0, 752);
         this.panel1.Margin = new System.Windows.Forms.Padding(4);
         this.panel1.Name = "panel1";
         this.panel1.Size = new System.Drawing.Size(1250, 50);
         this.panel1.TabIndex = 2;
         // 
         // BtnSave
         // 
         this.BtnSave.Dock = System.Windows.Forms.DockStyle.Right;
         this.BtnSave.Enabled = false;
         this.BtnSave.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.BtnSave.Location = new System.Drawing.Point(1012, 0);
         this.BtnSave.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.BtnSave.Name = "BtnSave";
         this.BtnSave.Size = new System.Drawing.Size(117, 46);
         this.BtnSave.TabIndex = 11;
         this.BtnSave.Text = "&Save";
         this.BtnSave.UseVisualStyleBackColor = true;
         // 
         // BtnUndo
         // 
         this.BtnUndo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.BtnUndo.Dock = System.Windows.Forms.DockStyle.Right;
         this.BtnUndo.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.BtnUndo.Location = new System.Drawing.Point(1129, 0);
         this.BtnUndo.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.BtnUndo.Name = "BtnUndo";
         this.BtnUndo.Padding = new System.Windows.Forms.Padding(8, 10, 8, 10);
         this.BtnUndo.Size = new System.Drawing.Size(117, 46);
         this.BtnUndo.TabIndex = 10;
         this.BtnUndo.Text = "Undo";
         this.BtnUndo.UseVisualStyleBackColor = true;
         // 
         // splitMain
         // 
         this.splitMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitMain.Location = new System.Drawing.Point(0, 50);
         this.splitMain.Margin = new System.Windows.Forms.Padding(4);
         this.splitMain.Name = "splitMain";
         // 
         // splitMain.Panel1
         // 
         this.splitMain.Panel1.Controls.Add(this.splitHeaderNonClient);
         this.splitMain.Panel1.Padding = new System.Windows.Forms.Padding(6, 8, 6, 8);
         // 
         // splitMain.Panel2
         // 
         this.splitMain.Panel2.Controls.Add(this.splitClients);
         this.splitMain.Panel2.Padding = new System.Windows.Forms.Padding(6, 8, 6, 8);
         this.splitMain.Size = new System.Drawing.Size(1250, 702);
         this.splitMain.SplitterDistance = 625;
         this.splitMain.SplitterWidth = 2;
         this.splitMain.TabIndex = 3;
         this.splitMain.TabStop = false;
         // 
         // splitHeaderNonClient
         // 
         this.splitHeaderNonClient.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitHeaderNonClient.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitHeaderNonClient.Location = new System.Drawing.Point(6, 8);
         this.splitHeaderNonClient.Name = "splitHeaderNonClient";
         this.splitHeaderNonClient.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitHeaderNonClient.Panel1
         // 
         this.splitHeaderNonClient.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitHeaderNonClient.Panel1.Controls.Add(this.label9);
         // 
         // splitHeaderNonClient.Panel2
         // 
         this.splitHeaderNonClient.Panel2.Controls.Add(this.splitContainer1);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelRemarks);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelAmount);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelDateTo);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelDateFrom);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelTransNo);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelDocID);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelTransDate);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelClientID);
         this.splitHeaderNonClient.Panel2.Controls.Add(this.panelTransTypeID);
         this.splitHeaderNonClient.Panel2.Padding = new System.Windows.Forms.Padding(4);
         this.splitHeaderNonClient.Size = new System.Drawing.Size(613, 686);
         this.splitHeaderNonClient.SplitterDistance = 46;
         this.splitHeaderNonClient.SplitterWidth = 2;
         this.splitHeaderNonClient.TabIndex = 0;
         // 
         // label9
         // 
         this.label9.BackColor = System.Drawing.Color.Transparent;
         this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label9.ForeColor = System.Drawing.Color.White;
         this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label9.ImageKey = "icons8-user-40.png";
         this.label9.Location = new System.Drawing.Point(0, 0);
         this.label9.Margin = new System.Windows.Forms.Padding(0);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(609, 42);
         this.label9.TabIndex = 1;
         this.label9.Text = "Transaction Details";
         this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // splitContainer1
         // 
         this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer1.Location = new System.Drawing.Point(4, 360);
         this.splitContainer1.Name = "splitContainer1";
         this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer1.Panel1
         // 
         this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer1.Panel1.Controls.Add(this.label23);
         // 
         // splitContainer1.Panel2
         // 
         this.splitContainer1.Panel2.Controls.Add(this.listView1);
         this.splitContainer1.Size = new System.Drawing.Size(605, 274);
         this.splitContainer1.SplitterDistance = 35;
         this.splitContainer1.SplitterWidth = 2;
         this.splitContainer1.TabIndex = 49;
         // 
         // label23
         // 
         this.label23.BackColor = System.Drawing.Color.Transparent;
         this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label23.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label23.ForeColor = System.Drawing.Color.White;
         this.label23.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label23.ImageKey = "(none)";
         this.label23.Location = new System.Drawing.Point(0, 0);
         this.label23.Name = "label23";
         this.label23.Size = new System.Drawing.Size(601, 31);
         this.label23.TabIndex = 1;
         this.label23.Text = "Prior Transaction Summary";
         this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // listView1
         // 
         this.listView1.Activation = System.Windows.Forms.ItemActivation.OneClick;
         this.listView1.Alignment = System.Windows.Forms.ListViewAlignment.Left;
         this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
         this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.listView1.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.listView1.FullRowSelect = true;
         this.listView1.GridLines = true;
         this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
         this.listView1.HideSelection = false;
         this.listView1.HoverSelection = true;
         this.listView1.Location = new System.Drawing.Point(0, 0);
         this.listView1.Name = "listView1";
         this.listView1.Size = new System.Drawing.Size(601, 233);
         this.listView1.SmallImageList = this.imageList1;
         this.listView1.TabIndex = 0;
         this.listView1.UseCompatibleStateImageBehavior = false;
         this.listView1.View = System.Windows.Forms.View.Details;
         // 
         // columnHeader1
         // 
         this.columnHeader1.Text = "Date";
         this.columnHeader1.Width = 90;
         // 
         // columnHeader2
         // 
         this.columnHeader2.Text = "Reference";
         this.columnHeader2.Width = 160;
         // 
         // columnHeader3
         // 
         this.columnHeader3.Text = "Period Covered";
         this.columnHeader3.Width = 150;
         // 
         // columnHeader4
         // 
         this.columnHeader4.Text = "Amount";
         this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
         this.columnHeader4.Width = 120;
         // 
         // imageList1
         // 
         this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
         this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
         this.imageList1.Images.SetKeyName(0, "icons8-calendar-40.png");
         this.imageList1.Images.SetKeyName(1, "icons8-cashbook-40.png");
         this.imageList1.Images.SetKeyName(2, "icons8-date-span-40.png");
         this.imageList1.Images.SetKeyName(3, "icons8-money-40.png");
         // 
         // panelRemarks
         // 
         this.panelRemarks.BackColor = System.Drawing.Color.White;
         this.panelRemarks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelRemarks.Controls.Add(this.label5);
         this.panelRemarks.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelRemarks.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelRemarks.Location = new System.Drawing.Point(4, 260);
         this.panelRemarks.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelRemarks.Name = "panelRemarks";
         this.panelRemarks.Padding = new System.Windows.Forms.Padding(1);
         this.panelRemarks.Size = new System.Drawing.Size(605, 100);
         this.panelRemarks.TabIndex = 48;
         // 
         // label5
         // 
         this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label5.Dock = System.Windows.Forms.DockStyle.Left;
         this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.ForeColor = System.Drawing.Color.White;
         this.label5.Location = new System.Drawing.Point(1, 1);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(200, 94);
         this.label5.TabIndex = 0;
         this.label5.Text = "Remarks";
         this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelAmount
         // 
         this.panelAmount.BackColor = System.Drawing.Color.White;
         this.panelAmount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelAmount.Controls.Add(this.Amount);
         this.panelAmount.Controls.Add(this.label8);
         this.panelAmount.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelAmount.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelAmount.Location = new System.Drawing.Point(4, 228);
         this.panelAmount.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelAmount.Name = "panelAmount";
         this.panelAmount.Padding = new System.Windows.Forms.Padding(1);
         this.panelAmount.Size = new System.Drawing.Size(605, 32);
         this.panelAmount.TabIndex = 47;
         // 
         // Amount
         // 
         this.Amount.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Amount.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Amount.Location = new System.Drawing.Point(201, 1);
         this.Amount.Name = "Amount";
         this.Amount.Size = new System.Drawing.Size(399, 32);
         this.Amount.TabIndex = 6;
         // 
         // label8
         // 
         this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label8.Dock = System.Windows.Forms.DockStyle.Left;
         this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label8.ForeColor = System.Drawing.Color.White;
         this.label8.Location = new System.Drawing.Point(1, 1);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(200, 26);
         this.label8.TabIndex = 0;
         this.label8.Text = "Amount Paid";
         this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelDateTo
         // 
         this.panelDateTo.BackColor = System.Drawing.Color.White;
         this.panelDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelDateTo.Controls.Add(this.DateTo);
         this.panelDateTo.Controls.Add(this.label7);
         this.panelDateTo.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelDateTo.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelDateTo.Location = new System.Drawing.Point(4, 196);
         this.panelDateTo.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelDateTo.Name = "panelDateTo";
         this.panelDateTo.Padding = new System.Windows.Forms.Padding(1, 3, 3, 3);
         this.panelDateTo.Size = new System.Drawing.Size(605, 32);
         this.panelDateTo.TabIndex = 46;
         // 
         // DateTo
         // 
         this.DateTo.Dock = System.Windows.Forms.DockStyle.Fill;
         this.DateTo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.DateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.DateTo.Location = new System.Drawing.Point(201, 3);
         this.DateTo.Name = "DateTo";
         this.DateTo.ShowUpDown = true;
         this.DateTo.Size = new System.Drawing.Size(397, 32);
         this.DateTo.TabIndex = 5;
         // 
         // label7
         // 
         this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label7.Dock = System.Windows.Forms.DockStyle.Left;
         this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label7.ForeColor = System.Drawing.Color.White;
         this.label7.Location = new System.Drawing.Point(1, 3);
         this.label7.MinimumSize = new System.Drawing.Size(0, 30);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(200, 30);
         this.label7.TabIndex = 0;
         this.label7.Text = "Valid Until";
         this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelDateFrom
         // 
         this.panelDateFrom.BackColor = System.Drawing.Color.White;
         this.panelDateFrom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelDateFrom.Controls.Add(this.DateFrom);
         this.panelDateFrom.Controls.Add(this.label6);
         this.panelDateFrom.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelDateFrom.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelDateFrom.Location = new System.Drawing.Point(4, 164);
         this.panelDateFrom.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelDateFrom.Name = "panelDateFrom";
         this.panelDateFrom.Padding = new System.Windows.Forms.Padding(1, 3, 3, 3);
         this.panelDateFrom.Size = new System.Drawing.Size(605, 32);
         this.panelDateFrom.TabIndex = 45;
         // 
         // DateFrom
         // 
         this.DateFrom.Dock = System.Windows.Forms.DockStyle.Fill;
         this.DateFrom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.DateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.DateFrom.Location = new System.Drawing.Point(201, 3);
         this.DateFrom.Name = "DateFrom";
         this.DateFrom.ShowUpDown = true;
         this.DateFrom.Size = new System.Drawing.Size(397, 32);
         this.DateFrom.TabIndex = 4;
         // 
         // label6
         // 
         this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label6.Dock = System.Windows.Forms.DockStyle.Left;
         this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label6.ForeColor = System.Drawing.Color.White;
         this.label6.Location = new System.Drawing.Point(1, 3);
         this.label6.MinimumSize = new System.Drawing.Size(0, 30);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(200, 30);
         this.label6.TabIndex = 0;
         this.label6.Text = "Valid From";
         this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelTransNo
         // 
         this.panelTransNo.BackColor = System.Drawing.Color.White;
         this.panelTransNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelTransNo.Controls.Add(this.TransNo);
         this.panelTransNo.Controls.Add(this.label4);
         this.panelTransNo.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelTransNo.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelTransNo.Location = new System.Drawing.Point(4, 132);
         this.panelTransNo.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelTransNo.Name = "panelTransNo";
         this.panelTransNo.Padding = new System.Windows.Forms.Padding(1);
         this.panelTransNo.Size = new System.Drawing.Size(605, 32);
         this.panelTransNo.TabIndex = 44;
         // 
         // TransNo
         // 
         this.TransNo.Dock = System.Windows.Forms.DockStyle.Fill;
         this.TransNo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.TransNo.Location = new System.Drawing.Point(201, 1);
         this.TransNo.Name = "TransNo";
         this.TransNo.Size = new System.Drawing.Size(399, 32);
         this.TransNo.TabIndex = 3;
         // 
         // label4
         // 
         this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label4.Dock = System.Windows.Forms.DockStyle.Left;
         this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label4.ForeColor = System.Drawing.Color.White;
         this.label4.Location = new System.Drawing.Point(1, 1);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(200, 26);
         this.label4.TabIndex = 0;
         this.label4.Text = "Document No.";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelDocID
         // 
         this.panelDocID.BackColor = System.Drawing.Color.White;
         this.panelDocID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelDocID.Controls.Add(this.DocID);
         this.panelDocID.Controls.Add(this.label3);
         this.panelDocID.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelDocID.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelDocID.Location = new System.Drawing.Point(4, 100);
         this.panelDocID.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelDocID.Name = "panelDocID";
         this.panelDocID.Padding = new System.Windows.Forms.Padding(1);
         this.panelDocID.Size = new System.Drawing.Size(605, 32);
         this.panelDocID.TabIndex = 43;
         // 
         // DocID
         // 
         this.DocID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.DocID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.DocID.FormattingEnabled = true;
         this.DocID.Location = new System.Drawing.Point(201, 1);
         this.DocID.Name = "DocID";
         this.DocID.Size = new System.Drawing.Size(399, 32);
         this.DocID.TabIndex = 2;
         this.DocID.Tag = "Businesses";
         // 
         // label3
         // 
         this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label3.Dock = System.Windows.Forms.DockStyle.Left;
         this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.ForeColor = System.Drawing.Color.White;
         this.label3.Location = new System.Drawing.Point(1, 1);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(200, 26);
         this.label3.TabIndex = 0;
         this.label3.Text = "Document";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelTransDate
         // 
         this.panelTransDate.BackColor = System.Drawing.Color.White;
         this.panelTransDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelTransDate.Controls.Add(this.TransDate);
         this.panelTransDate.Controls.Add(this.label2);
         this.panelTransDate.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelTransDate.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelTransDate.Location = new System.Drawing.Point(4, 68);
         this.panelTransDate.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelTransDate.Name = "panelTransDate";
         this.panelTransDate.Padding = new System.Windows.Forms.Padding(1, 3, 3, 3);
         this.panelTransDate.Size = new System.Drawing.Size(605, 32);
         this.panelTransDate.TabIndex = 42;
         // 
         // TransDate
         // 
         this.TransDate.Dock = System.Windows.Forms.DockStyle.Fill;
         this.TransDate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.TransDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.TransDate.Location = new System.Drawing.Point(201, 3);
         this.TransDate.MinimumSize = new System.Drawing.Size(4, 30);
         this.TransDate.Name = "TransDate";
         this.TransDate.ShowUpDown = true;
         this.TransDate.Size = new System.Drawing.Size(397, 32);
         this.TransDate.TabIndex = 1;
         // 
         // label2
         // 
         this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label2.Dock = System.Windows.Forms.DockStyle.Left;
         this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.ForeColor = System.Drawing.Color.White;
         this.label2.Location = new System.Drawing.Point(1, 3);
         this.label2.MinimumSize = new System.Drawing.Size(0, 30);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(200, 30);
         this.label2.TabIndex = 0;
         this.label2.Text = "Date Issued";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelClientID
         // 
         this.panelClientID.BackColor = System.Drawing.Color.White;
         this.panelClientID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelClientID.Controls.Add(this.ClientID);
         this.panelClientID.Controls.Add(this.label11);
         this.panelClientID.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelClientID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelClientID.Location = new System.Drawing.Point(4, 36);
         this.panelClientID.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelClientID.Name = "panelClientID";
         this.panelClientID.Padding = new System.Windows.Forms.Padding(1);
         this.panelClientID.Size = new System.Drawing.Size(605, 32);
         this.panelClientID.TabIndex = 34;
         // 
         // ClientID
         // 
         this.ClientID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.ClientID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.ClientID.FormattingEnabled = true;
         this.ClientID.Location = new System.Drawing.Point(201, 1);
         this.ClientID.Margin = new System.Windows.Forms.Padding(5);
         this.ClientID.Name = "ClientID";
         this.ClientID.Size = new System.Drawing.Size(399, 32);
         this.ClientID.TabIndex = 2;
         this.ClientID.Tag = "Businesses";
         this.ClientID.Text = "0";
         // 
         // label11
         // 
         this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label11.Dock = System.Windows.Forms.DockStyle.Left;
         this.label11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label11.ForeColor = System.Drawing.Color.White;
         this.label11.Location = new System.Drawing.Point(1, 1);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(200, 26);
         this.label11.TabIndex = 0;
         this.label11.Text = "Applicant";
         this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelTransTypeID
         // 
         this.panelTransTypeID.BackColor = System.Drawing.Color.White;
         this.panelTransTypeID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelTransTypeID.Controls.Add(this.TransTypeID);
         this.panelTransTypeID.Controls.Add(this.label19);
         this.panelTransTypeID.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelTransTypeID.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelTransTypeID.Location = new System.Drawing.Point(4, 4);
         this.panelTransTypeID.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelTransTypeID.Name = "panelTransTypeID";
         this.panelTransTypeID.Padding = new System.Windows.Forms.Padding(1);
         this.panelTransTypeID.Size = new System.Drawing.Size(605, 32);
         this.panelTransTypeID.TabIndex = 33;
         // 
         // TransTypeID
         // 
         this.TransTypeID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.TransTypeID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.TransTypeID.FormattingEnabled = true;
         this.TransTypeID.Location = new System.Drawing.Point(201, 1);
         this.TransTypeID.Name = "TransTypeID";
         this.TransTypeID.Size = new System.Drawing.Size(399, 32);
         this.TransTypeID.TabIndex = 2;
         this.TransTypeID.Tag = "Businesses";
         // 
         // label19
         // 
         this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label19.Dock = System.Windows.Forms.DockStyle.Left;
         this.label19.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label19.ForeColor = System.Drawing.Color.White;
         this.label19.Location = new System.Drawing.Point(1, 1);
         this.label19.Name = "label19";
         this.label19.Size = new System.Drawing.Size(200, 26);
         this.label19.TabIndex = 0;
         this.label19.Text = "Transaction Type";
         this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitClients
         // 
         this.splitClients.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitClients.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitClients.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitClients.Location = new System.Drawing.Point(6, 8);
         this.splitClients.Name = "splitClients";
         this.splitClients.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitClients.Panel1
         // 
         this.splitClients.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitClients.Panel1.Controls.Add(this.labelIssuedTo);
         // 
         // splitClients.Panel2
         // 
         this.splitClients.Panel2.Controls.Add(this.splitBusinessInfo);
         this.splitClients.Panel2.Controls.Add(this.splitClientContacts);
         this.splitClients.Panel2.Controls.Add(this.panelAddress);
         this.splitClients.Panel2.Controls.Add(this.panelLastName);
         this.splitClients.Panel2.Padding = new System.Windows.Forms.Padding(5);
         this.splitClients.Size = new System.Drawing.Size(611, 686);
         this.splitClients.SplitterDistance = 45;
         this.splitClients.SplitterWidth = 2;
         this.splitClients.TabIndex = 0;
         // 
         // labelIssuedTo
         // 
         this.labelIssuedTo.BackColor = System.Drawing.Color.Transparent;
         this.labelIssuedTo.Dock = System.Windows.Forms.DockStyle.Fill;
         this.labelIssuedTo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.labelIssuedTo.ForeColor = System.Drawing.Color.White;
         this.labelIssuedTo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.labelIssuedTo.ImageKey = "icons8-user-40.png";
         this.labelIssuedTo.Location = new System.Drawing.Point(0, 0);
         this.labelIssuedTo.Margin = new System.Windows.Forms.Padding(0);
         this.labelIssuedTo.Name = "labelIssuedTo";
         this.labelIssuedTo.Size = new System.Drawing.Size(607, 41);
         this.labelIssuedTo.TabIndex = 2;
         this.labelIssuedTo.Text = "Applicant Details";
         this.labelIssuedTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // splitBusinessInfo
         // 
         this.splitBusinessInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitBusinessInfo.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitBusinessInfo.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitBusinessInfo.Location = new System.Drawing.Point(5, 428);
         this.splitBusinessInfo.Name = "splitBusinessInfo";
         this.splitBusinessInfo.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitBusinessInfo.Panel1
         // 
         this.splitBusinessInfo.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitBusinessInfo.Panel1.Controls.Add(this.label13);
         // 
         // splitBusinessInfo.Panel2
         // 
         this.splitBusinessInfo.Panel2.Controls.Add(this.splitContainer2);
         this.splitBusinessInfo.Panel2.Controls.Add(this.panelBranchType);
         this.splitBusinessInfo.Panel2.Controls.Add(this.panelOwnership);
         this.splitBusinessInfo.Panel2.Controls.Add(this.panelCategory);
         this.splitBusinessInfo.Size = new System.Drawing.Size(601, 206);
         this.splitBusinessInfo.SplitterDistance = 35;
         this.splitBusinessInfo.SplitterWidth = 2;
         this.splitBusinessInfo.TabIndex = 23;
         // 
         // label13
         // 
         this.label13.BackColor = System.Drawing.Color.Transparent;
         this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label13.ForeColor = System.Drawing.Color.White;
         this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label13.ImageKey = "(none)";
         this.label13.Location = new System.Drawing.Point(0, 0);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(597, 31);
         this.label13.TabIndex = 1;
         this.label13.Text = "Business Details";
         this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // splitContainer2
         // 
         this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
         this.splitContainer2.IsSplitterFixed = true;
         this.splitContainer2.Location = new System.Drawing.Point(0, 96);
         this.splitContainer2.Name = "splitContainer2";
         // 
         // splitContainer2.Panel1
         // 
         this.splitContainer2.Panel1.Controls.Add(this.panel3);
         this.splitContainer2.Panel1.Controls.Add(this.panelAuthorizedCapital);
         // 
         // splitContainer2.Panel2
         // 
         this.splitContainer2.Panel2.Controls.Add(this.panel4);
         this.splitContainer2.Panel2.Controls.Add(this.panel2);
         this.splitContainer2.Size = new System.Drawing.Size(597, 69);
         this.splitContainer2.SplitterDistance = 464;
         this.splitContainer2.SplitterWidth = 1;
         this.splitContainer2.TabIndex = 27;
         // 
         // panel3
         // 
         this.panel3.BackColor = System.Drawing.Color.White;
         this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel3.Controls.Add(this.CapitalPaidUp);
         this.panel3.Controls.Add(this.label28);
         this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel3.Location = new System.Drawing.Point(0, 32);
         this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel3.Name = "panel3";
         this.panel3.Padding = new System.Windows.Forms.Padding(1);
         this.panel3.Size = new System.Drawing.Size(464, 32);
         this.panel3.TabIndex = 42;
         // 
         // CapitalPaidUp
         // 
         this.CapitalPaidUp.Dock = System.Windows.Forms.DockStyle.Fill;
         this.CapitalPaidUp.Location = new System.Drawing.Point(202, 1);
         this.CapitalPaidUp.Name = "CapitalPaidUp";
         this.CapitalPaidUp.Size = new System.Drawing.Size(257, 32);
         this.CapitalPaidUp.TabIndex = 6;
         // 
         // label28
         // 
         this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label28.Dock = System.Windows.Forms.DockStyle.Left;
         this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label28.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label28.ForeColor = System.Drawing.Color.White;
         this.label28.Location = new System.Drawing.Point(1, 1);
         this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label28.Name = "label28";
         this.label28.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label28.Size = new System.Drawing.Size(201, 26);
         this.label28.TabIndex = 2;
         this.label28.Text = "Paid-up Capital";
         this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelAuthorizedCapital
         // 
         this.panelAuthorizedCapital.BackColor = System.Drawing.Color.White;
         this.panelAuthorizedCapital.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelAuthorizedCapital.Controls.Add(this.CapitalAuthorized);
         this.panelAuthorizedCapital.Controls.Add(this.label29);
         this.panelAuthorizedCapital.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelAuthorizedCapital.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelAuthorizedCapital.Location = new System.Drawing.Point(0, 0);
         this.panelAuthorizedCapital.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelAuthorizedCapital.Name = "panelAuthorizedCapital";
         this.panelAuthorizedCapital.Padding = new System.Windows.Forms.Padding(1);
         this.panelAuthorizedCapital.Size = new System.Drawing.Size(464, 32);
         this.panelAuthorizedCapital.TabIndex = 41;
         // 
         // CapitalAuthorized
         // 
         this.CapitalAuthorized.Dock = System.Windows.Forms.DockStyle.Fill;
         this.CapitalAuthorized.Location = new System.Drawing.Point(202, 1);
         this.CapitalAuthorized.Name = "CapitalAuthorized";
         this.CapitalAuthorized.Size = new System.Drawing.Size(257, 32);
         this.CapitalAuthorized.TabIndex = 6;
         // 
         // label29
         // 
         this.label29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label29.Dock = System.Windows.Forms.DockStyle.Left;
         this.label29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label29.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label29.ForeColor = System.Drawing.Color.White;
         this.label29.Location = new System.Drawing.Point(1, 1);
         this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label29.Name = "label29";
         this.label29.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label29.Size = new System.Drawing.Size(201, 26);
         this.label29.TabIndex = 2;
         this.label29.Text = "Authorized Capital";
         this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel4
         // 
         this.panel4.BackColor = System.Drawing.Color.White;
         this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel4.Controls.Add(this.textBox2);
         this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel4.Location = new System.Drawing.Point(0, 32);
         this.panel4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel4.Name = "panel4";
         this.panel4.Padding = new System.Windows.Forms.Padding(1);
         this.panel4.Size = new System.Drawing.Size(132, 32);
         this.panel4.TabIndex = 43;
         // 
         // textBox2
         // 
         this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.textBox2.Location = new System.Drawing.Point(1, 1);
         this.textBox2.Name = "textBox2";
         this.textBox2.Size = new System.Drawing.Size(126, 32);
         this.textBox2.TabIndex = 7;
         this.textBox2.Text = "1";
         this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         // 
         // panel2
         // 
         this.panel2.BackColor = System.Drawing.Color.White;
         this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel2.Controls.Add(this.label31);
         this.panel2.Controls.Add(this.textBox1);
         this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel2.Location = new System.Drawing.Point(0, 0);
         this.panel2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel2.Name = "panel2";
         this.panel2.Padding = new System.Windows.Forms.Padding(1);
         this.panel2.Size = new System.Drawing.Size(132, 32);
         this.panel2.TabIndex = 42;
         // 
         // label31
         // 
         this.label31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label31.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label31.ForeColor = System.Drawing.Color.White;
         this.label31.Location = new System.Drawing.Point(1, 1);
         this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label31.Name = "label31";
         this.label31.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label31.Size = new System.Drawing.Size(126, 26);
         this.label31.TabIndex = 7;
         this.label31.Text = "No. of Workers";
         this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // textBox1
         // 
         this.textBox1.Location = new System.Drawing.Point(463, 1);
         this.textBox1.Name = "textBox1";
         this.textBox1.Size = new System.Drawing.Size(133, 32);
         this.textBox1.TabIndex = 6;
         // 
         // panelBranchType
         // 
         this.panelBranchType.BackColor = System.Drawing.Color.White;
         this.panelBranchType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelBranchType.Controls.Add(this.BranchType);
         this.panelBranchType.Controls.Add(this.label27);
         this.panelBranchType.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelBranchType.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelBranchType.Location = new System.Drawing.Point(0, 64);
         this.panelBranchType.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelBranchType.Name = "panelBranchType";
         this.panelBranchType.Padding = new System.Windows.Forms.Padding(1);
         this.panelBranchType.Size = new System.Drawing.Size(597, 32);
         this.panelBranchType.TabIndex = 26;
         // 
         // BranchType
         // 
         this.BranchType.Dock = System.Windows.Forms.DockStyle.Fill;
         this.BranchType.FormattingEnabled = true;
         this.BranchType.Location = new System.Drawing.Point(202, 1);
         this.BranchType.Name = "BranchType";
         this.BranchType.Size = new System.Drawing.Size(390, 32);
         this.BranchType.TabIndex = 6;
         this.BranchType.Tag = "Populate";
         // 
         // label27
         // 
         this.label27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label27.Dock = System.Windows.Forms.DockStyle.Left;
         this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label27.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label27.ForeColor = System.Drawing.Color.White;
         this.label27.Location = new System.Drawing.Point(1, 1);
         this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label27.Name = "label27";
         this.label27.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label27.Size = new System.Drawing.Size(201, 26);
         this.label27.TabIndex = 2;
         this.label27.Text = "Organization Type";
         this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelOwnership
         // 
         this.panelOwnership.BackColor = System.Drawing.Color.White;
         this.panelOwnership.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelOwnership.Controls.Add(this.Ownership);
         this.panelOwnership.Controls.Add(this.label24);
         this.panelOwnership.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelOwnership.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelOwnership.Location = new System.Drawing.Point(0, 32);
         this.panelOwnership.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelOwnership.Name = "panelOwnership";
         this.panelOwnership.Padding = new System.Windows.Forms.Padding(1);
         this.panelOwnership.Size = new System.Drawing.Size(597, 32);
         this.panelOwnership.TabIndex = 25;
         // 
         // Ownership
         // 
         this.Ownership.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Ownership.FormattingEnabled = true;
         this.Ownership.Location = new System.Drawing.Point(202, 1);
         this.Ownership.Name = "Ownership";
         this.Ownership.Size = new System.Drawing.Size(390, 32);
         this.Ownership.TabIndex = 6;
         this.Ownership.Tag = "Populate";
         // 
         // label24
         // 
         this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label24.Dock = System.Windows.Forms.DockStyle.Left;
         this.label24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label24.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label24.ForeColor = System.Drawing.Color.White;
         this.label24.Location = new System.Drawing.Point(1, 1);
         this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label24.Name = "label24";
         this.label24.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label24.Size = new System.Drawing.Size(201, 26);
         this.label24.TabIndex = 2;
         this.label24.Text = "Ownership Type";
         this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelCategory
         // 
         this.panelCategory.BackColor = System.Drawing.Color.White;
         this.panelCategory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelCategory.Controls.Add(this.Category);
         this.panelCategory.Controls.Add(this.label26);
         this.panelCategory.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelCategory.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelCategory.Location = new System.Drawing.Point(0, 0);
         this.panelCategory.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelCategory.Name = "panelCategory";
         this.panelCategory.Padding = new System.Windows.Forms.Padding(1);
         this.panelCategory.Size = new System.Drawing.Size(597, 32);
         this.panelCategory.TabIndex = 24;
         // 
         // Category
         // 
         this.Category.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Category.FormattingEnabled = true;
         this.Category.Location = new System.Drawing.Point(202, 1);
         this.Category.Name = "Category";
         this.Category.Size = new System.Drawing.Size(390, 32);
         this.Category.TabIndex = 3;
         this.Category.Tag = "Populate";
         // 
         // label26
         // 
         this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label26.Dock = System.Windows.Forms.DockStyle.Left;
         this.label26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label26.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label26.ForeColor = System.Drawing.Color.White;
         this.label26.Location = new System.Drawing.Point(1, 1);
         this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label26.Name = "label26";
         this.label26.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label26.Size = new System.Drawing.Size(201, 26);
         this.label26.TabIndex = 2;
         this.label26.Text = "Industry Type";
         this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitClientContacts
         // 
         this.splitClientContacts.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitClientContacts.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitClientContacts.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitClientContacts.Location = new System.Drawing.Point(5, 168);
         this.splitClientContacts.Name = "splitClientContacts";
         this.splitClientContacts.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitClientContacts.Panel1
         // 
         this.splitClientContacts.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitClientContacts.Panel1.Controls.Add(this.label15);
         // 
         // splitClientContacts.Panel2
         // 
         this.splitClientContacts.Panel2.Controls.Add(this.panelEMailAddress);
         this.splitClientContacts.Panel2.Controls.Add(this.panelTelephone);
         this.splitClientContacts.Panel2.Controls.Add(this.panelMobile);
         this.splitClientContacts.Panel2.Controls.Add(this.panelContactSalutation);
         this.splitClientContacts.Panel2.Controls.Add(this.panelContactDesignation);
         this.splitClientContacts.Panel2.Controls.Add(this.panelContactPerson);
         this.splitClientContacts.Size = new System.Drawing.Size(601, 260);
         this.splitClientContacts.SplitterDistance = 35;
         this.splitClientContacts.SplitterWidth = 1;
         this.splitClientContacts.TabIndex = 22;
         // 
         // label15
         // 
         this.label15.BackColor = System.Drawing.Color.Transparent;
         this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label15.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label15.ForeColor = System.Drawing.Color.White;
         this.label15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label15.ImageKey = "icons8-contact-details-40.png";
         this.label15.Location = new System.Drawing.Point(0, 0);
         this.label15.Name = "label15";
         this.label15.Size = new System.Drawing.Size(597, 31);
         this.label15.TabIndex = 0;
         this.label15.Text = "Contact Details";
         this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panelEMailAddress
         // 
         this.panelEMailAddress.AccessibleDescription = "";
         this.panelEMailAddress.BackColor = System.Drawing.Color.Navy;
         this.panelEMailAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelEMailAddress.Controls.Add(this.eMailAddress);
         this.panelEMailAddress.Controls.Add(this.label16);
         this.panelEMailAddress.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelEMailAddress.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelEMailAddress.Location = new System.Drawing.Point(0, 160);
         this.panelEMailAddress.Name = "panelEMailAddress";
         this.panelEMailAddress.Size = new System.Drawing.Size(597, 60);
         this.panelEMailAddress.TabIndex = 26;
         // 
         // eMailAddress
         // 
         this.eMailAddress.Dock = System.Windows.Forms.DockStyle.Fill;
         this.eMailAddress.Location = new System.Drawing.Point(201, 0);
         this.eMailAddress.Multiline = true;
         this.eMailAddress.Name = "eMailAddress";
         this.eMailAddress.Size = new System.Drawing.Size(392, 56);
         this.eMailAddress.TabIndex = 4;
         // 
         // label16
         // 
         this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label16.Dock = System.Windows.Forms.DockStyle.Left;
         this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label16.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label16.ForeColor = System.Drawing.Color.White;
         this.label16.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label16.ImageKey = "icons8-group-message-40.png";
         this.label16.Location = new System.Drawing.Point(0, 0);
         this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label16.Name = "label16";
         this.label16.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label16.Size = new System.Drawing.Size(201, 56);
         this.label16.TabIndex = 2;
         this.label16.Text = "E-Mail";
         this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelTelephone
         // 
         this.panelTelephone.BackColor = System.Drawing.Color.White;
         this.panelTelephone.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelTelephone.Controls.Add(this.Telephone);
         this.panelTelephone.Controls.Add(this.label17);
         this.panelTelephone.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelTelephone.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelTelephone.Location = new System.Drawing.Point(0, 128);
         this.panelTelephone.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelTelephone.Name = "panelTelephone";
         this.panelTelephone.Padding = new System.Windows.Forms.Padding(1);
         this.panelTelephone.Size = new System.Drawing.Size(597, 32);
         this.panelTelephone.TabIndex = 25;
         // 
         // Telephone
         // 
         this.Telephone.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Telephone.Location = new System.Drawing.Point(201, 1);
         this.Telephone.Name = "Telephone";
         this.Telephone.Size = new System.Drawing.Size(391, 32);
         this.Telephone.TabIndex = 4;
         // 
         // label17
         // 
         this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label17.Dock = System.Windows.Forms.DockStyle.Left;
         this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label17.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label17.ForeColor = System.Drawing.Color.White;
         this.label17.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label17.ImageKey = "Phone number.png";
         this.label17.Location = new System.Drawing.Point(1, 1);
         this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label17.Name = "label17";
         this.label17.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label17.Size = new System.Drawing.Size(200, 26);
         this.label17.TabIndex = 2;
         this.label17.Text = "Tel. No.";
         this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelMobile
         // 
         this.panelMobile.AccessibleDescription = "";
         this.panelMobile.BackColor = System.Drawing.Color.White;
         this.panelMobile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelMobile.Controls.Add(this.Mobile);
         this.panelMobile.Controls.Add(this.label18);
         this.panelMobile.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelMobile.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelMobile.Location = new System.Drawing.Point(0, 96);
         this.panelMobile.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelMobile.Name = "panelMobile";
         this.panelMobile.Padding = new System.Windows.Forms.Padding(1);
         this.panelMobile.Size = new System.Drawing.Size(597, 32);
         this.panelMobile.TabIndex = 24;
         // 
         // Mobile
         // 
         this.Mobile.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Mobile.Location = new System.Drawing.Point(201, 1);
         this.Mobile.Name = "Mobile";
         this.Mobile.Size = new System.Drawing.Size(391, 32);
         this.Mobile.TabIndex = 3;
         // 
         // label18
         // 
         this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label18.Dock = System.Windows.Forms.DockStyle.Left;
         this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label18.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label18.ForeColor = System.Drawing.Color.White;
         this.label18.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label18.ImageKey = "Mobile-phone.png";
         this.label18.Location = new System.Drawing.Point(1, 1);
         this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label18.Name = "label18";
         this.label18.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label18.Size = new System.Drawing.Size(200, 26);
         this.label18.TabIndex = 2;
         this.label18.Text = "Mobile No.";
         this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelContactSalutation
         // 
         this.panelContactSalutation.BackColor = System.Drawing.Color.White;
         this.panelContactSalutation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelContactSalutation.Controls.Add(this.ContactSalutation);
         this.panelContactSalutation.Controls.Add(this.Salutation);
         this.panelContactSalutation.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelContactSalutation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelContactSalutation.Location = new System.Drawing.Point(0, 64);
         this.panelContactSalutation.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelContactSalutation.MinimumSize = new System.Drawing.Size(4, 32);
         this.panelContactSalutation.Name = "panelContactSalutation";
         this.panelContactSalutation.Padding = new System.Windows.Forms.Padding(1);
         this.panelContactSalutation.Size = new System.Drawing.Size(597, 32);
         this.panelContactSalutation.TabIndex = 17;
         // 
         // ContactSalutation
         // 
         this.ContactSalutation.Dock = System.Windows.Forms.DockStyle.Fill;
         this.ContactSalutation.Location = new System.Drawing.Point(201, 1);
         this.ContactSalutation.Name = "ContactSalutation";
         this.ContactSalutation.Size = new System.Drawing.Size(391, 32);
         this.ContactSalutation.TabIndex = 3;
         // 
         // Salutation
         // 
         this.Salutation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.Salutation.Dock = System.Windows.Forms.DockStyle.Left;
         this.Salutation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.Salutation.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Salutation.ForeColor = System.Drawing.Color.White;
         this.Salutation.Location = new System.Drawing.Point(1, 1);
         this.Salutation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.Salutation.Name = "Salutation";
         this.Salutation.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.Salutation.Size = new System.Drawing.Size(200, 26);
         this.Salutation.TabIndex = 2;
         this.Salutation.Text = "Salutation";
         this.Salutation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelContactDesignation
         // 
         this.panelContactDesignation.BackColor = System.Drawing.Color.White;
         this.panelContactDesignation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelContactDesignation.Controls.Add(this.ContactDesignation);
         this.panelContactDesignation.Controls.Add(this.label21);
         this.panelContactDesignation.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelContactDesignation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelContactDesignation.Location = new System.Drawing.Point(0, 32);
         this.panelContactDesignation.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelContactDesignation.MinimumSize = new System.Drawing.Size(4, 32);
         this.panelContactDesignation.Name = "panelContactDesignation";
         this.panelContactDesignation.Padding = new System.Windows.Forms.Padding(1);
         this.panelContactDesignation.Size = new System.Drawing.Size(597, 32);
         this.panelContactDesignation.TabIndex = 16;
         // 
         // ContactDesignation
         // 
         this.ContactDesignation.Dock = System.Windows.Forms.DockStyle.Fill;
         this.ContactDesignation.Location = new System.Drawing.Point(201, 1);
         this.ContactDesignation.Name = "ContactDesignation";
         this.ContactDesignation.Size = new System.Drawing.Size(391, 32);
         this.ContactDesignation.TabIndex = 3;
         // 
         // label21
         // 
         this.label21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label21.Dock = System.Windows.Forms.DockStyle.Left;
         this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label21.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label21.ForeColor = System.Drawing.Color.White;
         this.label21.Location = new System.Drawing.Point(1, 1);
         this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label21.Name = "label21";
         this.label21.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label21.Size = new System.Drawing.Size(200, 26);
         this.label21.TabIndex = 2;
         this.label21.Text = "Designation";
         this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelContactPerson
         // 
         this.panelContactPerson.BackColor = System.Drawing.Color.White;
         this.panelContactPerson.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelContactPerson.Controls.Add(this.ContactPerson);
         this.panelContactPerson.Controls.Add(this.label20);
         this.panelContactPerson.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelContactPerson.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelContactPerson.Location = new System.Drawing.Point(0, 0);
         this.panelContactPerson.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelContactPerson.MinimumSize = new System.Drawing.Size(4, 32);
         this.panelContactPerson.Name = "panelContactPerson";
         this.panelContactPerson.Padding = new System.Windows.Forms.Padding(1);
         this.panelContactPerson.Size = new System.Drawing.Size(597, 32);
         this.panelContactPerson.TabIndex = 15;
         // 
         // ContactPerson
         // 
         this.ContactPerson.Dock = System.Windows.Forms.DockStyle.Fill;
         this.ContactPerson.Location = new System.Drawing.Point(201, 1);
         this.ContactPerson.Name = "ContactPerson";
         this.ContactPerson.Size = new System.Drawing.Size(391, 32);
         this.ContactPerson.TabIndex = 3;
         // 
         // label20
         // 
         this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label20.Dock = System.Windows.Forms.DockStyle.Left;
         this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label20.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label20.ForeColor = System.Drawing.Color.White;
         this.label20.Location = new System.Drawing.Point(1, 1);
         this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label20.Name = "label20";
         this.label20.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label20.Size = new System.Drawing.Size(200, 26);
         this.label20.TabIndex = 2;
         this.label20.Text = "Contact Person";
         this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelAddress
         // 
         this.panelAddress.Controls.Add(this.panelCityID);
         this.panelAddress.Controls.Add(this.panelBrgyID);
         this.panelAddress.Controls.Add(this.panelPurok);
         this.panelAddress.Controls.Add(this.panelHouseNo);
         this.panelAddress.Controls.Add(this.listBox1);
         this.panelAddress.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelAddress.Location = new System.Drawing.Point(5, 37);
         this.panelAddress.Name = "panelAddress";
         this.panelAddress.Size = new System.Drawing.Size(601, 131);
         this.panelAddress.TabIndex = 21;
         // 
         // panelCityID
         // 
         this.panelCityID.BackColor = System.Drawing.Color.White;
         this.panelCityID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelCityID.Controls.Add(this.CityID);
         this.panelCityID.Controls.Add(this.label25);
         this.panelCityID.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelCityID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelCityID.Location = new System.Drawing.Point(18, 96);
         this.panelCityID.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelCityID.Name = "panelCityID";
         this.panelCityID.Padding = new System.Windows.Forms.Padding(1);
         this.panelCityID.Size = new System.Drawing.Size(583, 32);
         this.panelCityID.TabIndex = 44;
         // 
         // CityID
         // 
         this.CityID.BackColor = System.Drawing.Color.White;
         this.CityID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.CityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.CityID.FormattingEnabled = true;
         this.CityID.Location = new System.Drawing.Point(182, 1);
         this.CityID.Name = "CityID";
         this.CityID.Size = new System.Drawing.Size(396, 32);
         this.CityID.TabIndex = 3;
         // 
         // label25
         // 
         this.label25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label25.Dock = System.Windows.Forms.DockStyle.Left;
         this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label25.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label25.ForeColor = System.Drawing.Color.White;
         this.label25.Location = new System.Drawing.Point(1, 1);
         this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label25.Name = "label25";
         this.label25.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label25.Size = new System.Drawing.Size(181, 26);
         this.label25.TabIndex = 2;
         this.label25.Text = "City/Mun.";
         this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelBrgyID
         // 
         this.panelBrgyID.BackColor = System.Drawing.Color.White;
         this.panelBrgyID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelBrgyID.Controls.Add(this.BrgyID);
         this.panelBrgyID.Controls.Add(this.label14);
         this.panelBrgyID.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelBrgyID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelBrgyID.Location = new System.Drawing.Point(18, 64);
         this.panelBrgyID.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelBrgyID.MinimumSize = new System.Drawing.Size(4, 32);
         this.panelBrgyID.Name = "panelBrgyID";
         this.panelBrgyID.Padding = new System.Windows.Forms.Padding(1);
         this.panelBrgyID.Size = new System.Drawing.Size(583, 32);
         this.panelBrgyID.TabIndex = 43;
         // 
         // BrgyID
         // 
         this.BrgyID.BackColor = System.Drawing.Color.White;
         this.BrgyID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.BrgyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.BrgyID.FormattingEnabled = true;
         this.BrgyID.Location = new System.Drawing.Point(182, 1);
         this.BrgyID.Name = "BrgyID";
         this.BrgyID.Size = new System.Drawing.Size(396, 32);
         this.BrgyID.TabIndex = 3;
         // 
         // label14
         // 
         this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label14.Dock = System.Windows.Forms.DockStyle.Left;
         this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label14.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label14.ForeColor = System.Drawing.Color.White;
         this.label14.Location = new System.Drawing.Point(1, 1);
         this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label14.Name = "label14";
         this.label14.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label14.Size = new System.Drawing.Size(181, 26);
         this.label14.TabIndex = 2;
         this.label14.Text = "Barangay";
         this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelPurok
         // 
         this.panelPurok.BackColor = System.Drawing.Color.White;
         this.panelPurok.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelPurok.Controls.Add(this.purokID);
         this.panelPurok.Controls.Add(this.label22);
         this.panelPurok.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelPurok.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelPurok.Location = new System.Drawing.Point(18, 32);
         this.panelPurok.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelPurok.Name = "panelPurok";
         this.panelPurok.Padding = new System.Windows.Forms.Padding(1);
         this.panelPurok.Size = new System.Drawing.Size(583, 32);
         this.panelPurok.TabIndex = 22;
         // 
         // purokID
         // 
         this.purokID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.purokID.FormattingEnabled = true;
         this.purokID.Location = new System.Drawing.Point(182, 1);
         this.purokID.Name = "purokID";
         this.purokID.Size = new System.Drawing.Size(396, 32);
         this.purokID.TabIndex = 3;
         // 
         // label22
         // 
         this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label22.Dock = System.Windows.Forms.DockStyle.Left;
         this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label22.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label22.ForeColor = System.Drawing.Color.White;
         this.label22.Location = new System.Drawing.Point(1, 1);
         this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label22.Name = "label22";
         this.label22.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label22.Size = new System.Drawing.Size(181, 26);
         this.label22.TabIndex = 2;
         this.label22.Text = "Purok";
         this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panelHouseNo
         // 
         this.panelHouseNo.BackColor = System.Drawing.Color.White;
         this.panelHouseNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelHouseNo.Controls.Add(this.houseNo);
         this.panelHouseNo.Controls.Add(this.label12);
         this.panelHouseNo.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelHouseNo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelHouseNo.Location = new System.Drawing.Point(18, 0);
         this.panelHouseNo.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelHouseNo.Name = "panelHouseNo";
         this.panelHouseNo.Padding = new System.Windows.Forms.Padding(1);
         this.panelHouseNo.Size = new System.Drawing.Size(583, 32);
         this.panelHouseNo.TabIndex = 21;
         // 
         // houseNo
         // 
         this.houseNo.Dock = System.Windows.Forms.DockStyle.Fill;
         this.houseNo.Location = new System.Drawing.Point(182, 1);
         this.houseNo.Name = "houseNo";
         this.houseNo.Size = new System.Drawing.Size(396, 32);
         this.houseNo.TabIndex = 3;
         // 
         // label12
         // 
         this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label12.Dock = System.Windows.Forms.DockStyle.Left;
         this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label12.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label12.ForeColor = System.Drawing.Color.White;
         this.label12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label12.ImageKey = "(none)";
         this.label12.Location = new System.Drawing.Point(1, 1);
         this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label12.Name = "label12";
         this.label12.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label12.Size = new System.Drawing.Size(181, 26);
         this.label12.TabIndex = 2;
         this.label12.Text = "House No.";
         this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // listBox1
         // 
         this.listBox1.BackColor = System.Drawing.Color.Navy;
         this.listBox1.Dock = System.Windows.Forms.DockStyle.Left;
         this.listBox1.Enabled = false;
         this.listBox1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.listBox1.ForeColor = System.Drawing.Color.White;
         this.listBox1.FormattingEnabled = true;
         this.listBox1.ItemHeight = 18;
         this.listBox1.Items.AddRange(new object[] {
            "A",
            "D",
            "D",
            "R",
            "E",
            "S",
            "S"});
         this.listBox1.Location = new System.Drawing.Point(0, 0);
         this.listBox1.Name = "listBox1";
         this.listBox1.Size = new System.Drawing.Size(18, 131);
         this.listBox1.TabIndex = 20;
         // 
         // panelLastName
         // 
         this.panelLastName.BackColor = System.Drawing.Color.White;
         this.panelLastName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelLastName.Controls.Add(this.LastName);
         this.panelLastName.Controls.Add(this.label10);
         this.panelLastName.Dock = System.Windows.Forms.DockStyle.Top;
         this.panelLastName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panelLastName.Location = new System.Drawing.Point(5, 5);
         this.panelLastName.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panelLastName.MinimumSize = new System.Drawing.Size(4, 32);
         this.panelLastName.Name = "panelLastName";
         this.panelLastName.Padding = new System.Windows.Forms.Padding(1);
         this.panelLastName.Size = new System.Drawing.Size(601, 32);
         this.panelLastName.TabIndex = 14;
         // 
         // LastName
         // 
         this.LastName.Dock = System.Windows.Forms.DockStyle.Fill;
         this.LastName.Location = new System.Drawing.Point(201, 1);
         this.LastName.Name = "LastName";
         this.LastName.Size = new System.Drawing.Size(395, 32);
         this.LastName.TabIndex = 3;
         // 
         // label10
         // 
         this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label10.Dock = System.Windows.Forms.DockStyle.Left;
         this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label10.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label10.ForeColor = System.Drawing.Color.White;
         this.label10.Location = new System.Drawing.Point(1, 1);
         this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label10.Name = "label10";
         this.label10.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label10.Size = new System.Drawing.Size(200, 26);
         this.label10.TabIndex = 2;
         this.label10.Text = "Business Name";
         this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // EntryForm_Business
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 24F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(1250, 802);
         this.Controls.Add(this.splitMain);
         this.Controls.Add(this.panel1);
         this.Controls.Add(this.menuPanel);
         this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Margin = new System.Windows.Forms.Padding(4);
         this.Name = "EntryForm_Business";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "EntryForm_Business";
         this.Load += new System.EventHandler(this.EntryForm_Business_Load);
         this.menuPanel.ResumeLayout(false);
         this.menuPanel.PerformLayout();
         this.panel1.ResumeLayout(false);
         this.splitMain.Panel1.ResumeLayout(false);
         this.splitMain.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
         this.splitMain.ResumeLayout(false);
         this.splitHeaderNonClient.Panel1.ResumeLayout(false);
         this.splitHeaderNonClient.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitHeaderNonClient)).EndInit();
         this.splitHeaderNonClient.ResumeLayout(false);
         this.splitContainer1.Panel1.ResumeLayout(false);
         this.splitContainer1.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
         this.splitContainer1.ResumeLayout(false);
         this.panelRemarks.ResumeLayout(false);
         this.panelAmount.ResumeLayout(false);
         this.panelAmount.PerformLayout();
         this.panelDateTo.ResumeLayout(false);
         this.panelDateFrom.ResumeLayout(false);
         this.panelTransNo.ResumeLayout(false);
         this.panelTransNo.PerformLayout();
         this.panelDocID.ResumeLayout(false);
         this.panelTransDate.ResumeLayout(false);
         this.panelClientID.ResumeLayout(false);
         this.panelTransTypeID.ResumeLayout(false);
         this.splitClients.Panel1.ResumeLayout(false);
         this.splitClients.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitClients)).EndInit();
         this.splitClients.ResumeLayout(false);
         this.splitBusinessInfo.Panel1.ResumeLayout(false);
         this.splitBusinessInfo.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitBusinessInfo)).EndInit();
         this.splitBusinessInfo.ResumeLayout(false);
         this.splitContainer2.Panel1.ResumeLayout(false);
         this.splitContainer2.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
         this.splitContainer2.ResumeLayout(false);
         this.panel3.ResumeLayout(false);
         this.panel3.PerformLayout();
         this.panelAuthorizedCapital.ResumeLayout(false);
         this.panelAuthorizedCapital.PerformLayout();
         this.panel4.ResumeLayout(false);
         this.panel4.PerformLayout();
         this.panel2.ResumeLayout(false);
         this.panel2.PerformLayout();
         this.panelBranchType.ResumeLayout(false);
         this.panelOwnership.ResumeLayout(false);
         this.panelCategory.ResumeLayout(false);
         this.splitClientContacts.Panel1.ResumeLayout(false);
         this.splitClientContacts.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitClientContacts)).EndInit();
         this.splitClientContacts.ResumeLayout(false);
         this.panelEMailAddress.ResumeLayout(false);
         this.panelEMailAddress.PerformLayout();
         this.panelTelephone.ResumeLayout(false);
         this.panelTelephone.PerformLayout();
         this.panelMobile.ResumeLayout(false);
         this.panelMobile.PerformLayout();
         this.panelContactSalutation.ResumeLayout(false);
         this.panelContactSalutation.PerformLayout();
         this.panelContactDesignation.ResumeLayout(false);
         this.panelContactDesignation.PerformLayout();
         this.panelContactPerson.ResumeLayout(false);
         this.panelContactPerson.PerformLayout();
         this.panelAddress.ResumeLayout(false);
         this.panelCityID.ResumeLayout(false);
         this.panelBrgyID.ResumeLayout(false);
         this.panelPurok.ResumeLayout(false);
         this.panelHouseNo.ResumeLayout(false);
         this.panelHouseNo.PerformLayout();
         this.panelLastName.ResumeLayout(false);
         this.panelLastName.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Panel menuPanel;
      private System.Windows.Forms.Label bClose;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.SplitContainer splitMain;
      private System.Windows.Forms.Button BtnSave;
      private System.Windows.Forms.Button BtnUndo;
      private System.Windows.Forms.ComboBox TransID;
      private System.Windows.Forms.TextBox TransHeaderID;
      private System.Windows.Forms.SplitContainer splitHeaderNonClient;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.SplitContainer splitClients;
      private System.Windows.Forms.Label labelIssuedTo;
      private System.Windows.Forms.Panel panelLastName;
      private System.Windows.Forms.TextBox LastName;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Panel panelAddress;
      private System.Windows.Forms.Panel panelHouseNo;
      private System.Windows.Forms.TextBox houseNo;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.ListBox listBox1;
      private System.Windows.Forms.SplitContainer splitClientContacts;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Panel panelTransTypeID;
      private System.Windows.Forms.ComboBox TransTypeID;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Panel panelMobile;
      private System.Windows.Forms.TextBox Mobile;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.Panel panelContactSalutation;
      private System.Windows.Forms.TextBox ContactSalutation;
      private System.Windows.Forms.Label Salutation;
      private System.Windows.Forms.Panel panelContactDesignation;
      private System.Windows.Forms.TextBox ContactDesignation;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.Panel panelContactPerson;
      private System.Windows.Forms.TextBox ContactPerson;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.Panel panelPurok;
      private System.Windows.Forms.ComboBox purokID;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.Panel panelCityID;
      private System.Windows.Forms.ComboBox CityID;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.Panel panelBrgyID;
      private System.Windows.Forms.ComboBox BrgyID;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Panel panelTelephone;
      private System.Windows.Forms.TextBox Telephone;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.SplitContainer splitBusinessInfo;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Panel panelEMailAddress;
      private System.Windows.Forms.TextBox eMailAddress;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.ListView listView1;
      private System.Windows.Forms.ColumnHeader columnHeader1;
      private System.Windows.Forms.ColumnHeader columnHeader2;
      private System.Windows.Forms.ColumnHeader columnHeader3;
      private System.Windows.Forms.ColumnHeader columnHeader4;
      private System.Windows.Forms.ImageList imageList1;
      private System.Windows.Forms.Panel panelRemarks;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Panel panelAmount;
      private System.Windows.Forms.TextBox Amount;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Panel panelDateTo;
      private System.Windows.Forms.DateTimePicker DateTo;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Panel panelDateFrom;
      private System.Windows.Forms.DateTimePicker DateFrom;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Panel panelTransNo;
      private System.Windows.Forms.TextBox TransNo;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Panel panelDocID;
      private System.Windows.Forms.ComboBox DocID;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Panel panelTransDate;
      private System.Windows.Forms.DateTimePicker TransDate;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Panel panelClientID;
      private System.Windows.Forms.ComboBox ClientID;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Panel panelBranchType;
      private System.Windows.Forms.ComboBox BranchType;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Panel panelOwnership;
      private System.Windows.Forms.ComboBox Ownership;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Panel panelCategory;
      private System.Windows.Forms.ComboBox Category;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.SplitContainer splitContainer2;
      private System.Windows.Forms.Panel panel3;
      private System.Windows.Forms.TextBox CapitalPaidUp;
      private System.Windows.Forms.Label label28;
      private System.Windows.Forms.Panel panelAuthorizedCapital;
      private System.Windows.Forms.TextBox CapitalAuthorized;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.Panel panel4;
      private System.Windows.Forms.TextBox textBox2;
      private System.Windows.Forms.Panel panel2;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.TextBox textBox1;
   }
}