﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
   public partial class Form_Main : Form
   {
      public bool isExit = true;
      private MySQLDatabase db;
      public Form_Main()
      {
         InitializeComponent();
         db = DatabaseManager.GetInstance();
         bUser.Text = ActiveUser.clientname;
         if (ActiveUser.photo == null)
         {
            bUser.Image = OvalImage(new Bitmap(BIS.Properties.Resources.user, 30, 30));
         }
         else
         {
            //photo ng user ilagay
         }
         LoadBaranggayDetails();
      }

      public void LoadBaranggayDetails()
      {
            applabel.Text = AppConfiguration.GetConfig("appcaption");
            baranggayName.Text = AppConfiguration.GetConfig("brgyname");
            baranggayAddress.Text = AppConfiguration.GetConfig("brgyaddress1");
         /*IDataReader reader = db.Select("SELECT appcaption, brgylogo, citylogo, brgyname, brgyaddress1 FROM appdefaults");
         if (reader != null)
         {
            if (reader.Read())
            {
               applabel.Text = DataTypeManager.ParseDataToString(reader.GetValue(0));
               string brgylogo = DataTypeManager.ParseDataToString(reader.GetValue(1));
               try
               {
                  pbBaranggayLogo.Image = Image.FromFile(brgylogo);
               }
               catch (Exception e)
               {
                  Console.WriteLine(e);
                  pbBaranggayLogo.Image = BIS.Properties.Resources.davao_logo_color_300x300;
               }
               baranggayName.Text = DataTypeManager.ParseDataToString(reader.GetValue(3));
               baranggayAddress.Text = DataTypeManager.ParseDataToString(reader.GetValue(4));
               reader.Close();
            }
         }
         db.CloseConnection();*/
      }

      public static Image OvalImage(Image img)
      {
         Bitmap bmp = new Bitmap(img.Width, img.Height);
         using (GraphicsPath gp = new GraphicsPath())
         {
            gp.AddEllipse(0, 0, img.Width, img.Height);
            using (Graphics gr = Graphics.FromImage(bmp))
            {
               gr.SetClip(gp);
               gr.DrawImage(img, Point.Empty);
            }
         }
         return bmp;
      }

      private void BusinessToolStripMenuItem_Click(object sender, EventArgs e)
      {
         
         //Test test = new Test();
         //test.ShowDialog();
         // test.Dispose();
      }

      private void OfficialsToolStripMenuItem_Click(object sender, EventArgs e)
      {
         HighlightMenu((ToolStripMenuItem)sender);
      }

      private void BlotterToolStripMenuItem_Click(object sender, EventArgs e)
      {

      }

      private void InboxToolStripMenuItem_Click(object sender, EventArgs e)
      {
         //HighlightMenu((ToolStripMenuItem)sender);
      }

      private void OutboxToolStripMenuItem_Click(object sender, EventArgs e)
      {
         //HighlightMenu((ToolStripMenuItem)sender);
      }

      private void BirthdayToolStripMenuItem_Click(object sender, EventArgs e)
      {
         //Close();
      }

      private void HighlightMenu(ToolStripMenuItem menuitem)
      {
         menuitem.BackColor = Color.FromArgb(61, 84, 118);
         foreach (ToolStripMenuItem c in menuStrip1.Items)
         {
            if (c != menuitem)
            {
               c.BackColor = Color.FromArgb(53, 64, 82);
            }
         }
      }

      public const int WM_NCLBUTTONDOWN = 0xA1;
      public const int HT_CAPTION = 0x2;

      [System.Runtime.InteropServices.DllImport("user32.dll")]
      public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
      [System.Runtime.InteropServices.DllImport("user32.dll")]
      public static extern bool ReleaseCapture();

      private void Form_Main_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
      {
         if (e.Button == MouseButtons.Left)
         {
            ReleaseCapture();
            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
         }
      }

      private void ToolStripMenuItem1_Click(object sender, EventArgs e)
      {
         HighlightMenu((ToolStripMenuItem)sender);
      }

      private void LogoutToolStripMenuItem_Click(object sender, EventArgs e)
      {
         isExit = false;
         Close();
      }

      private void Form_Main_Load(object sender, EventArgs e)
      {
            SMSModem modem = SMSModem.GetInstance();
            /*foreach(string port in SerialPort.GetPortNames())
            {
                if(modem.OpenPort(port, 9600))
                {
                    MessageBox.Show(port);
                    break;
                }
            }
            if(!modem.TestConnection())
            {
                MessageBox.Show("GSM Device not detected");
            }*/
      }

      private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void SettingsToolStripMenuItem_Click(object sender, EventArgs e)
      {
         Form_FPReading fpr = new Form_FPReading();
         fpr.ShowDialog();
         fpr.Dispose();
      }

      private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
      {
         foreach(Form child in MdiChildren)
         {
            child.Close();
            child.Dispose();
         }
         ToolStripMenuItem item = (ToolStripMenuItem)e.ClickedItem;
        Form frm = null;
        switch (e.ClickedItem.Name)
         {
           
            case "bBusiness":
               HighlightMenu(item);
                    frm = new Form_Transactions("Business", typeof(EntryForm_Business), null, new DatabaseTableView("vwbusinesses", "Name of Business Firm"));
               break;
            case "bResidents":
                HighlightMenu(item);
                frm = new Form_Transactions("Residents", typeof(EntryForm_Residents), new Control_ResidentsSubForm(), new DatabaseTableView("vwresidents", "Name of Resident"));
                break;
            case "blotterToolStripMenuItem":
               HighlightMenu(item);
                frm = new Form_Transactions("Blotter", typeof(EntryForm_Blotter), null, new DatabaseTableView("vwresidents", "Name of Resident"));
               break;
            case "bExit":
               Close();
               break;
            case "officialsToolStripMenuItem":
               HighlightMenu(item);
               MyTableLayoutPanel MyTableLayoutPanel = new MyTableLayoutPanel();
               MyTableLayoutPanel.ShowDialog();
               MyTableLayoutPanel.Dispose();
               break;
            default:
               HighlightMenu(item);
               break;
         }
         if(frm != null)
            {
                frm.MdiParent = this;
                frm.Dock = DockStyle.Fill;
                frm.Show();
            }
         item.BackColor = Color.FromArgb(53, 64, 82);
      }

      private void DateBeg_ValueChanged(object sender, EventArgs e)
      {

      }

        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Form_SMS fsms = new Form_SMS();
            fsms.MdiParent = this;
            fsms.Dock = DockStyle.Fill;
            fsms.Show();
        }

        private void Applabel_DoubleClick(object sender, EventArgs e)
        {
            if(this.WindowState != FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void Label1_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void SettingsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Form_Settings fs = new Form_Settings();
            if(fs.ShowDialog() == DialogResult.OK)
            {
                LoadBaranggayDetails();
            }
            fs.Dispose();
        }

        private void Form_Main_Shown(object sender, EventArgs e)
        {
            FPReader reader = FPReaderManager.GetInstance();
            string hardwares = "";
            if(reader.CurrentReader == null)
            {
                hardwares += "\r\n\t- Fingerprint reader";
            }
            if(!SMSModem.GetInstance().TestConnection())
            {
                hardwares += "\r\n\t- SMS modem";
            }
            if(hardwares.Length > 0)
            {
                MessageBox.Show("The ff. devices were not found:\r\n" + hardwares, "Missing Devices", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

      private void UserstoolStripMenuItem_Click(object sender, EventArgs e)
      {
         EntryForm_Users userfrm = new EntryForm_Users();
         userfrm.ShowDialog();
         userfrm.Dispose();
      }
   }
}
