﻿namespace BIS
{
    partial class EntryForm_Residents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         this.menuPanel = new System.Windows.Forms.Panel();
         this.bClose = new System.Windows.Forms.Label();
         this.bFP = new System.Windows.Forms.Button();
         this.bConInf = new System.Windows.Forms.Button();
         this.bPDetails = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.bNext = new System.Windows.Forms.Button();
         this.panel1 = new System.Windows.Forms.Panel();
         this.panel2 = new System.Windows.Forms.Panel();
         this.tabControl1 = new System.Windows.Forms.TabControl();
         this.tabPage1 = new System.Windows.Forms.TabPage();
         this.button2 = new System.Windows.Forms.Button();
         this.bBrowsePhoto = new System.Windows.Forms.Button();
         this.panelContactDetails = new System.Windows.Forms.Panel();
         this.bRemoveContact = new System.Windows.Forms.Button();
         this.bEditContact = new System.Windows.Forms.Button();
         this.bAddContact = new System.Windows.Forms.Button();
         this.listView1 = new System.Windows.Forms.ListView();
         this.contacttype = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
         this.contact = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
         this.smsgroup = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
         this.label21 = new System.Windows.Forms.Label();
         this.panelAddress = new System.Windows.Forms.Panel();
         this.provAddress = new System.Windows.Forms.TextBox();
         this.ProvID = new System.Windows.Forms.ComboBox();
         this.label14 = new System.Windows.Forms.Label();
         this.label13 = new System.Windows.Forms.Label();
         this.CityID = new System.Windows.Forms.ComboBox();
         this.BrgyID = new System.Windows.Forms.ComboBox();
         this.purokID = new System.Windows.Forms.ComboBox();
         this.label8 = new System.Windows.Forms.Label();
         this.label10 = new System.Windows.Forms.Label();
         this.label11 = new System.Windows.Forms.Label();
         this.houseNo = new System.Windows.Forms.TextBox();
         this.label12 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.panelClientName = new System.Windows.Forms.Panel();
         this.Alias = new System.Windows.Forms.TextBox();
         this.label5 = new System.Windows.Forms.Label();
         this.lastName = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.middleName = new System.Windows.Forms.TextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.firstName = new System.Windows.Forms.TextBox();
         this.label7 = new System.Windows.Forms.Label();
         this.tabPage2 = new System.Windows.Forms.TabPage();
         this.panelImmediateFamily = new System.Windows.Forms.Panel();
         this.mAddress = new System.Windows.Forms.TextBox();
         this.Mother = new System.Windows.Forms.TextBox();
         this.label50 = new System.Windows.Forms.Label();
         this.fAddress = new System.Windows.Forms.TextBox();
         this.Father = new System.Windows.Forms.TextBox();
         this.label49 = new System.Windows.Forms.Label();
         this.sAddress = new System.Windows.Forms.TextBox();
         this.Spouse = new System.Windows.Forms.TextBox();
         this.label48 = new System.Windows.Forms.Label();
         this.label45 = new System.Windows.Forms.Label();
         this.label46 = new System.Windows.Forms.Label();
         this.label47 = new System.Windows.Forms.Label();
         this.label44 = new System.Windows.Forms.Label();
         this.panelDeceasedDetails = new System.Windows.Forms.Panel();
         this.POD = new System.Windows.Forms.ComboBox();
         this.label32 = new System.Windows.Forms.Label();
         this.DOD = new System.Windows.Forms.DateTimePicker();
         this.label43 = new System.Windows.Forms.Label();
         this.label31 = new System.Windows.Forms.Label();
         this.panelMiscellaneousDetails = new System.Windows.Forms.Panel();
         this.SCID = new System.Windows.Forms.TextBox();
         this.Disability = new System.Windows.Forms.ComboBox();
         this.label37 = new System.Windows.Forms.Label();
         this.label42 = new System.Windows.Forms.Label();
         this.label30 = new System.Windows.Forms.Label();
         this.panelEmploymentDetails = new System.Windows.Forms.Panel();
         this.Industry = new System.Windows.Forms.ComboBox();
         this.Occupation = new System.Windows.Forms.ComboBox();
         this.WorkerType = new System.Windows.Forms.ComboBox();
         this.Employment = new System.Windows.Forms.ComboBox();
         this.label38 = new System.Windows.Forms.Label();
         this.label39 = new System.Windows.Forms.Label();
         this.label40 = new System.Windows.Forms.Label();
         this.label41 = new System.Windows.Forms.Label();
         this.label29 = new System.Windows.Forms.Label();
         this.panelPhysicalDetails = new System.Windows.Forms.Panel();
         this.Weight = new System.Windows.Forms.NumericUpDown();
         this.Height = new System.Windows.Forms.NumericUpDown();
         this.label54 = new System.Windows.Forms.Label();
         this.Hair = new System.Windows.Forms.ComboBox();
         this.Complexion = new System.Windows.Forms.ComboBox();
         this.Build = new System.Windows.Forms.ComboBox();
         this.label33 = new System.Windows.Forms.Label();
         this.label34 = new System.Windows.Forms.Label();
         this.label35 = new System.Windows.Forms.Label();
         this.label36 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label15 = new System.Windows.Forms.Label();
         this.panelPersonalDetails = new System.Windows.Forms.Panel();
         this.POB = new System.Windows.Forms.ComboBox();
         this.DOB = new System.Windows.Forms.DateTimePicker();
         this.Attainment = new System.Windows.Forms.ComboBox();
         this.Citizenship = new System.Windows.Forms.ComboBox();
         this.Ethnicity = new System.Windows.Forms.ComboBox();
         this.Religion = new System.Windows.Forms.ComboBox();
         this.Status = new System.Windows.Forms.ComboBox();
         this.Gender = new System.Windows.Forms.ComboBox();
         this.label27 = new System.Windows.Forms.Label();
         this.label26 = new System.Windows.Forms.Label();
         this.label25 = new System.Windows.Forms.Label();
         this.label20 = new System.Windows.Forms.Label();
         this.label16 = new System.Windows.Forms.Label();
         this.label17 = new System.Windows.Forms.Label();
         this.label18 = new System.Windows.Forms.Label();
         this.label19 = new System.Windows.Forms.Label();
         this.tabPage3 = new System.Windows.Forms.TabPage();
         this.panelFPs = new System.Windows.Forms.Panel();
         this.btnCancel = new System.Windows.Forms.Button();
         this.txtMessage = new System.Windows.Forms.TextBox();
         this.residentPhoto = new System.Windows.Forms.PictureBox();
         this.pbFingerprint = new System.Windows.Forms.PictureBox();
         this.menuPanel.SuspendLayout();
         this.panel1.SuspendLayout();
         this.panel2.SuspendLayout();
         this.tabControl1.SuspendLayout();
         this.tabPage1.SuspendLayout();
         this.panelContactDetails.SuspendLayout();
         this.panelAddress.SuspendLayout();
         this.panelClientName.SuspendLayout();
         this.tabPage2.SuspendLayout();
         this.panelImmediateFamily.SuspendLayout();
         this.panelDeceasedDetails.SuspendLayout();
         this.panelMiscellaneousDetails.SuspendLayout();
         this.panelEmploymentDetails.SuspendLayout();
         this.panelPhysicalDetails.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.Weight)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.Height)).BeginInit();
         this.panelPersonalDetails.SuspendLayout();
         this.tabPage3.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.residentPhoto)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbFingerprint)).BeginInit();
         this.SuspendLayout();
         // 
         // menuPanel
         // 
         this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.menuPanel.Controls.Add(this.bClose);
         this.menuPanel.Controls.Add(this.bFP);
         this.menuPanel.Controls.Add(this.bConInf);
         this.menuPanel.Controls.Add(this.bPDetails);
         this.menuPanel.Controls.Add(this.label1);
         this.menuPanel.Dock = System.Windows.Forms.DockStyle.Top;
         this.menuPanel.Location = new System.Drawing.Point(0, 0);
         this.menuPanel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.menuPanel.Name = "menuPanel";
         this.menuPanel.Size = new System.Drawing.Size(842, 60);
         this.menuPanel.TabIndex = 100;
         this.menuPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EntryForm_Residents_MouseDown);
         // 
         // bClose
         // 
         this.bClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
         this.bClose.Dock = System.Windows.Forms.DockStyle.Right;
         this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bClose.ForeColor = System.Drawing.Color.Silver;
         this.bClose.Location = new System.Drawing.Point(782, 0);
         this.bClose.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.bClose.Name = "bClose";
         this.bClose.Size = new System.Drawing.Size(60, 60);
         this.bClose.TabIndex = 5;
         this.bClose.Text = "X";
         this.bClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.bClose.Click += new System.EventHandler(this.Label5_Click);
         this.bClose.MouseEnter += new System.EventHandler(this.Label5_MouseHover);
         this.bClose.MouseLeave += new System.EventHandler(this.BClose_MouseLeave);
         // 
         // bFP
         // 
         this.bFP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.bFP.Dock = System.Windows.Forms.DockStyle.Left;
         this.bFP.FlatAppearance.BorderSize = 0;
         this.bFP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bFP.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bFP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(200)))), ((int)(((byte)(252)))));
         this.bFP.Location = new System.Drawing.Point(370, 0);
         this.bFP.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bFP.Name = "bFP";
         this.bFP.Size = new System.Drawing.Size(100, 60);
         this.bFP.TabIndex = 4;
         this.bFP.TabStop = false;
         this.bFP.Text = "Fingerprint";
         this.bFP.UseVisualStyleBackColor = false;
         this.bFP.Click += new System.EventHandler(this.Button4_Click);
         // 
         // bConInf
         // 
         this.bConInf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.bConInf.Dock = System.Windows.Forms.DockStyle.Left;
         this.bConInf.FlatAppearance.BorderSize = 0;
         this.bConInf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bConInf.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bConInf.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(200)))), ((int)(((byte)(252)))));
         this.bConInf.Location = new System.Drawing.Point(270, 0);
         this.bConInf.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bConInf.Name = "bConInf";
         this.bConInf.Size = new System.Drawing.Size(100, 60);
         this.bConInf.TabIndex = 102;
         this.bConInf.TabStop = false;
         this.bConInf.Text = "Personal Information";
         this.bConInf.UseVisualStyleBackColor = false;
         this.bConInf.Click += new System.EventHandler(this.Button2_Click);
         // 
         // bPDetails
         // 
         this.bPDetails.BackColor = System.Drawing.Color.White;
         this.bPDetails.Dock = System.Windows.Forms.DockStyle.Left;
         this.bPDetails.FlatAppearance.BorderSize = 0;
         this.bPDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bPDetails.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bPDetails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
         this.bPDetails.Location = new System.Drawing.Point(170, 0);
         this.bPDetails.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bPDetails.Name = "bPDetails";
         this.bPDetails.Size = new System.Drawing.Size(100, 60);
         this.bPDetails.TabIndex = 101;
         this.bPDetails.TabStop = false;
         this.bPDetails.Text = "Basic Information";
         this.bPDetails.UseVisualStyleBackColor = false;
         this.bPDetails.Click += new System.EventHandler(this.Button1_Click);
         // 
         // label1
         // 
         this.label1.Dock = System.Windows.Forms.DockStyle.Left;
         this.label1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.ForeColor = System.Drawing.Color.White;
         this.label1.Location = new System.Drawing.Point(0, 0);
         this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(170, 60);
         this.label1.TabIndex = 0;
         this.label1.Text = "Resident";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EntryForm_Residents_MouseDown);
         // 
         // bNext
         // 
         this.bNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bNext.FlatAppearance.BorderSize = 0;
         this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bNext.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bNext.ForeColor = System.Drawing.Color.White;
         this.bNext.Location = new System.Drawing.Point(729, 537);
         this.bNext.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bNext.Name = "bNext";
         this.bNext.Size = new System.Drawing.Size(103, 33);
         this.bNext.TabIndex = 40;
         this.bNext.Text = "Next";
         this.bNext.UseVisualStyleBackColor = false;
         this.bNext.Click += new System.EventHandler(this.Button3_Click_1);
         // 
         // panel1
         // 
         this.panel1.Controls.Add(this.panel2);
         this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel1.Location = new System.Drawing.Point(0, 60);
         this.panel1.Name = "panel1";
         this.panel1.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
         this.panel1.Size = new System.Drawing.Size(842, 468);
         this.panel1.TabIndex = 101;
         // 
         // panel2
         // 
         this.panel2.BackColor = System.Drawing.Color.White;
         this.panel2.Controls.Add(this.tabControl1);
         this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.panel2.Location = new System.Drawing.Point(10, 0);
         this.panel2.Name = "panel2";
         this.panel2.Size = new System.Drawing.Size(822, 468);
         this.panel2.TabIndex = 0;
         // 
         // tabControl1
         // 
         this.tabControl1.Controls.Add(this.tabPage1);
         this.tabControl1.Controls.Add(this.tabPage2);
         this.tabControl1.Controls.Add(this.tabPage3);
         this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
         this.tabControl1.Location = new System.Drawing.Point(0, 0);
         this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
         this.tabControl1.MaximumSize = new System.Drawing.Size(822, 514);
         this.tabControl1.Name = "tabControl1";
         this.tabControl1.Padding = new System.Drawing.Point(0, 0);
         this.tabControl1.SelectedIndex = 0;
         this.tabControl1.Size = new System.Drawing.Size(822, 468);
         this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
         this.tabControl1.TabIndex = 3;
         this.tabControl1.TabStop = false;
         this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.TabControl1_SelectedIndexChanged);
         // 
         // tabPage1
         // 
         this.tabPage1.BackColor = System.Drawing.Color.White;
         this.tabPage1.Controls.Add(this.button2);
         this.tabPage1.Controls.Add(this.bBrowsePhoto);
         this.tabPage1.Controls.Add(this.residentPhoto);
         this.tabPage1.Controls.Add(this.panelContactDetails);
         this.tabPage1.Controls.Add(this.label21);
         this.tabPage1.Controls.Add(this.panelAddress);
         this.tabPage1.Controls.Add(this.label6);
         this.tabPage1.Controls.Add(this.label9);
         this.tabPage1.Controls.Add(this.panelClientName);
         this.tabPage1.Location = new System.Drawing.Point(4, 5);
         this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
         this.tabPage1.Name = "tabPage1";
         this.tabPage1.Padding = new System.Windows.Forms.Padding(30, 34, 30, 34);
         this.tabPage1.Size = new System.Drawing.Size(814, 459);
         this.tabPage1.TabIndex = 0;
         this.tabPage1.Enter += new System.EventHandler(this.TabPage1_Enter);
         // 
         // button2
         // 
         this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.button2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.button2.ForeColor = System.Drawing.Color.White;
         this.button2.Location = new System.Drawing.Point(704, 239);
         this.button2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.button2.Name = "button2";
         this.button2.Size = new System.Drawing.Size(89, 32);
         this.button2.TabIndex = 38;
         this.button2.TabStop = false;
         this.button2.Text = "Capture";
         this.button2.UseVisualStyleBackColor = false;
         this.button2.Click += new System.EventHandler(this.button2_Click_1);
         // 
         // bBrowsePhoto
         // 
         this.bBrowsePhoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bBrowsePhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bBrowsePhoto.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bBrowsePhoto.ForeColor = System.Drawing.Color.White;
         this.bBrowsePhoto.Location = new System.Drawing.Point(704, 276);
         this.bBrowsePhoto.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bBrowsePhoto.Name = "bBrowsePhoto";
         this.bBrowsePhoto.Size = new System.Drawing.Size(89, 32);
         this.bBrowsePhoto.TabIndex = 3;
         this.bBrowsePhoto.Text = "Browse";
         this.bBrowsePhoto.UseVisualStyleBackColor = false;
         this.bBrowsePhoto.Click += new System.EventHandler(this.button1_Click_1);
         // 
         // panelContactDetails
         // 
         this.panelContactDetails.BackColor = System.Drawing.Color.White;
         this.panelContactDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelContactDetails.Controls.Add(this.bRemoveContact);
         this.panelContactDetails.Controls.Add(this.bEditContact);
         this.panelContactDetails.Controls.Add(this.bAddContact);
         this.panelContactDetails.Controls.Add(this.listView1);
         this.panelContactDetails.Location = new System.Drawing.Point(410, 49);
         this.panelContactDetails.Name = "panelContactDetails";
         this.panelContactDetails.Size = new System.Drawing.Size(396, 181);
         this.panelContactDetails.TabIndex = 11;
         // 
         // bRemoveContact
         // 
         this.bRemoveContact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bRemoveContact.Enabled = false;
         this.bRemoveContact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bRemoveContact.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bRemoveContact.ForeColor = System.Drawing.Color.White;
         this.bRemoveContact.Location = new System.Drawing.Point(299, 139);
         this.bRemoveContact.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bRemoveContact.Name = "bRemoveContact";
         this.bRemoveContact.Size = new System.Drawing.Size(89, 32);
         this.bRemoveContact.TabIndex = 6;
         this.bRemoveContact.Text = "Remove";
         this.bRemoveContact.UseVisualStyleBackColor = false;
         this.bRemoveContact.Click += new System.EventHandler(this.BRemoveContact_Click);
         // 
         // bEditContact
         // 
         this.bEditContact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bEditContact.Enabled = false;
         this.bEditContact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bEditContact.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bEditContact.ForeColor = System.Drawing.Color.White;
         this.bEditContact.Location = new System.Drawing.Point(202, 139);
         this.bEditContact.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bEditContact.Name = "bEditContact";
         this.bEditContact.Size = new System.Drawing.Size(89, 32);
         this.bEditContact.TabIndex = 5;
         this.bEditContact.Text = "Edit";
         this.bEditContact.UseVisualStyleBackColor = false;
         this.bEditContact.Click += new System.EventHandler(this.BEditContact_Click);
         // 
         // bAddContact
         // 
         this.bAddContact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bAddContact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bAddContact.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bAddContact.ForeColor = System.Drawing.Color.White;
         this.bAddContact.Location = new System.Drawing.Point(105, 139);
         this.bAddContact.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bAddContact.Name = "bAddContact";
         this.bAddContact.Size = new System.Drawing.Size(89, 32);
         this.bAddContact.TabIndex = 4;
         this.bAddContact.Text = "Add";
         this.bAddContact.UseVisualStyleBackColor = false;
         this.bAddContact.Click += new System.EventHandler(this.bAddContact_Click);
         // 
         // listView1
         // 
         this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.contacttype,
            this.contact,
            this.smsgroup});
         this.listView1.Dock = System.Windows.Forms.DockStyle.Top;
         this.listView1.FullRowSelect = true;
         this.listView1.GridLines = true;
         this.listView1.HideSelection = false;
         this.listView1.Location = new System.Drawing.Point(0, 0);
         this.listView1.MultiSelect = false;
         this.listView1.Name = "listView1";
         this.listView1.Size = new System.Drawing.Size(392, 134);
         this.listView1.TabIndex = 0;
         this.listView1.UseCompatibleStateImageBehavior = false;
         this.listView1.View = System.Windows.Forms.View.Details;
         this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
         // 
         // contacttype
         // 
         this.contacttype.Text = "Contact Type";
         this.contacttype.Width = 117;
         // 
         // contact
         // 
         this.contact.Text = "Contact";
         this.contact.Width = 124;
         // 
         // smsgroup
         // 
         this.smsgroup.Text = "SMS Group";
         this.smsgroup.Width = 130;
         // 
         // label21
         // 
         this.label21.BackColor = System.Drawing.Color.Navy;
         this.label21.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label21.ForeColor = System.Drawing.Color.White;
         this.label21.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label21.ImageKey = "icons8-user-40.png";
         this.label21.Location = new System.Drawing.Point(409, 15);
         this.label21.Margin = new System.Windows.Forms.Padding(0);
         this.label21.Name = "label21";
         this.label21.Size = new System.Drawing.Size(397, 31);
         this.label21.TabIndex = 29;
         this.label21.Text = "Contact Details";
         this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panelAddress
         // 
         this.panelAddress.BackColor = System.Drawing.Color.White;
         this.panelAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelAddress.Controls.Add(this.provAddress);
         this.panelAddress.Controls.Add(this.ProvID);
         this.panelAddress.Controls.Add(this.label14);
         this.panelAddress.Controls.Add(this.label13);
         this.panelAddress.Controls.Add(this.CityID);
         this.panelAddress.Controls.Add(this.BrgyID);
         this.panelAddress.Controls.Add(this.purokID);
         this.panelAddress.Controls.Add(this.label8);
         this.panelAddress.Controls.Add(this.label10);
         this.panelAddress.Controls.Add(this.label11);
         this.panelAddress.Controls.Add(this.houseNo);
         this.panelAddress.Controls.Add(this.label12);
         this.panelAddress.Location = new System.Drawing.Point(9, 202);
         this.panelAddress.Name = "panelAddress";
         this.panelAddress.Size = new System.Drawing.Size(396, 162);
         this.panelAddress.TabIndex = 28;
         // 
         // provAddress
         // 
         this.provAddress.Location = new System.Drawing.Point(110, 131);
         this.provAddress.MinimumSize = new System.Drawing.Size(4, 25);
         this.provAddress.Name = "provAddress";
         this.provAddress.Size = new System.Drawing.Size(281, 29);
         this.provAddress.TabIndex = 10;
         this.provAddress.Tag = "dbbarangay";
         // 
         // ProvID
         // 
         this.ProvID.BackColor = System.Drawing.Color.White;
         this.ProvID.DropDownWidth = 200;
         this.ProvID.FormattingEnabled = true;
         this.ProvID.Location = new System.Drawing.Point(110, 105);
         this.ProvID.MinimumSize = new System.Drawing.Size(281, 0);
         this.ProvID.Name = "ProvID";
         this.ProvID.Size = new System.Drawing.Size(281, 30);
         this.ProvID.TabIndex = 9;
         this.ProvID.Tag = "dbbarangay";
         // 
         // label14
         // 
         this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label14.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label14.ForeColor = System.Drawing.Color.White;
         this.label14.Location = new System.Drawing.Point(1, 131);
         this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label14.MinimumSize = new System.Drawing.Size(109, 25);
         this.label14.Name = "label14";
         this.label14.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label14.Size = new System.Drawing.Size(109, 25);
         this.label14.TabIndex = 33;
         this.label14.Text = "Prov. Address";
         this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label13
         // 
         this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label13.ForeColor = System.Drawing.Color.White;
         this.label13.Location = new System.Drawing.Point(1, 105);
         this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label13.MinimumSize = new System.Drawing.Size(109, 25);
         this.label13.Name = "label13";
         this.label13.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label13.Size = new System.Drawing.Size(109, 25);
         this.label13.TabIndex = 32;
         this.label13.Text = "Province";
         this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // CityID
         // 
         this.CityID.BackColor = System.Drawing.Color.White;
         this.CityID.DropDownWidth = 200;
         this.CityID.FormattingEnabled = true;
         this.CityID.Location = new System.Drawing.Point(110, 79);
         this.CityID.MinimumSize = new System.Drawing.Size(281, 0);
         this.CityID.Name = "CityID";
         this.CityID.Size = new System.Drawing.Size(281, 30);
         this.CityID.TabIndex = 8;
         this.CityID.Tag = "dbbarangay";
         // 
         // BrgyID
         // 
         this.BrgyID.BackColor = System.Drawing.Color.White;
         this.BrgyID.DropDownWidth = 283;
         this.BrgyID.FormattingEnabled = true;
         this.BrgyID.Location = new System.Drawing.Point(110, 53);
         this.BrgyID.MinimumSize = new System.Drawing.Size(281, 0);
         this.BrgyID.Name = "BrgyID";
         this.BrgyID.Size = new System.Drawing.Size(281, 30);
         this.BrgyID.TabIndex = 7;
         this.BrgyID.Tag = "dbbarangay";
         // 
         // purokID
         // 
         this.purokID.DropDownWidth = 200;
         this.purokID.FormattingEnabled = true;
         this.purokID.Location = new System.Drawing.Point(110, 27);
         this.purokID.MinimumSize = new System.Drawing.Size(281, 0);
         this.purokID.Name = "purokID";
         this.purokID.Size = new System.Drawing.Size(281, 30);
         this.purokID.TabIndex = 6;
         // 
         // label8
         // 
         this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label8.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label8.ForeColor = System.Drawing.Color.White;
         this.label8.Location = new System.Drawing.Point(1, 79);
         this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label8.MinimumSize = new System.Drawing.Size(109, 25);
         this.label8.Name = "label8";
         this.label8.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label8.Size = new System.Drawing.Size(109, 25);
         this.label8.TabIndex = 29;
         this.label8.Text = "City/Mun.";
         this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label10
         // 
         this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label10.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label10.ForeColor = System.Drawing.Color.White;
         this.label10.Location = new System.Drawing.Point(1, 53);
         this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label10.MinimumSize = new System.Drawing.Size(109, 25);
         this.label10.Name = "label10";
         this.label10.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label10.Size = new System.Drawing.Size(109, 25);
         this.label10.TabIndex = 27;
         this.label10.Text = "Barangay";
         this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label11
         // 
         this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label11.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label11.ForeColor = System.Drawing.Color.White;
         this.label11.Location = new System.Drawing.Point(1, 27);
         this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label11.MinimumSize = new System.Drawing.Size(109, 25);
         this.label11.Name = "label11";
         this.label11.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label11.Size = new System.Drawing.Size(109, 25);
         this.label11.TabIndex = 25;
         this.label11.Text = "Purok";
         this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // houseNo
         // 
         this.houseNo.Location = new System.Drawing.Point(110, 1);
         this.houseNo.MinimumSize = new System.Drawing.Size(4, 25);
         this.houseNo.Name = "houseNo";
         this.houseNo.Size = new System.Drawing.Size(281, 29);
         this.houseNo.TabIndex = 5;
         // 
         // label12
         // 
         this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label12.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label12.ForeColor = System.Drawing.Color.White;
         this.label12.Location = new System.Drawing.Point(1, 1);
         this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label12.MinimumSize = new System.Drawing.Size(109, 25);
         this.label12.Name = "label12";
         this.label12.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label12.Size = new System.Drawing.Size(109, 25);
         this.label12.TabIndex = 23;
         this.label12.Text = "House No.";
         this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label6
         // 
         this.label6.BackColor = System.Drawing.Color.Navy;
         this.label6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label6.ForeColor = System.Drawing.Color.White;
         this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label6.ImageKey = "icons8-user-40.png";
         this.label6.Location = new System.Drawing.Point(9, 168);
         this.label6.Margin = new System.Windows.Forms.Padding(0);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(396, 31);
         this.label6.TabIndex = 27;
         this.label6.Text = "Address";
         this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label9
         // 
         this.label9.BackColor = System.Drawing.Color.Navy;
         this.label9.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label9.ForeColor = System.Drawing.Color.White;
         this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label9.ImageKey = "icons8-user-40.png";
         this.label9.Location = new System.Drawing.Point(9, 15);
         this.label9.Margin = new System.Windows.Forms.Padding(0);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(396, 31);
         this.label9.TabIndex = 26;
         this.label9.Text = "Client Name";
         this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panelClientName
         // 
         this.panelClientName.BackColor = System.Drawing.Color.White;
         this.panelClientName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelClientName.Controls.Add(this.Alias);
         this.panelClientName.Controls.Add(this.label5);
         this.panelClientName.Controls.Add(this.lastName);
         this.panelClientName.Controls.Add(this.label4);
         this.panelClientName.Controls.Add(this.middleName);
         this.panelClientName.Controls.Add(this.label2);
         this.panelClientName.Controls.Add(this.firstName);
         this.panelClientName.Controls.Add(this.label7);
         this.panelClientName.Location = new System.Drawing.Point(9, 49);
         this.panelClientName.Name = "panelClientName";
         this.panelClientName.Size = new System.Drawing.Size(396, 110);
         this.panelClientName.TabIndex = 25;
         // 
         // Alias
         // 
         this.Alias.Location = new System.Drawing.Point(110, 79);
         this.Alias.MinimumSize = new System.Drawing.Size(4, 25);
         this.Alias.Name = "Alias";
         this.Alias.Size = new System.Drawing.Size(281, 29);
         this.Alias.TabIndex = 4;
         // 
         // label5
         // 
         this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.ForeColor = System.Drawing.Color.White;
         this.label5.Location = new System.Drawing.Point(1, 79);
         this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label5.MinimumSize = new System.Drawing.Size(0, 25);
         this.label5.Name = "label5";
         this.label5.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label5.Size = new System.Drawing.Size(109, 25);
         this.label5.TabIndex = 29;
         this.label5.Text = "Alias";
         this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // lastName
         // 
         this.lastName.Location = new System.Drawing.Point(110, 53);
         this.lastName.MinimumSize = new System.Drawing.Size(4, 25);
         this.lastName.Name = "lastName";
         this.lastName.Size = new System.Drawing.Size(281, 29);
         this.lastName.TabIndex = 3;
         // 
         // label4
         // 
         this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label4.ForeColor = System.Drawing.Color.White;
         this.label4.Location = new System.Drawing.Point(1, 53);
         this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label4.MinimumSize = new System.Drawing.Size(0, 25);
         this.label4.Name = "label4";
         this.label4.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label4.Size = new System.Drawing.Size(109, 25);
         this.label4.TabIndex = 27;
         this.label4.Text = "Last Name";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // middleName
         // 
         this.middleName.Location = new System.Drawing.Point(110, 27);
         this.middleName.MinimumSize = new System.Drawing.Size(4, 25);
         this.middleName.Name = "middleName";
         this.middleName.Size = new System.Drawing.Size(281, 29);
         this.middleName.TabIndex = 2;
         // 
         // label2
         // 
         this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.ForeColor = System.Drawing.Color.White;
         this.label2.Location = new System.Drawing.Point(1, 27);
         this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label2.MinimumSize = new System.Drawing.Size(0, 25);
         this.label2.Name = "label2";
         this.label2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label2.Size = new System.Drawing.Size(109, 25);
         this.label2.TabIndex = 25;
         this.label2.Text = "Middle Name";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // firstName
         // 
         this.firstName.Location = new System.Drawing.Point(110, 1);
         this.firstName.MinimumSize = new System.Drawing.Size(4, 25);
         this.firstName.Name = "firstName";
         this.firstName.Size = new System.Drawing.Size(281, 29);
         this.firstName.TabIndex = 1;
         // 
         // label7
         // 
         this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label7.ForeColor = System.Drawing.Color.White;
         this.label7.Location = new System.Drawing.Point(1, 1);
         this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label7.MinimumSize = new System.Drawing.Size(0, 25);
         this.label7.Name = "label7";
         this.label7.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label7.Size = new System.Drawing.Size(109, 25);
         this.label7.TabIndex = 23;
         this.label7.Text = "First Name";
         this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // tabPage2
         // 
         this.tabPage2.BackColor = System.Drawing.Color.White;
         this.tabPage2.Controls.Add(this.panelImmediateFamily);
         this.tabPage2.Controls.Add(this.label44);
         this.tabPage2.Controls.Add(this.panelDeceasedDetails);
         this.tabPage2.Controls.Add(this.label31);
         this.tabPage2.Controls.Add(this.panelMiscellaneousDetails);
         this.tabPage2.Controls.Add(this.label30);
         this.tabPage2.Controls.Add(this.panelEmploymentDetails);
         this.tabPage2.Controls.Add(this.label29);
         this.tabPage2.Controls.Add(this.panelPhysicalDetails);
         this.tabPage2.Controls.Add(this.label3);
         this.tabPage2.Controls.Add(this.label15);
         this.tabPage2.Controls.Add(this.panelPersonalDetails);
         this.tabPage2.Location = new System.Drawing.Point(4, 5);
         this.tabPage2.Margin = new System.Windows.Forms.Padding(0);
         this.tabPage2.Name = "tabPage2";
         this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.tabPage2.Size = new System.Drawing.Size(814, 459);
         this.tabPage2.TabIndex = 1;
         this.tabPage2.Enter += new System.EventHandler(this.TabPage2_Enter);
         // 
         // panelImmediateFamily
         // 
         this.panelImmediateFamily.BackColor = System.Drawing.Color.White;
         this.panelImmediateFamily.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelImmediateFamily.Controls.Add(this.mAddress);
         this.panelImmediateFamily.Controls.Add(this.Mother);
         this.panelImmediateFamily.Controls.Add(this.label50);
         this.panelImmediateFamily.Controls.Add(this.fAddress);
         this.panelImmediateFamily.Controls.Add(this.Father);
         this.panelImmediateFamily.Controls.Add(this.label49);
         this.panelImmediateFamily.Controls.Add(this.sAddress);
         this.panelImmediateFamily.Controls.Add(this.Spouse);
         this.panelImmediateFamily.Controls.Add(this.label48);
         this.panelImmediateFamily.Controls.Add(this.label45);
         this.panelImmediateFamily.Controls.Add(this.label46);
         this.panelImmediateFamily.Controls.Add(this.label47);
         this.panelImmediateFamily.Location = new System.Drawing.Point(412, 287);
         this.panelImmediateFamily.Name = "panelImmediateFamily";
         this.panelImmediateFamily.Size = new System.Drawing.Size(396, 110);
         this.panelImmediateFamily.TabIndex = 40;
         // 
         // mAddress
         // 
         this.mAddress.Location = new System.Drawing.Point(223, 79);
         this.mAddress.Name = "mAddress";
         this.mAddress.Size = new System.Drawing.Size(168, 29);
         this.mAddress.TabIndex = 37;
         // 
         // Mother
         // 
         this.Mother.Location = new System.Drawing.Point(60, 79);
         this.Mother.Name = "Mother";
         this.Mother.Size = new System.Drawing.Size(164, 29);
         this.Mother.TabIndex = 36;
         // 
         // label50
         // 
         this.label50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label50.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label50.ForeColor = System.Drawing.Color.White;
         this.label50.Location = new System.Drawing.Point(1, 79);
         this.label50.Margin = new System.Windows.Forms.Padding(0);
         this.label50.Name = "label50";
         this.label50.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label50.Size = new System.Drawing.Size(59, 25);
         this.label50.TabIndex = 16;
         this.label50.Text = "Mother";
         this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // fAddress
         // 
         this.fAddress.Location = new System.Drawing.Point(223, 53);
         this.fAddress.Name = "fAddress";
         this.fAddress.Size = new System.Drawing.Size(168, 29);
         this.fAddress.TabIndex = 35;
         // 
         // Father
         // 
         this.Father.Location = new System.Drawing.Point(60, 53);
         this.Father.Name = "Father";
         this.Father.Size = new System.Drawing.Size(164, 29);
         this.Father.TabIndex = 34;
         // 
         // label49
         // 
         this.label49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label49.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label49.ForeColor = System.Drawing.Color.White;
         this.label49.Location = new System.Drawing.Point(1, 53);
         this.label49.Margin = new System.Windows.Forms.Padding(0);
         this.label49.Name = "label49";
         this.label49.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label49.Size = new System.Drawing.Size(59, 25);
         this.label49.TabIndex = 13;
         this.label49.Text = " Father";
         this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // sAddress
         // 
         this.sAddress.Location = new System.Drawing.Point(223, 27);
         this.sAddress.Name = "sAddress";
         this.sAddress.Size = new System.Drawing.Size(168, 29);
         this.sAddress.TabIndex = 33;
         // 
         // Spouse
         // 
         this.Spouse.Location = new System.Drawing.Point(60, 27);
         this.Spouse.Name = "Spouse";
         this.Spouse.Size = new System.Drawing.Size(164, 29);
         this.Spouse.TabIndex = 32;
         // 
         // label48
         // 
         this.label48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label48.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label48.ForeColor = System.Drawing.Color.White;
         this.label48.Location = new System.Drawing.Point(1, 27);
         this.label48.Margin = new System.Windows.Forms.Padding(0);
         this.label48.Name = "label48";
         this.label48.Size = new System.Drawing.Size(59, 25);
         this.label48.TabIndex = 9;
         this.label48.Text = " Spouse";
         this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label45
         // 
         this.label45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label45.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label45.ForeColor = System.Drawing.Color.White;
         this.label45.Location = new System.Drawing.Point(223, 1);
         this.label45.Margin = new System.Windows.Forms.Padding(0);
         this.label45.Name = "label45";
         this.label45.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label45.Size = new System.Drawing.Size(168, 25);
         this.label45.TabIndex = 8;
         this.label45.Text = "Address";
         this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label46
         // 
         this.label46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label46.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label46.ForeColor = System.Drawing.Color.White;
         this.label46.Location = new System.Drawing.Point(60, 1);
         this.label46.Margin = new System.Windows.Forms.Padding(0);
         this.label46.Name = "label46";
         this.label46.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label46.Size = new System.Drawing.Size(164, 25);
         this.label46.TabIndex = 7;
         this.label46.Text = "Name";
         this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label47
         // 
         this.label47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label47.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label47.ForeColor = System.Drawing.Color.White;
         this.label47.Location = new System.Drawing.Point(1, 1);
         this.label47.Margin = new System.Windows.Forms.Padding(0);
         this.label47.Name = "label47";
         this.label47.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label47.Size = new System.Drawing.Size(59, 25);
         this.label47.TabIndex = 6;
         this.label47.Text = " ";
         this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label44
         // 
         this.label44.BackColor = System.Drawing.Color.Navy;
         this.label44.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label44.ForeColor = System.Drawing.Color.White;
         this.label44.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label44.ImageKey = "icons8-user-40.png";
         this.label44.Location = new System.Drawing.Point(412, 253);
         this.label44.Margin = new System.Windows.Forms.Padding(0);
         this.label44.Name = "label44";
         this.label44.Size = new System.Drawing.Size(396, 31);
         this.label44.TabIndex = 39;
         this.label44.Text = "Immediate Family Details";
         this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panelDeceasedDetails
         // 
         this.panelDeceasedDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelDeceasedDetails.Controls.Add(this.POD);
         this.panelDeceasedDetails.Controls.Add(this.label32);
         this.panelDeceasedDetails.Controls.Add(this.DOD);
         this.panelDeceasedDetails.Controls.Add(this.label43);
         this.panelDeceasedDetails.Location = new System.Drawing.Point(9, 413);
         this.panelDeceasedDetails.Name = "panelDeceasedDetails";
         this.panelDeceasedDetails.Size = new System.Drawing.Size(396, 32);
         this.panelDeceasedDetails.TabIndex = 38;
         // 
         // POD
         // 
         this.POD.BackColor = System.Drawing.Color.White;
         this.POD.DropDownWidth = 283;
         this.POD.FormattingEnabled = true;
         this.POD.Location = new System.Drawing.Point(234, 1);
         this.POD.Name = "POD";
         this.POD.Size = new System.Drawing.Size(155, 30);
         this.POD.TabIndex = 39;
         // 
         // label32
         // 
         this.label32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label32.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label32.ForeColor = System.Drawing.Color.White;
         this.label32.Location = new System.Drawing.Point(173, 1);
         this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label32.Name = "label32";
         this.label32.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label32.Size = new System.Drawing.Size(63, 25);
         this.label32.TabIndex = 9;
         this.label32.Text = "Place";
         this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // DOD
         // 
         this.DOD.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.DOD.Location = new System.Drawing.Point(57, 1);
         this.DOD.Name = "DOD";
         this.DOD.ShowUpDown = true;
         this.DOD.Size = new System.Drawing.Size(117, 29);
         this.DOD.TabIndex = 38;
         // 
         // label43
         // 
         this.label43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label43.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label43.ForeColor = System.Drawing.Color.White;
         this.label43.Location = new System.Drawing.Point(1, 1);
         this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label43.Name = "label43";
         this.label43.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label43.Size = new System.Drawing.Size(56, 25);
         this.label43.TabIndex = 7;
         this.label43.Text = "Date";
         this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label31
         // 
         this.label31.BackColor = System.Drawing.Color.Navy;
         this.label31.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label31.ForeColor = System.Drawing.Color.White;
         this.label31.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label31.ImageKey = "icons8-user-40.png";
         this.label31.Location = new System.Drawing.Point(9, 378);
         this.label31.Margin = new System.Windows.Forms.Padding(0);
         this.label31.Name = "label31";
         this.label31.Size = new System.Drawing.Size(396, 31);
         this.label31.TabIndex = 35;
         this.label31.Text = "Deceased Details of Death";
         this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panelMiscellaneousDetails
         // 
         this.panelMiscellaneousDetails.BackColor = System.Drawing.Color.White;
         this.panelMiscellaneousDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelMiscellaneousDetails.Controls.Add(this.SCID);
         this.panelMiscellaneousDetails.Controls.Add(this.Disability);
         this.panelMiscellaneousDetails.Controls.Add(this.label37);
         this.panelMiscellaneousDetails.Controls.Add(this.label42);
         this.panelMiscellaneousDetails.Location = new System.Drawing.Point(412, 190);
         this.panelMiscellaneousDetails.Name = "panelMiscellaneousDetails";
         this.panelMiscellaneousDetails.Size = new System.Drawing.Size(396, 58);
         this.panelMiscellaneousDetails.TabIndex = 34;
         // 
         // SCID
         // 
         this.SCID.Location = new System.Drawing.Point(110, 1);
         this.SCID.Name = "SCID";
         this.SCID.Size = new System.Drawing.Size(281, 29);
         this.SCID.TabIndex = 30;
         // 
         // Disability
         // 
         this.Disability.BackColor = System.Drawing.Color.White;
         this.Disability.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Disability.DropDownWidth = 283;
         this.Disability.FormattingEnabled = true;
         this.Disability.Location = new System.Drawing.Point(111, 27);
         this.Disability.Name = "Disability";
         this.Disability.Size = new System.Drawing.Size(281, 30);
         this.Disability.TabIndex = 31;
         // 
         // label37
         // 
         this.label37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label37.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label37.ForeColor = System.Drawing.Color.White;
         this.label37.Location = new System.Drawing.Point(1, 27);
         this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label37.Name = "label37";
         this.label37.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label37.Size = new System.Drawing.Size(109, 25);
         this.label37.TabIndex = 25;
         this.label37.Text = "Disability";
         this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label42
         // 
         this.label42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label42.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label42.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label42.ForeColor = System.Drawing.Color.White;
         this.label42.Location = new System.Drawing.Point(1, 1);
         this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label42.Name = "label42";
         this.label42.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label42.Size = new System.Drawing.Size(109, 25);
         this.label42.TabIndex = 23;
         this.label42.Text = "Sr. Citizen ID";
         this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label30
         // 
         this.label30.BackColor = System.Drawing.Color.Navy;
         this.label30.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label30.ForeColor = System.Drawing.Color.White;
         this.label30.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label30.ImageKey = "icons8-user-40.png";
         this.label30.Location = new System.Drawing.Point(412, 156);
         this.label30.Margin = new System.Windows.Forms.Padding(0);
         this.label30.Name = "label30";
         this.label30.Size = new System.Drawing.Size(396, 31);
         this.label30.TabIndex = 33;
         this.label30.Text = "Miscellaneous Details";
         this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panelEmploymentDetails
         // 
         this.panelEmploymentDetails.BackColor = System.Drawing.Color.White;
         this.panelEmploymentDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelEmploymentDetails.Controls.Add(this.Industry);
         this.panelEmploymentDetails.Controls.Add(this.Occupation);
         this.panelEmploymentDetails.Controls.Add(this.WorkerType);
         this.panelEmploymentDetails.Controls.Add(this.Employment);
         this.panelEmploymentDetails.Controls.Add(this.label38);
         this.panelEmploymentDetails.Controls.Add(this.label39);
         this.panelEmploymentDetails.Controls.Add(this.label40);
         this.panelEmploymentDetails.Controls.Add(this.label41);
         this.panelEmploymentDetails.Location = new System.Drawing.Point(412, 38);
         this.panelEmploymentDetails.Name = "panelEmploymentDetails";
         this.panelEmploymentDetails.Size = new System.Drawing.Size(396, 110);
         this.panelEmploymentDetails.TabIndex = 32;
         // 
         // Industry
         // 
         this.Industry.BackColor = System.Drawing.Color.White;
         this.Industry.DropDownWidth = 283;
         this.Industry.FormattingEnabled = true;
         this.Industry.Location = new System.Drawing.Point(110, 79);
         this.Industry.Name = "Industry";
         this.Industry.Size = new System.Drawing.Size(281, 30);
         this.Industry.TabIndex = 29;
         // 
         // Occupation
         // 
         this.Occupation.BackColor = System.Drawing.Color.White;
         this.Occupation.DropDownWidth = 283;
         this.Occupation.FormattingEnabled = true;
         this.Occupation.Location = new System.Drawing.Point(110, 53);
         this.Occupation.Name = "Occupation";
         this.Occupation.Size = new System.Drawing.Size(281, 30);
         this.Occupation.TabIndex = 28;
         // 
         // WorkerType
         // 
         this.WorkerType.BackColor = System.Drawing.Color.White;
         this.WorkerType.DropDownWidth = 283;
         this.WorkerType.FormattingEnabled = true;
         this.WorkerType.Location = new System.Drawing.Point(110, 27);
         this.WorkerType.Name = "WorkerType";
         this.WorkerType.Size = new System.Drawing.Size(281, 30);
         this.WorkerType.TabIndex = 27;
         // 
         // Employment
         // 
         this.Employment.BackColor = System.Drawing.Color.White;
         this.Employment.DropDownWidth = 283;
         this.Employment.FormattingEnabled = true;
         this.Employment.Location = new System.Drawing.Point(110, 1);
         this.Employment.Name = "Employment";
         this.Employment.Size = new System.Drawing.Size(281, 30);
         this.Employment.TabIndex = 26;
         // 
         // label38
         // 
         this.label38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label38.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label38.ForeColor = System.Drawing.Color.White;
         this.label38.Location = new System.Drawing.Point(1, 79);
         this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label38.Name = "label38";
         this.label38.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label38.Size = new System.Drawing.Size(109, 25);
         this.label38.TabIndex = 29;
         this.label38.Text = "Industry";
         this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label39
         // 
         this.label39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label39.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label39.ForeColor = System.Drawing.Color.White;
         this.label39.Location = new System.Drawing.Point(1, 53);
         this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label39.Name = "label39";
         this.label39.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label39.Size = new System.Drawing.Size(109, 25);
         this.label39.TabIndex = 27;
         this.label39.Text = "Occupation";
         this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label40
         // 
         this.label40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label40.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label40.ForeColor = System.Drawing.Color.White;
         this.label40.Location = new System.Drawing.Point(1, 27);
         this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label40.Name = "label40";
         this.label40.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label40.Size = new System.Drawing.Size(109, 25);
         this.label40.TabIndex = 25;
         this.label40.Text = "Worker Type";
         this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label41
         // 
         this.label41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label41.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label41.ForeColor = System.Drawing.Color.White;
         this.label41.Location = new System.Drawing.Point(1, 1);
         this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label41.Name = "label41";
         this.label41.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label41.Size = new System.Drawing.Size(109, 25);
         this.label41.TabIndex = 23;
         this.label41.Text = "Status";
         this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label29
         // 
         this.label29.BackColor = System.Drawing.Color.Navy;
         this.label29.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label29.ForeColor = System.Drawing.Color.White;
         this.label29.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label29.ImageKey = "icons8-user-40.png";
         this.label29.Location = new System.Drawing.Point(412, 4);
         this.label29.Margin = new System.Windows.Forms.Padding(0);
         this.label29.Name = "label29";
         this.label29.Size = new System.Drawing.Size(396, 31);
         this.label29.TabIndex = 31;
         this.label29.Text = "Employment Details";
         this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panelPhysicalDetails
         // 
         this.panelPhysicalDetails.BackColor = System.Drawing.Color.White;
         this.panelPhysicalDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelPhysicalDetails.Controls.Add(this.Weight);
         this.panelPhysicalDetails.Controls.Add(this.Height);
         this.panelPhysicalDetails.Controls.Add(this.label54);
         this.panelPhysicalDetails.Controls.Add(this.Hair);
         this.panelPhysicalDetails.Controls.Add(this.Complexion);
         this.panelPhysicalDetails.Controls.Add(this.Build);
         this.panelPhysicalDetails.Controls.Add(this.label33);
         this.panelPhysicalDetails.Controls.Add(this.label34);
         this.panelPhysicalDetails.Controls.Add(this.label35);
         this.panelPhysicalDetails.Controls.Add(this.label36);
         this.panelPhysicalDetails.Location = new System.Drawing.Point(10, 263);
         this.panelPhysicalDetails.Name = "panelPhysicalDetails";
         this.panelPhysicalDetails.Size = new System.Drawing.Size(396, 110);
         this.panelPhysicalDetails.TabIndex = 30;
         // 
         // Weight
         // 
         this.Weight.DecimalPlaces = 2;
         this.Weight.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
         this.Weight.Location = new System.Drawing.Point(303, 79);
         this.Weight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
         this.Weight.Name = "Weight";
         this.Weight.Size = new System.Drawing.Size(88, 29);
         this.Weight.TabIndex = 25;
         this.Weight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         // 
         // Height
         // 
         this.Height.DecimalPlaces = 2;
         this.Height.Increment = new decimal(new int[] {
            50,
            0,
            0,
            131072});
         this.Height.Location = new System.Drawing.Point(110, 79);
         this.Height.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
         this.Height.Name = "Height";
         this.Height.Size = new System.Drawing.Size(85, 29);
         this.Height.TabIndex = 24;
         this.Height.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         // 
         // label54
         // 
         this.label54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label54.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label54.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label54.ForeColor = System.Drawing.Color.White;
         this.label54.Location = new System.Drawing.Point(195, 79);
         this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label54.Name = "label54";
         this.label54.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label54.Size = new System.Drawing.Size(109, 25);
         this.label54.TabIndex = 31;
         this.label54.Text = "Weight (kgs)";
         this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // Hair
         // 
         this.Hair.BackColor = System.Drawing.Color.White;
         this.Hair.DropDownWidth = 283;
         this.Hair.FormattingEnabled = true;
         this.Hair.Location = new System.Drawing.Point(110, 53);
         this.Hair.Name = "Hair";
         this.Hair.Size = new System.Drawing.Size(281, 30);
         this.Hair.TabIndex = 23;
         // 
         // Complexion
         // 
         this.Complexion.BackColor = System.Drawing.Color.White;
         this.Complexion.DropDownWidth = 283;
         this.Complexion.FormattingEnabled = true;
         this.Complexion.Location = new System.Drawing.Point(110, 27);
         this.Complexion.Name = "Complexion";
         this.Complexion.Size = new System.Drawing.Size(281, 30);
         this.Complexion.TabIndex = 22;
         // 
         // Build
         // 
         this.Build.BackColor = System.Drawing.Color.White;
         this.Build.DropDownWidth = 283;
         this.Build.FormattingEnabled = true;
         this.Build.Location = new System.Drawing.Point(110, 1);
         this.Build.Name = "Build";
         this.Build.Size = new System.Drawing.Size(281, 30);
         this.Build.TabIndex = 21;
         // 
         // label33
         // 
         this.label33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label33.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label33.ForeColor = System.Drawing.Color.White;
         this.label33.Location = new System.Drawing.Point(1, 79);
         this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label33.Name = "label33";
         this.label33.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label33.Size = new System.Drawing.Size(109, 25);
         this.label33.TabIndex = 29;
         this.label33.Text = "Height";
         this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label34
         // 
         this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label34.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label34.ForeColor = System.Drawing.Color.White;
         this.label34.Location = new System.Drawing.Point(1, 53);
         this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label34.Name = "label34";
         this.label34.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label34.Size = new System.Drawing.Size(109, 25);
         this.label34.TabIndex = 27;
         this.label34.Text = "Hair Color";
         this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label35
         // 
         this.label35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label35.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label35.ForeColor = System.Drawing.Color.White;
         this.label35.Location = new System.Drawing.Point(1, 27);
         this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label35.Name = "label35";
         this.label35.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label35.Size = new System.Drawing.Size(109, 25);
         this.label35.TabIndex = 25;
         this.label35.Text = "Skin";
         this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label36
         // 
         this.label36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label36.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label36.ForeColor = System.Drawing.Color.White;
         this.label36.Location = new System.Drawing.Point(1, 1);
         this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label36.Name = "label36";
         this.label36.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label36.Size = new System.Drawing.Size(109, 25);
         this.label36.TabIndex = 23;
         this.label36.Text = "Body";
         this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label3
         // 
         this.label3.BackColor = System.Drawing.Color.Navy;
         this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.ForeColor = System.Drawing.Color.White;
         this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label3.ImageKey = "icons8-user-40.png";
         this.label3.Location = new System.Drawing.Point(10, 229);
         this.label3.Margin = new System.Windows.Forms.Padding(0);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(396, 31);
         this.label3.TabIndex = 29;
         this.label3.Text = "Physical Details";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label15
         // 
         this.label15.BackColor = System.Drawing.Color.Navy;
         this.label15.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label15.ForeColor = System.Drawing.Color.White;
         this.label15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label15.ImageKey = "icons8-user-40.png";
         this.label15.Location = new System.Drawing.Point(10, 4);
         this.label15.Margin = new System.Windows.Forms.Padding(0);
         this.label15.Name = "label15";
         this.label15.Size = new System.Drawing.Size(396, 31);
         this.label15.TabIndex = 28;
         this.label15.Text = "Personal Details";
         this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panelPersonalDetails
         // 
         this.panelPersonalDetails.BackColor = System.Drawing.Color.White;
         this.panelPersonalDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panelPersonalDetails.Controls.Add(this.POB);
         this.panelPersonalDetails.Controls.Add(this.DOB);
         this.panelPersonalDetails.Controls.Add(this.Attainment);
         this.panelPersonalDetails.Controls.Add(this.Citizenship);
         this.panelPersonalDetails.Controls.Add(this.Ethnicity);
         this.panelPersonalDetails.Controls.Add(this.Religion);
         this.panelPersonalDetails.Controls.Add(this.Status);
         this.panelPersonalDetails.Controls.Add(this.Gender);
         this.panelPersonalDetails.Controls.Add(this.label27);
         this.panelPersonalDetails.Controls.Add(this.label26);
         this.panelPersonalDetails.Controls.Add(this.label25);
         this.panelPersonalDetails.Controls.Add(this.label20);
         this.panelPersonalDetails.Controls.Add(this.label16);
         this.panelPersonalDetails.Controls.Add(this.label17);
         this.panelPersonalDetails.Controls.Add(this.label18);
         this.panelPersonalDetails.Controls.Add(this.label19);
         this.panelPersonalDetails.Location = new System.Drawing.Point(10, 38);
         this.panelPersonalDetails.Name = "panelPersonalDetails";
         this.panelPersonalDetails.Size = new System.Drawing.Size(396, 188);
         this.panelPersonalDetails.TabIndex = 27;
         // 
         // POB
         // 
         this.POB.BackColor = System.Drawing.Color.White;
         this.POB.DropDownWidth = 283;
         this.POB.FormattingEnabled = true;
         this.POB.Location = new System.Drawing.Point(110, 51);
         this.POB.Name = "POB";
         this.POB.Size = new System.Drawing.Size(281, 30);
         this.POB.TabIndex = 16;
         // 
         // DOB
         // 
         this.DOB.CustomFormat = "MMMM dd, yyyy";
         this.DOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
         this.DOB.Location = new System.Drawing.Point(110, 27);
         this.DOB.Name = "DOB";
         this.DOB.ShowUpDown = true;
         this.DOB.Size = new System.Drawing.Size(281, 29);
         this.DOB.TabIndex = 15;
         // 
         // Attainment
         // 
         this.Attainment.BackColor = System.Drawing.Color.White;
         this.Attainment.DropDownWidth = 283;
         this.Attainment.FormattingEnabled = true;
         this.Attainment.Location = new System.Drawing.Point(110, 157);
         this.Attainment.Name = "Attainment";
         this.Attainment.Size = new System.Drawing.Size(281, 30);
         this.Attainment.TabIndex = 20;
         // 
         // Citizenship
         // 
         this.Citizenship.BackColor = System.Drawing.Color.White;
         this.Citizenship.DropDownWidth = 283;
         this.Citizenship.FormattingEnabled = true;
         this.Citizenship.Location = new System.Drawing.Point(110, 131);
         this.Citizenship.Name = "Citizenship";
         this.Citizenship.Size = new System.Drawing.Size(281, 30);
         this.Citizenship.TabIndex = 19;
         // 
         // Ethnicity
         // 
         this.Ethnicity.BackColor = System.Drawing.Color.White;
         this.Ethnicity.DropDownWidth = 283;
         this.Ethnicity.FormattingEnabled = true;
         this.Ethnicity.Location = new System.Drawing.Point(110, 105);
         this.Ethnicity.Name = "Ethnicity";
         this.Ethnicity.Size = new System.Drawing.Size(281, 30);
         this.Ethnicity.TabIndex = 18;
         // 
         // Religion
         // 
         this.Religion.BackColor = System.Drawing.Color.White;
         this.Religion.DropDownWidth = 283;
         this.Religion.FormattingEnabled = true;
         this.Religion.Location = new System.Drawing.Point(110, 79);
         this.Religion.Name = "Religion";
         this.Religion.Size = new System.Drawing.Size(281, 30);
         this.Religion.TabIndex = 17;
         // 
         // Status
         // 
         this.Status.BackColor = System.Drawing.Color.White;
         this.Status.DropDownWidth = 283;
         this.Status.FormattingEnabled = true;
         this.Status.Location = new System.Drawing.Point(300, 1);
         this.Status.Name = "Status";
         this.Status.Size = new System.Drawing.Size(91, 30);
         this.Status.TabIndex = 14;
         // 
         // Gender
         // 
         this.Gender.BackColor = System.Drawing.Color.White;
         this.Gender.DropDownWidth = 283;
         this.Gender.FormattingEnabled = true;
         this.Gender.Location = new System.Drawing.Point(110, 1);
         this.Gender.Name = "Gender";
         this.Gender.Size = new System.Drawing.Size(84, 30);
         this.Gender.TabIndex = 13;
         // 
         // label27
         // 
         this.label27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label27.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label27.ForeColor = System.Drawing.Color.White;
         this.label27.Location = new System.Drawing.Point(1, 157);
         this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label27.Name = "label27";
         this.label27.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label27.Size = new System.Drawing.Size(109, 25);
         this.label27.TabIndex = 34;
         this.label27.Text = "Education";
         this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label26
         // 
         this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label26.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label26.ForeColor = System.Drawing.Color.White;
         this.label26.Location = new System.Drawing.Point(1, 131);
         this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label26.Name = "label26";
         this.label26.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label26.Size = new System.Drawing.Size(109, 25);
         this.label26.TabIndex = 33;
         this.label26.Text = "Nationality";
         this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label25
         // 
         this.label25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label25.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label25.ForeColor = System.Drawing.Color.White;
         this.label25.Location = new System.Drawing.Point(1, 105);
         this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label25.Name = "label25";
         this.label25.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label25.Size = new System.Drawing.Size(109, 25);
         this.label25.TabIndex = 32;
         this.label25.Text = "Ethnicity";
         this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label20
         // 
         this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label20.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label20.ForeColor = System.Drawing.Color.White;
         this.label20.Location = new System.Drawing.Point(1, 79);
         this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label20.Name = "label20";
         this.label20.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label20.Size = new System.Drawing.Size(109, 25);
         this.label20.TabIndex = 31;
         this.label20.Text = "Religion";
         this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label16
         // 
         this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label16.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label16.ForeColor = System.Drawing.Color.White;
         this.label16.Location = new System.Drawing.Point(1, 53);
         this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label16.Name = "label16";
         this.label16.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label16.Size = new System.Drawing.Size(109, 25);
         this.label16.TabIndex = 29;
         this.label16.Text = "Birth Place";
         this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label17
         // 
         this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label17.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label17.ForeColor = System.Drawing.Color.White;
         this.label17.Location = new System.Drawing.Point(1, 27);
         this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label17.Name = "label17";
         this.label17.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label17.Size = new System.Drawing.Size(109, 25);
         this.label17.TabIndex = 27;
         this.label17.Text = "Date of Birth";
         this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label18
         // 
         this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label18.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label18.ForeColor = System.Drawing.Color.White;
         this.label18.Location = new System.Drawing.Point(194, 1);
         this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label18.Name = "label18";
         this.label18.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label18.Size = new System.Drawing.Size(109, 25);
         this.label18.TabIndex = 25;
         this.label18.Text = "Civil Status";
         this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label19
         // 
         this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label19.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label19.ForeColor = System.Drawing.Color.White;
         this.label19.Location = new System.Drawing.Point(1, 1);
         this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label19.Name = "label19";
         this.label19.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label19.Size = new System.Drawing.Size(109, 25);
         this.label19.TabIndex = 23;
         this.label19.Text = "Gender";
         this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // tabPage3
         // 
         this.tabPage3.BackColor = System.Drawing.Color.White;
         this.tabPage3.Controls.Add(this.panelFPs);
         this.tabPage3.Controls.Add(this.btnCancel);
         this.tabPage3.Controls.Add(this.txtMessage);
         this.tabPage3.Controls.Add(this.pbFingerprint);
         this.tabPage3.Location = new System.Drawing.Point(4, 5);
         this.tabPage3.Margin = new System.Windows.Forms.Padding(0);
         this.tabPage3.Name = "tabPage3";
         this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.tabPage3.Size = new System.Drawing.Size(814, 459);
         this.tabPage3.TabIndex = 2;
         this.tabPage3.Enter += new System.EventHandler(this.TabPage3_Enter);
         // 
         // panelFPs
         // 
         this.panelFPs.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panelFPs.Location = new System.Drawing.Point(4, 361);
         this.panelFPs.Name = "panelFPs";
         this.panelFPs.Size = new System.Drawing.Size(806, 92);
         this.panelFPs.TabIndex = 6;
         // 
         // btnCancel
         // 
         this.btnCancel.Enabled = false;
         this.btnCancel.Location = new System.Drawing.Point(523, 200);
         this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.btnCancel.Name = "btnCancel";
         this.btnCancel.Size = new System.Drawing.Size(92, 33);
         this.btnCancel.TabIndex = 5;
         this.btnCancel.Text = "Cancel";
         this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
         // 
         // txtMessage
         // 
         this.txtMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
         this.txtMessage.Location = new System.Drawing.Point(523, 6);
         this.txtMessage.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.txtMessage.Multiline = true;
         this.txtMessage.Name = "txtMessage";
         this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtMessage.Size = new System.Drawing.Size(284, 173);
         this.txtMessage.TabIndex = 3;
         // 
         // residentPhoto
         // 
         this.residentPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.residentPhoto.Location = new System.Drawing.Point(497, 236);
         this.residentPhoto.Name = "residentPhoto";
         this.residentPhoto.Size = new System.Drawing.Size(200, 200);
         this.residentPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.residentPhoto.TabIndex = 37;
         this.residentPhoto.TabStop = false;
         // 
         // pbFingerprint
         // 
         this.pbFingerprint.Location = new System.Drawing.Point(623, 191);
         this.pbFingerprint.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.pbFingerprint.Name = "pbFingerprint";
         this.pbFingerprint.Size = new System.Drawing.Size(184, 167);
         this.pbFingerprint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pbFingerprint.TabIndex = 4;
         this.pbFingerprint.TabStop = false;
         // 
         // EntryForm_Residents
         // 
         this.AcceptButton = this.bNext;
         this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.LightSteelBlue;
         this.ClientSize = new System.Drawing.Size(842, 582);
         this.ControlBox = false;
         this.Controls.Add(this.panel1);
         this.Controls.Add(this.bNext);
         this.Controls.Add(this.menuPanel);
         this.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.Name = "EntryForm_Residents";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
         this.Text = "Personal";
         this.Load += new System.EventHandler(this.EntryForm_Residents_Load);
         this.menuPanel.ResumeLayout(false);
         this.panel1.ResumeLayout(false);
         this.panel2.ResumeLayout(false);
         this.tabControl1.ResumeLayout(false);
         this.tabPage1.ResumeLayout(false);
         this.panelContactDetails.ResumeLayout(false);
         this.panelAddress.ResumeLayout(false);
         this.panelAddress.PerformLayout();
         this.panelClientName.ResumeLayout(false);
         this.panelClientName.PerformLayout();
         this.tabPage2.ResumeLayout(false);
         this.panelImmediateFamily.ResumeLayout(false);
         this.panelImmediateFamily.PerformLayout();
         this.panelDeceasedDetails.ResumeLayout(false);
         this.panelMiscellaneousDetails.ResumeLayout(false);
         this.panelMiscellaneousDetails.PerformLayout();
         this.panelEmploymentDetails.ResumeLayout(false);
         this.panelPhysicalDetails.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.Weight)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.Height)).EndInit();
         this.panelPersonalDetails.ResumeLayout(false);
         this.tabPage3.ResumeLayout(false);
         this.tabPage3.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.residentPhoto)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbFingerprint)).EndInit();
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel menuPanel;
        private System.Windows.Forms.Button bConInf;
        private System.Windows.Forms.Button bPDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bFP;
        private System.Windows.Forms.Label bClose;
        private System.Windows.Forms.Button bNext;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bBrowsePhoto;
        private System.Windows.Forms.PictureBox residentPhoto;
        private System.Windows.Forms.Panel panelContactDetails;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panelAddress;
        private System.Windows.Forms.TextBox provAddress;
        private System.Windows.Forms.ComboBox ProvID;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox CityID;
        private System.Windows.Forms.ComboBox BrgyID;
        private System.Windows.Forms.ComboBox purokID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox houseNo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelClientName;
        private System.Windows.Forms.TextBox Alias;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox lastName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox middleName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox firstName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panelImmediateFamily;
        private System.Windows.Forms.TextBox mAddress;
        private System.Windows.Forms.TextBox Mother;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox fAddress;
        private System.Windows.Forms.TextBox Father;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox sAddress;
        private System.Windows.Forms.TextBox Spouse;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel panelDeceasedDetails;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DateTimePicker DOD;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panelMiscellaneousDetails;
        private System.Windows.Forms.TextBox SCID;
        private System.Windows.Forms.ComboBox Disability;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panelEmploymentDetails;
        private System.Windows.Forms.ComboBox Industry;
        private System.Windows.Forms.ComboBox Occupation;
        private System.Windows.Forms.ComboBox WorkerType;
        private System.Windows.Forms.ComboBox Employment;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panelPhysicalDetails;
        private System.Windows.Forms.NumericUpDown Weight;
        private System.Windows.Forms.NumericUpDown Height;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox Hair;
        private System.Windows.Forms.ComboBox Complexion;
        private System.Windows.Forms.ComboBox Build;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panelPersonalDetails;
        private System.Windows.Forms.DateTimePicker DOB;
        private System.Windows.Forms.ComboBox Attainment;
        private System.Windows.Forms.ComboBox Citizenship;
        private System.Windows.Forms.ComboBox Ethnicity;
        private System.Windows.Forms.ComboBox Religion;
        private System.Windows.Forms.ComboBox Status;
        private System.Windows.Forms.ComboBox Gender;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.PictureBox pbFingerprint;
        private System.Windows.Forms.ComboBox POD;
        private System.Windows.Forms.ComboBox POB;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader contacttype;
        private System.Windows.Forms.ColumnHeader contact;
        private System.Windows.Forms.ColumnHeader smsgroup;
        private System.Windows.Forms.Button bRemoveContact;
        private System.Windows.Forms.Button bEditContact;
        private System.Windows.Forms.Button bAddContact;
        private System.Windows.Forms.Panel panelFPs;
    }
}