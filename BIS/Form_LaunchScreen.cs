﻿using DPUruNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Collections.Specialized;

namespace BIS
{
    public partial class Form_LaunchScreen : Form
    {
        public Form_LaunchScreen()
        {
            InitializeComponent();
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            backgroundWorker1.ProgressChanged += backgroundWorker1_ProgressChanged;
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
            backgroundWorker1.RunWorkerAsync();
        }

        private bool isStringsNull(params string[] temp)
        {
            foreach(string s in temp)
            {
                if(s == null)
                {
                    return false;
                }
            }
            return true;
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Thread.Sleep(1000);
            backgroundWorker1.ReportProgress(1);
            string encryptionKey = AppConfiguration.GetConfig("encryptionkey");
            backgroundWorker1.ReportProgress(1);
            Thread.Sleep(500);
            /*if (AppConfiguration.GetConfig("db_name") == null)
            {
                AppConfiguration.SetConfig("db_name", HashComputer.Encryptor.EncryptData("dbstandarddata", encryptionKey));
            }
            if (AppConfiguration.GetConfig("db_uid") == null)
            {
                AppConfiguration.SetConfig("db_uid", HashComputer.Encryptor.EncryptData("bimsofficial", encryptionKey));
            }
            if(AppConfiguration.GetConfig("db_password") == null)
            {
                AppConfiguration.SetConfig("db_password", HashComputer.Encryptor.EncryptData("bimsofficialpassword112143112154", encryptionKey));
            }*/
            string dbmain = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("dbmain"), encryptionKey);
            string dbserver = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("dbserver"), encryptionKey);
            string pw = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("db_password"), encryptionKey);
            string uid = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("db_uid"), encryptionKey);
            string dbstandardname = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("dbstandard"), encryptionKey);
            string dbbiometricsname = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("dbbiometrics"), encryptionKey);
            string dbsyslogsname = HashComputer.Encryptor.DecryptData(AppConfiguration.GetConfig("dbsyslogs"), encryptionKey);
            if(!isStringsNull(dbmain, dbserver, pw, uid, dbstandardname, dbbiometricsname, dbsyslogsname))
            {
                backgroundWorker1.ReportProgress(-1); //hide launch screen;
                DialogResult result = MessageBox.Show("We have detected that some of the required application settings are not yet configured. The application can't run unless these are configured.\r\n\r\nDo you want to configure it now?", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
                else
                {
                    Form_Settings fs = new Form_Settings();
                    if (fs.ShowDialog() != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                    else
                    {
                        backgroundWorker1.ReportProgress(0); //show launch screen;
                    }
                }
            }
            backgroundWorker1.ReportProgress(5);
            Thread.Sleep(1000);
            MySQLDatabase db = DatabaseManager.GetInstance();
            backgroundWorker1.ReportProgress(10);
            Thread.Sleep(1000);
            DatabaseManager.GetInstance("dbstandard");
            DatabaseManager.GetInstance("dbbiometrics");
            DatabaseManager.GetInstance("dbsyslogs");
            backgroundWorker1.ReportProgress(20);
            Thread.Sleep(1000);
            SMSModem modem = SMSModem.GetInstance();
            foreach (string port in SerialPort.GetPortNames())
            {
                if (modem.OpenPort(port, 9600))
                {
                    break;
                }
            }
            backgroundWorker1.ReportProgress(50);
            Thread.Sleep(1000);
            FPReader reader = FPReaderManager.GetInstance();
            if (ReaderCollection.GetReaders().Count < 1)
            {
                //MessageBox.Show("Fingerprint reader was not detected");
            }
            else
            {
                reader.CurrentReader = ReaderCollection.GetReaders()[0];
            }
            backgroundWorker1.ReportProgress(80);
            Thread.Sleep(1000);
            if (AppConfiguration.GetConfig("syslocation") == null)
            {
                AppConfiguration.SetConfig("syslocation", Convert.ToString(db.ScalarSelect("SELECT syslocation FROM appdefaults")));
            }
            if (AppConfiguration.GetConfig("barangayid") == null)
            {
                AppConfiguration.SetConfig("barangayid", Convert.ToString(db.ScalarSelect("SELECT brgyid FROM appdefaults")));
            }
            IDataReader readerr = db.Select("SELECT appcaption, brgyname, brgyaddress1, brgyaddress2, brgyaddress3, brgycontactno, brgyemailad FROM appdefaults");
            if (readerr != null)
            {
                if (readerr.Read())
                {
                    for (int i = 0; i < readerr.FieldCount; i++)
                    {
                        string key = readerr.GetName(i);
                        if (AppConfiguration.GetConfig(key) == null)
                        {
                            AppConfiguration.SetConfig(key, DataTypeManager.ParseDataToString(readerr.GetValue(i)));
                        }
                    }
                }
                readerr.Close();
                readerr.Dispose();
            }
            backgroundWorker1.ReportProgress(100);
            Thread.Sleep(1000);
        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            switch(e.ProgressPercentage)
            {
                case -1:
                    this.Hide();
                    break;
                case 0:
                    this.Show();
                    break;
                case 1:
                    progressBar.Visible = true;
                    label1.Visible = true;
                    break;
                case 5:
                    labelAction.Text = "Setting up connection to main database";
                    break;
                case 10:
                    labelAction.Text = "Setting up connection to other databases";
                    break;
                case 20:
                    labelAction.Text = "Detecting SMS module";
                    break;
                case 50:
                    labelAction.Text = "Detecting Fingerprint reader";
                    break;
                case 80:
                    labelAction.Text = "Retrieving application settings";
                    break;
                case 100:
                    labelAction.Text = "Starting up application..";
                    break;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if(e.Cancelled)
            {
                MessageBox.Show("Shutting down application.");
                this.Close();
            }
            else
            {
                startApplication();
            }
        }

      public static void ThreadProc()
        {
            Application.Run(new Form_Login());
        }

        public void startApplication()
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(ThreadProc));
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
            this.Close();
        }
    }
}
