﻿namespace BIS
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
         this.panel2 = new System.Windows.Forms.Panel();
         this.applabel = new System.Windows.Forms.Label();
         this.menuStrip2 = new System.Windows.Forms.MenuStrip();
         this.bUser = new System.Windows.Forms.ToolStripMenuItem();
         this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.UserstoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
         this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
         this.panel3 = new System.Windows.Forms.Panel();
         this.baranggayName = new System.Windows.Forms.Label();
         this.baranggayAddress = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.pbBaranggayLogo = new System.Windows.Forms.PictureBox();
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.bBusiness = new System.Windows.Forms.ToolStripMenuItem();
         this.inboxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.outboxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.blotterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
         this.officialsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.bResidents = new System.Windows.Forms.ToolStripMenuItem();
         this.bExit = new System.Windows.Forms.ToolStripMenuItem();
         this.panel1 = new System.Windows.Forms.Panel();
         this.panel2.SuspendLayout();
         this.menuStrip2.SuspendLayout();
         this.panel3.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbBaranggayLogo)).BeginInit();
         this.menuStrip1.SuspendLayout();
         this.panel1.SuspendLayout();
         this.SuspendLayout();
         // 
         // panel2
         // 
         this.panel2.BackColor = System.Drawing.Color.White;
         this.panel2.Controls.Add(this.applabel);
         this.panel2.Controls.Add(this.menuStrip2);
         this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel2.Location = new System.Drawing.Point(267, 0);
         this.panel2.Margin = new System.Windows.Forms.Padding(4);
         this.panel2.Name = "panel2";
         this.panel2.Padding = new System.Windows.Forms.Padding(5);
         this.panel2.Size = new System.Drawing.Size(1333, 75);
         this.panel2.TabIndex = 6;
         this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_Main_MouseDown);
         // 
         // applabel
         // 
         this.applabel.Dock = System.Windows.Forms.DockStyle.Left;
         this.applabel.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.applabel.ForeColor = System.Drawing.Color.SteelBlue;
         this.applabel.Location = new System.Drawing.Point(5, 5);
         this.applabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.applabel.Name = "applabel";
         this.applabel.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
         this.applabel.Size = new System.Drawing.Size(485, 65);
         this.applabel.TabIndex = 3;
         this.applabel.Text = "APPLACTION LABEL";
         this.applabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.applabel.DoubleClick += new System.EventHandler(this.Applabel_DoubleClick);
         this.applabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_Main_MouseDown);
         // 
         // menuStrip2
         // 
         this.menuStrip2.BackColor = System.Drawing.Color.Transparent;
         this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Right;
         this.menuStrip2.ImageScalingSize = new System.Drawing.Size(25, 25);
         this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bUser,
            this.UserstoolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.toolStripMenuItem4,
            this.toolStripMenuItem3});
         this.menuStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
         this.menuStrip2.Location = new System.Drawing.Point(903, 5);
         this.menuStrip2.Name = "menuStrip2";
         this.menuStrip2.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
         this.menuStrip2.ShowItemToolTips = true;
         this.menuStrip2.Size = new System.Drawing.Size(425, 65);
         this.menuStrip2.TabIndex = 0;
         this.menuStrip2.Text = "menuStrip2";
         // 
         // bUser
         // 
         this.bUser.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
         this.bUser.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileToolStripMenuItem,
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem});
         this.bUser.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bUser.ForeColor = System.Drawing.Color.SteelBlue;
         this.bUser.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.bUser.Name = "bUser";
         this.bUser.Size = new System.Drawing.Size(117, 61);
         this.bUser.Text = "User\'s Name";
         this.bUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         // 
         // profileToolStripMenuItem
         // 
         this.profileToolStripMenuItem.ForeColor = System.Drawing.Color.SteelBlue;
         this.profileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("profileToolStripMenuItem.Image")));
         this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
         this.profileToolStripMenuItem.Size = new System.Drawing.Size(140, 28);
         this.profileToolStripMenuItem.Text = "Profile";
         // 
         // logoutToolStripMenuItem
         // 
         this.logoutToolStripMenuItem.ForeColor = System.Drawing.Color.SteelBlue;
         this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
         this.logoutToolStripMenuItem.Size = new System.Drawing.Size(140, 28);
         this.logoutToolStripMenuItem.Text = "Logout";
         this.logoutToolStripMenuItem.Click += new System.EventHandler(this.LogoutToolStripMenuItem_Click);
         // 
         // exitToolStripMenuItem
         // 
         this.exitToolStripMenuItem.ForeColor = System.Drawing.Color.SteelBlue;
         this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
         this.exitToolStripMenuItem.Size = new System.Drawing.Size(140, 28);
         this.exitToolStripMenuItem.Text = "Exit";
         this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
         // 
         // UserstoolStripMenuItem
         // 
         this.UserstoolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("UserstoolStripMenuItem.Image")));
         this.UserstoolStripMenuItem.Name = "UserstoolStripMenuItem";
         this.UserstoolStripMenuItem.Size = new System.Drawing.Size(37, 61);
         this.UserstoolStripMenuItem.Click += new System.EventHandler(this.UserstoolStripMenuItem_Click);
         // 
         // settingsToolStripMenuItem
         // 
         this.settingsToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
         this.settingsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
         this.settingsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("settingsToolStripMenuItem.Image")));
         this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
         this.settingsToolStripMenuItem.Size = new System.Drawing.Size(37, 61);
         this.settingsToolStripMenuItem.Text = "Settings";
         this.settingsToolStripMenuItem.ToolTipText = "Settings";
         this.settingsToolStripMenuItem.Click += new System.EventHandler(this.SettingsToolStripMenuItem_Click_1);
         // 
         // toolStripMenuItem4
         // 
         this.toolStripMenuItem4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
         this.toolStripMenuItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
         this.toolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem4.Image")));
         this.toolStripMenuItem4.Name = "toolStripMenuItem4";
         this.toolStripMenuItem4.Size = new System.Drawing.Size(37, 61);
         this.toolStripMenuItem4.Text = "Birthdays";
         this.toolStripMenuItem4.ToolTipText = "Birthdays";
         this.toolStripMenuItem4.Click += new System.EventHandler(this.BirthdayToolStripMenuItem_Click);
         // 
         // toolStripMenuItem3
         // 
         this.toolStripMenuItem3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
         this.toolStripMenuItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
         this.toolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.Image")));
         this.toolStripMenuItem3.Name = "toolStripMenuItem3";
         this.toolStripMenuItem3.Size = new System.Drawing.Size(37, 61);
         this.toolStripMenuItem3.Text = "Settings";
         this.toolStripMenuItem3.ToolTipText = "SMS";
         this.toolStripMenuItem3.Click += new System.EventHandler(this.ToolStripMenuItem3_Click);
         // 
         // panel3
         // 
         this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(64)))), ((int)(((byte)(82)))));
         this.panel3.Controls.Add(this.baranggayName);
         this.panel3.Controls.Add(this.baranggayAddress);
         this.panel3.Controls.Add(this.label1);
         this.panel3.Controls.Add(this.pbBaranggayLogo);
         this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel3.Location = new System.Drawing.Point(0, 0);
         this.panel3.Margin = new System.Windows.Forms.Padding(4);
         this.panel3.Name = "panel3";
         this.panel3.Size = new System.Drawing.Size(267, 346);
         this.panel3.TabIndex = 4;
         // 
         // baranggayName
         // 
         this.baranggayName.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.baranggayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.baranggayName.ForeColor = System.Drawing.Color.White;
         this.baranggayName.Location = new System.Drawing.Point(0, 255);
         this.baranggayName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.baranggayName.Name = "baranggayName";
         this.baranggayName.Size = new System.Drawing.Size(267, 34);
         this.baranggayName.TabIndex = 4;
         this.baranggayName.Text = "Baranggay Name";
         this.baranggayName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // baranggayAddress
         // 
         this.baranggayAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(64)))), ((int)(((byte)(82)))));
         this.baranggayAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.baranggayAddress.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.baranggayAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.baranggayAddress.ForeColor = System.Drawing.Color.White;
         this.baranggayAddress.Location = new System.Drawing.Point(0, 289);
         this.baranggayAddress.Margin = new System.Windows.Forms.Padding(4);
         this.baranggayAddress.Multiline = true;
         this.baranggayAddress.Name = "baranggayAddress";
         this.baranggayAddress.ReadOnly = true;
         this.baranggayAddress.Size = new System.Drawing.Size(267, 57);
         this.baranggayAddress.TabIndex = 4;
         this.baranggayAddress.TabStop = false;
         this.baranggayAddress.Text = "Baranggay Address";
         this.baranggayAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         // 
         // label1
         // 
         this.label1.Dock = System.Windows.Forms.DockStyle.Top;
         this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label1.Font = new System.Drawing.Font("Forte", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.ForeColor = System.Drawing.Color.White;
         this.label1.Location = new System.Drawing.Point(0, 0);
         this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(267, 74);
         this.label1.TabIndex = 4;
         this.label1.Text = "Accents";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.label1.DoubleClick += new System.EventHandler(this.Label1_DoubleClick);
         // 
         // pbBaranggayLogo
         // 
         this.pbBaranggayLogo.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pbBaranggayLogo.ErrorImage")));
         this.pbBaranggayLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbBaranggayLogo.Image")));
         this.pbBaranggayLogo.Location = new System.Drawing.Point(47, 78);
         this.pbBaranggayLogo.Margin = new System.Windows.Forms.Padding(4);
         this.pbBaranggayLogo.Name = "pbBaranggayLogo";
         this.pbBaranggayLogo.Size = new System.Drawing.Size(173, 160);
         this.pbBaranggayLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pbBaranggayLogo.TabIndex = 4;
         this.pbBaranggayLogo.TabStop = false;
         // 
         // menuStrip1
         // 
         this.menuStrip1.AutoSize = false;
         this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(64)))), ((int)(((byte)(82)))));
         this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
         this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(0);
         this.menuStrip1.ImageScalingSize = new System.Drawing.Size(35, 35);
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bBusiness,
            this.inboxToolStripMenuItem,
            this.outboxToolStripMenuItem,
            this.blotterToolStripMenuItem,
            this.toolStripMenuItem1,
            this.officialsToolStripMenuItem,
            this.bResidents,
            this.bExit});
         this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
         this.menuStrip1.Location = new System.Drawing.Point(0, 346);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.Padding = new System.Windows.Forms.Padding(0);
         this.menuStrip1.Size = new System.Drawing.Size(267, 516);
         this.menuStrip1.TabIndex = 5;
         this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
         // 
         // bBusiness
         // 
         this.bBusiness.AutoSize = false;
         this.bBusiness.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bBusiness.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
         this.bBusiness.Image = ((System.Drawing.Image)(resources.GetObject("bBusiness.Image")));
         this.bBusiness.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bBusiness.Name = "bBusiness";
         this.bBusiness.Padding = new System.Windows.Forms.Padding(0);
         this.bBusiness.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.bBusiness.Size = new System.Drawing.Size(200, 47);
         this.bBusiness.Text = "  Permits";
         this.bBusiness.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bBusiness.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
         // 
         // inboxToolStripMenuItem
         // 
         this.inboxToolStripMenuItem.AutoSize = false;
         this.inboxToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.inboxToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
         this.inboxToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("inboxToolStripMenuItem.Image")));
         this.inboxToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.inboxToolStripMenuItem.Name = "inboxToolStripMenuItem";
         this.inboxToolStripMenuItem.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.inboxToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.inboxToolStripMenuItem.Size = new System.Drawing.Size(200, 47);
         this.inboxToolStripMenuItem.Text = "  Certifications";
         this.inboxToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.inboxToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
         // 
         // outboxToolStripMenuItem
         // 
         this.outboxToolStripMenuItem.AutoSize = false;
         this.outboxToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.outboxToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
         this.outboxToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("outboxToolStripMenuItem.Image")));
         this.outboxToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.outboxToolStripMenuItem.Name = "outboxToolStripMenuItem";
         this.outboxToolStripMenuItem.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.outboxToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.outboxToolStripMenuItem.Size = new System.Drawing.Size(200, 47);
         this.outboxToolStripMenuItem.Text = "  Resolutions";
         this.outboxToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.outboxToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
         // 
         // blotterToolStripMenuItem
         // 
         this.blotterToolStripMenuItem.AutoSize = false;
         this.blotterToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.blotterToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
         this.blotterToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("blotterToolStripMenuItem.Image")));
         this.blotterToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.blotterToolStripMenuItem.Name = "blotterToolStripMenuItem";
         this.blotterToolStripMenuItem.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.blotterToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.blotterToolStripMenuItem.Size = new System.Drawing.Size(200, 47);
         this.blotterToolStripMenuItem.Text = "  Blotter";
         this.blotterToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.blotterToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
         // 
         // toolStripMenuItem1
         // 
         this.toolStripMenuItem1.AutoSize = false;
         this.toolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.toolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
         this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
         this.toolStripMenuItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.toolStripMenuItem1.Name = "toolStripMenuItem1";
         this.toolStripMenuItem1.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.toolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.toolStripMenuItem1.Size = new System.Drawing.Size(200, 47);
         this.toolStripMenuItem1.Text = "  Projects";
         this.toolStripMenuItem1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.toolStripMenuItem1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
         // 
         // officialsToolStripMenuItem
         // 
         this.officialsToolStripMenuItem.AutoSize = false;
         this.officialsToolStripMenuItem.Checked = true;
         this.officialsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
         this.officialsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.officialsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
         this.officialsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("officialsToolStripMenuItem.Image")));
         this.officialsToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.officialsToolStripMenuItem.Name = "officialsToolStripMenuItem";
         this.officialsToolStripMenuItem.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.officialsToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.officialsToolStripMenuItem.Size = new System.Drawing.Size(200, 47);
         this.officialsToolStripMenuItem.Text = "  Officials";
         this.officialsToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.officialsToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
         // 
         // bResidents
         // 
         this.bResidents.AutoSize = false;
         this.bResidents.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bResidents.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
         this.bResidents.Image = ((System.Drawing.Image)(resources.GetObject("bResidents.Image")));
         this.bResidents.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bResidents.Name = "bResidents";
         this.bResidents.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.bResidents.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.bResidents.Size = new System.Drawing.Size(200, 47);
         this.bResidents.Text = "  Residents";
         this.bResidents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bResidents.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
         // 
         // bExit
         // 
         this.bExit.AutoSize = false;
         this.bExit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
         this.bExit.Image = ((System.Drawing.Image)(resources.GetObject("bExit.Image")));
         this.bExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bExit.Name = "bExit";
         this.bExit.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
         this.bExit.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.bExit.Size = new System.Drawing.Size(200, 47);
         this.bExit.Text = "  Exit";
         this.bExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.bExit.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
         // 
         // panel1
         // 
         this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
         this.panel1.Controls.Add(this.menuStrip1);
         this.panel1.Controls.Add(this.panel3);
         this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
         this.panel1.Location = new System.Drawing.Point(0, 0);
         this.panel1.Margin = new System.Windows.Forms.Padding(4);
         this.panel1.Name = "panel1";
         this.panel1.Size = new System.Drawing.Size(267, 862);
         this.panel1.TabIndex = 0;
         // 
         // Form_Main
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
         this.ClientSize = new System.Drawing.Size(1600, 862);
         this.Controls.Add(this.panel2);
         this.Controls.Add(this.panel1);
         this.DoubleBuffered = true;
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.IsMdiContainer = true;
         this.Margin = new System.Windows.Forms.Padding(4);
         this.Name = "Form_Main";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
         this.Load += new System.EventHandler(this.Form_Main_Load);
         this.Shown += new System.EventHandler(this.Form_Main_Shown);
         this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_Main_MouseDown);
         this.panel2.ResumeLayout(false);
         this.panel2.PerformLayout();
         this.menuStrip2.ResumeLayout(false);
         this.menuStrip2.PerformLayout();
         this.panel3.ResumeLayout(false);
         this.panel3.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbBaranggayLogo)).EndInit();
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         this.panel1.ResumeLayout(false);
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label baranggayName;
        private System.Windows.Forms.TextBox baranggayAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbBaranggayLogo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bBusiness;
        private System.Windows.Forms.ToolStripMenuItem inboxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outboxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blotterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem officialsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bResidents;
        private System.Windows.Forms.ToolStripMenuItem bExit;
        private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.Label applabel;
      private System.Windows.Forms.MenuStrip menuStrip2;
      private System.Windows.Forms.ToolStripMenuItem bUser;
      private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem UserstoolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
      private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
   }
}