﻿namespace BIS
{
   partial class EntryForm_Users
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntryForm_Users));
         this.panel1 = new System.Windows.Forms.Panel();
         this.splitContainer1 = new System.Windows.Forms.SplitContainer();
         this.button1 = new System.Windows.Forms.Button();
         this.button2 = new System.Windows.Forms.Button();
         this.label14 = new System.Windows.Forms.Label();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
         this.splitContainer1.Panel1.SuspendLayout();
         this.splitContainer1.SuspendLayout();
         this.SuspendLayout();
         // 
         // panel1
         // 
         this.panel1.BackColor = System.Drawing.Color.Transparent;
         this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panel1.Location = new System.Drawing.Point(10, 558);
         this.panel1.Margin = new System.Windows.Forms.Padding(4);
         this.panel1.Name = "panel1";
         this.panel1.Padding = new System.Windows.Forms.Padding(10);
         this.panel1.Size = new System.Drawing.Size(891, 62);
         this.panel1.TabIndex = 2;
         // 
         // splitContainer1
         // 
         this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
         this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer1.Location = new System.Drawing.Point(10, 10);
         this.splitContainer1.Name = "splitContainer1";
         this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer1.Panel1
         // 
         this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent;
         this.splitContainer1.Panel1.Controls.Add(this.label14);
         this.splitContainer1.Panel1.Controls.Add(this.button1);
         this.splitContainer1.Panel1.Controls.Add(this.button2);
         this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(10);
         // 
         // splitContainer1.Panel2
         // 
         this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Transparent;
         this.splitContainer1.Size = new System.Drawing.Size(891, 548);
         this.splitContainer1.SplitterDistance = 69;
         this.splitContainer1.TabIndex = 3;
         // 
         // button1
         // 
         this.button1.Dock = System.Windows.Forms.DockStyle.Right;
         this.button1.Location = new System.Drawing.Point(701, 10);
         this.button1.Margin = new System.Windows.Forms.Padding(4);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(90, 49);
         this.button1.TabIndex = 111;
         this.button1.Text = "Save";
         this.button1.UseVisualStyleBackColor = true;
         // 
         // button2
         // 
         this.button2.Dock = System.Windows.Forms.DockStyle.Right;
         this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
         this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
         this.button2.Location = new System.Drawing.Point(791, 10);
         this.button2.Margin = new System.Windows.Forms.Padding(10);
         this.button2.Name = "button2";
         this.button2.Size = new System.Drawing.Size(90, 49);
         this.button2.TabIndex = 110;
         this.button2.Text = "Cancel";
         this.button2.UseVisualStyleBackColor = true;
         this.button2.Click += new System.EventHandler(this.button2_Click);
         // 
         // label14
         // 
         this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label14.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label14.ForeColor = System.Drawing.Color.White;
         this.label14.Location = new System.Drawing.Point(10, 10);
         this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size(691, 49);
         this.label14.TabIndex = 112;
         this.label14.Text = "GROUPS && USERS";
         this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // EntryForm_Users
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
         this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
         this.ClientSize = new System.Drawing.Size(911, 630);
         this.Controls.Add(this.splitContainer1);
         this.Controls.Add(this.panel1);
         this.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.Name = "EntryForm_Users";
         this.Padding = new System.Windows.Forms.Padding(10);
         this.ShowInTaskbar = false;
         this.Text = "SYSTEM USERS";
         this.splitContainer1.Panel1.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
         this.splitContainer1.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.Button button2;
   }
}