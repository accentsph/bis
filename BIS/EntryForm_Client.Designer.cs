﻿namespace BIS
{
   partial class EntryForm_Client
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntryForm_Client));
         this.MenuPanel = new System.Windows.Forms.Panel();
         this.isPerson = new System.Windows.Forms.CheckBox();
         this.ClientID = new System.Windows.Forms.TextBox();
         this.BtnSave = new System.Windows.Forms.Button();
         this.BtnUndo = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.bClose = new System.Windows.Forms.Label();
         this.splitContainer1 = new System.Windows.Forms.SplitContainer();
         this.splitContainer10 = new System.Windows.Forms.SplitContainer();
         this.label48 = new System.Windows.Forms.Label();
         this.tabControl2 = new System.Windows.Forms.TabControl();
         this.tabPage4 = new System.Windows.Forms.TabPage();
         this.panel35 = new System.Windows.Forms.Panel();
         this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
         this.label47 = new System.Windows.Forms.Label();
         this.panel34 = new System.Windows.Forms.Panel();
         this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
         this.label46 = new System.Windows.Forms.Label();
         this.panel33 = new System.Windows.Forms.Panel();
         this.Position = new System.Windows.Forms.ComboBox();
         this.label45 = new System.Windows.Forms.Label();
         this.tabPage5 = new System.Windows.Forms.TabPage();
         this.panel41 = new System.Windows.Forms.Panel();
         this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
         this.label56 = new System.Windows.Forms.Label();
         this.panel42 = new System.Windows.Forms.Panel();
         this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
         this.label57 = new System.Windows.Forms.Label();
         this.panel44 = new System.Windows.Forms.Panel();
         this.comboBox3 = new System.Windows.Forms.ComboBox();
         this.label59 = new System.Windows.Forms.Label();
         this.panel43 = new System.Windows.Forms.Panel();
         this.Designation = new System.Windows.Forms.ComboBox();
         this.label58 = new System.Windows.Forms.Label();
         this.tabPage6 = new System.Windows.Forms.TabPage();
         this.imageList2 = new System.Windows.Forms.ImageList(this.components);
         this.splitContainer4 = new System.Windows.Forms.SplitContainer();
         this.label10 = new System.Windows.Forms.Label();
         this.panel1 = new System.Windows.Forms.Panel();
         this.eMailAddress = new System.Windows.Forms.TextBox();
         this.label11 = new System.Windows.Forms.Label();
         this.panel4 = new System.Windows.Forms.Panel();
         this.Telephone = new System.Windows.Forms.TextBox();
         this.label12 = new System.Windows.Forms.Label();
         this.panel5 = new System.Windows.Forms.Panel();
         this.Mobile = new System.Windows.Forms.TextBox();
         this.label13 = new System.Windows.Forms.Label();
         this.splitContainer3 = new System.Windows.Forms.SplitContainer();
         this.label5 = new System.Windows.Forms.Label();
         this.panel40 = new System.Windows.Forms.Panel();
         this.ProvAddress = new System.Windows.Forms.TextBox();
         this.label55 = new System.Windows.Forms.Label();
         this.panel23 = new System.Windows.Forms.Panel();
         this.ProvID = new System.Windows.Forms.ComboBox();
         this.label28 = new System.Windows.Forms.Label();
         this.panel19 = new System.Windows.Forms.Panel();
         this.CityID = new System.Windows.Forms.ComboBox();
         this.label25 = new System.Windows.Forms.Label();
         this.panel9 = new System.Windows.Forms.Panel();
         this.BrgyID = new System.Windows.Forms.ComboBox();
         this.label8 = new System.Windows.Forms.Label();
         this.panel8 = new System.Windows.Forms.Panel();
         this.purokID = new System.Windows.Forms.ComboBox();
         this.label6 = new System.Windows.Forms.Label();
         this.panel7 = new System.Windows.Forms.Panel();
         this.houseNo = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.splitContainer2 = new System.Windows.Forms.SplitContainer();
         this.label9 = new System.Windows.Forms.Label();
         this.panel32 = new System.Windows.Forms.Panel();
         this.Alias = new System.Windows.Forms.TextBox();
         this.label44 = new System.Windows.Forms.Label();
         this.panel3 = new System.Windows.Forms.Panel();
         this.LastName = new System.Windows.Forms.TextBox();
         this.label3 = new System.Windows.Forms.Label();
         this.panel2 = new System.Windows.Forms.Panel();
         this.middleName = new System.Windows.Forms.TextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.panel6 = new System.Windows.Forms.Panel();
         this.firstName = new System.Windows.Forms.TextBox();
         this.label7 = new System.Windows.Forms.Label();
         this.splitContainer6 = new System.Windows.Forms.SplitContainer();
         this.panel31 = new System.Windows.Forms.Panel();
         this.splitContainer14 = new System.Windows.Forms.SplitContainer();
         this.label43 = new System.Windows.Forms.Label();
         this.splitContainer8 = new System.Windows.Forms.SplitContainer();
         this.label24 = new System.Windows.Forms.Label();
         this.panel21 = new System.Windows.Forms.Panel();
         this.POD = new System.Windows.Forms.TextBox();
         this.label26 = new System.Windows.Forms.Label();
         this.DOD = new System.Windows.Forms.DateTimePicker();
         this.label32 = new System.Windows.Forms.Label();
         this.splitContainer9 = new System.Windows.Forms.SplitContainer();
         this.label29 = new System.Windows.Forms.Label();
         this.panel24 = new System.Windows.Forms.Panel();
         this.Disability = new System.Windows.Forms.ComboBox();
         this.label33 = new System.Windows.Forms.Label();
         this.panel27 = new System.Windows.Forms.Panel();
         this.SCID = new System.Windows.Forms.TextBox();
         this.label34 = new System.Windows.Forms.Label();
         this.splitContainer7 = new System.Windows.Forms.SplitContainer();
         this.label23 = new System.Windows.Forms.Label();
         this.panel45 = new System.Windows.Forms.Panel();
         this.Occupation = new System.Windows.Forms.ComboBox();
         this.label60 = new System.Windows.Forms.Label();
         this.panel22 = new System.Windows.Forms.Panel();
         this.Industry = new System.Windows.Forms.ComboBox();
         this.label27 = new System.Windows.Forms.Label();
         this.panel25 = new System.Windows.Forms.Panel();
         this.WorkerType = new System.Windows.Forms.ComboBox();
         this.label30 = new System.Windows.Forms.Label();
         this.panel26 = new System.Windows.Forms.Panel();
         this.Employment = new System.Windows.Forms.ComboBox();
         this.label31 = new System.Windows.Forms.Label();
         this.splitContainer15 = new System.Windows.Forms.SplitContainer();
         this.label49 = new System.Windows.Forms.Label();
         this.panel39 = new System.Windows.Forms.Panel();
         this.Weight = new System.Windows.Forms.NumericUpDown();
         this.label54 = new System.Windows.Forms.Label();
         this.Height = new System.Windows.Forms.NumericUpDown();
         this.label53 = new System.Windows.Forms.Label();
         this.panel38 = new System.Windows.Forms.Panel();
         this.Hair = new System.Windows.Forms.ComboBox();
         this.label52 = new System.Windows.Forms.Label();
         this.panel37 = new System.Windows.Forms.Panel();
         this.Complexion = new System.Windows.Forms.ComboBox();
         this.label51 = new System.Windows.Forms.Label();
         this.panel36 = new System.Windows.Forms.Panel();
         this.Build = new System.Windows.Forms.ComboBox();
         this.label50 = new System.Windows.Forms.Label();
         this.splitContainer5 = new System.Windows.Forms.SplitContainer();
         this.label14 = new System.Windows.Forms.Label();
         this.panel18 = new System.Windows.Forms.Panel();
         this.Attainment = new System.Windows.Forms.ComboBox();
         this.label22 = new System.Windows.Forms.Label();
         this.panel16 = new System.Windows.Forms.Panel();
         this.Citizenship = new System.Windows.Forms.ComboBox();
         this.label20 = new System.Windows.Forms.Label();
         this.panel15 = new System.Windows.Forms.Panel();
         this.Ethnicity = new System.Windows.Forms.ComboBox();
         this.label19 = new System.Windows.Forms.Label();
         this.panel14 = new System.Windows.Forms.Panel();
         this.Religion = new System.Windows.Forms.ComboBox();
         this.label18 = new System.Windows.Forms.Label();
         this.panel17 = new System.Windows.Forms.Panel();
         this.POB = new System.Windows.Forms.TextBox();
         this.label21 = new System.Windows.Forms.Label();
         this.panel11 = new System.Windows.Forms.Panel();
         this.DOB = new System.Windows.Forms.DateTimePicker();
         this.label15 = new System.Windows.Forms.Label();
         this.panel12 = new System.Windows.Forms.Panel();
         this.CivilStatus = new System.Windows.Forms.ComboBox();
         this.label16 = new System.Windows.Forms.Label();
         this.panel13 = new System.Windows.Forms.Panel();
         this.Gender = new System.Windows.Forms.ComboBox();
         this.label17 = new System.Windows.Forms.Label();
         this.splitContainer11 = new System.Windows.Forms.SplitContainer();
         this.splitContainer13 = new System.Windows.Forms.SplitContainer();
         this.label35 = new System.Windows.Forms.Label();
         this.panel30 = new System.Windows.Forms.Panel();
         this.mAddress = new System.Windows.Forms.TextBox();
         this.Mother = new System.Windows.Forms.TextBox();
         this.label39 = new System.Windows.Forms.Label();
         this.panel29 = new System.Windows.Forms.Panel();
         this.fAddress = new System.Windows.Forms.TextBox();
         this.Father = new System.Windows.Forms.TextBox();
         this.label38 = new System.Windows.Forms.Label();
         this.textBox15 = new System.Windows.Forms.TextBox();
         this.panel28 = new System.Windows.Forms.Panel();
         this.sAddress = new System.Windows.Forms.TextBox();
         this.Spouse = new System.Windows.Forms.TextBox();
         this.label37 = new System.Windows.Forms.Label();
         this.panel10 = new System.Windows.Forms.Panel();
         this.label41 = new System.Windows.Forms.Label();
         this.label40 = new System.Windows.Forms.Label();
         this.label36 = new System.Windows.Forms.Label();
         this.splitContainer12 = new System.Windows.Forms.SplitContainer();
         this.bConInf = new System.Windows.Forms.Button();
         this.bFP = new System.Windows.Forms.Button();
         this.bPDetails = new System.Windows.Forms.Button();
         this.tabControl1 = new System.Windows.Forms.TabControl();
         this.tabPage1 = new System.Windows.Forms.TabPage();
         this.button3 = new System.Windows.Forms.Button();
         this.button4 = new System.Windows.Forms.Button();
         this.pictureBox2 = new System.Windows.Forms.PictureBox();
         this.tabPage2 = new System.Windows.Forms.TabPage();
         this.label42 = new System.Windows.Forms.Label();
         this.tReader = new System.Windows.Forms.TextBox();
         this.bSetReader = new System.Windows.Forms.Button();
         this.btnCancel = new System.Windows.Forms.Button();
         this.txtMessage = new System.Windows.Forms.TextBox();
         this.pbFingerprint = new System.Windows.Forms.PictureBox();
         this.tabPage3 = new System.Windows.Forms.TabPage();
         this.MenuPanel.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
         this.splitContainer1.Panel1.SuspendLayout();
         this.splitContainer1.Panel2.SuspendLayout();
         this.splitContainer1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).BeginInit();
         this.splitContainer10.Panel1.SuspendLayout();
         this.splitContainer10.Panel2.SuspendLayout();
         this.splitContainer10.SuspendLayout();
         this.tabControl2.SuspendLayout();
         this.tabPage4.SuspendLayout();
         this.panel35.SuspendLayout();
         this.panel34.SuspendLayout();
         this.panel33.SuspendLayout();
         this.tabPage5.SuspendLayout();
         this.panel41.SuspendLayout();
         this.panel42.SuspendLayout();
         this.panel44.SuspendLayout();
         this.panel43.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
         this.splitContainer4.Panel1.SuspendLayout();
         this.splitContainer4.Panel2.SuspendLayout();
         this.splitContainer4.SuspendLayout();
         this.panel1.SuspendLayout();
         this.panel4.SuspendLayout();
         this.panel5.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
         this.splitContainer3.Panel1.SuspendLayout();
         this.splitContainer3.Panel2.SuspendLayout();
         this.splitContainer3.SuspendLayout();
         this.panel40.SuspendLayout();
         this.panel23.SuspendLayout();
         this.panel19.SuspendLayout();
         this.panel9.SuspendLayout();
         this.panel8.SuspendLayout();
         this.panel7.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
         this.splitContainer2.Panel1.SuspendLayout();
         this.splitContainer2.Panel2.SuspendLayout();
         this.splitContainer2.SuspendLayout();
         this.panel32.SuspendLayout();
         this.panel3.SuspendLayout();
         this.panel2.SuspendLayout();
         this.panel6.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
         this.splitContainer6.Panel1.SuspendLayout();
         this.splitContainer6.Panel2.SuspendLayout();
         this.splitContainer6.SuspendLayout();
         this.panel31.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).BeginInit();
         this.splitContainer14.Panel1.SuspendLayout();
         this.splitContainer14.Panel2.SuspendLayout();
         this.splitContainer14.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).BeginInit();
         this.splitContainer8.Panel1.SuspendLayout();
         this.splitContainer8.Panel2.SuspendLayout();
         this.splitContainer8.SuspendLayout();
         this.panel21.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).BeginInit();
         this.splitContainer9.Panel1.SuspendLayout();
         this.splitContainer9.Panel2.SuspendLayout();
         this.splitContainer9.SuspendLayout();
         this.panel24.SuspendLayout();
         this.panel27.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
         this.splitContainer7.Panel1.SuspendLayout();
         this.splitContainer7.Panel2.SuspendLayout();
         this.splitContainer7.SuspendLayout();
         this.panel45.SuspendLayout();
         this.panel22.SuspendLayout();
         this.panel25.SuspendLayout();
         this.panel26.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).BeginInit();
         this.splitContainer15.Panel1.SuspendLayout();
         this.splitContainer15.Panel2.SuspendLayout();
         this.splitContainer15.SuspendLayout();
         this.panel39.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.Weight)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.Height)).BeginInit();
         this.panel38.SuspendLayout();
         this.panel37.SuspendLayout();
         this.panel36.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
         this.splitContainer5.Panel1.SuspendLayout();
         this.splitContainer5.Panel2.SuspendLayout();
         this.splitContainer5.SuspendLayout();
         this.panel18.SuspendLayout();
         this.panel16.SuspendLayout();
         this.panel15.SuspendLayout();
         this.panel14.SuspendLayout();
         this.panel17.SuspendLayout();
         this.panel11.SuspendLayout();
         this.panel12.SuspendLayout();
         this.panel13.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).BeginInit();
         this.splitContainer11.Panel1.SuspendLayout();
         this.splitContainer11.Panel2.SuspendLayout();
         this.splitContainer11.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).BeginInit();
         this.splitContainer13.Panel1.SuspendLayout();
         this.splitContainer13.Panel2.SuspendLayout();
         this.splitContainer13.SuspendLayout();
         this.panel30.SuspendLayout();
         this.panel29.SuspendLayout();
         this.panel28.SuspendLayout();
         this.panel10.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).BeginInit();
         this.splitContainer12.Panel1.SuspendLayout();
         this.splitContainer12.Panel2.SuspendLayout();
         this.splitContainer12.SuspendLayout();
         this.tabControl1.SuspendLayout();
         this.tabPage1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
         this.tabPage2.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbFingerprint)).BeginInit();
         this.SuspendLayout();
         // 
         // MenuPanel
         // 
         this.MenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.MenuPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.MenuPanel.Controls.Add(this.isPerson);
         this.MenuPanel.Controls.Add(this.ClientID);
         this.MenuPanel.Controls.Add(this.BtnSave);
         this.MenuPanel.Controls.Add(this.BtnUndo);
         this.MenuPanel.Controls.Add(this.label1);
         this.MenuPanel.Controls.Add(this.bClose);
         this.MenuPanel.Dock = System.Windows.Forms.DockStyle.Top;
         this.MenuPanel.Location = new System.Drawing.Point(2, 2);
         this.MenuPanel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.MenuPanel.Name = "MenuPanel";
         this.MenuPanel.Padding = new System.Windows.Forms.Padding(8);
         this.MenuPanel.Size = new System.Drawing.Size(1443, 70);
         this.MenuPanel.TabIndex = 1;
         // 
         // isPerson
         // 
         this.isPerson.AutoSize = true;
         this.isPerson.Checked = true;
         this.isPerson.CheckState = System.Windows.Forms.CheckState.Checked;
         this.isPerson.Location = new System.Drawing.Point(837, 24);
         this.isPerson.Name = "isPerson";
         this.isPerson.Size = new System.Drawing.Size(98, 27);
         this.isPerson.TabIndex = 11;
         this.isPerson.Text = "isPerson";
         this.isPerson.UseVisualStyleBackColor = true;
         this.isPerson.Visible = false;
         // 
         // ClientID
         // 
         this.ClientID.Location = new System.Drawing.Point(638, 30);
         this.ClientID.Name = "ClientID";
         this.ClientID.Size = new System.Drawing.Size(100, 29);
         this.ClientID.TabIndex = 10;
         this.ClientID.Text = "0";
         this.ClientID.Visible = false;
         // 
         // BtnSave
         // 
         this.BtnSave.Dock = System.Windows.Forms.DockStyle.Right;
         this.BtnSave.Enabled = false;
         this.BtnSave.Location = new System.Drawing.Point(1128, 8);
         this.BtnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.BtnSave.Name = "BtnSave";
         this.BtnSave.Size = new System.Drawing.Size(101, 50);
         this.BtnSave.TabIndex = 9;
         this.BtnSave.Text = "&Save";
         this.BtnSave.UseVisualStyleBackColor = true;
         // 
         // BtnUndo
         // 
         this.BtnUndo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.BtnUndo.Dock = System.Windows.Forms.DockStyle.Right;
         this.BtnUndo.Location = new System.Drawing.Point(1229, 8);
         this.BtnUndo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.BtnUndo.Name = "BtnUndo";
         this.BtnUndo.Padding = new System.Windows.Forms.Padding(6, 7, 6, 7);
         this.BtnUndo.Size = new System.Drawing.Size(101, 50);
         this.BtnUndo.TabIndex = 8;
         this.BtnUndo.Text = "Undo";
         this.BtnUndo.UseVisualStyleBackColor = true;
         // 
         // label1
         // 
         this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.ForeColor = System.Drawing.Color.White;
         this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
         this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label1.Location = new System.Drawing.Point(8, 8);
         this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label1.Name = "label1";
         this.label1.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
         this.label1.Size = new System.Drawing.Size(1322, 50);
         this.label1.TabIndex = 7;
         this.label1.Text = "     Client Entry";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // bClose
         // 
         this.bClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
         this.bClose.Dock = System.Windows.Forms.DockStyle.Right;
         this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bClose.ForeColor = System.Drawing.Color.Silver;
         this.bClose.Location = new System.Drawing.Point(1330, 8);
         this.bClose.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.bClose.Name = "bClose";
         this.bClose.Size = new System.Drawing.Size(101, 50);
         this.bClose.TabIndex = 6;
         this.bClose.Text = "X";
         this.bClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.bClose.Visible = false;
         this.bClose.Click += new System.EventHandler(this.bClose_Click);
         // 
         // splitContainer1
         // 
         this.splitContainer1.BackColor = System.Drawing.Color.White;
         this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer1.Location = new System.Drawing.Point(2, 72);
         this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.splitContainer1.Name = "splitContainer1";
         // 
         // splitContainer1.Panel1
         // 
         this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Gainsboro;
         this.splitContainer1.Panel1.Controls.Add(this.splitContainer10);
         this.splitContainer1.Panel1.Controls.Add(this.splitContainer4);
         this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
         this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
         this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(8);
         // 
         // splitContainer1.Panel2
         // 
         this.splitContainer1.Panel2.Controls.Add(this.splitContainer6);
         this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(1);
         this.splitContainer1.Size = new System.Drawing.Size(1443, 826);
         this.splitContainer1.SplitterDistance = 485;
         this.splitContainer1.TabIndex = 4;
         // 
         // splitContainer10
         // 
         this.splitContainer10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer10.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer10.Location = new System.Drawing.Point(8, 563);
         this.splitContainer10.Name = "splitContainer10";
         this.splitContainer10.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer10.Panel1
         // 
         this.splitContainer10.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer10.Panel1.Controls.Add(this.label48);
         this.splitContainer10.Panel1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         // 
         // splitContainer10.Panel2
         // 
         this.splitContainer10.Panel2.Controls.Add(this.tabControl2);
         this.splitContainer10.Size = new System.Drawing.Size(469, 255);
         this.splitContainer10.SplitterDistance = 35;
         this.splitContainer10.TabIndex = 4;
         // 
         // label48
         // 
         this.label48.BackColor = System.Drawing.Color.Transparent;
         this.label48.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label48.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label48.ForeColor = System.Drawing.Color.White;
         this.label48.Location = new System.Drawing.Point(5, 0);
         this.label48.Name = "label48";
         this.label48.Size = new System.Drawing.Size(460, 31);
         this.label48.TabIndex = 1;
         this.label48.Text = "Official Capacity in the Barangay";
         this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // tabControl2
         // 
         this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
         this.tabControl2.Controls.Add(this.tabPage4);
         this.tabControl2.Controls.Add(this.tabPage5);
         this.tabControl2.Controls.Add(this.tabPage6);
         this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tabControl2.ImageList = this.imageList2;
         this.tabControl2.Location = new System.Drawing.Point(0, 0);
         this.tabControl2.Name = "tabControl2";
         this.tabControl2.SelectedIndex = 0;
         this.tabControl2.Size = new System.Drawing.Size(465, 212);
         this.tabControl2.TabIndex = 0;
         // 
         // tabPage4
         // 
         this.tabPage4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.tabPage4.Controls.Add(this.panel35);
         this.tabPage4.Controls.Add(this.panel34);
         this.tabPage4.Controls.Add(this.panel33);
         this.tabPage4.ImageKey = "users_32.png";
         this.tabPage4.Location = new System.Drawing.Point(4, 34);
         this.tabPage4.Name = "tabPage4";
         this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage4.Size = new System.Drawing.Size(457, 174);
         this.tabPage4.TabIndex = 0;
         this.tabPage4.Text = "Official";
         // 
         // panel35
         // 
         this.panel35.BackColor = System.Drawing.Color.White;
         this.panel35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel35.Controls.Add(this.dateTimePicker4);
         this.panel35.Controls.Add(this.label47);
         this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel35.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel35.Location = new System.Drawing.Point(3, 63);
         this.panel35.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel35.Name = "panel35";
         this.panel35.Padding = new System.Windows.Forms.Padding(1);
         this.panel35.Size = new System.Drawing.Size(449, 30);
         this.panel35.TabIndex = 19;
         // 
         // dateTimePicker4
         // 
         this.dateTimePicker4.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.dateTimePicker4.Location = new System.Drawing.Point(156, 1);
         this.dateTimePicker4.Name = "dateTimePicker4";
         this.dateTimePicker4.ShowUpDown = true;
         this.dateTimePicker4.Size = new System.Drawing.Size(288, 29);
         this.dateTimePicker4.TabIndex = 3;
         // 
         // label47
         // 
         this.label47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label47.Dock = System.Windows.Forms.DockStyle.Left;
         this.label47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label47.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label47.ForeColor = System.Drawing.Color.White;
         this.label47.Location = new System.Drawing.Point(1, 1);
         this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label47.Name = "label47";
         this.label47.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label47.Size = new System.Drawing.Size(155, 24);
         this.label47.TabIndex = 2;
         this.label47.Text = "Last Day of Duty";
         this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel34
         // 
         this.panel34.BackColor = System.Drawing.Color.White;
         this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel34.Controls.Add(this.dateTimePicker2);
         this.panel34.Controls.Add(this.label46);
         this.panel34.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel34.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel34.Location = new System.Drawing.Point(3, 33);
         this.panel34.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel34.Name = "panel34";
         this.panel34.Padding = new System.Windows.Forms.Padding(1);
         this.panel34.Size = new System.Drawing.Size(449, 30);
         this.panel34.TabIndex = 18;
         // 
         // dateTimePicker2
         // 
         this.dateTimePicker2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.dateTimePicker2.Location = new System.Drawing.Point(156, 1);
         this.dateTimePicker2.Name = "dateTimePicker2";
         this.dateTimePicker2.ShowUpDown = true;
         this.dateTimePicker2.Size = new System.Drawing.Size(288, 29);
         this.dateTimePicker2.TabIndex = 3;
         // 
         // label46
         // 
         this.label46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label46.Dock = System.Windows.Forms.DockStyle.Left;
         this.label46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label46.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label46.ForeColor = System.Drawing.Color.White;
         this.label46.Location = new System.Drawing.Point(1, 1);
         this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label46.Name = "label46";
         this.label46.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label46.Size = new System.Drawing.Size(155, 24);
         this.label46.TabIndex = 2;
         this.label46.Text = "First Day of Duty";
         this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel33
         // 
         this.panel33.BackColor = System.Drawing.Color.White;
         this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel33.Controls.Add(this.Position);
         this.panel33.Controls.Add(this.label45);
         this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel33.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel33.Location = new System.Drawing.Point(3, 3);
         this.panel33.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel33.Name = "panel33";
         this.panel33.Padding = new System.Windows.Forms.Padding(1);
         this.panel33.Size = new System.Drawing.Size(449, 30);
         this.panel33.TabIndex = 17;
         // 
         // Position
         // 
         this.Position.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Position.FormattingEnabled = true;
         this.Position.Location = new System.Drawing.Point(156, 1);
         this.Position.Name = "Position";
         this.Position.Size = new System.Drawing.Size(288, 30);
         this.Position.TabIndex = 3;
         // 
         // label45
         // 
         this.label45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label45.Dock = System.Windows.Forms.DockStyle.Left;
         this.label45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label45.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label45.ForeColor = System.Drawing.Color.White;
         this.label45.Location = new System.Drawing.Point(1, 1);
         this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label45.Name = "label45";
         this.label45.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label45.Size = new System.Drawing.Size(155, 24);
         this.label45.TabIndex = 2;
         this.label45.Text = "Position";
         this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // tabPage5
         // 
         this.tabPage5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.tabPage5.Controls.Add(this.panel41);
         this.tabPage5.Controls.Add(this.panel42);
         this.tabPage5.Controls.Add(this.panel44);
         this.tabPage5.Controls.Add(this.panel43);
         this.tabPage5.ImageKey = "User group.png";
         this.tabPage5.Location = new System.Drawing.Point(4, 34);
         this.tabPage5.Name = "tabPage5";
         this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage5.Size = new System.Drawing.Size(457, 174);
         this.tabPage5.TabIndex = 1;
         this.tabPage5.Text = "Employee";
         this.tabPage5.UseVisualStyleBackColor = true;
         // 
         // panel41
         // 
         this.panel41.BackColor = System.Drawing.Color.White;
         this.panel41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel41.Controls.Add(this.dateTimePicker1);
         this.panel41.Controls.Add(this.label56);
         this.panel41.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel41.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel41.Location = new System.Drawing.Point(3, 93);
         this.panel41.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel41.Name = "panel41";
         this.panel41.Padding = new System.Windows.Forms.Padding(1);
         this.panel41.Size = new System.Drawing.Size(449, 30);
         this.panel41.TabIndex = 32;
         // 
         // dateTimePicker1
         // 
         this.dateTimePicker1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.dateTimePicker1.Location = new System.Drawing.Point(156, 1);
         this.dateTimePicker1.Name = "dateTimePicker1";
         this.dateTimePicker1.ShowUpDown = true;
         this.dateTimePicker1.Size = new System.Drawing.Size(288, 29);
         this.dateTimePicker1.TabIndex = 3;
         // 
         // label56
         // 
         this.label56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label56.Dock = System.Windows.Forms.DockStyle.Left;
         this.label56.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label56.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label56.ForeColor = System.Drawing.Color.White;
         this.label56.Location = new System.Drawing.Point(1, 1);
         this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label56.Name = "label56";
         this.label56.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label56.Size = new System.Drawing.Size(155, 24);
         this.label56.TabIndex = 2;
         this.label56.Text = "Date End";
         this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel42
         // 
         this.panel42.BackColor = System.Drawing.Color.White;
         this.panel42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel42.Controls.Add(this.dateTimePicker3);
         this.panel42.Controls.Add(this.label57);
         this.panel42.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel42.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel42.Location = new System.Drawing.Point(3, 63);
         this.panel42.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel42.Name = "panel42";
         this.panel42.Padding = new System.Windows.Forms.Padding(1);
         this.panel42.Size = new System.Drawing.Size(449, 30);
         this.panel42.TabIndex = 31;
         // 
         // dateTimePicker3
         // 
         this.dateTimePicker3.Dock = System.Windows.Forms.DockStyle.Fill;
         this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.dateTimePicker3.Location = new System.Drawing.Point(156, 1);
         this.dateTimePicker3.Name = "dateTimePicker3";
         this.dateTimePicker3.ShowUpDown = true;
         this.dateTimePicker3.Size = new System.Drawing.Size(288, 29);
         this.dateTimePicker3.TabIndex = 3;
         // 
         // label57
         // 
         this.label57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label57.Dock = System.Windows.Forms.DockStyle.Left;
         this.label57.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label57.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label57.ForeColor = System.Drawing.Color.White;
         this.label57.Location = new System.Drawing.Point(1, 1);
         this.label57.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label57.Name = "label57";
         this.label57.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label57.Size = new System.Drawing.Size(155, 24);
         this.label57.TabIndex = 2;
         this.label57.Text = "Date Start";
         this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel44
         // 
         this.panel44.BackColor = System.Drawing.Color.White;
         this.panel44.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel44.Controls.Add(this.comboBox3);
         this.panel44.Controls.Add(this.label59);
         this.panel44.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel44.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel44.Location = new System.Drawing.Point(3, 33);
         this.panel44.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel44.Name = "panel44";
         this.panel44.Padding = new System.Windows.Forms.Padding(1);
         this.panel44.Size = new System.Drawing.Size(449, 30);
         this.panel44.TabIndex = 21;
         // 
         // comboBox3
         // 
         this.comboBox3.Dock = System.Windows.Forms.DockStyle.Fill;
         this.comboBox3.FormattingEnabled = true;
         this.comboBox3.Location = new System.Drawing.Point(156, 1);
         this.comboBox3.Name = "comboBox3";
         this.comboBox3.Size = new System.Drawing.Size(288, 30);
         this.comboBox3.TabIndex = 3;
         // 
         // label59
         // 
         this.label59.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label59.Dock = System.Windows.Forms.DockStyle.Left;
         this.label59.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label59.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label59.ForeColor = System.Drawing.Color.White;
         this.label59.Location = new System.Drawing.Point(1, 1);
         this.label59.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label59.Name = "label59";
         this.label59.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label59.Size = new System.Drawing.Size(155, 24);
         this.label59.TabIndex = 2;
         this.label59.Text = "Department";
         this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel43
         // 
         this.panel43.BackColor = System.Drawing.Color.White;
         this.panel43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel43.Controls.Add(this.Designation);
         this.panel43.Controls.Add(this.label58);
         this.panel43.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel43.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel43.Location = new System.Drawing.Point(3, 3);
         this.panel43.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel43.Name = "panel43";
         this.panel43.Padding = new System.Windows.Forms.Padding(1);
         this.panel43.Size = new System.Drawing.Size(449, 30);
         this.panel43.TabIndex = 20;
         // 
         // Designation
         // 
         this.Designation.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Designation.FormattingEnabled = true;
         this.Designation.Location = new System.Drawing.Point(156, 1);
         this.Designation.Name = "Designation";
         this.Designation.Size = new System.Drawing.Size(288, 30);
         this.Designation.TabIndex = 3;
         // 
         // label58
         // 
         this.label58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label58.Dock = System.Windows.Forms.DockStyle.Left;
         this.label58.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label58.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label58.ForeColor = System.Drawing.Color.White;
         this.label58.Location = new System.Drawing.Point(1, 1);
         this.label58.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label58.Name = "label58";
         this.label58.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label58.Size = new System.Drawing.Size(155, 24);
         this.label58.TabIndex = 2;
         this.label58.Text = "Position";
         this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // tabPage6
         // 
         this.tabPage6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.tabPage6.ImageKey = "users_32.png";
         this.tabPage6.Location = new System.Drawing.Point(4, 34);
         this.tabPage6.Name = "tabPage6";
         this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage6.Size = new System.Drawing.Size(457, 174);
         this.tabPage6.TabIndex = 2;
         this.tabPage6.Text = "System User";
         this.tabPage6.UseVisualStyleBackColor = true;
         // 
         // imageList2
         // 
         this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
         this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
         this.imageList2.Images.SetKeyName(0, "icons8-home-page-40.png");
         this.imageList2.Images.SetKeyName(1, "icons8-building-40.png");
         this.imageList2.Images.SetKeyName(2, "icons8-contact-details-40.png");
         this.imageList2.Images.SetKeyName(3, "icons8-address-book-40.png");
         this.imageList2.Images.SetKeyName(4, "icons8-road-40.png");
         this.imageList2.Images.SetKeyName(5, "icons8-email-40.png");
         this.imageList2.Images.SetKeyName(6, "icons8-home-page-40.png");
         this.imageList2.Images.SetKeyName(7, "icons8-iphone-40.png");
         this.imageList2.Images.SetKeyName(8, "icons8-phone-40.png");
         this.imageList2.Images.SetKeyName(9, "icons8-group-message-40.png");
         this.imageList2.Images.SetKeyName(10, "icons8-automatic-40.png");
         this.imageList2.Images.SetKeyName(11, "icons8-user-40.png");
         this.imageList2.Images.SetKeyName(12, "icons8-overview-40.png");
         this.imageList2.Images.SetKeyName(13, "icons8-general-ledger-40.png");
         this.imageList2.Images.SetKeyName(14, "icons8-bill-40.png");
         this.imageList2.Images.SetKeyName(15, "icons8-people-40.png");
         this.imageList2.Images.SetKeyName(16, "icons8-user-groups-40.png");
         this.imageList2.Images.SetKeyName(17, "house_48.png");
         this.imageList2.Images.SetKeyName(18, "calendar_48.png");
         this.imageList2.Images.SetKeyName(19, "People.png");
         this.imageList2.Images.SetKeyName(20, "calendar_add.png");
         this.imageList2.Images.SetKeyName(21, "calendar_remove.png");
         this.imageList2.Images.SetKeyName(22, "digital_camera.png");
         this.imageList2.Images.SetKeyName(23, "cd.png");
         this.imageList2.Images.SetKeyName(24, "calendar_add.png");
         this.imageList2.Images.SetKeyName(25, "calendar_remove.png");
         this.imageList2.Images.SetKeyName(26, "Accessibility.png");
         this.imageList2.Images.SetKeyName(27, "MyPhone_Menu.png");
         this.imageList2.Images.SetKeyName(28, "Phone number.png");
         this.imageList2.Images.SetKeyName(29, "Mobile-phone.png");
         this.imageList2.Images.SetKeyName(30, "profile_edit.png");
         this.imageList2.Images.SetKeyName(31, "users_32.png");
         this.imageList2.Images.SetKeyName(32, "User group.png");
         this.imageList2.Images.SetKeyName(33, "user.png");
         // 
         // splitContainer4
         // 
         this.splitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer4.Location = new System.Drawing.Point(8, 401);
         this.splitContainer4.Name = "splitContainer4";
         this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer4.Panel1
         // 
         this.splitContainer4.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer4.Panel1.Controls.Add(this.label10);
         // 
         // splitContainer4.Panel2
         // 
         this.splitContainer4.Panel2.Controls.Add(this.panel1);
         this.splitContainer4.Panel2.Controls.Add(this.panel4);
         this.splitContainer4.Panel2.Controls.Add(this.panel5);
         this.splitContainer4.Size = new System.Drawing.Size(469, 162);
         this.splitContainer4.SplitterDistance = 35;
         this.splitContainer4.SplitterWidth = 1;
         this.splitContainer4.TabIndex = 3;
         // 
         // label10
         // 
         this.label10.BackColor = System.Drawing.Color.Transparent;
         this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label10.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label10.ForeColor = System.Drawing.Color.White;
         this.label10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label10.ImageKey = "icons8-contact-details-40.png";
         this.label10.ImageList = this.imageList2;
         this.label10.Location = new System.Drawing.Point(0, 0);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(465, 31);
         this.label10.TabIndex = 0;
         this.label10.Text = "Contact Details";
         this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel1
         // 
         this.panel1.BackColor = System.Drawing.Color.Navy;
         this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel1.Controls.Add(this.eMailAddress);
         this.panel1.Controls.Add(this.label11);
         this.panel1.Location = new System.Drawing.Point(0, 60);
         this.panel1.Name = "panel1";
         this.panel1.Size = new System.Drawing.Size(465, 60);
         this.panel1.TabIndex = 17;
         // 
         // eMailAddress
         // 
         this.eMailAddress.Dock = System.Windows.Forms.DockStyle.Fill;
         this.eMailAddress.Location = new System.Drawing.Point(128, 0);
         this.eMailAddress.Multiline = true;
         this.eMailAddress.Name = "eMailAddress";
         this.eMailAddress.Size = new System.Drawing.Size(333, 56);
         this.eMailAddress.TabIndex = 4;
         // 
         // label11
         // 
         this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label11.Dock = System.Windows.Forms.DockStyle.Left;
         this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label11.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label11.ForeColor = System.Drawing.Color.White;
         this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label11.ImageKey = "icons8-group-message-40.png";
         this.label11.ImageList = this.imageList2;
         this.label11.Location = new System.Drawing.Point(0, 0);
         this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label11.Name = "label11";
         this.label11.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label11.Size = new System.Drawing.Size(128, 56);
         this.label11.TabIndex = 2;
         this.label11.Text = "E-Mail";
         this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel4
         // 
         this.panel4.BackColor = System.Drawing.Color.White;
         this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel4.Controls.Add(this.Telephone);
         this.panel4.Controls.Add(this.label12);
         this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel4.Location = new System.Drawing.Point(0, 30);
         this.panel4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel4.Name = "panel4";
         this.panel4.Padding = new System.Windows.Forms.Padding(1);
         this.panel4.Size = new System.Drawing.Size(465, 30);
         this.panel4.TabIndex = 16;
         // 
         // Telephone
         // 
         this.Telephone.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Telephone.Location = new System.Drawing.Point(129, 1);
         this.Telephone.Name = "Telephone";
         this.Telephone.Size = new System.Drawing.Size(331, 29);
         this.Telephone.TabIndex = 4;
         // 
         // label12
         // 
         this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label12.Dock = System.Windows.Forms.DockStyle.Left;
         this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label12.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label12.ForeColor = System.Drawing.Color.White;
         this.label12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label12.ImageKey = "Phone number.png";
         this.label12.ImageList = this.imageList2;
         this.label12.Location = new System.Drawing.Point(1, 1);
         this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label12.Name = "label12";
         this.label12.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label12.Size = new System.Drawing.Size(128, 24);
         this.label12.TabIndex = 2;
         this.label12.Text = "Tel. No.";
         this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel5
         // 
         this.panel5.BackColor = System.Drawing.Color.White;
         this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel5.Controls.Add(this.Mobile);
         this.panel5.Controls.Add(this.label13);
         this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel5.Location = new System.Drawing.Point(0, 0);
         this.panel5.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel5.Name = "panel5";
         this.panel5.Padding = new System.Windows.Forms.Padding(1);
         this.panel5.Size = new System.Drawing.Size(465, 30);
         this.panel5.TabIndex = 15;
         // 
         // Mobile
         // 
         this.Mobile.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Mobile.Location = new System.Drawing.Point(129, 1);
         this.Mobile.Name = "Mobile";
         this.Mobile.Size = new System.Drawing.Size(331, 29);
         this.Mobile.TabIndex = 3;
         // 
         // label13
         // 
         this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label13.Dock = System.Windows.Forms.DockStyle.Left;
         this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label13.ForeColor = System.Drawing.Color.White;
         this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label13.ImageKey = "Mobile-phone.png";
         this.label13.ImageList = this.imageList2;
         this.label13.Location = new System.Drawing.Point(1, 1);
         this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label13.Name = "label13";
         this.label13.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label13.Size = new System.Drawing.Size(128, 24);
         this.label13.TabIndex = 2;
         this.label13.Text = "Mobile No.";
         this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer3
         // 
         this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer3.Location = new System.Drawing.Point(8, 173);
         this.splitContainer3.Name = "splitContainer3";
         this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer3.Panel1
         // 
         this.splitContainer3.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer3.Panel1.Controls.Add(this.label5);
         // 
         // splitContainer3.Panel2
         // 
         this.splitContainer3.Panel2.Controls.Add(this.panel40);
         this.splitContainer3.Panel2.Controls.Add(this.panel23);
         this.splitContainer3.Panel2.Controls.Add(this.panel19);
         this.splitContainer3.Panel2.Controls.Add(this.panel9);
         this.splitContainer3.Panel2.Controls.Add(this.panel8);
         this.splitContainer3.Panel2.Controls.Add(this.panel7);
         this.splitContainer3.Size = new System.Drawing.Size(469, 228);
         this.splitContainer3.SplitterDistance = 35;
         this.splitContainer3.SplitterWidth = 1;
         this.splitContainer3.TabIndex = 2;
         // 
         // label5
         // 
         this.label5.BackColor = System.Drawing.Color.Transparent;
         this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.ForeColor = System.Drawing.Color.White;
         this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label5.ImageKey = "icons8-home-page-40.png";
         this.label5.ImageList = this.imageList2;
         this.label5.Location = new System.Drawing.Point(0, 0);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(465, 31);
         this.label5.TabIndex = 0;
         this.label5.Text = "Address";
         this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel40
         // 
         this.panel40.BackColor = System.Drawing.Color.White;
         this.panel40.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel40.Controls.Add(this.ProvAddress);
         this.panel40.Controls.Add(this.label55);
         this.panel40.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel40.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel40.Location = new System.Drawing.Point(0, 155);
         this.panel40.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel40.Name = "panel40";
         this.panel40.Padding = new System.Windows.Forms.Padding(1);
         this.panel40.Size = new System.Drawing.Size(465, 35);
         this.panel40.TabIndex = 20;
         // 
         // ProvAddress
         // 
         this.ProvAddress.Dock = System.Windows.Forms.DockStyle.Fill;
         this.ProvAddress.Location = new System.Drawing.Point(129, 1);
         this.ProvAddress.Name = "ProvAddress";
         this.ProvAddress.Size = new System.Drawing.Size(331, 29);
         this.ProvAddress.TabIndex = 3;
         // 
         // label55
         // 
         this.label55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label55.Dock = System.Windows.Forms.DockStyle.Left;
         this.label55.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label55.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label55.ForeColor = System.Drawing.Color.White;
         this.label55.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label55.ImageKey = "(none)";
         this.label55.Location = new System.Drawing.Point(1, 1);
         this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label55.Name = "label55";
         this.label55.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label55.Size = new System.Drawing.Size(128, 29);
         this.label55.TabIndex = 2;
         this.label55.Text = "Prov. Address";
         this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel23
         // 
         this.panel23.BackColor = System.Drawing.Color.White;
         this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel23.Controls.Add(this.ProvID);
         this.panel23.Controls.Add(this.label28);
         this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel23.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel23.Location = new System.Drawing.Point(0, 125);
         this.panel23.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel23.Name = "panel23";
         this.panel23.Padding = new System.Windows.Forms.Padding(1);
         this.panel23.Size = new System.Drawing.Size(465, 30);
         this.panel23.TabIndex = 19;
         // 
         // ProvID
         // 
         this.ProvID.BackColor = System.Drawing.Color.White;
         this.ProvID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.ProvID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.ProvID.FormattingEnabled = true;
         this.ProvID.Location = new System.Drawing.Point(129, 1);
         this.ProvID.Name = "ProvID";
         this.ProvID.Size = new System.Drawing.Size(331, 30);
         this.ProvID.TabIndex = 3;
         // 
         // label28
         // 
         this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label28.Dock = System.Windows.Forms.DockStyle.Left;
         this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label28.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label28.ForeColor = System.Drawing.Color.White;
         this.label28.Location = new System.Drawing.Point(1, 1);
         this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label28.Name = "label28";
         this.label28.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label28.Size = new System.Drawing.Size(128, 24);
         this.label28.TabIndex = 2;
         this.label28.Text = "Province";
         this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel19
         // 
         this.panel19.BackColor = System.Drawing.Color.White;
         this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel19.Controls.Add(this.CityID);
         this.panel19.Controls.Add(this.label25);
         this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel19.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel19.Location = new System.Drawing.Point(0, 95);
         this.panel19.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel19.Name = "panel19";
         this.panel19.Padding = new System.Windows.Forms.Padding(1);
         this.panel19.Size = new System.Drawing.Size(465, 30);
         this.panel19.TabIndex = 18;
         this.panel19.Paint += new System.Windows.Forms.PaintEventHandler(this.panel19_Paint_1);
         // 
         // CityID
         // 
         this.CityID.BackColor = System.Drawing.Color.White;
         this.CityID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.CityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.CityID.FormattingEnabled = true;
         this.CityID.Location = new System.Drawing.Point(129, 1);
         this.CityID.Name = "CityID";
         this.CityID.Size = new System.Drawing.Size(331, 30);
         this.CityID.TabIndex = 3;
         // 
         // label25
         // 
         this.label25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label25.Dock = System.Windows.Forms.DockStyle.Left;
         this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label25.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label25.ForeColor = System.Drawing.Color.White;
         this.label25.Location = new System.Drawing.Point(1, 1);
         this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label25.Name = "label25";
         this.label25.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label25.Size = new System.Drawing.Size(128, 24);
         this.label25.TabIndex = 2;
         this.label25.Text = "City/Mun.";
         this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel9
         // 
         this.panel9.BackColor = System.Drawing.Color.White;
         this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel9.Controls.Add(this.BrgyID);
         this.panel9.Controls.Add(this.label8);
         this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel9.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel9.Location = new System.Drawing.Point(0, 65);
         this.panel9.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel9.Name = "panel9";
         this.panel9.Padding = new System.Windows.Forms.Padding(1);
         this.panel9.Size = new System.Drawing.Size(465, 30);
         this.panel9.TabIndex = 17;
         // 
         // BrgyID
         // 
         this.BrgyID.BackColor = System.Drawing.Color.White;
         this.BrgyID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.BrgyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.BrgyID.FormattingEnabled = true;
         this.BrgyID.Location = new System.Drawing.Point(129, 1);
         this.BrgyID.Name = "BrgyID";
         this.BrgyID.Size = new System.Drawing.Size(331, 30);
         this.BrgyID.TabIndex = 3;
         // 
         // label8
         // 
         this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label8.Dock = System.Windows.Forms.DockStyle.Left;
         this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label8.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label8.ForeColor = System.Drawing.Color.White;
         this.label8.Location = new System.Drawing.Point(1, 1);
         this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label8.Name = "label8";
         this.label8.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label8.Size = new System.Drawing.Size(128, 24);
         this.label8.TabIndex = 2;
         this.label8.Text = "Barangay";
         this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel8
         // 
         this.panel8.BackColor = System.Drawing.Color.White;
         this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel8.Controls.Add(this.purokID);
         this.panel8.Controls.Add(this.label6);
         this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel8.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel8.Location = new System.Drawing.Point(0, 35);
         this.panel8.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel8.Name = "panel8";
         this.panel8.Padding = new System.Windows.Forms.Padding(1);
         this.panel8.Size = new System.Drawing.Size(465, 30);
         this.panel8.TabIndex = 16;
         this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
         // 
         // purokID
         // 
         this.purokID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.purokID.FormattingEnabled = true;
         this.purokID.Location = new System.Drawing.Point(129, 1);
         this.purokID.Name = "purokID";
         this.purokID.Size = new System.Drawing.Size(331, 30);
         this.purokID.TabIndex = 3;
         this.purokID.SelectedIndexChanged += new System.EventHandler(this.purokID_SelectedIndexChanged);
         // 
         // label6
         // 
         this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label6.Dock = System.Windows.Forms.DockStyle.Left;
         this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label6.ForeColor = System.Drawing.Color.White;
         this.label6.Location = new System.Drawing.Point(1, 1);
         this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label6.Name = "label6";
         this.label6.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label6.Size = new System.Drawing.Size(128, 24);
         this.label6.TabIndex = 2;
         this.label6.Text = "Purok";
         this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel7
         // 
         this.panel7.BackColor = System.Drawing.Color.White;
         this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel7.Controls.Add(this.houseNo);
         this.panel7.Controls.Add(this.label4);
         this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel7.Location = new System.Drawing.Point(0, 0);
         this.panel7.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel7.Name = "panel7";
         this.panel7.Padding = new System.Windows.Forms.Padding(1);
         this.panel7.Size = new System.Drawing.Size(465, 35);
         this.panel7.TabIndex = 15;
         // 
         // houseNo
         // 
         this.houseNo.Dock = System.Windows.Forms.DockStyle.Fill;
         this.houseNo.Location = new System.Drawing.Point(129, 1);
         this.houseNo.Name = "houseNo";
         this.houseNo.Size = new System.Drawing.Size(331, 29);
         this.houseNo.TabIndex = 3;
         // 
         // label4
         // 
         this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label4.Dock = System.Windows.Forms.DockStyle.Left;
         this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label4.ForeColor = System.Drawing.Color.White;
         this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label4.ImageKey = "(none)";
         this.label4.Location = new System.Drawing.Point(1, 1);
         this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label4.Name = "label4";
         this.label4.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label4.Size = new System.Drawing.Size(128, 29);
         this.label4.TabIndex = 2;
         this.label4.Text = "House No.";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer2
         // 
         this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer2.Location = new System.Drawing.Point(8, 8);
         this.splitContainer2.Name = "splitContainer2";
         this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer2.Panel1
         // 
         this.splitContainer2.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer2.Panel1.Controls.Add(this.label9);
         // 
         // splitContainer2.Panel2
         // 
         this.splitContainer2.Panel2.Controls.Add(this.panel32);
         this.splitContainer2.Panel2.Controls.Add(this.panel3);
         this.splitContainer2.Panel2.Controls.Add(this.panel2);
         this.splitContainer2.Panel2.Controls.Add(this.panel6);
         this.splitContainer2.Size = new System.Drawing.Size(469, 165);
         this.splitContainer2.SplitterDistance = 35;
         this.splitContainer2.SplitterWidth = 1;
         this.splitContainer2.TabIndex = 1;
         // 
         // label9
         // 
         this.label9.BackColor = System.Drawing.Color.Transparent;
         this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label9.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label9.ForeColor = System.Drawing.Color.White;
         this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label9.ImageKey = "icons8-user-40.png";
         this.label9.ImageList = this.imageList2;
         this.label9.Location = new System.Drawing.Point(0, 0);
         this.label9.Margin = new System.Windows.Forms.Padding(0);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(465, 31);
         this.label9.TabIndex = 0;
         this.label9.Text = "Client Name";
         this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel32
         // 
         this.panel32.BackColor = System.Drawing.Color.White;
         this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel32.Controls.Add(this.Alias);
         this.panel32.Controls.Add(this.label44);
         this.panel32.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panel32.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel32.Location = new System.Drawing.Point(0, 95);
         this.panel32.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel32.Name = "panel32";
         this.panel32.Padding = new System.Windows.Forms.Padding(1);
         this.panel32.Size = new System.Drawing.Size(465, 30);
         this.panel32.TabIndex = 14;
         // 
         // Alias
         // 
         this.Alias.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Alias.Location = new System.Drawing.Point(129, 1);
         this.Alias.Name = "Alias";
         this.Alias.Size = new System.Drawing.Size(331, 29);
         this.Alias.TabIndex = 3;
         // 
         // label44
         // 
         this.label44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label44.Dock = System.Windows.Forms.DockStyle.Left;
         this.label44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label44.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label44.ForeColor = System.Drawing.Color.White;
         this.label44.Location = new System.Drawing.Point(1, 1);
         this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label44.Name = "label44";
         this.label44.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label44.Size = new System.Drawing.Size(128, 24);
         this.label44.TabIndex = 2;
         this.label44.Text = "Alias";
         this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel3
         // 
         this.panel3.BackColor = System.Drawing.Color.White;
         this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel3.Controls.Add(this.LastName);
         this.panel3.Controls.Add(this.label3);
         this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel3.Location = new System.Drawing.Point(0, 60);
         this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel3.Name = "panel3";
         this.panel3.Padding = new System.Windows.Forms.Padding(1);
         this.panel3.Size = new System.Drawing.Size(465, 30);
         this.panel3.TabIndex = 13;
         // 
         // LastName
         // 
         this.LastName.Dock = System.Windows.Forms.DockStyle.Fill;
         this.LastName.Location = new System.Drawing.Point(129, 1);
         this.LastName.Name = "LastName";
         this.LastName.Size = new System.Drawing.Size(331, 29);
         this.LastName.TabIndex = 3;
         // 
         // label3
         // 
         this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label3.Dock = System.Windows.Forms.DockStyle.Left;
         this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.ForeColor = System.Drawing.Color.White;
         this.label3.Location = new System.Drawing.Point(1, 1);
         this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label3.Name = "label3";
         this.label3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label3.Size = new System.Drawing.Size(128, 24);
         this.label3.TabIndex = 2;
         this.label3.Text = "Last Name";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel2
         // 
         this.panel2.BackColor = System.Drawing.Color.Gainsboro;
         this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel2.Controls.Add(this.middleName);
         this.panel2.Controls.Add(this.label2);
         this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel2.Location = new System.Drawing.Point(0, 30);
         this.panel2.Name = "panel2";
         this.panel2.Padding = new System.Windows.Forms.Padding(1);
         this.panel2.Size = new System.Drawing.Size(465, 30);
         this.panel2.TabIndex = 12;
         // 
         // middleName
         // 
         this.middleName.Dock = System.Windows.Forms.DockStyle.Fill;
         this.middleName.Location = new System.Drawing.Point(129, 1);
         this.middleName.Name = "middleName";
         this.middleName.Size = new System.Drawing.Size(331, 29);
         this.middleName.TabIndex = 3;
         // 
         // label2
         // 
         this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label2.Dock = System.Windows.Forms.DockStyle.Left;
         this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.ForeColor = System.Drawing.Color.White;
         this.label2.Location = new System.Drawing.Point(1, 1);
         this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label2.Name = "label2";
         this.label2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label2.Size = new System.Drawing.Size(128, 24);
         this.label2.TabIndex = 2;
         this.label2.Text = "Middle Name";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel6
         // 
         this.panel6.BackColor = System.Drawing.Color.White;
         this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel6.Controls.Add(this.firstName);
         this.panel6.Controls.Add(this.label7);
         this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel6.Location = new System.Drawing.Point(0, 0);
         this.panel6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel6.Name = "panel6";
         this.panel6.Padding = new System.Windows.Forms.Padding(1);
         this.panel6.Size = new System.Drawing.Size(465, 30);
         this.panel6.TabIndex = 11;
         // 
         // firstName
         // 
         this.firstName.Dock = System.Windows.Forms.DockStyle.Fill;
         this.firstName.Location = new System.Drawing.Point(129, 1);
         this.firstName.Name = "firstName";
         this.firstName.Size = new System.Drawing.Size(331, 29);
         this.firstName.TabIndex = 3;
         // 
         // label7
         // 
         this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label7.Dock = System.Windows.Forms.DockStyle.Left;
         this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label7.ForeColor = System.Drawing.Color.White;
         this.label7.Location = new System.Drawing.Point(1, 1);
         this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label7.Name = "label7";
         this.label7.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label7.Size = new System.Drawing.Size(128, 24);
         this.label7.TabIndex = 2;
         this.label7.Text = "First Name";
         this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer6
         // 
         this.splitContainer6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer6.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer6.Location = new System.Drawing.Point(1, 1);
         this.splitContainer6.Name = "splitContainer6";
         // 
         // splitContainer6.Panel1
         // 
         this.splitContainer6.Panel1.BackColor = System.Drawing.Color.Gainsboro;
         this.splitContainer6.Panel1.Controls.Add(this.panel31);
         this.splitContainer6.Panel1.Controls.Add(this.splitContainer5);
         this.splitContainer6.Panel1.Padding = new System.Windows.Forms.Padding(8);
         // 
         // splitContainer6.Panel2
         // 
         this.splitContainer6.Panel2.BackColor = System.Drawing.Color.Gainsboro;
         this.splitContainer6.Panel2.Controls.Add(this.splitContainer11);
         this.splitContainer6.Panel2.Padding = new System.Windows.Forms.Padding(8);
         this.splitContainer6.Size = new System.Drawing.Size(952, 824);
         this.splitContainer6.SplitterDistance = 427;
         this.splitContainer6.SplitterWidth = 2;
         this.splitContainer6.TabIndex = 0;
         // 
         // panel31
         // 
         this.panel31.Controls.Add(this.splitContainer14);
         this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
         this.panel31.Location = new System.Drawing.Point(8, 287);
         this.panel31.Name = "panel31";
         this.panel31.Size = new System.Drawing.Size(407, 525);
         this.panel31.TabIndex = 9;
         // 
         // splitContainer14
         // 
         this.splitContainer14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer14.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer14.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer14.IsSplitterFixed = true;
         this.splitContainer14.Location = new System.Drawing.Point(0, 0);
         this.splitContainer14.Name = "splitContainer14";
         this.splitContainer14.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer14.Panel1
         // 
         this.splitContainer14.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer14.Panel1.Controls.Add(this.label43);
         this.splitContainer14.Panel1Collapsed = true;
         // 
         // splitContainer14.Panel2
         // 
         this.splitContainer14.Panel2.Controls.Add(this.splitContainer8);
         this.splitContainer14.Panel2.Controls.Add(this.splitContainer9);
         this.splitContainer14.Panel2.Controls.Add(this.splitContainer7);
         this.splitContainer14.Panel2.Controls.Add(this.splitContainer15);
         this.splitContainer14.Size = new System.Drawing.Size(407, 525);
         this.splitContainer14.SplitterDistance = 35;
         this.splitContainer14.TabIndex = 0;
         // 
         // label43
         // 
         this.label43.BackColor = System.Drawing.Color.Transparent;
         this.label43.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label43.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label43.ForeColor = System.Drawing.Color.White;
         this.label43.Location = new System.Drawing.Point(0, 0);
         this.label43.Name = "label43";
         this.label43.Size = new System.Drawing.Size(146, 31);
         this.label43.TabIndex = 1;
         this.label43.Text = "Miscellaneous Details";
         this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // splitContainer8
         // 
         this.splitContainer8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.splitContainer8.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer8.Location = new System.Drawing.Point(0, 410);
         this.splitContainer8.MaximumSize = new System.Drawing.Size(0, 135);
         this.splitContainer8.MinimumSize = new System.Drawing.Size(0, 99);
         this.splitContainer8.Name = "splitContainer8";
         this.splitContainer8.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer8.Panel1
         // 
         this.splitContainer8.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer8.Panel1.Controls.Add(this.label24);
         // 
         // splitContainer8.Panel2
         // 
         this.splitContainer8.Panel2.BackColor = System.Drawing.Color.Gainsboro;
         this.splitContainer8.Panel2.Controls.Add(this.panel21);
         this.splitContainer8.Panel2.Padding = new System.Windows.Forms.Padding(1);
         this.splitContainer8.Size = new System.Drawing.Size(403, 111);
         this.splitContainer8.SplitterDistance = 30;
         this.splitContainer8.SplitterWidth = 2;
         this.splitContainer8.TabIndex = 33;
         // 
         // label24
         // 
         this.label24.BackColor = System.Drawing.Color.Transparent;
         this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label24.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label24.ForeColor = System.Drawing.Color.White;
         this.label24.Location = new System.Drawing.Point(0, 0);
         this.label24.Name = "label24";
         this.label24.Size = new System.Drawing.Size(399, 26);
         this.label24.TabIndex = 0;
         this.label24.Text = "Deceased Details of Death";
         this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel21
         // 
         this.panel21.BackColor = System.Drawing.Color.White;
         this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel21.Controls.Add(this.POD);
         this.panel21.Controls.Add(this.label26);
         this.panel21.Controls.Add(this.DOD);
         this.panel21.Controls.Add(this.label32);
         this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel21.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel21.Location = new System.Drawing.Point(1, 1);
         this.panel21.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel21.Name = "panel21";
         this.panel21.Padding = new System.Windows.Forms.Padding(1);
         this.panel21.Size = new System.Drawing.Size(397, 30);
         this.panel21.TabIndex = 13;
         // 
         // POD
         // 
         this.POD.Dock = System.Windows.Forms.DockStyle.Fill;
         this.POD.Location = new System.Drawing.Point(236, 1);
         this.POD.Name = "POD";
         this.POD.Size = new System.Drawing.Size(156, 29);
         this.POD.TabIndex = 6;
         // 
         // label26
         // 
         this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label26.Dock = System.Windows.Forms.DockStyle.Left;
         this.label26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label26.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label26.ForeColor = System.Drawing.Color.White;
         this.label26.Location = new System.Drawing.Point(173, 1);
         this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label26.Name = "label26";
         this.label26.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label26.Size = new System.Drawing.Size(63, 24);
         this.label26.TabIndex = 5;
         this.label26.Text = "Place";
         this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // DOD
         // 
         this.DOD.Dock = System.Windows.Forms.DockStyle.Left;
         this.DOD.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.DOD.Location = new System.Drawing.Point(57, 1);
         this.DOD.Name = "DOD";
         this.DOD.ShowUpDown = true;
         this.DOD.Size = new System.Drawing.Size(116, 29);
         this.DOD.TabIndex = 3;
         // 
         // label32
         // 
         this.label32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label32.Dock = System.Windows.Forms.DockStyle.Left;
         this.label32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label32.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label32.ForeColor = System.Drawing.Color.White;
         this.label32.Location = new System.Drawing.Point(1, 1);
         this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label32.Name = "label32";
         this.label32.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label32.Size = new System.Drawing.Size(56, 24);
         this.label32.TabIndex = 2;
         this.label32.Text = "Date";
         this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer9
         // 
         this.splitContainer9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer9.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitContainer9.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer9.Location = new System.Drawing.Point(0, 313);
         this.splitContainer9.MaximumSize = new System.Drawing.Size(0, 135);
         this.splitContainer9.MinimumSize = new System.Drawing.Size(0, 99);
         this.splitContainer9.Name = "splitContainer9";
         this.splitContainer9.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer9.Panel1
         // 
         this.splitContainer9.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer9.Panel1.Controls.Add(this.label29);
         // 
         // splitContainer9.Panel2
         // 
         this.splitContainer9.Panel2.BackColor = System.Drawing.Color.Gainsboro;
         this.splitContainer9.Panel2.Controls.Add(this.panel24);
         this.splitContainer9.Panel2.Controls.Add(this.panel27);
         this.splitContainer9.Panel2.Padding = new System.Windows.Forms.Padding(1);
         this.splitContainer9.Size = new System.Drawing.Size(403, 99);
         this.splitContainer9.SplitterDistance = 30;
         this.splitContainer9.SplitterWidth = 2;
         this.splitContainer9.TabIndex = 32;
         // 
         // label29
         // 
         this.label29.BackColor = System.Drawing.Color.Transparent;
         this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label29.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label29.ForeColor = System.Drawing.Color.White;
         this.label29.Location = new System.Drawing.Point(0, 0);
         this.label29.Name = "label29";
         this.label29.Size = new System.Drawing.Size(399, 26);
         this.label29.TabIndex = 0;
         this.label29.Text = "Miscellaneous Details";
         this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel24
         // 
         this.panel24.BackColor = System.Drawing.Color.White;
         this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel24.Controls.Add(this.Disability);
         this.panel24.Controls.Add(this.label33);
         this.panel24.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panel24.Location = new System.Drawing.Point(1, 32);
         this.panel24.Name = "panel24";
         this.panel24.Padding = new System.Windows.Forms.Padding(1);
         this.panel24.Size = new System.Drawing.Size(397, 30);
         this.panel24.TabIndex = 18;
         // 
         // Disability
         // 
         this.Disability.BackColor = System.Drawing.Color.White;
         this.Disability.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Disability.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Disability.FormattingEnabled = true;
         this.Disability.Location = new System.Drawing.Point(129, 1);
         this.Disability.Name = "Disability";
         this.Disability.Size = new System.Drawing.Size(263, 30);
         this.Disability.TabIndex = 5;
         // 
         // label33
         // 
         this.label33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label33.Dock = System.Windows.Forms.DockStyle.Left;
         this.label33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label33.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label33.ForeColor = System.Drawing.Color.White;
         this.label33.Location = new System.Drawing.Point(1, 1);
         this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label33.Name = "label33";
         this.label33.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label33.Size = new System.Drawing.Size(128, 24);
         this.label33.TabIndex = 2;
         this.label33.Text = "Disability";
         this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel27
         // 
         this.panel27.BackColor = System.Drawing.Color.White;
         this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel27.Controls.Add(this.SCID);
         this.panel27.Controls.Add(this.label34);
         this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel27.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel27.Location = new System.Drawing.Point(1, 1);
         this.panel27.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel27.Name = "panel27";
         this.panel27.Padding = new System.Windows.Forms.Padding(1);
         this.panel27.Size = new System.Drawing.Size(397, 30);
         this.panel27.TabIndex = 13;
         // 
         // SCID
         // 
         this.SCID.Dock = System.Windows.Forms.DockStyle.Fill;
         this.SCID.Location = new System.Drawing.Point(130, 1);
         this.SCID.Name = "SCID";
         this.SCID.Size = new System.Drawing.Size(262, 29);
         this.SCID.TabIndex = 5;
         // 
         // label34
         // 
         this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label34.Dock = System.Windows.Forms.DockStyle.Left;
         this.label34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label34.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label34.ForeColor = System.Drawing.Color.White;
         this.label34.Location = new System.Drawing.Point(1, 1);
         this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label34.Name = "label34";
         this.label34.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label34.Size = new System.Drawing.Size(129, 24);
         this.label34.TabIndex = 2;
         this.label34.Text = "Sr. Citizen ID";
         this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer7
         // 
         this.splitContainer7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitContainer7.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer7.Location = new System.Drawing.Point(0, 158);
         this.splitContainer7.MaximumSize = new System.Drawing.Size(0, 170);
         this.splitContainer7.MinimumSize = new System.Drawing.Size(0, 127);
         this.splitContainer7.Name = "splitContainer7";
         this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer7.Panel1
         // 
         this.splitContainer7.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer7.Panel1.Controls.Add(this.label23);
         // 
         // splitContainer7.Panel2
         // 
         this.splitContainer7.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
         this.splitContainer7.Panel2.Controls.Add(this.panel45);
         this.splitContainer7.Panel2.Controls.Add(this.panel22);
         this.splitContainer7.Panel2.Controls.Add(this.panel25);
         this.splitContainer7.Panel2.Controls.Add(this.panel26);
         this.splitContainer7.Panel2.Padding = new System.Windows.Forms.Padding(1);
         this.splitContainer7.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer7_Panel2_Paint);
         this.splitContainer7.Size = new System.Drawing.Size(403, 155);
         this.splitContainer7.SplitterDistance = 30;
         this.splitContainer7.SplitterWidth = 1;
         this.splitContainer7.TabIndex = 31;
         // 
         // label23
         // 
         this.label23.BackColor = System.Drawing.Color.Transparent;
         this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label23.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label23.ForeColor = System.Drawing.Color.White;
         this.label23.Location = new System.Drawing.Point(0, 0);
         this.label23.Name = "label23";
         this.label23.Size = new System.Drawing.Size(399, 26);
         this.label23.TabIndex = 0;
         this.label23.Text = "Employment Details";
         this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel45
         // 
         this.panel45.BackColor = System.Drawing.Color.White;
         this.panel45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel45.Controls.Add(this.Occupation);
         this.panel45.Controls.Add(this.label60);
         this.panel45.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel45.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel45.Location = new System.Drawing.Point(1, 61);
         this.panel45.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel45.Name = "panel45";
         this.panel45.Padding = new System.Windows.Forms.Padding(1);
         this.panel45.Size = new System.Drawing.Size(397, 30);
         this.panel45.TabIndex = 21;
         // 
         // Occupation
         // 
         this.Occupation.BackColor = System.Drawing.Color.White;
         this.Occupation.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Occupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Occupation.FormattingEnabled = true;
         this.Occupation.Location = new System.Drawing.Point(129, 1);
         this.Occupation.Name = "Occupation";
         this.Occupation.Size = new System.Drawing.Size(263, 30);
         this.Occupation.TabIndex = 4;
         // 
         // label60
         // 
         this.label60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label60.Dock = System.Windows.Forms.DockStyle.Left;
         this.label60.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label60.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label60.ForeColor = System.Drawing.Color.White;
         this.label60.Location = new System.Drawing.Point(1, 1);
         this.label60.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label60.Name = "label60";
         this.label60.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label60.Size = new System.Drawing.Size(128, 24);
         this.label60.TabIndex = 2;
         this.label60.Text = "Occupation";
         this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel22
         // 
         this.panel22.BackColor = System.Drawing.Color.White;
         this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel22.Controls.Add(this.Industry);
         this.panel22.Controls.Add(this.label27);
         this.panel22.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panel22.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel22.Location = new System.Drawing.Point(1, 89);
         this.panel22.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel22.Name = "panel22";
         this.panel22.Padding = new System.Windows.Forms.Padding(1);
         this.panel22.Size = new System.Drawing.Size(397, 30);
         this.panel22.TabIndex = 20;
         // 
         // Industry
         // 
         this.Industry.BackColor = System.Drawing.Color.White;
         this.Industry.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Industry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Industry.FormattingEnabled = true;
         this.Industry.Location = new System.Drawing.Point(129, 1);
         this.Industry.Name = "Industry";
         this.Industry.Size = new System.Drawing.Size(263, 30);
         this.Industry.TabIndex = 4;
         // 
         // label27
         // 
         this.label27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label27.Dock = System.Windows.Forms.DockStyle.Left;
         this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label27.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label27.ForeColor = System.Drawing.Color.White;
         this.label27.Location = new System.Drawing.Point(1, 1);
         this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label27.Name = "label27";
         this.label27.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label27.Size = new System.Drawing.Size(128, 24);
         this.label27.TabIndex = 2;
         this.label27.Text = "Industry";
         this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel25
         // 
         this.panel25.BackColor = System.Drawing.Color.White;
         this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel25.Controls.Add(this.WorkerType);
         this.panel25.Controls.Add(this.label30);
         this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel25.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel25.Location = new System.Drawing.Point(1, 31);
         this.panel25.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel25.Name = "panel25";
         this.panel25.Padding = new System.Windows.Forms.Padding(1);
         this.panel25.Size = new System.Drawing.Size(397, 30);
         this.panel25.TabIndex = 12;
         // 
         // WorkerType
         // 
         this.WorkerType.BackColor = System.Drawing.Color.White;
         this.WorkerType.Dock = System.Windows.Forms.DockStyle.Fill;
         this.WorkerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.WorkerType.FormattingEnabled = true;
         this.WorkerType.Location = new System.Drawing.Point(129, 1);
         this.WorkerType.Name = "WorkerType";
         this.WorkerType.Size = new System.Drawing.Size(263, 30);
         this.WorkerType.TabIndex = 4;
         // 
         // label30
         // 
         this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label30.Dock = System.Windows.Forms.DockStyle.Left;
         this.label30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label30.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label30.ForeColor = System.Drawing.Color.White;
         this.label30.Location = new System.Drawing.Point(1, 1);
         this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label30.Name = "label30";
         this.label30.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label30.Size = new System.Drawing.Size(128, 24);
         this.label30.TabIndex = 2;
         this.label30.Text = "Worker Type";
         this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel26
         // 
         this.panel26.BackColor = System.Drawing.Color.White;
         this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel26.Controls.Add(this.Employment);
         this.panel26.Controls.Add(this.label31);
         this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel26.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel26.Location = new System.Drawing.Point(1, 1);
         this.panel26.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel26.Name = "panel26";
         this.panel26.Padding = new System.Windows.Forms.Padding(1);
         this.panel26.Size = new System.Drawing.Size(397, 30);
         this.panel26.TabIndex = 11;
         // 
         // Employment
         // 
         this.Employment.BackColor = System.Drawing.Color.White;
         this.Employment.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Employment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Employment.FormattingEnabled = true;
         this.Employment.Location = new System.Drawing.Point(129, 1);
         this.Employment.Name = "Employment";
         this.Employment.Size = new System.Drawing.Size(263, 30);
         this.Employment.TabIndex = 4;
         // 
         // label31
         // 
         this.label31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label31.Dock = System.Windows.Forms.DockStyle.Left;
         this.label31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label31.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label31.ForeColor = System.Drawing.Color.White;
         this.label31.Location = new System.Drawing.Point(1, 1);
         this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label31.Name = "label31";
         this.label31.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label31.Size = new System.Drawing.Size(128, 24);
         this.label31.TabIndex = 2;
         this.label31.Text = "Status";
         this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer15
         // 
         this.splitContainer15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer15.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitContainer15.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer15.Location = new System.Drawing.Point(0, 0);
         this.splitContainer15.Name = "splitContainer15";
         this.splitContainer15.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer15.Panel1
         // 
         this.splitContainer15.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer15.Panel1.Controls.Add(this.label49);
         // 
         // splitContainer15.Panel2
         // 
         this.splitContainer15.Panel2.BackColor = System.Drawing.Color.Gainsboro;
         this.splitContainer15.Panel2.Controls.Add(this.panel39);
         this.splitContainer15.Panel2.Controls.Add(this.panel38);
         this.splitContainer15.Panel2.Controls.Add(this.panel37);
         this.splitContainer15.Panel2.Controls.Add(this.panel36);
         this.splitContainer15.Panel2.Padding = new System.Windows.Forms.Padding(1);
         this.splitContainer15.Size = new System.Drawing.Size(403, 158);
         this.splitContainer15.SplitterDistance = 30;
         this.splitContainer15.SplitterWidth = 2;
         this.splitContainer15.TabIndex = 9;
         // 
         // label49
         // 
         this.label49.BackColor = System.Drawing.Color.Transparent;
         this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label49.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label49.ForeColor = System.Drawing.Color.White;
         this.label49.Location = new System.Drawing.Point(0, 0);
         this.label49.Name = "label49";
         this.label49.Size = new System.Drawing.Size(399, 26);
         this.label49.TabIndex = 0;
         this.label49.Text = "Physical Details";
         this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel39
         // 
         this.panel39.BackColor = System.Drawing.Color.White;
         this.panel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel39.Controls.Add(this.Weight);
         this.panel39.Controls.Add(this.label54);
         this.panel39.Controls.Add(this.Height);
         this.panel39.Controls.Add(this.label53);
         this.panel39.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panel39.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel39.Location = new System.Drawing.Point(1, 91);
         this.panel39.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel39.Name = "panel39";
         this.panel39.Padding = new System.Windows.Forms.Padding(1);
         this.panel39.Size = new System.Drawing.Size(397, 30);
         this.panel39.TabIndex = 23;
         // 
         // Weight
         // 
         this.Weight.DecimalPlaces = 2;
         this.Weight.Dock = System.Windows.Forms.DockStyle.Right;
         this.Weight.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
         this.Weight.Location = new System.Drawing.Point(314, 1);
         this.Weight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
         this.Weight.Name = "Weight";
         this.Weight.Size = new System.Drawing.Size(78, 29);
         this.Weight.TabIndex = 16;
         this.Weight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.Weight.DoubleClick += new System.EventHandler(this.OpenConverter);
         // 
         // label54
         // 
         this.label54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label54.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label54.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label54.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label54.ForeColor = System.Drawing.Color.White;
         this.label54.Location = new System.Drawing.Point(205, 1);
         this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label54.Name = "label54";
         this.label54.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label54.Size = new System.Drawing.Size(187, 24);
         this.label54.TabIndex = 14;
         this.label54.Text = "Weight (kgs)";
         this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label54.DoubleClick += new System.EventHandler(this.OpenConverter);
         // 
         // Height
         // 
         this.Height.DecimalPlaces = 2;
         this.Height.Dock = System.Windows.Forms.DockStyle.Left;
         this.Height.Increment = new decimal(new int[] {
            50,
            0,
            0,
            131072});
         this.Height.Location = new System.Drawing.Point(130, 1);
         this.Height.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
         this.Height.Name = "Height";
         this.Height.Size = new System.Drawing.Size(75, 29);
         this.Height.TabIndex = 3;
         this.Height.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.Height.DoubleClick += new System.EventHandler(this.OpenConverter);
         // 
         // label53
         // 
         this.label53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label53.Dock = System.Windows.Forms.DockStyle.Left;
         this.label53.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label53.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label53.ForeColor = System.Drawing.Color.White;
         this.label53.Location = new System.Drawing.Point(1, 1);
         this.label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label53.Name = "label53";
         this.label53.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label53.Size = new System.Drawing.Size(129, 24);
         this.label53.TabIndex = 2;
         this.label53.Text = "Height (cm)";
         this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label53.DoubleClick += new System.EventHandler(this.OpenConverter);
         // 
         // panel38
         // 
         this.panel38.BackColor = System.Drawing.Color.White;
         this.panel38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel38.Controls.Add(this.Hair);
         this.panel38.Controls.Add(this.label52);
         this.panel38.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel38.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel38.Location = new System.Drawing.Point(1, 61);
         this.panel38.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel38.Name = "panel38";
         this.panel38.Padding = new System.Windows.Forms.Padding(1);
         this.panel38.Size = new System.Drawing.Size(397, 30);
         this.panel38.TabIndex = 22;
         // 
         // Hair
         // 
         this.Hair.BackColor = System.Drawing.Color.White;
         this.Hair.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Hair.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Hair.FormattingEnabled = true;
         this.Hair.Location = new System.Drawing.Point(129, 1);
         this.Hair.Name = "Hair";
         this.Hair.Size = new System.Drawing.Size(263, 30);
         this.Hair.TabIndex = 4;
         // 
         // label52
         // 
         this.label52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label52.Dock = System.Windows.Forms.DockStyle.Left;
         this.label52.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label52.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label52.ForeColor = System.Drawing.Color.White;
         this.label52.Location = new System.Drawing.Point(1, 1);
         this.label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label52.Name = "label52";
         this.label52.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label52.Size = new System.Drawing.Size(128, 24);
         this.label52.TabIndex = 2;
         this.label52.Text = "Hair Color";
         this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel37
         // 
         this.panel37.BackColor = System.Drawing.Color.White;
         this.panel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel37.Controls.Add(this.Complexion);
         this.panel37.Controls.Add(this.label51);
         this.panel37.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel37.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel37.Location = new System.Drawing.Point(1, 31);
         this.panel37.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel37.Name = "panel37";
         this.panel37.Padding = new System.Windows.Forms.Padding(1);
         this.panel37.Size = new System.Drawing.Size(397, 30);
         this.panel37.TabIndex = 21;
         // 
         // Complexion
         // 
         this.Complexion.BackColor = System.Drawing.Color.White;
         this.Complexion.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Complexion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Complexion.FormattingEnabled = true;
         this.Complexion.Location = new System.Drawing.Point(129, 1);
         this.Complexion.Name = "Complexion";
         this.Complexion.Size = new System.Drawing.Size(263, 30);
         this.Complexion.TabIndex = 4;
         // 
         // label51
         // 
         this.label51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label51.Dock = System.Windows.Forms.DockStyle.Left;
         this.label51.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label51.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label51.ForeColor = System.Drawing.Color.White;
         this.label51.Location = new System.Drawing.Point(1, 1);
         this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label51.Name = "label51";
         this.label51.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label51.Size = new System.Drawing.Size(128, 24);
         this.label51.TabIndex = 2;
         this.label51.Text = "Skin";
         this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel36
         // 
         this.panel36.BackColor = System.Drawing.Color.White;
         this.panel36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel36.Controls.Add(this.Build);
         this.panel36.Controls.Add(this.label50);
         this.panel36.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel36.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel36.Location = new System.Drawing.Point(1, 1);
         this.panel36.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel36.Name = "panel36";
         this.panel36.Padding = new System.Windows.Forms.Padding(1);
         this.panel36.Size = new System.Drawing.Size(397, 30);
         this.panel36.TabIndex = 20;
         // 
         // Build
         // 
         this.Build.BackColor = System.Drawing.Color.White;
         this.Build.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Build.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Build.FormattingEnabled = true;
         this.Build.Location = new System.Drawing.Point(129, 1);
         this.Build.Name = "Build";
         this.Build.Size = new System.Drawing.Size(263, 30);
         this.Build.TabIndex = 4;
         // 
         // label50
         // 
         this.label50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label50.Dock = System.Windows.Forms.DockStyle.Left;
         this.label50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label50.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label50.ForeColor = System.Drawing.Color.White;
         this.label50.Location = new System.Drawing.Point(1, 1);
         this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label50.Name = "label50";
         this.label50.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label50.Size = new System.Drawing.Size(128, 24);
         this.label50.TabIndex = 2;
         this.label50.Text = "Body";
         this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer5
         // 
         this.splitContainer5.BackColor = System.Drawing.Color.DarkGray;
         this.splitContainer5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Top;
         this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer5.Location = new System.Drawing.Point(8, 8);
         this.splitContainer5.MaximumSize = new System.Drawing.Size(0, 280);
         this.splitContainer5.MinimumSize = new System.Drawing.Size(0, 275);
         this.splitContainer5.Name = "splitContainer5";
         this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer5.Panel1
         // 
         this.splitContainer5.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer5.Panel1.Controls.Add(this.label14);
         this.splitContainer5.Panel1.Padding = new System.Windows.Forms.Padding(1);
         // 
         // splitContainer5.Panel2
         // 
         this.splitContainer5.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
         this.splitContainer5.Panel2.Controls.Add(this.panel18);
         this.splitContainer5.Panel2.Controls.Add(this.panel16);
         this.splitContainer5.Panel2.Controls.Add(this.panel15);
         this.splitContainer5.Panel2.Controls.Add(this.panel14);
         this.splitContainer5.Panel2.Controls.Add(this.panel17);
         this.splitContainer5.Panel2.Controls.Add(this.panel11);
         this.splitContainer5.Panel2.Controls.Add(this.panel12);
         this.splitContainer5.Panel2.Controls.Add(this.panel13);
         this.splitContainer5.Panel2.Margin = new System.Windows.Forms.Padding(0, 14, 0, 0);
         this.splitContainer5.Panel2.Padding = new System.Windows.Forms.Padding(2);
         this.splitContainer5.Size = new System.Drawing.Size(407, 279);
         this.splitContainer5.SplitterDistance = 30;
         this.splitContainer5.SplitterWidth = 1;
         this.splitContainer5.TabIndex = 5;
         // 
         // label14
         // 
         this.label14.BackColor = System.Drawing.Color.Transparent;
         this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label14.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label14.ForeColor = System.Drawing.Color.White;
         this.label14.Location = new System.Drawing.Point(1, 1);
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size(401, 24);
         this.label14.TabIndex = 0;
         this.label14.Text = "Deceased Details of Death";
         this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel18
         // 
         this.panel18.BackColor = System.Drawing.Color.White;
         this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel18.Controls.Add(this.Attainment);
         this.panel18.Controls.Add(this.label22);
         this.panel18.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panel18.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel18.Location = new System.Drawing.Point(2, 212);
         this.panel18.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel18.Name = "panel18";
         this.panel18.Padding = new System.Windows.Forms.Padding(1);
         this.panel18.Size = new System.Drawing.Size(399, 30);
         this.panel18.TabIndex = 23;
         // 
         // Attainment
         // 
         this.Attainment.BackColor = System.Drawing.Color.White;
         this.Attainment.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Attainment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Attainment.FormattingEnabled = true;
         this.Attainment.Location = new System.Drawing.Point(129, 1);
         this.Attainment.Name = "Attainment";
         this.Attainment.Size = new System.Drawing.Size(265, 30);
         this.Attainment.TabIndex = 4;
         // 
         // label22
         // 
         this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label22.Dock = System.Windows.Forms.DockStyle.Left;
         this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label22.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label22.ForeColor = System.Drawing.Color.White;
         this.label22.Location = new System.Drawing.Point(1, 1);
         this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label22.Name = "label22";
         this.label22.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label22.Size = new System.Drawing.Size(128, 24);
         this.label22.TabIndex = 2;
         this.label22.Text = "Education";
         this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel16
         // 
         this.panel16.BackColor = System.Drawing.Color.White;
         this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel16.Controls.Add(this.Citizenship);
         this.panel16.Controls.Add(this.label20);
         this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel16.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel16.Location = new System.Drawing.Point(2, 182);
         this.panel16.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel16.Name = "panel16";
         this.panel16.Padding = new System.Windows.Forms.Padding(1);
         this.panel16.Size = new System.Drawing.Size(399, 30);
         this.panel16.TabIndex = 22;
         // 
         // Citizenship
         // 
         this.Citizenship.BackColor = System.Drawing.Color.White;
         this.Citizenship.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Citizenship.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Citizenship.FormattingEnabled = true;
         this.Citizenship.Location = new System.Drawing.Point(129, 1);
         this.Citizenship.Name = "Citizenship";
         this.Citizenship.Size = new System.Drawing.Size(265, 30);
         this.Citizenship.TabIndex = 4;
         // 
         // label20
         // 
         this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label20.Dock = System.Windows.Forms.DockStyle.Left;
         this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label20.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label20.ForeColor = System.Drawing.Color.White;
         this.label20.Location = new System.Drawing.Point(1, 1);
         this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label20.Name = "label20";
         this.label20.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label20.Size = new System.Drawing.Size(128, 24);
         this.label20.TabIndex = 2;
         this.label20.Text = "Nationality";
         this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel15
         // 
         this.panel15.BackColor = System.Drawing.Color.White;
         this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel15.Controls.Add(this.Ethnicity);
         this.panel15.Controls.Add(this.label19);
         this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel15.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel15.Location = new System.Drawing.Point(2, 152);
         this.panel15.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel15.Name = "panel15";
         this.panel15.Padding = new System.Windows.Forms.Padding(1);
         this.panel15.Size = new System.Drawing.Size(399, 30);
         this.panel15.TabIndex = 21;
         // 
         // Ethnicity
         // 
         this.Ethnicity.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Ethnicity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Ethnicity.FormattingEnabled = true;
         this.Ethnicity.Location = new System.Drawing.Point(129, 1);
         this.Ethnicity.Name = "Ethnicity";
         this.Ethnicity.Size = new System.Drawing.Size(265, 30);
         this.Ethnicity.TabIndex = 4;
         // 
         // label19
         // 
         this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label19.Dock = System.Windows.Forms.DockStyle.Left;
         this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label19.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label19.ForeColor = System.Drawing.Color.White;
         this.label19.Location = new System.Drawing.Point(1, 1);
         this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label19.Name = "label19";
         this.label19.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label19.Size = new System.Drawing.Size(128, 24);
         this.label19.TabIndex = 2;
         this.label19.Text = "Ethnicity";
         this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel14
         // 
         this.panel14.BackColor = System.Drawing.Color.White;
         this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel14.Controls.Add(this.Religion);
         this.panel14.Controls.Add(this.label18);
         this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel14.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel14.Location = new System.Drawing.Point(2, 122);
         this.panel14.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel14.Name = "panel14";
         this.panel14.Padding = new System.Windows.Forms.Padding(1);
         this.panel14.Size = new System.Drawing.Size(399, 30);
         this.panel14.TabIndex = 19;
         // 
         // Religion
         // 
         this.Religion.BackColor = System.Drawing.Color.White;
         this.Religion.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Religion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Religion.FormattingEnabled = true;
         this.Religion.Location = new System.Drawing.Point(129, 1);
         this.Religion.Name = "Religion";
         this.Religion.Size = new System.Drawing.Size(265, 30);
         this.Religion.TabIndex = 4;
         // 
         // label18
         // 
         this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label18.Dock = System.Windows.Forms.DockStyle.Left;
         this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label18.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label18.ForeColor = System.Drawing.Color.White;
         this.label18.Location = new System.Drawing.Point(1, 1);
         this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label18.Name = "label18";
         this.label18.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label18.Size = new System.Drawing.Size(128, 24);
         this.label18.TabIndex = 2;
         this.label18.Text = "Religion";
         this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label18.Click += new System.EventHandler(this.label18_Click);
         // 
         // panel17
         // 
         this.panel17.BackColor = System.Drawing.Color.White;
         this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel17.Controls.Add(this.POB);
         this.panel17.Controls.Add(this.label21);
         this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel17.Location = new System.Drawing.Point(2, 92);
         this.panel17.Name = "panel17";
         this.panel17.Padding = new System.Windows.Forms.Padding(1);
         this.panel17.Size = new System.Drawing.Size(399, 30);
         this.panel17.TabIndex = 18;
         // 
         // POB
         // 
         this.POB.Dock = System.Windows.Forms.DockStyle.Fill;
         this.POB.Location = new System.Drawing.Point(129, 1);
         this.POB.Name = "POB";
         this.POB.Size = new System.Drawing.Size(265, 29);
         this.POB.TabIndex = 4;
         // 
         // label21
         // 
         this.label21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label21.Dock = System.Windows.Forms.DockStyle.Left;
         this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label21.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label21.ForeColor = System.Drawing.Color.White;
         this.label21.Location = new System.Drawing.Point(1, 1);
         this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label21.Name = "label21";
         this.label21.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label21.Size = new System.Drawing.Size(128, 24);
         this.label21.TabIndex = 2;
         this.label21.Text = "Birth Place";
         this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel11
         // 
         this.panel11.BackColor = System.Drawing.Color.White;
         this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel11.Controls.Add(this.DOB);
         this.panel11.Controls.Add(this.label15);
         this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel11.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel11.Location = new System.Drawing.Point(2, 62);
         this.panel11.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel11.Name = "panel11";
         this.panel11.Padding = new System.Windows.Forms.Padding(1);
         this.panel11.Size = new System.Drawing.Size(399, 30);
         this.panel11.TabIndex = 13;
         // 
         // DOB
         // 
         this.DOB.Dock = System.Windows.Forms.DockStyle.Fill;
         this.DOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.DOB.Location = new System.Drawing.Point(129, 1);
         this.DOB.Name = "DOB";
         this.DOB.ShowUpDown = true;
         this.DOB.Size = new System.Drawing.Size(265, 29);
         this.DOB.TabIndex = 4;
         // 
         // label15
         // 
         this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label15.Dock = System.Windows.Forms.DockStyle.Left;
         this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label15.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label15.ForeColor = System.Drawing.Color.White;
         this.label15.Location = new System.Drawing.Point(1, 1);
         this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label15.Name = "label15";
         this.label15.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label15.Size = new System.Drawing.Size(128, 24);
         this.label15.TabIndex = 2;
         this.label15.Text = "Date of Birth";
         this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel12
         // 
         this.panel12.BackColor = System.Drawing.Color.White;
         this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel12.Controls.Add(this.CivilStatus);
         this.panel12.Controls.Add(this.label16);
         this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel12.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel12.Location = new System.Drawing.Point(2, 32);
         this.panel12.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel12.Name = "panel12";
         this.panel12.Padding = new System.Windows.Forms.Padding(1);
         this.panel12.Size = new System.Drawing.Size(399, 30);
         this.panel12.TabIndex = 12;
         // 
         // CivilStatus
         // 
         this.CivilStatus.BackColor = System.Drawing.Color.White;
         this.CivilStatus.Dock = System.Windows.Forms.DockStyle.Fill;
         this.CivilStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.CivilStatus.FormattingEnabled = true;
         this.CivilStatus.Location = new System.Drawing.Point(129, 1);
         this.CivilStatus.Name = "CivilStatus";
         this.CivilStatus.Size = new System.Drawing.Size(265, 30);
         this.CivilStatus.TabIndex = 4;
         // 
         // label16
         // 
         this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label16.Dock = System.Windows.Forms.DockStyle.Left;
         this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label16.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label16.ForeColor = System.Drawing.Color.White;
         this.label16.Location = new System.Drawing.Point(1, 1);
         this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label16.Name = "label16";
         this.label16.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label16.Size = new System.Drawing.Size(128, 24);
         this.label16.TabIndex = 2;
         this.label16.Text = "Civil Status";
         this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel13
         // 
         this.panel13.BackColor = System.Drawing.Color.White;
         this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel13.Controls.Add(this.Gender);
         this.panel13.Controls.Add(this.label17);
         this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel13.Location = new System.Drawing.Point(2, 2);
         this.panel13.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel13.Name = "panel13";
         this.panel13.Padding = new System.Windows.Forms.Padding(1);
         this.panel13.Size = new System.Drawing.Size(399, 30);
         this.panel13.TabIndex = 11;
         // 
         // Gender
         // 
         this.Gender.BackColor = System.Drawing.Color.White;
         this.Gender.Dock = System.Windows.Forms.DockStyle.Fill;
         this.Gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.Gender.FormattingEnabled = true;
         this.Gender.Location = new System.Drawing.Point(129, 1);
         this.Gender.Name = "Gender";
         this.Gender.Size = new System.Drawing.Size(265, 30);
         this.Gender.TabIndex = 4;
         // 
         // label17
         // 
         this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label17.Dock = System.Windows.Forms.DockStyle.Left;
         this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label17.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label17.ForeColor = System.Drawing.Color.White;
         this.label17.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.label17.ImageKey = "People.png";
         this.label17.ImageList = this.imageList2;
         this.label17.Location = new System.Drawing.Point(1, 1);
         this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label17.Name = "label17";
         this.label17.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
         this.label17.Size = new System.Drawing.Size(128, 24);
         this.label17.TabIndex = 2;
         this.label17.Text = "Gender";
         this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer11
         // 
         this.splitContainer11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer11.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer11.IsSplitterFixed = true;
         this.splitContainer11.Location = new System.Drawing.Point(8, 8);
         this.splitContainer11.Name = "splitContainer11";
         this.splitContainer11.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer11.Panel1
         // 
         this.splitContainer11.Panel1.Controls.Add(this.splitContainer13);
         // 
         // splitContainer11.Panel2
         // 
         this.splitContainer11.Panel2.Controls.Add(this.splitContainer12);
         this.splitContainer11.Size = new System.Drawing.Size(507, 808);
         this.splitContainer11.SplitterDistance = 168;
         this.splitContainer11.SplitterWidth = 2;
         this.splitContainer11.TabIndex = 0;
         // 
         // splitContainer13
         // 
         this.splitContainer13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer13.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer13.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer13.IsSplitterFixed = true;
         this.splitContainer13.Location = new System.Drawing.Point(0, 0);
         this.splitContainer13.MaximumSize = new System.Drawing.Size(0, 162);
         this.splitContainer13.MinimumSize = new System.Drawing.Size(0, 150);
         this.splitContainer13.Name = "splitContainer13";
         this.splitContainer13.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer13.Panel1
         // 
         this.splitContainer13.Panel1.BackColor = System.Drawing.Color.Navy;
         this.splitContainer13.Panel1.Controls.Add(this.label35);
         this.splitContainer13.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
         // 
         // splitContainer13.Panel2
         // 
         this.splitContainer13.Panel2.Controls.Add(this.panel30);
         this.splitContainer13.Panel2.Controls.Add(this.panel29);
         this.splitContainer13.Panel2.Controls.Add(this.panel28);
         this.splitContainer13.Panel2.Controls.Add(this.panel10);
         this.splitContainer13.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
         this.splitContainer13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
         this.splitContainer13.Size = new System.Drawing.Size(507, 162);
         this.splitContainer13.SplitterDistance = 35;
         this.splitContainer13.SplitterWidth = 2;
         this.splitContainer13.TabIndex = 0;
         // 
         // label35
         // 
         this.label35.BackColor = System.Drawing.Color.Transparent;
         this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label35.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label35.ForeColor = System.Drawing.Color.White;
         this.label35.Location = new System.Drawing.Point(0, 0);
         this.label35.Name = "label35";
         this.label35.Size = new System.Drawing.Size(503, 31);
         this.label35.TabIndex = 1;
         this.label35.Text = "Immediate Family Details";
         this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // panel30
         // 
         this.panel30.BackColor = System.Drawing.Color.White;
         this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel30.Controls.Add(this.mAddress);
         this.panel30.Controls.Add(this.Mother);
         this.panel30.Controls.Add(this.label39);
         this.panel30.Dock = System.Windows.Forms.DockStyle.Bottom;
         this.panel30.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel30.Location = new System.Drawing.Point(0, 91);
         this.panel30.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel30.Name = "panel30";
         this.panel30.Padding = new System.Windows.Forms.Padding(1);
         this.panel30.Size = new System.Drawing.Size(503, 30);
         this.panel30.TabIndex = 15;
         // 
         // mAddress
         // 
         this.mAddress.Dock = System.Windows.Forms.DockStyle.Fill;
         this.mAddress.Location = new System.Drawing.Point(283, 1);
         this.mAddress.Name = "mAddress";
         this.mAddress.Size = new System.Drawing.Size(215, 29);
         this.mAddress.TabIndex = 6;
         // 
         // Mother
         // 
         this.Mother.Dock = System.Windows.Forms.DockStyle.Left;
         this.Mother.Location = new System.Drawing.Point(76, 1);
         this.Mother.Name = "Mother";
         this.Mother.Size = new System.Drawing.Size(207, 29);
         this.Mother.TabIndex = 5;
         // 
         // label39
         // 
         this.label39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label39.Dock = System.Windows.Forms.DockStyle.Left;
         this.label39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label39.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label39.ForeColor = System.Drawing.Color.White;
         this.label39.Location = new System.Drawing.Point(1, 1);
         this.label39.Margin = new System.Windows.Forms.Padding(0);
         this.label39.Name = "label39";
         this.label39.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label39.Size = new System.Drawing.Size(75, 24);
         this.label39.TabIndex = 2;
         this.label39.Text = "Mother";
         this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel29
         // 
         this.panel29.BackColor = System.Drawing.Color.White;
         this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel29.Controls.Add(this.fAddress);
         this.panel29.Controls.Add(this.Father);
         this.panel29.Controls.Add(this.label38);
         this.panel29.Controls.Add(this.textBox15);
         this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel29.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel29.Location = new System.Drawing.Point(0, 60);
         this.panel29.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel29.Name = "panel29";
         this.panel29.Padding = new System.Windows.Forms.Padding(1);
         this.panel29.Size = new System.Drawing.Size(503, 30);
         this.panel29.TabIndex = 14;
         // 
         // fAddress
         // 
         this.fAddress.Dock = System.Windows.Forms.DockStyle.Fill;
         this.fAddress.Location = new System.Drawing.Point(283, 1);
         this.fAddress.Name = "fAddress";
         this.fAddress.Size = new System.Drawing.Size(215, 29);
         this.fAddress.TabIndex = 7;
         // 
         // Father
         // 
         this.Father.Dock = System.Windows.Forms.DockStyle.Left;
         this.Father.Location = new System.Drawing.Point(76, 1);
         this.Father.Name = "Father";
         this.Father.Size = new System.Drawing.Size(207, 29);
         this.Father.TabIndex = 6;
         // 
         // label38
         // 
         this.label38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label38.Dock = System.Windows.Forms.DockStyle.Left;
         this.label38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label38.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label38.ForeColor = System.Drawing.Color.White;
         this.label38.Location = new System.Drawing.Point(1, 1);
         this.label38.Margin = new System.Windows.Forms.Padding(0);
         this.label38.Name = "label38";
         this.label38.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label38.Size = new System.Drawing.Size(75, 24);
         this.label38.TabIndex = 5;
         this.label38.Text = "Father";
         this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // textBox15
         // 
         this.textBox15.Dock = System.Windows.Forms.DockStyle.Fill;
         this.textBox15.Location = new System.Drawing.Point(1, 1);
         this.textBox15.Name = "textBox15";
         this.textBox15.Size = new System.Drawing.Size(497, 29);
         this.textBox15.TabIndex = 4;
         // 
         // panel28
         // 
         this.panel28.BackColor = System.Drawing.Color.White;
         this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel28.Controls.Add(this.sAddress);
         this.panel28.Controls.Add(this.Spouse);
         this.panel28.Controls.Add(this.label37);
         this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel28.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel28.Location = new System.Drawing.Point(0, 30);
         this.panel28.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel28.Name = "panel28";
         this.panel28.Padding = new System.Windows.Forms.Padding(1);
         this.panel28.Size = new System.Drawing.Size(503, 30);
         this.panel28.TabIndex = 13;
         // 
         // sAddress
         // 
         this.sAddress.Dock = System.Windows.Forms.DockStyle.Fill;
         this.sAddress.Location = new System.Drawing.Point(283, 1);
         this.sAddress.Name = "sAddress";
         this.sAddress.Size = new System.Drawing.Size(215, 29);
         this.sAddress.TabIndex = 6;
         // 
         // Spouse
         // 
         this.Spouse.Dock = System.Windows.Forms.DockStyle.Left;
         this.Spouse.Location = new System.Drawing.Point(76, 1);
         this.Spouse.Name = "Spouse";
         this.Spouse.Size = new System.Drawing.Size(207, 29);
         this.Spouse.TabIndex = 5;
         // 
         // label37
         // 
         this.label37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label37.Dock = System.Windows.Forms.DockStyle.Left;
         this.label37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label37.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label37.ForeColor = System.Drawing.Color.White;
         this.label37.Location = new System.Drawing.Point(1, 1);
         this.label37.Margin = new System.Windows.Forms.Padding(0);
         this.label37.Name = "label37";
         this.label37.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label37.Size = new System.Drawing.Size(75, 24);
         this.label37.TabIndex = 3;
         this.label37.Text = "Spouse";
         this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // panel10
         // 
         this.panel10.BackColor = System.Drawing.Color.White;
         this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.panel10.Controls.Add(this.label41);
         this.panel10.Controls.Add(this.label40);
         this.panel10.Controls.Add(this.label36);
         this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
         this.panel10.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.panel10.Location = new System.Drawing.Point(0, 0);
         this.panel10.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
         this.panel10.Name = "panel10";
         this.panel10.Padding = new System.Windows.Forms.Padding(1);
         this.panel10.Size = new System.Drawing.Size(503, 30);
         this.panel10.TabIndex = 12;
         // 
         // label41
         // 
         this.label41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label41.Dock = System.Windows.Forms.DockStyle.Fill;
         this.label41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label41.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label41.ForeColor = System.Drawing.Color.White;
         this.label41.Location = new System.Drawing.Point(283, 1);
         this.label41.Margin = new System.Windows.Forms.Padding(0);
         this.label41.Name = "label41";
         this.label41.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label41.Size = new System.Drawing.Size(215, 24);
         this.label41.TabIndex = 5;
         this.label41.Text = "Address";
         this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label40
         // 
         this.label40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label40.Dock = System.Windows.Forms.DockStyle.Left;
         this.label40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label40.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label40.ForeColor = System.Drawing.Color.White;
         this.label40.Location = new System.Drawing.Point(76, 1);
         this.label40.Margin = new System.Windows.Forms.Padding(0);
         this.label40.Name = "label40";
         this.label40.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label40.Size = new System.Drawing.Size(207, 24);
         this.label40.TabIndex = 4;
         this.label40.Text = "Name";
         this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label36
         // 
         this.label36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
         this.label36.Dock = System.Windows.Forms.DockStyle.Left;
         this.label36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.label36.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label36.ForeColor = System.Drawing.Color.White;
         this.label36.Location = new System.Drawing.Point(1, 1);
         this.label36.Margin = new System.Windows.Forms.Padding(0);
         this.label36.Name = "label36";
         this.label36.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
         this.label36.Size = new System.Drawing.Size(75, 24);
         this.label36.TabIndex = 3;
         this.label36.Text = " ";
         this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // splitContainer12
         // 
         this.splitContainer12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.splitContainer12.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer12.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.splitContainer12.IsSplitterFixed = true;
         this.splitContainer12.Location = new System.Drawing.Point(0, 0);
         this.splitContainer12.Name = "splitContainer12";
         this.splitContainer12.Orientation = System.Windows.Forms.Orientation.Horizontal;
         // 
         // splitContainer12.Panel1
         // 
         this.splitContainer12.Panel1.Controls.Add(this.bConInf);
         this.splitContainer12.Panel1.Controls.Add(this.bFP);
         this.splitContainer12.Panel1.Controls.Add(this.bPDetails);
         this.splitContainer12.Panel1Collapsed = true;
         // 
         // splitContainer12.Panel2
         // 
         this.splitContainer12.Panel2.Controls.Add(this.tabControl1);
         this.splitContainer12.Size = new System.Drawing.Size(507, 638);
         this.splitContainer12.SplitterWidth = 2;
         this.splitContainer12.TabIndex = 1;
         // 
         // bConInf
         // 
         this.bConInf.BackColor = System.Drawing.Color.Navy;
         this.bConInf.Dock = System.Windows.Forms.DockStyle.Left;
         this.bConInf.FlatAppearance.BorderSize = 0;
         this.bConInf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bConInf.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bConInf.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(200)))), ((int)(((byte)(252)))));
         this.bConInf.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
         this.bConInf.ImageKey = "iconmonstr-pen-4-240.png";
         this.bConInf.Location = new System.Drawing.Point(330, 0);
         this.bConInf.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bConInf.Name = "bConInf";
         this.bConInf.Size = new System.Drawing.Size(165, 46);
         this.bConInf.TabIndex = 8;
         this.bConInf.Text = "Signature";
         this.bConInf.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.bConInf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
         this.bConInf.UseVisualStyleBackColor = false;
         // 
         // bFP
         // 
         this.bFP.BackColor = System.Drawing.Color.Navy;
         this.bFP.Dock = System.Windows.Forms.DockStyle.Left;
         this.bFP.FlatAppearance.BorderSize = 0;
         this.bFP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bFP.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bFP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(200)))), ((int)(((byte)(252)))));
         this.bFP.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
         this.bFP.ImageKey = "iconmonstr-fingerprint-18-240.png";
         this.bFP.Location = new System.Drawing.Point(165, 0);
         this.bFP.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bFP.Name = "bFP";
         this.bFP.Size = new System.Drawing.Size(165, 46);
         this.bFP.TabIndex = 7;
         this.bFP.Text = "Finger Print";
         this.bFP.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.bFP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
         this.bFP.UseVisualStyleBackColor = false;
         // 
         // bPDetails
         // 
         this.bPDetails.BackColor = System.Drawing.Color.White;
         this.bPDetails.Dock = System.Windows.Forms.DockStyle.Left;
         this.bPDetails.FlatAppearance.BorderSize = 0;
         this.bPDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bPDetails.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bPDetails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
         this.bPDetails.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
         this.bPDetails.ImageKey = "iconmonstr-photo-camera-4-240.png";
         this.bPDetails.Location = new System.Drawing.Point(0, 0);
         this.bPDetails.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bPDetails.Name = "bPDetails";
         this.bPDetails.Size = new System.Drawing.Size(165, 46);
         this.bPDetails.TabIndex = 5;
         this.bPDetails.Text = "Picture";
         this.bPDetails.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
         this.bPDetails.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
         this.bPDetails.UseVisualStyleBackColor = false;
         // 
         // tabControl1
         // 
         this.tabControl1.AllowDrop = true;
         this.tabControl1.Controls.Add(this.tabPage1);
         this.tabControl1.Controls.Add(this.tabPage2);
         this.tabControl1.Controls.Add(this.tabPage3);
         this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tabControl1.ImageList = this.imageList2;
         this.tabControl1.ItemSize = new System.Drawing.Size(130, 27);
         this.tabControl1.Location = new System.Drawing.Point(0, 0);
         this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
         this.tabControl1.Name = "tabControl1";
         this.tabControl1.Padding = new System.Drawing.Point(0, 0);
         this.tabControl1.SelectedIndex = 0;
         this.tabControl1.Size = new System.Drawing.Size(503, 634);
         this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
         this.tabControl1.TabIndex = 29;
         // 
         // tabPage1
         // 
         this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.tabPage1.Controls.Add(this.button3);
         this.tabPage1.Controls.Add(this.button4);
         this.tabPage1.Controls.Add(this.pictureBox2);
         this.tabPage1.ImageKey = "digital_camera.png";
         this.tabPage1.Location = new System.Drawing.Point(4, 31);
         this.tabPage1.Name = "tabPage1";
         this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage1.Size = new System.Drawing.Size(495, 599);
         this.tabPage1.TabIndex = 0;
         this.tabPage1.Text = "Picture";
         this.tabPage1.ToolTipText = "Capture Picture";
         // 
         // button3
         // 
         this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.button3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.button3.ForeColor = System.Drawing.Color.White;
         this.button3.Location = new System.Drawing.Point(93, 423);
         this.button3.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.button3.Name = "button3";
         this.button3.Size = new System.Drawing.Size(132, 51);
         this.button3.TabIndex = 28;
         this.button3.Text = "Capture";
         this.button3.UseVisualStyleBackColor = false;
         // 
         // button4
         // 
         this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.button4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.button4.ForeColor = System.Drawing.Color.White;
         this.button4.Location = new System.Drawing.Point(258, 423);
         this.button4.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.button4.Name = "button4";
         this.button4.Size = new System.Drawing.Size(132, 51);
         this.button4.TabIndex = 27;
         this.button4.Text = "Browse";
         this.button4.UseVisualStyleBackColor = false;
         // 
         // pictureBox2
         // 
         this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.pictureBox2.Location = new System.Drawing.Point(93, 75);
         this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.pictureBox2.Name = "pictureBox2";
         this.pictureBox2.Size = new System.Drawing.Size(298, 335);
         this.pictureBox2.TabIndex = 26;
         this.pictureBox2.TabStop = false;
         // 
         // tabPage2
         // 
         this.tabPage2.Controls.Add(this.label42);
         this.tabPage2.Controls.Add(this.tReader);
         this.tabPage2.Controls.Add(this.bSetReader);
         this.tabPage2.Controls.Add(this.btnCancel);
         this.tabPage2.Controls.Add(this.txtMessage);
         this.tabPage2.Controls.Add(this.pbFingerprint);
         this.tabPage2.ImageKey = "icons8-automatic-40.png";
         this.tabPage2.Location = new System.Drawing.Point(4, 31);
         this.tabPage2.Name = "tabPage2";
         this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage2.Size = new System.Drawing.Size(491, 595);
         this.tabPage2.TabIndex = 1;
         this.tabPage2.Text = "Finger Prints";
         this.tabPage2.ToolTipText = "Capture Finger Prints";
         this.tabPage2.UseVisualStyleBackColor = true;
         // 
         // label42
         // 
         this.label42.AutoSize = true;
         this.label42.ForeColor = System.Drawing.Color.RoyalBlue;
         this.label42.Location = new System.Drawing.Point(-116, 490);
         this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label42.Name = "label42";
         this.label42.Size = new System.Drawing.Size(69, 23);
         this.label42.TabIndex = 14;
         this.label42.Text = "Reader:";
         // 
         // tReader
         // 
         this.tReader.BackColor = System.Drawing.Color.White;
         this.tReader.Location = new System.Drawing.Point(7, 480);
         this.tReader.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.tReader.Name = "tReader";
         this.tReader.ReadOnly = true;
         this.tReader.Size = new System.Drawing.Size(311, 29);
         this.tReader.TabIndex = 13;
         // 
         // bSetReader
         // 
         this.bSetReader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
         this.bSetReader.FlatAppearance.BorderSize = 0;
         this.bSetReader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.bSetReader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.bSetReader.ForeColor = System.Drawing.Color.White;
         this.bSetReader.Location = new System.Drawing.Point(340, 399);
         this.bSetReader.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.bSetReader.Name = "bSetReader";
         this.bSetReader.Size = new System.Drawing.Size(145, 59);
         this.bSetReader.TabIndex = 12;
         this.bSetReader.Text = "Set Reader";
         this.bSetReader.UseVisualStyleBackColor = false;
         // 
         // btnCancel
         // 
         this.btnCancel.Enabled = false;
         this.btnCancel.Location = new System.Drawing.Point(340, 459);
         this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.btnCancel.Name = "btnCancel";
         this.btnCancel.Size = new System.Drawing.Size(145, 59);
         this.btnCancel.TabIndex = 11;
         this.btnCancel.Text = "Cancel";
         // 
         // txtMessage
         // 
         this.txtMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
         this.txtMessage.Location = new System.Drawing.Point(281, 4);
         this.txtMessage.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.txtMessage.Multiline = true;
         this.txtMessage.Name = "txtMessage";
         this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtMessage.Size = new System.Drawing.Size(201, 244);
         this.txtMessage.TabIndex = 9;
         // 
         // pbFingerprint
         // 
         this.pbFingerprint.Location = new System.Drawing.Point(24, 113);
         this.pbFingerprint.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
         this.pbFingerprint.Name = "pbFingerprint";
         this.pbFingerprint.Size = new System.Drawing.Size(225, 254);
         this.pbFingerprint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pbFingerprint.TabIndex = 10;
         this.pbFingerprint.TabStop = false;
         // 
         // tabPage3
         // 
         this.tabPage3.ImageKey = "profile_edit.png";
         this.tabPage3.Location = new System.Drawing.Point(4, 31);
         this.tabPage3.Name = "tabPage3";
         this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage3.Size = new System.Drawing.Size(491, 595);
         this.tabPage3.TabIndex = 2;
         this.tabPage3.Text = "Signature";
         this.tabPage3.ToolTipText = "Capture Signature Specimens";
         this.tabPage3.UseVisualStyleBackColor = true;
         // 
         // EntryForm_Client
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.AutoSize = true;
         this.ClientSize = new System.Drawing.Size(1447, 900);
         this.ControlBox = false;
         this.Controls.Add(this.splitContainer1);
         this.Controls.Add(this.MenuPanel);
         this.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
         this.Name = "EntryForm_Client";
         this.Padding = new System.Windows.Forms.Padding(2);
         this.Text = "Client";
         this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
         this.Load += new System.EventHandler(this.EntryForm_Client_Load);
         this.Resize += new System.EventHandler(this.EntryForm_Client_Resize);
         this.MenuPanel.ResumeLayout(false);
         this.MenuPanel.PerformLayout();
         this.splitContainer1.Panel1.ResumeLayout(false);
         this.splitContainer1.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
         this.splitContainer1.ResumeLayout(false);
         this.splitContainer10.Panel1.ResumeLayout(false);
         this.splitContainer10.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).EndInit();
         this.splitContainer10.ResumeLayout(false);
         this.tabControl2.ResumeLayout(false);
         this.tabPage4.ResumeLayout(false);
         this.panel35.ResumeLayout(false);
         this.panel34.ResumeLayout(false);
         this.panel33.ResumeLayout(false);
         this.tabPage5.ResumeLayout(false);
         this.panel41.ResumeLayout(false);
         this.panel42.ResumeLayout(false);
         this.panel44.ResumeLayout(false);
         this.panel43.ResumeLayout(false);
         this.splitContainer4.Panel1.ResumeLayout(false);
         this.splitContainer4.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
         this.splitContainer4.ResumeLayout(false);
         this.panel1.ResumeLayout(false);
         this.panel1.PerformLayout();
         this.panel4.ResumeLayout(false);
         this.panel4.PerformLayout();
         this.panel5.ResumeLayout(false);
         this.panel5.PerformLayout();
         this.splitContainer3.Panel1.ResumeLayout(false);
         this.splitContainer3.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
         this.splitContainer3.ResumeLayout(false);
         this.panel40.ResumeLayout(false);
         this.panel40.PerformLayout();
         this.panel23.ResumeLayout(false);
         this.panel19.ResumeLayout(false);
         this.panel9.ResumeLayout(false);
         this.panel8.ResumeLayout(false);
         this.panel7.ResumeLayout(false);
         this.panel7.PerformLayout();
         this.splitContainer2.Panel1.ResumeLayout(false);
         this.splitContainer2.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
         this.splitContainer2.ResumeLayout(false);
         this.panel32.ResumeLayout(false);
         this.panel32.PerformLayout();
         this.panel3.ResumeLayout(false);
         this.panel3.PerformLayout();
         this.panel2.ResumeLayout(false);
         this.panel2.PerformLayout();
         this.panel6.ResumeLayout(false);
         this.panel6.PerformLayout();
         this.splitContainer6.Panel1.ResumeLayout(false);
         this.splitContainer6.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
         this.splitContainer6.ResumeLayout(false);
         this.panel31.ResumeLayout(false);
         this.splitContainer14.Panel1.ResumeLayout(false);
         this.splitContainer14.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).EndInit();
         this.splitContainer14.ResumeLayout(false);
         this.splitContainer8.Panel1.ResumeLayout(false);
         this.splitContainer8.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).EndInit();
         this.splitContainer8.ResumeLayout(false);
         this.panel21.ResumeLayout(false);
         this.panel21.PerformLayout();
         this.splitContainer9.Panel1.ResumeLayout(false);
         this.splitContainer9.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).EndInit();
         this.splitContainer9.ResumeLayout(false);
         this.panel24.ResumeLayout(false);
         this.panel27.ResumeLayout(false);
         this.panel27.PerformLayout();
         this.splitContainer7.Panel1.ResumeLayout(false);
         this.splitContainer7.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
         this.splitContainer7.ResumeLayout(false);
         this.panel45.ResumeLayout(false);
         this.panel22.ResumeLayout(false);
         this.panel25.ResumeLayout(false);
         this.panel26.ResumeLayout(false);
         this.splitContainer15.Panel1.ResumeLayout(false);
         this.splitContainer15.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).EndInit();
         this.splitContainer15.ResumeLayout(false);
         this.panel39.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.Weight)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.Height)).EndInit();
         this.panel38.ResumeLayout(false);
         this.panel37.ResumeLayout(false);
         this.panel36.ResumeLayout(false);
         this.splitContainer5.Panel1.ResumeLayout(false);
         this.splitContainer5.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
         this.splitContainer5.ResumeLayout(false);
         this.panel18.ResumeLayout(false);
         this.panel16.ResumeLayout(false);
         this.panel15.ResumeLayout(false);
         this.panel14.ResumeLayout(false);
         this.panel17.ResumeLayout(false);
         this.panel17.PerformLayout();
         this.panel11.ResumeLayout(false);
         this.panel12.ResumeLayout(false);
         this.panel13.ResumeLayout(false);
         this.splitContainer11.Panel1.ResumeLayout(false);
         this.splitContainer11.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).EndInit();
         this.splitContainer11.ResumeLayout(false);
         this.splitContainer13.Panel1.ResumeLayout(false);
         this.splitContainer13.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).EndInit();
         this.splitContainer13.ResumeLayout(false);
         this.panel30.ResumeLayout(false);
         this.panel30.PerformLayout();
         this.panel29.ResumeLayout(false);
         this.panel29.PerformLayout();
         this.panel28.ResumeLayout(false);
         this.panel28.PerformLayout();
         this.panel10.ResumeLayout(false);
         this.splitContainer12.Panel1.ResumeLayout(false);
         this.splitContainer12.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).EndInit();
         this.splitContainer12.ResumeLayout(false);
         this.tabControl1.ResumeLayout(false);
         this.tabPage1.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
         this.tabPage2.ResumeLayout(false);
         this.tabPage2.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbFingerprint)).EndInit();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Panel MenuPanel;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label bClose;
      private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.SplitContainer splitContainer4;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Panel panel4;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Panel panel5;
      private System.Windows.Forms.TextBox Mobile;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.SplitContainer splitContainer3;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Panel panel9;
      private System.Windows.Forms.ComboBox BrgyID;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Panel panel8;
      private System.Windows.Forms.ComboBox purokID;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Panel panel7;
      private System.Windows.Forms.TextBox houseNo;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.SplitContainer splitContainer2;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Panel panel3;
      private System.Windows.Forms.TextBox LastName;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Panel panel2;
      private System.Windows.Forms.TextBox middleName;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Panel panel6;
      private System.Windows.Forms.TextBox firstName;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.TextBox Telephone;
      private System.Windows.Forms.SplitContainer splitContainer6;
      private System.Windows.Forms.SplitContainer splitContainer5;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Panel panel18;
      private System.Windows.Forms.ComboBox Attainment;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.Panel panel16;
      private System.Windows.Forms.ComboBox Citizenship;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.Panel panel15;
      private System.Windows.Forms.ComboBox Ethnicity;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Panel panel14;
      private System.Windows.Forms.ComboBox Religion;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.Panel panel17;
      private System.Windows.Forms.TextBox POB;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.Panel panel11;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Panel panel12;
      private System.Windows.Forms.ComboBox CivilStatus;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Panel panel13;
      private System.Windows.Forms.ComboBox Gender;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.Panel panel23;
      private System.Windows.Forms.ComboBox ProvID;
      private System.Windows.Forms.Label label28;
      private System.Windows.Forms.Panel panel19;
      private System.Windows.Forms.ComboBox CityID;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.SplitContainer splitContainer10;
      private System.Windows.Forms.TabControl tabControl2;
      private System.Windows.Forms.Button BtnSave;
      private System.Windows.Forms.Button BtnUndo;
      private System.Windows.Forms.SplitContainer splitContainer11;
      private System.Windows.Forms.SplitContainer splitContainer13;
      private System.Windows.Forms.Label label35;
      private System.Windows.Forms.Panel panel30;
      private System.Windows.Forms.Label label39;
      private System.Windows.Forms.Panel panel29;
      private System.Windows.Forms.Label label38;
      private System.Windows.Forms.TextBox textBox15;
      private System.Windows.Forms.Panel panel28;
      private System.Windows.Forms.Label label37;
      private System.Windows.Forms.Panel panel10;
      private System.Windows.Forms.Label label41;
      private System.Windows.Forms.Label label40;
      private System.Windows.Forms.Label label36;
      private System.Windows.Forms.SplitContainer splitContainer12;
      private System.Windows.Forms.Button bFP;
      private System.Windows.Forms.Button bPDetails;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TextBox mAddress;
      private System.Windows.Forms.TextBox Mother;
      private System.Windows.Forms.TextBox fAddress;
      private System.Windows.Forms.TextBox Father;
      private System.Windows.Forms.TextBox sAddress;
      private System.Windows.Forms.TextBox Spouse;
      private System.Windows.Forms.Button bConInf;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.TabPage tabPage2;
      private System.Windows.Forms.TabPage tabPage3;
      private System.Windows.Forms.Button button3;
      private System.Windows.Forms.Button button4;
      private System.Windows.Forms.PictureBox pictureBox2;
      private System.Windows.Forms.Label label42;
      private System.Windows.Forms.TextBox tReader;
      private System.Windows.Forms.Button bSetReader;
      private System.Windows.Forms.Button btnCancel;
      private System.Windows.Forms.PictureBox pbFingerprint;
      private System.Windows.Forms.TextBox txtMessage;
      private System.Windows.Forms.ImageList imageList2;
      private System.Windows.Forms.Panel panel31;
      private System.Windows.Forms.TabPage tabPage4;
      private System.Windows.Forms.TabPage tabPage5;
      private System.Windows.Forms.SplitContainer splitContainer14;
      private System.Windows.Forms.Label label43;
      private System.Windows.Forms.CheckBox isPerson;
      private System.Windows.Forms.TextBox ClientID;
      private System.Windows.Forms.Label label48;
      private System.Windows.Forms.Panel panel35;
      private System.Windows.Forms.DateTimePicker dateTimePicker4;
      private System.Windows.Forms.Label label47;
      private System.Windows.Forms.Panel panel34;
      private System.Windows.Forms.DateTimePicker dateTimePicker2;
      private System.Windows.Forms.Label label46;
      private System.Windows.Forms.Panel panel33;
      private System.Windows.Forms.ComboBox Position;
      private System.Windows.Forms.Label label45;
      private System.Windows.Forms.TabPage tabPage6;
      private System.Windows.Forms.Panel panel32;
      private System.Windows.Forms.TextBox Alias;
      private System.Windows.Forms.Label label44;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.TextBox eMailAddress;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.SplitContainer splitContainer15;
      private System.Windows.Forms.Label label49;
      private System.Windows.Forms.Panel panel39;
      private System.Windows.Forms.Label label53;
      private System.Windows.Forms.Panel panel38;
      private System.Windows.Forms.ComboBox Hair;
      private System.Windows.Forms.Label label52;
      private System.Windows.Forms.Panel panel37;
      private System.Windows.Forms.ComboBox Complexion;
      private System.Windows.Forms.Label label51;
      private System.Windows.Forms.Panel panel36;
      private System.Windows.Forms.ComboBox Build;
      private System.Windows.Forms.Label label50;
      private System.Windows.Forms.SplitContainer splitContainer7;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.Panel panel22;
      private System.Windows.Forms.ComboBox Industry;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Panel panel25;
      private System.Windows.Forms.ComboBox WorkerType;
      private System.Windows.Forms.Label label30;
      private System.Windows.Forms.Panel panel26;
      private System.Windows.Forms.ComboBox Employment;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.SplitContainer splitContainer8;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Panel panel21;
      private System.Windows.Forms.DateTimePicker DOD;
      private System.Windows.Forms.Label label32;
      private System.Windows.Forms.SplitContainer splitContainer9;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.Panel panel24;
      private System.Windows.Forms.ComboBox Disability;
      private System.Windows.Forms.Label label33;
      private System.Windows.Forms.Panel panel27;
      private System.Windows.Forms.TextBox SCID;
      private System.Windows.Forms.Label label34;
      private System.Windows.Forms.Panel panel40;
      private System.Windows.Forms.TextBox ProvAddress;
      private System.Windows.Forms.Label label55;
      private System.Windows.Forms.DateTimePicker DOB;
      private System.Windows.Forms.Panel panel41;
      private System.Windows.Forms.DateTimePicker dateTimePicker1;
      private System.Windows.Forms.Label label56;
      private System.Windows.Forms.Panel panel42;
      private System.Windows.Forms.DateTimePicker dateTimePicker3;
      private System.Windows.Forms.Label label57;
      private System.Windows.Forms.Panel panel44;
      private System.Windows.Forms.ComboBox comboBox3;
      private System.Windows.Forms.Label label59;
      private System.Windows.Forms.Panel panel43;
      private System.Windows.Forms.ComboBox Designation;
      private System.Windows.Forms.Label label58;
      private System.Windows.Forms.Panel panel45;
      private System.Windows.Forms.ComboBox Occupation;
      private System.Windows.Forms.Label label60;
      private System.Windows.Forms.NumericUpDown Weight;
      private System.Windows.Forms.Label label54;
      private System.Windows.Forms.NumericUpDown Height;
      private System.Windows.Forms.TextBox POD;
      private System.Windows.Forms.Label label26;
   }
}