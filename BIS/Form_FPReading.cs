﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DPUruNet;

namespace BIS
{
    public partial class Form_FPReading : Form
    {
        FPReader reader;
        List<Fmd> preenrollmentFmds;
        int count;
        public Form_FPReading()
        {
            InitializeComponent();
            reader = FPReaderManager.GetInstance();
            tReaders.DisplayMember = "Reader";
            tReaders.ValueMember = "SerialNumber";
            preenrollmentFmds = new List<Fmd>();
            count = 0;
            try
            {
                foreach (Reader reader in ReaderCollection.GetReaders())
                {
                    tReaders.Items.Add(reader.Description.SerialNumber);
                }
                if(tReaders.Items.Count > 0)
                {
                    tReaders.SelectedIndex = 0;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show(ex.Message + "\r\n\r\nPlease check if DigitalPersona service has been started", "Cannot access readers");
                Close();
            }
            

            SendMessage(Action.SendResult, "Place a finger on the reader.\r\n\r\n");
        }

        private void Form_FPReading_Load(object sender, EventArgs e)
        {

        }

        private void TReaders_SelectedIndexChanged(object sender, EventArgs e)
        {
            preenrollmentFmds.Clear();
            if(reader.CurrentReader != null)
            {
                reader.CancelCaptureAndCloseReader(this.OnCaptured);
            }
            reader.CurrentReader = ReaderCollection.GetReaders()[tReaders.SelectedIndex];
            if(!reader.OpenReader())
            {
                MessageBox.Show("Can't open reader. Please make sure the Fingerprint reader is attached.");
                Close();
            }
            if (!reader.StartCaptureAsync(this.OnCaptured))
            {
                this.Close();
            }
        }

        private void OnCaptured(CaptureResult captureResult)
        {
            try
            {
                // Check capture quality and throw an error if bad.
                if (!reader.CheckCaptureResult(captureResult)) return;
                foreach (Fid.Fiv fiv in captureResult.Data.Views)
                {
                    SendMessage(Action.SendBitmap, reader.CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height));
                }
                count++;

                DataResult<Fmd> resultConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);
                SendMessage(Action.ClearResultBox, null);
                SendMessage(Action.SendResult, "PLACE THE SAME FINGER ON THE READER.\r\n\r\n\r\nA finger was captured.  \r\nCount:  " + (count));

                if (resultConversion.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    reader.Reset = true;
                    throw new Exception(resultConversion.ResultCode.ToString());
                }

                preenrollmentFmds.Add(resultConversion.Data);

                if (count >= 4)
                {
                    DataResult<Fmd> resultEnrollment = DPUruNet.Enrollment.CreateEnrollmentFmd(Constants.Formats.Fmd.ANSI, preenrollmentFmds);

                    if (resultEnrollment.ResultCode == Constants.ResultCode.DP_SUCCESS)
                    {
                        SendMessage(Action.SendMessage, "An enrollment FMD was successfully created. This form will now close.");
                        preenrollmentFmds.Clear();
                        count = 0;
                        SendMessage(Action.CloseForm, null);
                        return;
                    }
                    else if (resultEnrollment.ResultCode == Constants.ResultCode.DP_ENROLLMENT_INVALID_SET)
                    {
                        SendMessage(Action.SendMessage, "Enrollment was unsuccessful.  Please try again.");
                        SendMessage(Action.ClearResultBox, null);
                        SendMessage(Action.SendResult, "Place a finger on the reader.\r\n");
                        preenrollmentFmds.Clear();
                        count = 0;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                // Send error message, then close form
                SendMessage(Action.SendMessage, "Error:  " + ex.Message);
            }
        }

        #region SendMessage
        private enum Action
        {
            SendBitmap,
            SendMessage,
            SendResult,
            ClearResultBox,
            CloseForm
        }

        private delegate void SendMessageCallback(Action action, object payload);
        private void SendMessage(Action action, object payload)
        {
            try
            {
                if (this.pbFP.InvokeRequired || this.tResults.InvokeRequired)
                {
                    SendMessageCallback d = new SendMessageCallback(SendMessage);
                    this.Invoke(d, new object[] { action, payload });
                }
                else
                {
                    switch (action)
                    {
                        case Action.SendMessage:
                            MessageBox.Show((string)payload);
                            break;
                        case Action.SendResult:
                            tResults.Text = (string)payload + tResults.Text;
                            break;
                        case Action.SendBitmap:
                            pbFP.Image = (Bitmap)payload;
                            pbFP.Refresh();
                            break;
                        case Action.CloseForm:
                            reader.CurrentReader.Reset();
                            Close();
                            break;
                        case Action.ClearResultBox:
                            tResults.Clear();
                            break;
                    }
                }
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                MessageBox.Show(err.Message);
            }
        }
        #endregion

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form_FPReading_FormClosed(object sender, FormClosedEventArgs e)
        {
            reader.CancelCaptureAndCloseReader(this.OnCaptured);
        }
    }
}
