﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Form_Transactions : Form
    {
        private MySQLDatabase db;
        private Type entryForm;
        private DatabaseTableView view;
        SubForm subForm;
        public Form_Transactions(string formTitle, Type entryForm, UserControl subForm = null, DatabaseTableView view = null)
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
            formLabel.Text = formTitle;
            this.entryForm = entryForm;
            this.view = view;
            if(subForm != null)
            {
                subForm.Dock = DockStyle.Fill;
                panelTransactionControl.Controls.Add(subForm);
                if(subForm is SubForm)
                {
                    this.subForm = (SubForm)subForm;
                    this.subForm.Listview = listView1;
                }
            }
            if (this.view != null)
            {
                standardMenu.View = view;
                standardMenu.ListView = listView1;
                LoadListView(true);
            }
            LoadListView();
            standardMenu.ButtonNew.Click += new EventHandler(ButtonNew_Click);
            standardMenu.ButtonEdit.Click += new EventHandler(ButtonEdit_Click);
            SetMenuButtonsStatus(false);
        }

        private void LoadListView(bool firstLoad = false)
        {
            if (this.view != null)
            {
                IDataReader reader = db.Select("SELECT * FROM " + this.view.Name);
                ListViewManager.populateListView(listView1, reader, true);
                SetMenuButtonsStatus(listView1.SelectedItems.Count < 1 ? false : true);
                subForm.ClearData();
            }
        }
        public Control_StandardControls StandardMenu
        {
            get { return standardMenu; }
        }

        private void ButtonNew_Click(object sender, EventArgs e)
        {
            try
            {
                Form form = (Form)Activator.CreateInstance(entryForm);
                form.ShowDialog();
                form.Dispose();
                LoadListView();
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                MessageBox.Show(err.Message);
            }
        }
        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Form form = (Form)Activator.CreateInstance(entryForm, listView1.SelectedItems[0].Name);
                form.ShowDialog();
                form.Dispose();
                LoadListView();
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                MessageBox.Show(err.Message);
            }
        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetMenuButtonsStatus(listView1.SelectedItems.Count < 1 ? false : true);
        }

        private void SetMenuButtonsStatus(bool status)
        {
            standardMenu.ButtonEdit.Enabled = status;
            //standardMenu.ButtonPrint.Enabled = status;
            standardMenu.ButtonDelete.Enabled = status;
            standardMenu.ButtonPrint_PDF.Enabled = status;
            standardMenu.ButtonPrint_Printer.Enabled = status;
            standardMenu.ButtonPrint_Screen.Enabled = status;
        }
    }
}
