﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIS
{
    public partial class Control_ResidentsSubForm : UserControl, SubForm
    {
        private ListView listview;
        private MySQLDatabase db;
        private string clientid;

        public ListView Listview
        {
            set 
            {
                listview = value;
                listview.SelectedIndexChanged += listview_SelectedIndexChanged;
            }
        }

        public void ClearData()
        {
            ClearData(this);
        }

        private void ClearData(Control control)
        {
            foreach (Control c in control.Controls)
            {
                c.Tag = null;
                c.Name = null;
                c.Text = null;
                if (c is PictureBox)
                {
                    ((PictureBox)c).Image = null;
                }
                else
                {
                    ClearData(c);
                }
            }
        }

        public Control_ResidentsSubForm()
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
        }

        private void listview_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listview.SelectedItems.Count > 0)
            {
                List<SQLParameter> prms = new List<SQLParameter>();
                clientid = listview.SelectedItems[0].Name;
                prms.Add(new SQLParameter("clientid", clientid));
                clientname.Text = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT clientname FROM tbclients", prms));
                FileManager.LoadImageToPictureBox(Convert.ToString(db.ScalarSelect("SELECT picpath FROM tbclients", prms)), clientPhoto);
            }
        }
    }
}
